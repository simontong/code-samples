import { useCallback } from 'react';
import { validateYupSchema } from '../../code.0/isomorphic/utils/validateYupSchema';

/**
 * @constructor
 */
const useYupValidationResolver = (validationSchema) => {
  return useCallback(
    async (data) => {
      const { errors, values } = await validateYupSchema(validationSchema, data);
      return {
        errors: errors === null ? {} : errors,
        values,
      };
    },
    [validationSchema],
  );
};

export default useYupValidationResolver;