import React, { useEffect } from 'react';
import { AppContext, useAppState } from '../context/AppContext';
import { HelmetProvider } from 'react-helmet-async';
import ReactModalPromiseContainer from 'react-modal-promise';
import { Toaster } from 'react-hot-toast';
import { usePage } from '@inertiajs/inertia-react';
import HelmetHead from "./Layouts/HelmetHead";

export default function RootComponent({ children }) {
  const { selectors, state, actions } = useAppState();
  const { userVotes } = usePage().props;

  // set users votes
  useEffect(() => {
    actions.votes.set(userVotes.votes);
  }, []);

  // prettier-ignore
  return (
    <React.StrictMode>
      <AppContext.Provider value={{ selectors, state, actions }}>
        <HelmetProvider>
          <>
            <Toaster/>
            <ReactModalPromiseContainer/>
            {children}
          </>
        </HelmetProvider>
      </AppContext.Provider>
    </React.StrictMode>
  );
}
