import React, { useState } from 'react';
import isEqual from 'react-fast-compare';
import { useFormContext, Controller } from 'react-hook-form';
import ListingSearch from '../../Listing/ListingsSearch/ListingSearch';
import FormTip from '../../Form/FormTip';
import ErrorMessage from '../../Form/ErrorMessage';
import ListingLogo from '../../Listing/ListingLogo';
import { FaTimesCircle } from 'react-icons/fa';
import { formatCurrency } from '../../../util/formatting';

/**
 * selected item
 */
const SelectedItem = React.memo(({ item, price = 0, onRemoveClick }) => (
  <div className="flex items-center px-3 py-3 space-x-4 bg-gray-900 rounded">
    <div className="flex-1 flex items-center space-x-3">
      <div>
        {/* logo */}
        <ListingLogo variant="sm" logo={item.logo} alt={item.name} className="w-12 h-12" />
      </div>

      {/* name */}
      <div className="flex-1 space-y-1">
        <span className="flex flex-col space-y-1">
          <span>{item.name}</span>
          <span className="uppercase text-gray-500">{item.symbol}</span>
        </span>
      </div>
    </div>

    {/* price */}
    <div className="text-right">{formatCurrency(price)}</div>

    {/* remove */}
    <div className="text-right">
      <FaTimesCircle
        onClick={onRemoveClick}
        className="cursor-pointer text-red-500 hover:text-red-700 -mt-1"
      />
    </div>
  </div>
));

/**
 * input
 */
const InputListingSearch = React.memo(({ name, label, tip, price }) => {
  const { control } = useFormContext();

  return (
    <Controller
      name={name}
      control={control}
      render={({ field: { name, value, ref, onChange }, fieldState }) => {
        const showPreview = !fieldState.error && value;

        /**
         * on click listing item
         */
        const handleClickItem = (item) => () => {
          onChange(item);
        };

        /**
         * on click remove button
         */
        const onRemoveClick = () => {
          onChange(null);
        };

        return (
          <div className="min-h-[72px]">
            {showPreview && (
              <SelectedItem item={value} price={price} onRemoveClick={onRemoveClick} />
            )}

            {!showPreview && (
              <div className="space-y-1">
                {label && (
                  <label className="space-y-1">
                    <span className="form-label">{label}</span>
                  </label>
                )}

                <ListingSearch handleClickItem={handleClickItem} />

                {tip && <FormTip message={tip} />}
                <ErrorMessage name={name} />
              </div>
            )}
          </div>
        );
      }}
    />
  );
}, isEqual);

export default InputListingSearch;
