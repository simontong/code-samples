import { object } from 'yup';
import { advertiseRules } from '../../../../code.0/isomorphic/rules/advertiseRules';

/**
 * schema
 */
const validationSchema = () => {
  // prettier-ignore
  return object({
    email: advertiseRules.checkout.email,

    payMethod: advertiseRules.checkout.payMethod,
  });
};

export default validationSchema;