import React, { useState } from 'react';
import isEqual from 'react-fast-compare';
import { useForm, FormProvider } from 'react-hook-form';
import SubmitButton from '../../Form/SubmitButton';
import { FaShoppingCart } from 'react-icons/fa';
import FormGroup from '../../Form/FormGroup';
import InputField from '../../Form/InputField';
import InputSelect from '../../Form/InputSelect';
import useYupValidationResolver from '../../../hooks/useYupValidationResolver';
import validationSchema from './validationSchema';
import { useAppContext } from '../../../context/AppContext';
import securityVerification from '../../Util/securityVerification';
import { StatusCodes } from 'http-status-codes';
import setErrorsFromServer from '../../Form/setErrorsFromServer';
import headsUpNotification from '../../Util/headsUpNotification';
import { Inertia } from '@inertiajs/inertia';

const payMethodValues = [
  // {
  //   group: 'Mainnet',
  //   items: [
  //     { value: 'bnb', label: 'BNB' },
  //     { value: 'eth', label: 'ETH' },
  //   ],
  // },
  {
    group: 'BSC Chain',
    items: [
      { value: 'busd', label: 'BUSD (BEP-20)' },
      { value: 'usdc', label: 'USDC (BEP-20)' },
      { value: 'usdt', label: 'USDT (BEP-20)' },
    ],
  },
  // {
  //   group: 'ETH Chain',
  //   items: [
  //     { value: 'shib', label: 'SHIB (ERC-20)' },
  //     { value: 'usdt', label: 'USDT (ERC-20)' },
  //   ],
  // },
];

/**
 * initial values
 */
const defaultValues = {
  email: '',
  payMethod: 'busd',
};

/**
 * checkout form
 */
const CheckoutForm = React.memo(() => {
  const methods = useForm({
    resolver: useYupValidationResolver(validationSchema()),
    mode: 'onChange',
    reValidateMode: 'onChange',
    defaultValues,
  });
  const { handleSubmit } = methods;
  const { selectors } = useAppContext();
  const { lineItems } = selectors.cart;
  const [isLoading, setIsLoading] = useState(false);

  /**
   * submit
   */
  const onSubmit = async (input) => {
    setIsLoading(true);

    const json = {
      ...input,
      lineItems,
    };

    const res = await securityVerification('/advertise/checkout', { json })
      .catch((err) => console.error(err))
      .finally(() => {
        setIsLoading(false);
      });

    // user clicked close on security
    if (res === false) {
      return;
    }

    const data = await res.json();

    // validation error
    if (res.status === StatusCodes.UNPROCESSABLE_ENTITY) {
      setErrorsFromServer(methods, data);
      return;
    }

    // todo:
    console.log(data);
  };

  return (
    <FormProvider {...methods}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="flex flex-col space-y-6">
          {/* field: email */}
          <FormGroup name="email">
            <InputField name="email" label="Email" placeholder="eg. john@smith.com" />
          </FormGroup>

          {/* field: pay method */}
          <FormGroup name="payMethod">
            <InputSelect
              name="payMethod"
              label="Payment Method"
              // placeholder="Choose a payment method"
              options={payMethodValues}
            />
          </FormGroup>

          <div className="self-center">
            <SubmitButton icon={<FaShoppingCart />} isLoading={isLoading}>
              Checkout
            </SubmitButton>
          </div>
        </div>
      </form>
    </FormProvider>
  );
}, isEqual);

export default CheckoutForm;
