import React from 'react';
import isEqual from 'react-fast-compare';
import TableRow from '../../Table/TableRow';
import TableCell from '../../Table/TableCell';
import { formatCurrency } from '../../../util/formatting';
import BasketLineItemPromotedCoin from '../Products/PromotedCoin/BasketLineItemPromotedCoin';
import BasketLineItemBannerAd from '../Products/BannerAd/BasketLineItemBannerAd';
import { useAppContext } from '../../../context/AppContext';
import CheckoutForm from '../CheckoutForm/CheckoutForm';

// todo: remove on live
const dummyData =
  process.env.NODE_ENV !== 'development'
    ? []
    : [
        {
          id: 'e0851e77-db99-4952-ad94-2e8f1b9b73fb-1637017200000',
          price: 75,
          product: {
            id: 'promoted-coin',
            price: 75,
            name: 'Promoted Coin',
            desc: 'List your coins, front and center, on our homepage!',
          },
          data: {
            listing: {
              id: 'e0851e77-db99-4952-ad94-2e8f1b9b73fb',
              slug: 'bnbch',
              symbol: 'BNBCH',
              name: 'BNB Cash',
              logo: '60dfae19b2adb6f679159f132cbf88c7c934f34f.jpg',
            },
            variant: {
              date: new Date(),
            },
          },
        },
      ];

/**
 * basket items
 */
const Basket = React.memo(() => {
  const { selectors } = useAppContext();
  const { lineItems, subTotal, discount, total } = selectors.cart;

  // todo: remove on live
  if (!lineItems.length && dummyData.length) {
    lineItems.push(...dummyData);
  }

  // if cart is empty
  if (lineItems.length === 0) {
    return <div className="p-10 text-center font-bold">Your basket is empty</div>;
  }

  return (
    <div>
      <table className="min-w-full">
        <tbody>
          {lineItems.map((lineItem, num) => (
            <TableRow
              key={lineItem.id}
              className={num % 2 ? 'bg-gray-800' : 'bg-gray-700 bg-opacity-20'}
            >
              {lineItem.product.id === 'promoted-coin' && (
                <BasketLineItemPromotedCoin lineItem={lineItem} />
              )}

              {lineItem.product.id === 'banner-ad' && (
                <BasketLineItemBannerAd lineItem={lineItem} />
              )}
            </TableRow>
          ))}
        </tbody>

        <tfoot className="bg-gray-700 border-t-2 border-b-2 border-gray-500">
          <tr>
            <TableCell className="flex-1 uppercase" colSpan={2}>
              Sub Total
            </TableCell>
            <TableCell className="text-right">{formatCurrency(subTotal)}</TableCell>
            <TableCell className="px-0" />
          </tr>

          {discount > 0 && (
            <tr>
              <TableCell className="flex-1 uppercase" colSpan={2}>
                Discount
              </TableCell>
              <TableCell className="text-right">{formatCurrency(discount)}</TableCell>
              <TableCell className="px-0" />
            </tr>
          )}

          <tr className="border-t border-gray-500">
            <TableCell className="flex-1 uppercase font-bold" colSpan={2}>
              Total
            </TableCell>
            <TableCell className="text-right font-bold">{formatCurrency(total)}</TableCell>
            <TableCell className="px-0" />
          </tr>
        </tfoot>
      </table>

      <div className="px-4 pt-6 pb-4">
        <CheckoutForm />
      </div>
    </div>
  );
}, isEqual);

export default Basket;
