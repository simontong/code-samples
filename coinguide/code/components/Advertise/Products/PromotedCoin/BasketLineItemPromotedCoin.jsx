import React from 'react';
import isEqual from 'react-fast-compare';
import TableCell from '../../../Table/TableCell';
import ListingLogo from '../../../Listing/ListingLogo';
import { formatCurrency } from '../../../../util/formatting';
import { FaCalendarAlt, FaTimesCircle } from 'react-icons/fa';
import { DateTime } from 'luxon';
import { useAppContext } from '../../../../context/AppContext';

/**
 * basket listing item
 */
const BasketLineItemPromotedCoin = React.memo(({ lineItem }) => {
  const { actions } = useAppContext();
  const { id, data, product } = lineItem;
  const { listing, variant } = data;

  return (
    <>
      <TableCell className="w-[74px] pr-0 align-top">
        <ListingLogo variant="sm" logo={listing.logo} />
      </TableCell>

      <TableCell className="space-y-1">
        <div className="flex items-center space-x-2 font-bold uppercase">
          {product.icon}
          <span>{product.name}</span>
        </div>

        <div>
          <span className="space-x-1">
            <span>{listing.name}</span>
            <span className="inline-block uppercase px-1 bg-gray-700 rounded">
              {listing.symbol}
            </span>
          </span>
        </div>

        <div className="flex items-center space-x-1">
          <FaCalendarAlt />
          <span>{DateTime.fromJSDate(variant.date).toLocaleString(DateTime.DATE_MED)}</span>
        </div>
      </TableCell>

      <TableCell className="text-right">{formatCurrency(product.price)}</TableCell>

      <TableCell className="w-[34px] pl-0 pr-4 text-right">
        <FaTimesCircle
          onClick={() => actions.cart.remove(id)}
          className="cursor-pointer hover:text-red-500 -mt-1"
        />
      </TableCell>
    </>
  );
}, isEqual);

export default BasketLineItemPromotedCoin;
