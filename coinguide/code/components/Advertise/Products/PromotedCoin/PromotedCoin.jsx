import React, { useEffect } from 'react';
import isEqual from 'react-fast-compare';
import FormGroup from '../../../Form/FormGroup';
import InputListingSearch from '../../Form/InputListingSearch';
import { LISTING_PROMOTE_BOOKING_LIMITS } from '../../../../../code.0/isomorphic/consts/listingConsts';
import { useForm, FormProvider } from 'react-hook-form';
import useYupValidationResolver from '../../../../hooks/useYupValidationResolver';
import validationSchema from './validationSchema';
import SubmitButton from '../../../Form/SubmitButton';
import { FaPlusCircle } from 'react-icons/fa';
import { useAppContext } from '../../../../context/AppContext';
import { get, sortBy } from 'lodash';
import { DateTime } from 'luxon';
import InputDayPicker from '../../../Form/DatePicker/InputDayPicker';

/**
 * initial values
 */
const defaultValues = {
  listing: null,
  dates: [],
};

/**
 * promoted coin
 */
const PromotedCoin = React.memo(({ product, opts }) => {
  const { selectors, actions } = useAppContext();
  const methods = useForm({
    resolver: useYupValidationResolver(validationSchema()),
    mode: 'onChange',
    reValidateMode: 'onChange',
    defaultValues,
  });
  const { handleSubmit, formState, reset, watch } = methods;
  const pendingTotal = watch('dates').length * product.price;
  const { existingDates } = opts;

  /**
   * submit form
   */
  const onAddClick = async (input) => {
    const listing = input.listing;
    const dates = sortBy(input.dates, (d) => d.valueOf());

    // add all dates as variants
    for (const date of dates) {
      const id = [listing.id, date.valueOf()].join('-');

      actions.cart.add({
        id,
        price: product.price,
        product,
        data: {
          listing,
          variant: { date },
        },
      });
    }
  };

  /**
   * on form submit, reset
   */
  useEffect(() => {
    if (formState.isSubmitSuccessful) {
      reset();
    }
  }, [formState, reset]);

  return (
    <FormProvider {...methods}>
      <form onSubmit={handleSubmit(onAddClick)}>
        <div className="space-y-6 flex flex-col">
          {/* field: search coin */}
          <FormGroup name="listing">
            <InputListingSearch name="listing" label="Search for your coin" price={pendingTotal} />
          </FormGroup>

          {/* field: pick days */}
          <FormGroup name="dates">
            <InputDayPicker
              name="dates"
              label="Choose promotion dates"
              disabledDays={(date) => {
                const now = DateTime.now().toUTC();
                const dt = DateTime.fromJSDate(date).toUTC();

                const dtMinDays = dt.minus({ days: LISTING_PROMOTE_BOOKING_LIMITS.minDays });
                const dtMaxDays = dt.minus({ days: LISTING_PROMOTE_BOOKING_LIMITS.maxDays });
                if (now.diff(dtMinDays).as('days') > 0 || now.diff(dtMaxDays).as('days') < 0) {
                  return true;
                }

                const dateInBasket = selectors.cart.lineItems.filter((i) => {
                  const lineDate = get(i, 'data.variant.date');
                  return lineDate && lineDate.valueOf() === date.valueOf();
                });

                const dtSql = dt.toSQLDate();
                const existingDate = existingDates.find((i) => i.date === dtSql);
                const existingCount = (existingDate ? existingDate.count : 0) + dateInBasket.length;
                if (existingCount >= LISTING_PROMOTE_BOOKING_LIMITS.maxPromoteCoins) {
                  return true;
                }
              }}
            />
          </FormGroup>
        </div>

        <div className="flex justify-center pt-7">
          <div className="pt-2 self-center sm:self-start">
            <SubmitButton>
              <FaPlusCircle />
              <span>Add to Your Basket</span>
            </SubmitButton>
          </div>
        </div>
      </form>
    </FormProvider>
  );
}, isEqual);

export default PromotedCoin;