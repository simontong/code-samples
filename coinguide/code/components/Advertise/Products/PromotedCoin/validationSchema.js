import { object } from 'yup';
import { advertiseRules } from '../../../../../code.0/isomorphic/rules/advertiseRules';

/**
 * validation
 */
const validationSchema = () => {
  return object({
    listing: advertiseRules.promotedCoin.listing,

    dates: advertiseRules.promotedCoin.dates,
  });
};

export default validationSchema;