import React from 'react';
import isEqual from 'react-fast-compare';
import PromotedCoin from './PromotedCoin/PromotedCoin';
import BannerAd from './BannerAd/BannerAd';
import { Link, usePage } from '@inertiajs/inertia-react';
import { absoluteUrl } from '../../../util/url';
import css from '../../../util/css';
import { formatCurrency } from '../../../util/formatting';
import { find } from 'lodash';

/**
 * product item
 */
const ProductLink = React.memo(
  ({ product, selectedProductId }) => (
    <Link
      preserveState
      href={absoluteUrl({ search: `product=${product.id}` })}
      className={css(
        'flex-1 px-3 py-2 space-y-1 border-2 shadow text-gray-200 hover:text-gray-200 rounded cursor-pointer',
        selectedProductId === product.id
          ? 'bg-gray-600 border-transparent shadow-none'
          : 'border-gray-700 hover:bg-gray-700',
      )}
    >
      <div className="flex items-center space-x-2 font-bold uppercase">
        {product.icon}
        <span>{product.name}</span>
      </div>

      <div className="inline-block px-1 rounded bg-yellow-500 text-gray-900">
        {formatCurrency(product.price)}
      </div>

      <div>{product.desc}</div>
    </Link>
  ),
  isEqual,
);

/**
 * products
 */
const Products = React.memo(({ products, promotedCoinOpts, promotedBannerOpts }) => {
  const { url } = usePage();
  const selectedProductId = new URLSearchParams(url.split('?').pop()).get('product');
  const selectedProduct = find(products, ['id', selectedProductId]);

  return (
    <>
      <div className="flex space-x-3">
        {products.map((product, key) => (
          <ProductLink key={key} selectedProductId={selectedProductId} product={product} />
        ))}
      </div>

      {selectedProductId && (
        <div className="mt-6">
          {selectedProductId === 'promoted-coin' && (
            <PromotedCoin product={selectedProduct} opts={promotedCoinOpts} />
          )}

          {selectedProductId === 'banner-ad' && (
            <BannerAd product={selectedProduct} opts={promotedBannerOpts} />
          )}
        </div>
      )}
    </>
  );
}, isEqual);

export default Products;
