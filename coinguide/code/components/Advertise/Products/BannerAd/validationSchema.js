import { object } from 'yup';
import { advertiseRules } from '../../../../../code.0/isomorphic/rules/advertiseRules';
import { ruleParams } from '../../../../../code.0/isomorphic/rules/advertiseRules';

/**
 * validate logo url
 */
async function validateAsset(file, position) {
  const { maxFilesize, allowTypes, maxDimensionsPerPosition } = ruleParams.asset;
  const maxDimensions = maxDimensionsPerPosition[position];
  const dimensions = maxDimensions.join('x');
  const img = new Image();
  const objectUrl = URL.createObjectURL(file);

  // check file type
  if (!allowTypes.includes(file.type)) {
    const filetypes = allowTypes.map((i) => i.replace(/^.+?\/(.+)$/, '$1')).join(', ');
    return [null, `\${path} must be of type: ${filetypes}`];
  }

  // check file size
  if (file.size >= maxFilesize) {
    return [null, `\${path} file size must be less than ${Math.round(maxFilesize / 1024)}kb`];
  }

  // wait for image to load
  try {
    await new Promise((resolve, reject) => {
      img.onerror = () => reject(`\${path} failed to process`);
      img.onload = resolve;
      img.src = objectUrl;
    });
  } catch (err) {
    return [null, err];
  }

  // check if image too big
  if (img.width !== maxDimensions[0] || img.height !== maxDimensions[1]) {
    return [null, `\${path} dimensions must be exactly ${dimensions}`];
  }

  return [objectUrl, null];
}

/**
 * schema
 */
const validationSchema = () => {
  // prettier-ignore
  return object({
    position: advertiseRules.bannerAd.position,

    asset: advertiseRules.bannerAd.asset
      .test('image', '', async function (value) {
        const file = value instanceof FileList ? value.item(0) : value;

        if (!file) {
          return true;
        }

        const position = this.parent.position;
        const [, error] = await validateAsset(file, position);

        // prettier-ignore
        return error
          ? this.createError({ message: error })
          : true;
      }),

    dates: advertiseRules.bannerAd.dates,
  });
};

export default validationSchema;