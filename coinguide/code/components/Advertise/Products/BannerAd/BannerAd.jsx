import React, { useEffect, useState } from 'react';
import isEqual from 'react-fast-compare';
import { FormProvider, useForm } from 'react-hook-form';
import useYupValidationResolver from '../../../../hooks/useYupValidationResolver';
import validationSchema from './validationSchema';
import FormGroup from '../../../Form/FormGroup';
import SubmitButton from '../../../Form/SubmitButton';
import { FaPlusCircle, FaTimesCircle } from 'react-icons/fa';
import {
  BANNER_DIMENSIONS,
  BANNER_POSITIONS,
  BANNER_PROMOTE_BOOKING_LIMITS,
} from '../../../../../code.0/isomorphic/consts/bannerConsts';
import InputRadioGroup from '../../../Form/InputRadioGroup';
import { ruleParams } from '../../../../../code.0/isomorphic/rules/advertiseRules';
import { formatCurrency } from '../../../../util/formatting';
import { get, sortBy } from 'lodash';
import { useAppContext } from '../../../../context/AppContext';
import { DateTime } from 'luxon';
import InputDayPicker from '../../../Form/DatePicker/InputDayPicker';
import InputFileUpload from '../../../Form/InputFileUpload';
import css from '../../../../util/css';
import Tooltip from "../../../Tooltip/Tooltip";

export const positionsValues = [
  {
    label: `Top homepage banner`,
    value: BANNER_POSITIONS.homeTop,
  },
  {
    label: `Fixed bottom sitewide banner`,
    value: BANNER_POSITIONS.siteBottomFixed,
  },
];

/**
 * initial values
 */
const defaultValues = {
  asset: null,
  position: '',
  dates: [],
};

/**
 * banner ad
 */
const BannerAd = React.memo(({ product, opts }) => {
  const { existingDates } = opts;
  const { selectors, actions } = useAppContext();
  const [assetUrl, setAssetUrl] = useState('');
  const methods = useForm({
    resolver: useYupValidationResolver(validationSchema()),
    mode: 'onChange',
    reValidateMode: 'onChange',
    defaultValues,
  });
  const { handleSubmit, formState, reset, watch, setValue } = methods;
  const currentDates = watch('dates');
  const currentAsset = watch('asset');
  const currentPosition = watch('position');
  const positionDimensions = BANNER_DIMENSIONS[currentPosition] || [];

  const pendingTotal = currentDates.length * product.price;
  const hasPosition = !!currentPosition;
  const hasAsset = currentAsset && currentAsset.length;
  const hasPositionOrAssetError =
    !!get(formState.errors, 'asset') || !!get(formState.errors, 'position');
  const showBasketLinePreview = !hasPositionOrAssetError && !!assetUrl && hasPosition && hasAsset;

  /**
   * check if date is available
   * @param date
   * @returns {boolean}
   */
  const isDateAvailable = (date) => {
    // if no position set yet then allow all days (date selector is disabled at this point)
    if (!currentPosition) {
      return false;
    }

    const now = DateTime.now().toUTC();
    const dt = DateTime.fromJSDate(date).toUTC();

    const dtMinDays = dt.minus({ days: BANNER_PROMOTE_BOOKING_LIMITS.minDays });
    const dtMaxDays = dt.minus({ days: BANNER_PROMOTE_BOOKING_LIMITS.maxDays });
    if (now.diff(dtMinDays).as('days') > 0 || now.diff(dtMaxDays).as('days') < 0) {
      return true;
    }

    const dateInBasket = selectors.cart.lineItems.filter((i) => {
      const position = get(i, 'data.position');
      if (position !== currentPosition) {
        return;
      }

      const lineDate = get(i, 'data.variant.date');
      return lineDate && lineDate.valueOf() === date.valueOf();
    });

    const dtSql = dt.toSQLDate();
    const existingDate = existingDates.find(
      (i) => currentPosition === i.position && i.date === dtSql,
    );
    const existingCount = (existingDate ? existingDate.count : 0) + dateInBasket.length;

    let maxPerPosition = BANNER_PROMOTE_BOOKING_LIMITS.maxPerPosition[currentPosition];
    if (!maxPerPosition) {
      console.error('Max per position missing limit for %s', currentPosition);
      maxPerPosition = 1;
    }

    if (existingCount >= maxPerPosition) {
      return true;
    }
  };

  /**
   * submit form
   */
  const onAddClick = async (input) => {
    const asset = input.asset;
    const dates = sortBy(input.dates, (d) => d.valueOf());

    // add all dates as variants
    for (const date of dates) {
      // const id = [asset[0].name, input.position, date.valueOf()].join('-');
      const id = Date.now();

      actions.cart.add({
        id,
        price: product.price,
        product,
        data: {
          asset,
          position: input.position,
          variant: { date },
        },
      });
    }
  };

  /**
   * on remove asset click
   */
  const onRemoveClick = () => {
    setValue('asset', null);
  };

  /**
   * on form submit, reset
   */
  useEffect(() => {
    if (formState.isSubmitSuccessful) {
      reset();
    }
  }, [formState, reset]);

  /**
   * when asset changes
   */
  useEffect(() => {
    if (!currentAsset || !currentAsset.length || hasPositionOrAssetError) {
      setAssetUrl('');
      return;
    }

    const assetUrl = URL.createObjectURL(currentAsset[0]);
    setAssetUrl(assetUrl);
  }, [hasPositionOrAssetError, currentAsset]);

  return (
    <FormProvider {...methods}>
      <form onSubmit={handleSubmit(onAddClick)}>
        <div className="space-y-6 flex flex-col">
          {/* basket line */}
          {showBasketLinePreview && (
            <div className="flex items-center px-3 py-3 space-x-4 bg-gray-900 rounded">
              <div className="flex-1 flex items-center space-x-3">
                <div className="self-start">
                  <Tooltip content={<img src={assetUrl} alt="" />}>
                    <div className="w-[54px] h-[54px] cursor-pointer shadow relative overflow-hidden rounded-sm font-bold">
                      <img src={assetUrl} alt="" className="w-full h-full object-cover" />
                      <div className="absolute top-0 left-0 right-0 py-1 text-xs bg-gray-900 bg-opacity-80 tracking-wider text-gray-200 text-center">
                        {positionDimensions.join('x')}
                      </div>
                    </div>
                  </Tooltip>
                </div>

                {/* name */}
                <div className="flex-1 space-y-1">
                  <div>{positionsValues.find((i) => i.value === watch('position')).label}</div>
                </div>

                {/* price */}
                <div className="text-right">{formatCurrency(pendingTotal)}</div>

                {/* remove */}
                <div className="text-right">
                  <FaTimesCircle
                    onClick={onRemoveClick}
                    className="cursor-pointer text-red-500 hover:text-red-700 -mt-1"
                  />
                </div>
              </div>
            </div>
          )}

          {/* field: position */}
          <FormGroup name="position" className={showBasketLinePreview ? 'hidden' : ''}>
            <InputRadioGroup name="position" label="Banner Position" options={positionsValues} />
          </FormGroup>

          {/* field: banner image */}
          <div className={showBasketLinePreview ? 'hidden' : ''}>
            <Tooltip content="Please select Banner Position first" disabled={hasPosition}>
              <div>
                <FormGroup
                  name="asset"
                  className={css(!hasPosition && 'pointer-events-none opacity-30')}
                >
                  <InputFileUpload
                    name="asset"
                    label="Asset Upload"
                    accept={ruleParams.asset.allowTypes.join(',')}
                    placeholder={
                      positionDimensions.length > 0 ? (
                        <span>
                          Required dimensions:{' '}
                          <span className="text-orange-500 font-bold">
                            {positionDimensions.join('x')}
                          </span>
                        </span>
                      ) : (
                        ''
                      )
                    }
                  />
                </FormGroup>
              </div>
            </Tooltip>
          </div>

          {/* field: pick days */}
          <Tooltip content="Please select Banner Position first" disabled={hasPosition}>
            <div>
              <FormGroup
                name="date"
                className={css(!hasPosition && 'pointer-events-none opacity-30')}
              >
                <InputDayPicker
                  name="dates"
                  label="Choose promotion dates"
                  disabledDays={isDateAvailable}
                />
              </FormGroup>
            </div>
          </Tooltip>
        </div>

        <div className="flex justify-center pt-7">
          <div className="pt-2 self-center sm:self-start">
            <SubmitButton>
              <FaPlusCircle />
              <span>Add to Your Basket</span>
            </SubmitButton>
          </div>
        </div>
      </form>
    </FormProvider>
  );
}, isEqual);

export default BannerAd;