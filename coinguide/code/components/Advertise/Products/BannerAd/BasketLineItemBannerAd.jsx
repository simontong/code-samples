import React from 'react';
import isEqual from 'react-fast-compare';
import TableCell from '../../../Table/TableCell';
import { formatCurrency } from '../../../../util/formatting';
import { FaCalendarAlt, FaTimesCircle } from 'react-icons/fa';
import { useAppContext } from '../../../../context/AppContext';
import { DateTime } from 'luxon';
import { BANNER_DIMENSIONS } from '../../../../../code.0/isomorphic/consts/bannerConsts';
import { positionsValues } from './BannerAd';
import Tooltip from "../../../Tooltip/Tooltip";

/**
 * basket listing item
 */
const BasketLineItemPromotedCoin = React.memo(({ lineItem }) => {
  const { actions } = useAppContext();
  const { id, data, product } = lineItem;
  const { position, variant } = data;

  const assetUrl = URL.createObjectURL(data.asset);
  const positionLabel = positionsValues.find((i) => i.value === position).label;
  const positionDimensions = BANNER_DIMENSIONS[position];

  return (
    <>
      <TableCell className="w-[74px] pr-0 align-top">
        <Tooltip content={<img src={assetUrl} alt="" />}>
          <div className="w-[54px] h-[54px] cursor-pointer shadow relative overflow-hidden rounded-sm font-bold">
            <img src={assetUrl} alt="" className="w-full h-full object-cover" />
            <div className="absolute top-0 left-0 right-0 py-1 text-xs bg-gray-900 bg-opacity-80 tracking-wider text-gray-200 text-center">
              {positionDimensions.join('x')}
            </div>
          </div>
        </Tooltip>
      </TableCell>

      <TableCell className="space-y-1">
        <div className="flex items-center space-x-2 font-bold uppercase">
          {product.icon}
          <span>{product.name}</span>
        </div>

        <div>
          <span className="space-x-1">
            <span>{positionLabel}</span>
            {/*
            <span className="inline-block uppercase px-1 bg-gray-700 rounded">
              {listing.symbol}
            </span>
*/}
          </span>
        </div>

        <div className="flex items-center space-x-1">
          <FaCalendarAlt />
          <span>{DateTime.fromJSDate(variant.date).toLocaleString(DateTime.DATE_MED)}</span>
        </div>
      </TableCell>

      <TableCell className="text-right">{formatCurrency(product.price)}</TableCell>

      <TableCell className="w-[34px] pl-0 pr-4 text-right">
        <FaTimesCircle
          onClick={() => actions.cart.remove(id)}
          className="cursor-pointer hover:text-red-500 -mt-1"
        />
      </TableCell>
    </>
  );
}, isEqual);

export default BasketLineItemPromotedCoin;