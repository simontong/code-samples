import React from 'react';
import {
  FaChartLine,
  FaChevronDown,
  FaDiscord,
  FaFacebook,
  FaInstagram,
  FaLinkedin,
  FaMedium,
  FaReddit,
  FaTelegram,
  FaTiktok,
  FaTwitter,
  FaWallet,
} from 'react-icons/fa';
import { BsChatFill, BsFillPeopleFill, BsGlobe2 } from 'react-icons/bs';
import isEqual from 'react-fast-compare';
import css from '../util/css';
import { entries, isEmpty, pick } from 'lodash';
import Tooltip from './Tooltip/Tooltip';

const urlTypeMetas = {
  bogged: { name: 'Bogged', icon: <FaChartLine /> },
  dextools: { name: 'Dextools', icon: <FaChartLine /> },
  discord: { name: 'Discord', icon: <FaDiscord /> },
  facebook: { name: 'Facebook', icon: <FaFacebook /> },
  homepage: { name: (v) => new URL(v).hostname, icon: <BsGlobe2 /> },
  presale: { name: 'Presale Link', icon: <FaWallet /> },
  linkedin: { name: 'LinkedIn', icon: <FaLinkedin /> },
  instagram: { name: 'Instagram', icon: <FaInstagram /> },
  medium: { name: 'Medium', icon: <FaMedium /> },
  poocoin: { name: 'Poocoin', icon: <FaChartLine /> },
  reddit: { name: 'Reddit', icon: <FaReddit /> },
  telegram: { name: 'Telegram', icon: <FaTelegram /> },
  tiktok: { name: 'Tiktok', icon: <FaTiktok /> },
  twitter: { name: 'Twitter', icon: <FaTwitter /> },
};

const urlGroups = {
  chat: {
    name: 'Chat',
    icon: <BsChatFill />,
    types: ['discord', 'instagram', 'linkedin', 'tiktok', 'telegram'],
  },
  chart: {
    name: 'Chart',
    icon: <FaChartLine />,
    types: ['bogged', 'dextools', 'poocoin'],
  },
  community: {
    name: 'Community',
    icon: <BsFillPeopleFill />,
    types: ['facebook', 'medium', 'reddit', 'twitter'],
  },
  homepage: {
    name: 'Website',
    icon: <BsGlobe2 />,
    types: ['homepage'],
  },
  presale: {
    name: 'Presale Link',
    icon: <FaWallet />,
    types: ['presale'],
  },
};

/**
 * Listing url or dropdown
 */
const ListingItemOrDropdownItems = ({ urlGroup, urls }) => {
  const arrUrls = entries(urls);

  // ascertain if this is a single url button not a dropdown of links
  const isSingleUrl = arrUrls.length === 1 && arrUrls[0][1].length === 1;

  /**
   * prep item(s) links
   */
  const items = arrUrls.map(([type, value], key) => {
    const meta = urlTypeMetas[type];

    if (!meta) {
      console.log(`URL type meta '${type}' does not exist`);
      return null;
    }

    const { name, icon } = meta;
    const label = typeof name === 'function' ? name(value) : name;

    return (
      <a
        key={key}
        className={css(
          'flex items-center space-x-1 text-white',
          isSingleUrl && 'btn btn-round btn-gray',
        )}
        href={value}
        target="_blank"
      >
        {icon}
        <span>{label}</span>
      </a>
    );
  });

  /**
   * if single button
   */
  if (isSingleUrl) {
    return items;
  }

  /**
   * if a dropdown of URLs
   */
  const tooltip = (
    <div className="flex flex-col justify-center min-w-[100px] space-y-2 p-1">{items}</div>
  );

  return (
    <Tooltip content={tooltip} allowHTML interactive placement="bottom" className="shadow-lg">
      <div className="inline-flex items-center space-x-1 btn btn-round btn-gray">
        {urlGroup.icon}
        <span>{urlGroup.name}</span>
        <FaChevronDown size={12} />
      </div>
    </Tooltip>
  );
};

/**
 * Listing urls
 */
const ListingUrls = React.memo(({ urls, type, ...props }) => {
  const urlGroup = urlGroups[type];

  if (!urlGroup) {
    console.log(`URL group type '${type}' does not exist`);
    return null;
  }

  const processedUrls = React.useMemo(() => pick(urls, urlGroup.types), [urls]);

  if (isEmpty(processedUrls)) {
    return null;
  }

  return (
    <div {...props}>
      <ListingItemOrDropdownItems urlGroup={urlGroup} urls={processedUrls} />
    </div>
  );
}, isEqual);

export default ListingUrls;
