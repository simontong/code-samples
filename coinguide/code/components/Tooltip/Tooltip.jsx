import React from 'react';
import isEqual from 'react-fast-compare';
import Tippy from '@tippyjs/react';
import css from '../../util/css';
import 'tippy.js/dist/tippy.css';
import 'tippy.js/animations/shift-away-subtle.css';

/**
 * tooltip
 */
const Tooltip = React.memo(({ children, className, ...props }) => {
  return (
    <Tippy className={css('shadow-lg', className)} {...props}>
      {children}
    </Tippy>
  );
}, isEqual);

export default Tooltip;
