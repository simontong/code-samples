import React, {useEffect} from 'react';
import { useFormContext, Controller } from 'react-hook-form';
import ErrorMessage from './ErrorMessage';
import FormTip from './FormTip';
import isEqual from 'react-fast-compare';

/**
 * input field
 */
const InputFileUpload = React.memo(
  ({ name, icon, label = '', tip = '', className = '', ...props }) => {
    return (
      <div className="space-y-1">
        <label className="space-y-1">
          {label && <span className="form-label">{label}</span>}

          <Controller
            name={name}
            render={({ field: { name, onChange, value, ref } }) => {
              const placeholderOrFilename =
                value && value.length ? value[0].filename : props.placeholder;

              return (
                <div className="flex items-center relative space-x-3">
                  {icon && (
                    <span className="absolute mt-[1px] left-3 text-[18px] text-gray-500">
                      {icon}
                    </span>
                  )}
                  <div className="btn btn-gray btn-md">Select File</div>
                  <div>{placeholderOrFilename}</div>
                  <input
                    type="file"
                    name={name}
                    // className={css('form-input w-full', icon && 'pl-10', className)}
                    className="absolute hidden"
                    onChange={(e) => onChange(e.target.files)}
                    {...props}
                  />
                </div>
              );
            }}
          />
        </label>

        {tip && <FormTip message={tip} />}
        <ErrorMessage name={name} />
      </div>
    );
  },
  isEqual,
);

export default InputFileUpload;
