import React, { useState } from 'react';
import ErrorMessage from './ErrorMessage';
import { useFormContext, Controller } from 'react-hook-form';
import FormTip from './FormTip';
import { Editor } from '@tinymce/tinymce-react';
import '../../../code.0/resources/assets/css/tinymce.css';
import isEqual from 'react-fast-compare';
import css from '../../util/css';
import config from '../../util/config';

/**
 * config init
 */
const init = {
  // width: wrapRef.current?.offsetWidth,
  width: '100%',
  min_height: 150,
  menubar: false,
  plugins: [
    'emoticons lists autolink link',
    // 'advlist autolink lists link image charmap print preview anchor',
    // 'searchreplace visualblocks code fullscreen',
    // 'insertdatetime media table paste code help wordcount',
  ],
  toolbar: [
    'emoticons link',
    'bold italic',
    'alignleft aligncenter alignright alignjustify',
    'bullist numlist outdent indent',
  ].join(' | '),
  // 'undo redo | formatselect | ' +
  // 'bold italic backcolor | alignleft aligncenter ' +
  // 'alignright alignjustify | bullist numlist outdent indent | ' +
  // 'removeformat | help',
  // body_class: 'tinymce',
  // content_css: false,
  // skin: false,
  content_style: `
      body {
        background-color: transparent;
        color: #fff;
        padding: 0.5rem 0.75rem;
        margin: 0;
      }

      .mce-content-body {
        outline: none;
      }

/*
      table.mceLayout, textarea.tinyMCE {
          width: 100% !important;
      }
      */

      /*
      .mce-content-body > p:first-child {
        margin-top: 0;
      }
      */
    `,
  forced_root_block: '',
  skin: 'oxide-dark',
  content_css: 'dark',
  // valid_elements: 'p[style],strong,em,span[style],a[href],ul,ol,li',
  valid_styles: {
    '*': '',
  },
  branding: false,
  // inline: true,
};

/**
 * @constructor
 */
const InputHtmlTextarea = React.memo(({ name, label, tip }) => {
  const { tinyMceApiKey } = config();
  const { control } = useFormContext();
  const [focusWrap, setFocusWrap] = useState(false);

  return (
    <div className="space-y-1">
      <label className="space-y-1">
        {label && <span className="form-label">{label}</span>}

        <Controller
          name={name}
          control={control}
          render={({ field: { name, onChange, value, onBlur } }) => {
            const handleFocus = () => {
              setFocusWrap(true);
            };

            const handleBlur = async (e) => {
              setFocusWrap(false);
              await onBlur(e);
            };

            return (
              <div className={css('html-textarea', focusWrap && 'is-focused')}>
                <Editor
                  textareaName={name}
                  value={value}
                  apiKey={tinyMceApiKey}
                  init={init}
                  onEditorChange={onChange}
                  onFocus={handleFocus}
                  onBlur={handleBlur}
                />
              </div>
            );
          }}
        />
      </label>

      {tip && <FormTip message={tip} />}
      <ErrorMessage name={name} />
    </div>
  );
}, isEqual);

export default InputHtmlTextarea;