import React from 'react';
import { get, useFormContext } from 'react-hook-form';
import isEqual from 'react-fast-compare';
import css from '../../util/css';

/**
 * @constructor
 */
const FormGroup = React.memo(({ name, className, children }) => {
  const { formState } = useFormContext();
  const hasErrors = get(formState.errors, name);

  return <div className={css('form-group', hasErrors && 'has-errors', className)}>{children}</div>;
}, isEqual);

export default FormGroup;
