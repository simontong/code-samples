import React from 'react';
import css from '../../util/css';
import { useFormContext } from 'react-hook-form';
import ErrorMessage from './ErrorMessage';
import FormTip from './FormTip';
import isEqual from 'react-fast-compare';

/**
 * input field
 */
const InputField = React.memo(({
  name,
  icon,
  label = '',
  tip = '',
  className = '',
  type = 'text',
  ...props
}) => {
  const { register } = useFormContext();
  const field = register(name);

  return (
    <div className="space-y-1">
      <label className="space-y-1">
        {label && <span className="form-label">{label}</span>}

        <div className="flex items-center relative">
          {icon && <span className="absolute mt-[1px] left-3 text-[18px] text-gray-500">{icon}</span>}
          <input
            type={type}
            className={css('form-input w-full', icon && 'pl-10', className)}
            {...field}
            {...props}
          />
        </div>
      </label>

      {tip && <FormTip message={tip} />}
      <ErrorMessage name={field.name} />
    </div>
  );
}, isEqual);

export default InputField;
