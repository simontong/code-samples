import React from 'react';
import ErrorMessage from './ErrorMessage';
import { useFormContext } from 'react-hook-form';
import FormTip from './FormTip';
import css from '../../util/css';
import isEqual from 'react-fast-compare';

/**
 * @constructor
 */
const InputTextarea = React.memo(({ name, label, tip, className,  ...props }) => {
  const { register } = useFormContext();
  const field = register(name);

  return (
    <div className="space-y-1">
      <label className="space-y-1">
        {label && <span className="form-label">{label}</span>}

        <textarea className={css('form-input w-full', className)} {...field} {...props} />
      </label>

      {tip && <FormTip message={tip} />}
      <ErrorMessage name={field.name} />
    </div>
  );
}, isEqual);

export default InputTextarea;
