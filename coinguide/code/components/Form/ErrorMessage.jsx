import React from 'react';
import { get, useFormContext } from 'react-hook-form';
import isEqual from 'react-fast-compare';

/**
 * @constructor
 */
const ErrorMessage = React.memo(({ name }) => {
  const { formState } = useFormContext();
  const error = get(formState.errors, name);

  if (!error) {
    return null;
  }

  return <span className="form-error">{error.message}</span>;
}, isEqual);

export default ErrorMessage;
