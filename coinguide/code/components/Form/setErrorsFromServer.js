const setErrorsFromServer = ({ setError, setFocus }, data) => {
  if (!data.errors) {
    console.error('Something went wrong. Response:', data);
    return;
  }

  // set errors
  let isFirst = true;
  for (const [path, { type, message }] of Object.entries(data.errors)) {
    setError(path, { type, message });

    // focus on first error's field
    if (isFirst) {
      setFocus(path);
      isFirst = false;
    }
  }
};

export default setErrorsFromServer;
