import React from 'react';
import { useFormContext } from 'react-hook-form';
import FormTip from './FormTip';
import ErrorMessage from './ErrorMessage';
import css from '../../util/css';

/**
 * @constructor
 */
const InputCheckbox = ({ name, label, tip, className, ...props }) => {
  const { register } = useFormContext();
  const field = register(name);

  // prettier-ignore
  return (
    <div className="inline-block space-y-1">
      <label className="flex items-start cursor-pointer space-x-2">

        <input type="checkbox"
               className={css('form-checkbox mt-[3px]', className)}
               {...field}
               {...props}
        />

        <span>{label}</span>
      </label>

      {tip && <FormTip message={tip}/>}
      <ErrorMessage name={field.name}/>
    </div>
  );
};

export default InputCheckbox;
