import React from 'react';
import isEqual from 'react-fast-compare';

/**
 * @constructor
 */
const FormTip = React.memo(({ message }) => {
  return <span className="block mt-1 text-sm text-gray-500">{message}</span>;
}, isEqual);

export default FormTip;
