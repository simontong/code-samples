import React from 'react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import { useFormContext, Controller} from 'react-hook-form';
import { FaCalendarAlt } from 'react-icons/fa';
import isEqual from 'react-fast-compare';
import FormTip from '../FormTip';
import ErrorMessage from '../ErrorMessage';
import { dayPickerProps, formatDate } from './InputDayPicker';

/**
 * input date picker
 */
const InputDatePicker = React.memo(({ name, label, tip, options, className, ...props }) => {
  const { control } = useFormContext();

  return (
    <Controller
      name={name}
      control={control}
      render={({ field }) => {
        return (
          <div className="space-y-1">
            <label className="block space-y-1" htmlFor={field.name}>
              {label && <span className="form-label">{label}</span>}
            </label>

            <div className="relative flex items-center">
              <FaCalendarAlt className="absolute z-10 left-3" />
              <DayPickerInput
                placeholder="YYYY-MM-DD"
                formatDate={formatDate}
                onDayChange={(day, DayModifiers, dayPickerInput) => {
                  return field.onChange(dayPickerInput.getInput().value);
                }}
                value={field.value}
                // showOverlay
                inputProps={{
                  id: field.name,
                  type: 'text',
                  className: 'form-input w-full pl-9 cursor-pointer',
                  autoComplete: 'off',
                  name,
                  onBlur: field.onBlur,
                  ...props,
                  maxLength: 10,
                }}
                classNames={{
                  container: 'dpi-container',
                  overlay: 'dpi-overlay absolute z-30 mt-1 rounded shadow-lg bg-gray-900',
                  overlayWrapper: 'dpi-overlayWrapper relative select-none',
                }}
                dayPickerProps={dayPickerProps}
              />
            </div>

            {tip && <FormTip message={tip} />}
            <ErrorMessage name={name} />
          </div>
        );
      }}
    />
  );
}, isEqual);

export default InputDatePicker;
