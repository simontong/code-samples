import css from '../../../util/css';
import React from 'react';
import { FaChevronLeft, FaChevronRight } from 'react-icons/fa';
import isEqual from 'react-fast-compare';
import { Controller, useFormContext } from 'react-hook-form';
import FormTip from '../FormTip';
import ErrorMessage from '../ErrorMessage';
import DayPicker, { DateUtils } from 'react-day-picker';
import { DateTime } from 'luxon';
import { get } from 'lodash';
import Tooltip from "../../Tooltip/Tooltip";

/**
 * parse modifiers
 */
const parseModifiers = (modifiers) => {
  return {
    isSelected: !!get(modifiers, 'dpi-selected'),
    isToday: !!get(modifiers, 'dpi-today'),
    isOutside: !!get(modifiers, 'dpi-outside'),
    isDisabled: !!get(modifiers, 'dpi-disabled'),
  };
};

/**
 * day renderer
 */
const renderDay = (date, modifiers) => {
  const day = date.getDate();
  const { isSelected, isToday, isOutside, isDisabled } = parseModifiers(modifiers);

  let modeCss = 'cursor-pointer text-gray-200 hover:text-white hover:bg-gray-700';

  if (isOutside) {
    modeCss = 'cursor-pointer text-gray-600 hover:text-white hover:bg-gray-700';
  }

  if (isToday) {
    modeCss = 'cursor-pointer text-red-600';
  }

  if (isSelected) {
    modeCss = 'cursor-pointer bg-white text-black hover:bg-gray-200';
  }

  if (isDisabled) {
    modeCss = 'bg-red-900 bg-opacity-20';

    if (isOutside) {
      modeCss += ' !bg-opacity-10 text-gray-600';
    }

    if (isToday) {
      modeCss += ' text-red-600';
    }
  }

  return (
    <Tooltip content={`Date unavailable`} disabled={!isDisabled}>
      <div className={css('px-2 py-1 rounded', modeCss)}>{day}</div>
    </Tooltip>
  );
};

/**
 * nav bar
 */
const NavbarElement = React.memo(
  ({
    className,
    classNames,
    dir,
    labels,
    locale,
    localeUtils,
    month,
    nextMonth,
    onNextClick,
    onPreviousClick,
    previousMonth,
    showNextButton,
    showPreviousButton,
  }) => {
    // className: "dpi-navBar"
    // classNames: {container: 'dpi-dp-container', wrapper: 'dpi-wrapper flex flex-col',
    // navBar: 'dpi-navBar', navButtonPrev: 'dpi-navButtonPrev', navButtonNext: 'dpi-navButtonNext', …}
    // dir: undefined
    // labels: {previousMonth: 'Previous Month', nextMonth: 'Next Month'}
    // locale: "en"
    // localeUtils: {formatDay: ƒ, formatMonthTitle: ƒ, formatWeekdayShort: ƒ, formatWeekdayLong: ƒ, getFirstDayOfWeek: ƒ, …}
    // month: Fri Oct 01 2021 12:00:00 GMT+1300 (New Zealand Daylight Time) {}
    // nextMonth: Mon Nov 01 2021 12:00:00 GMT+1300 (New Zealand Daylight Time) {}
    // onNextClick: ƒ (callback)
    // onPreviousClick: ƒ (callback)
    // previousMonth: Wed Sep 01 2021 12:00:00 GMT+1200 (New Zealand Standard Time) {}
    // showNextButton: true
    // showPreviousButton: true

    const formatMonth = (date) => localeUtils.formatMonthTitle(date);

    return (
      <div className={className}>
        <div className="flex-1 px-2 text-lg">{formatMonth(month)}</div>
        {showPreviousButton && (
          <div
            onClick={() => onPreviousClick()}
            title={formatMonth(previousMonth)}
            className={classNames.navButtonPrev}
          >
            <FaChevronLeft />
          </div>
        )}

        {showNextButton && (
          <div
            onClick={() => onNextClick()}
            title={formatMonth(nextMonth)}
            className={classNames.navButtonPrev}
          >
            <FaChevronRight />
          </div>
        )}
      </div>
    );
  },
  isEqual,
);

export const dayPickerProps = {
  todayButton: 'Go to Today',
  navbarElement: NavbarElement,
  renderDay,
  enableOutsideDaysClick: false,
  showOutsideDays: true,
  classNames: {
    container: 'dpi-dp-container',
    wrapper: 'dpi-wrapper flex flex-col space-y-2 px-2 py-3',

    navBar: 'dpi-navBar flex items-center px-2 space-x-2 select-none',
    navButtonPrev:
      'dpi-navButtonPrev px-2 py-1 cursor-pointer rounded text-gray-400 hover:text-white hover:bg-gray-700',
    navButtonNext:
      'dpi-navButtonNext px-2 py-1 cursor-pointer rounded text-gray-400 hover:text-white hover:bg-gray-700',

    months: 'dpi-months select-none',

    month: 'dpi-month table',
    caption: 'dpi-caption hidden',

    weekdays: 'dpi-weekdays table-header-group',
    weekdaysRow: 'dpi-weekdaysRow table-row',
    weekday: 'dpi-weekday table-cell text-center px-1 py-2 text-gray-500',

    body: 'dpi-body table-row-group',
    week: 'dpi-week table-row',
    day: 'dpi-day table-cell text-center p-1',
    today: 'dpi-today',
    selected: 'dpi-selected',
    outside: 'dpi-outside',

    interactionDisabled: 'dpi-interactionDisabled',
    disabled: 'dpi-disabled',
    footer: 'dpi-footer flex justify-center',
    navButtonInteractionDisabled: 'dpi-navButtonInteractionDisabled',
    todayButton: css(
      'dip-todayButton',
      'px-2 py-1 uppercase rounded text-gray-400 hover:text-white hover:bg-gray-700',
      // isToday && 'bg-gray-900 hover:bg-gray-900',
    ),
    weekNumber: 'dpi-weekNumber',
  },
};

/**
 * @param date
 * @returns {string}
 */
export const formatDate = (date) => DateTime.fromJSDate(date).toSQLDate();

/**
 * input date picker
 */
const InputDayPicker = React.memo(({ name, label, tip, ...props }) => {
  const { control } = useFormContext();

  const onDayClick = (field) => (day, modifiers, e) => {
    const { isDisabled } = parseModifiers(modifiers);

    // ignore clicks on disabled day
    if (isDisabled) {
      return;
    }

    const values = field.value || [];
    const selectedDays = modifiers['dpi-selected']
      ? values.filter((v) => !DateUtils.isSameDay(v, day))
      : values.concat(day);

    field.onChange(selectedDays);
  };

  return (
    <Controller
      name={name}
      control={control}
      render={({ field }) => {
        return (
          <div className="space-y-1">
            <label className="block space-y-1" htmlFor={field.name}>
              {label && <span className="form-label">{label}</span>}
            </label>

            <div className="relative flex items-center">
              <DayPicker
                {...dayPickerProps}
                onDayClick={onDayClick(field)}
                selectedDays={field.value}
                classNames={{
                  ...dayPickerProps.classNames,
                  container: css(
                    dayPickerProps.classNames.container,
                    'w-full py-1 bg-gray-900 rounded shadow-inner select-none',
                  ),
                  month: css(dayPickerProps.classNames.month, 'w-full'),
                }}
                {...props}
              />
            </div>

            {tip && <FormTip message={tip} />}
            <ErrorMessage name={name} />
          </div>
        );
      }}
    />
  );
}, isEqual);

export default InputDayPicker;
