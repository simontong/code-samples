import isEqual from 'react-fast-compare';
import React from 'react';
import css from '../../util/css';
import { useFormContext } from 'react-hook-form';
import FormTip from './FormTip';
import ErrorMessage from './ErrorMessage';

/**
 * option list
 */
const optionList = (values) => {
  if (values.group) {
    return (
      <optgroup key={values.group} label={values.group}>
        {optionList(values.items)}
      </optgroup>
    );
  }

  return values.map((opt, key2) => (
    <option key={[key2].join('-')} value={opt.value}>
      {opt.label}
    </option>
  ));
};

/**
 * input
 */
const InputSelect = React.memo(
  ({ name, label, tip, placeholder, className, options, ...props }) => {
    const { register } = useFormContext();
    const field = register(name);

    return (
      <div className="space-y-1">
        <label className="space-y-1">
          {label && <span className="form-label">{label}</span>}

          <select className={css('form-select w-full', className)} {...field} {...props}>
            {placeholder && <option value="">{placeholder}</option>}
            {options.map((values) => optionList(values))}
          </select>
        </label>

        {tip && <FormTip message={tip} />}
        <ErrorMessage name={field.name} />
      </div>
    );
  },
  isEqual,
);

export default InputSelect;
