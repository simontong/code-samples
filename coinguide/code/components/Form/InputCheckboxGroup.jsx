import React from 'react';
import FormTip from './FormTip';
import InputCheckbox from './InputCheckbox';

/**
 * @constructor
 */
const InputCheckboxGroup = ({ label, tip, checkboxes }) => {
  return (
    <div className="inline-block space-y-1">
      {label && <span className="form-label">{label}</span>}

      <div className="space-y-1 flex flex-col">
        {checkboxes.map((props, key) => (
          <InputCheckbox key={key} {...props} />
        ))}
      </div>

      {tip && <FormTip message={tip} />}
    </div>
  );
};

export default InputCheckboxGroup;
