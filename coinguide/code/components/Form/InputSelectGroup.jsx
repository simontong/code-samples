import React from 'react';
import ErrorMessage from './ErrorMessage';
import FormTip from './FormTip';
import { useFormContext } from 'react-hook-form';
import css from '../../util/css';

const InputSelectGroup = ({ name, label, tip, groupedOptions, className, ...props }) => {
  const { register } = useFormContext();
  const fields = [];

  return (
    <div className="space-y-1">
      <label className="space-y-1">
        {label && <span className="form-label">{label}</span>}

        <div className="space-x-1 flex justify-between">
          {groupedOptions.map((options, key) => {
            const fieldName = name + '[' + key + ']';
            const field = register(fieldName);
            fields.push(field);

            return (
              <select
                key={key}
                className={css('flex-1 form-select', className)}
                {...field}
                {...props}
              >
                {options.map((option, key) => {
                  // set label and value = option if not object
                  if (typeof option !== 'object') {
                    option = { label: option, value: option };
                  }

                  return (
                    <option key={key} value={option.value}>
                      {option.label}
                    </option>
                  );
                })}
              </select>
            );
          })}
        </div>
      </label>

      {tip && <FormTip message={tip} />}
      {fields.length && <ErrorMessage name={fields[0].name} />}
    </div>
  );
};

export default InputSelectGroup;
