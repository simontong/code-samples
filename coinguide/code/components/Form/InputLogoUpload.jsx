import { FaPlus } from 'react-icons/fa';
import React, { useEffect, useState } from 'react';
import { get, useFormContext } from 'react-hook-form';
import FormTip from './FormTip';
import ErrorMessage from './ErrorMessage';
import css from '../../util/css';

/**
 * @constructor
 */
const InputLogoUpload = ({
  wrapProps,
  innerWrapProps,
  name,
  label,
  tip,
  options,
  className,
  ...props
}) => {
  const { register, formState, watch } = useFormContext();
  const field = register(name);
  const logo = watch(field.name);
  const [src, setSrc] = useState('');
  const hasError = !!get(formState.errors, field.name);

  useEffect(() => {
    if (!logo || !logo.length || hasError) {
      setSrc('');
      return;
    }

    const url = URL.createObjectURL(logo[0]);
    setSrc(url);
  }, [hasError, logo]);

  return (
    <div className="space-y-1" {...wrapProps}>
      <label className="block space-y-1">
        {label && <span className="form-label">{label}</span>}

        <div
          {...innerWrapProps}
          className={css(
            'form-logo relative mx-auto rounded bg-gray-900 overflow-hidden',
            !src ? 'shadow-inner' : 'shadow',
            innerWrapProps && innerWrapProps.className,
          )}
        >
          <div
            className={css(
              'absolute z-20 flex items-center justify-center w-full h-full cursor-pointer',
              src && ' opacity-0 sm:hover:opacity-100 bg-black bg-opacity-50',
            )}
          >
            <input
              type="file"
              className={css('absolute opacity-0 cursor-pointer', className)}
              {...field}
              {...props}
            />
            <FaPlus size={48} />
          </div>

          {src && (
            <img src={src} alt="Logo" className=" absolute z-10 w-full h-full object-contain" />
          )}
        </div>
      </label>

      {tip && <FormTip message={tip} />}
      <ErrorMessage name={field.name} />
    </div>
  );
};

export default InputLogoUpload;
