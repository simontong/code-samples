import React from 'react';
import { useFormContext } from 'react-hook-form';
import FormTip from './FormTip';
import ErrorMessage from './ErrorMessage';
import css from '../../util/css';

/**
 * @constructor
 */
const InputRadioGroup = ({ name, label, tip, options }) => {
  const { register } = useFormContext();
  const field = register(name);

  return (
    <div className="inline-block space-y-1">
      {label && <span className="form-label">{label}</span>}

      <div className="space-y-1 flex flex-col">
        {options.map((opt, key) => {
          const { label, className, ...props } = opt;

          return (
            <label key={key} className="flex items-start cursor-pointer space-x-2">
              <input
                type="radio"
                className={css('form-radio mt-[2px]', className)}
                {...field}
                {...props}
              />
              <span>{label}</span>
            </label>
          );
        })}
      </div>

      {tip && <FormTip message={tip} />}
      <ErrorMessage name={field.name} />
    </div>
  );
};

export default InputRadioGroup;
