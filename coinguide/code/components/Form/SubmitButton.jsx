import React from 'react';
import { CgSpinner } from 'react-icons/cg';
import isEqual from 'react-fast-compare';
import css from '../../util/css';

const SubmitButton = React.memo(
  ({ icon, isLoading, children, className }) => (
    <button
      disabled={isLoading}
      type="submit"
      className={css('flex items-center space-x-2 btn btn-blue btn-md uppercase', className)}
    >
      {isLoading ? <CgSpinner className="animate-spin" /> : icon}
      <span className="flex items-center space-x-2">{children}</span>
    </button>
  ),
  isEqual,
);

export default SubmitButton;
