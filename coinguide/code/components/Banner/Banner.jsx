import React from 'react';
import isEqual from 'react-fast-compare';
import { Splide, SplideSlide } from '@splidejs/react-splide';
import '@splidejs/splide/dist/css/themes/splide-default.min.css';
import { FaAd } from 'react-icons/fa';
import { assetUrl } from '../../util/url';
import ReactPlayer from 'react-player';
import { usePage } from '@inertiajs/inertia-react';

/**
 * Banner object
 */
const BannerObject = React.memo(({ item }) => {
  const url = assetUrl(`/banners/${item.asset_url}`);

  if (url.toString().endsWith('.mp4')) {
    return (
      <ReactPlayer url={url} playing autoPlay loop muted playsinline width="100%" height="100%" />
    );
  }

  return <img src={url} className="w-full h-full object-cover block" alt="" />;
}, isEqual);

/**
 * banner inner
 */
const BannerInner = React.memo(({ banners }) => {
  return (
    <Splide
      options={{
        drag: false,
        pagination: false,
        arrows: false,
        lazyLoad: true,
        type: 'loop',
        autoplay: true,
        interval: 5500,
        // interval: 800,
        // direction: 'ttb',
        pauseOnHover: true,
        gap: 1,
      }}
    >
      {banners.map((item, key) => (
        <SplideSlide key={key}>
          <div className="relative">
            <a
              href={`/banner/click/${item.id}?url=${item.click_url}`}
              target="_blank"
              className="block"
            >
              <BannerObject item={item} />
            </a>

            <FaAd className="absolute bottom-[1px] right-[3px] text-[10px] sm:text-[14px] text-yellow-400 pointer-events-none" />
          </div>
        </SplideSlide>
      ))}
    </Splide>
  );
}, isEqual);

/**
 * banner
 */
const Banner = React.memo(({ position, ...props }) => {
  const { activeBanners } = usePage().props;

  const banners = React.useMemo(
    () => activeBanners.banners.filter((banner) => banner.position === position),
    [activeBanners],
  );

  if (banners.length === 0) {
    return null;
  }

  return (
    <div {...props}>
      <BannerInner banners={banners} />
    </div>
  );
}, isEqual);

export default Banner;
