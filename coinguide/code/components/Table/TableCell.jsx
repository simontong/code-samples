import React from 'react';
import css from '../../util/css';
import isEqual from 'react-fast-compare';

/**
 * listing cell
 */
const TableCell = React.memo(({ children, className, ...props }) => {
  // prettier-ignore
  return (
    <td className={css('px-4 py-3', className)} {...props}>
      {children}
    </td>
  );
}, isEqual);

export default TableCell;
