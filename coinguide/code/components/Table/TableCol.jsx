import React from 'react';
import css from '../../util/css';
import isEqual from 'react-fast-compare';

/**
 * listing col
 */
const TableCol = React.memo(({ children, className, ...props }) => {
  return (
    <th
      scope="col"
      className={css('px-4 py-3 text-xs uppercase whitespace-nowrap text-left', className)}
      {...props}
    >
      {children}
    </th>
  );
}, isEqual);

export default TableCol;
