import React from 'react';
import isEqual from 'react-fast-compare';
import css from '../../util/css';

const TableRow = React.memo(({ children, className }) => {
  return <tr className={css('hover:bg-gray-700 hover:bg-opacity-60', className)}>{children}</tr>;
}, isEqual);

export default TableRow;
