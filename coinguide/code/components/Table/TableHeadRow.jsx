import React from 'react';
import isEqual from 'react-fast-compare';
import css from '../../util/css';

const TableHeadRow = React.memo(({ children, className }) => {
  return <tr className={css('bg-gray-800 hidden sm:table-row', className)}>{children}</tr>;
}, isEqual);

export default TableHeadRow;
