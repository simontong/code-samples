import React from 'react';
import { DateTime } from 'luxon';
import isEqual from 'react-fast-compare';
import Tooltip from "../Tooltip/Tooltip";

const ListingRelativeDate = React.memo(({ date }) => {
  const dt = DateTime.fromISO(date);

  return (
    <Tooltip content={dt.toLocaleString(DateTime.DATETIME_SHORT)} className="shadow-lg">
      <span>{dt.toRelative()}</span>
    </Tooltip>
  );
}, isEqual);

export default ListingRelativeDate;
