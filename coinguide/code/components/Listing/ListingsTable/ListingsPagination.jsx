import React from 'react';
import { times } from 'lodash';
import { Link } from '@inertiajs/inertia-react';
import css from '../../../util/css';
import isEqual from 'react-fast-compare';
import '../../../../code.0/resources/assets/css/pagination.css';
import { absoluteUrl } from '../../../util/url';

/**
 * page link
 */
const PageLink = React.memo(({ only, href, isActive, children, className = '' }) => {
  return (
    <Link
      only={only}
      preserveScroll
      preserveState
      href={href}
      className={css(
        'relative inline-flex items-center px-2 py-2 border text-white hover:text-white',
        isActive ? 'pointer-events-none' : 'border-gray-700 bg-gray-800 hover:bg-gray-700 ',
        className,
      )}
    >
      {children}
    </Link>
  );
}, isEqual);

/**
 * listing pagination
 */
const ListingsPagination = React.memo(({ meta, only }) => {
  const currentPage = meta.current_page;
  const currentSiblingPage = currentPage > 1 ? currentPage - 1 : null;
  const firstPage = meta.first_page;
  const lastPage = meta.last_page;

  if (lastPage === 1) {
    return null;
  }

  const pages = times(meta.last_page, (n) => {
    const page = n + 1;
    const url = absoluteUrl();
    url.searchParams.set('page', String(page));

    return {
      page,
      url: url.href,
    };
  });

  const prevPage = currentPage > firstPage ? pages[currentPage - 2] : pages[0];
  const nextPage = currentPage < lastPage ? pages[currentPage] : pages[lastPage - 1];

  return (
    <div className="flex items-center justify-between">
      <div className="flex-1 flex items-center justify-center overflow-auto whitespace-nowrap">
        <PageLink
          only={only}
          href={prevPage?.url}
          isActive={currentPage === 1}
          className="hidden sm:inline-flex rounded-l-full -mr-1 border-gray-700 bg-gray-800"
        >
          <span className="sr-only">Previous</span>
          <svg
            className="w-[22px] h-[20px]"
            xmlns={'http:www.w3.org/2000/svg'}
            viewBox="0 0 20 20"
            fill="currentColor"
            aria-hidden="true"
          >
            <path
              fillRule="evenodd"
              d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
              clipRule="evenodd"
            />
          </svg>
        </PageLink>

        <ul
          className="pagination relative z-0 inline-flex rounded-md shadow-sm -space-x-px"
          aria-label="Pagination"
        >
          {pages.map((item, key) => (
            <li
              key={key}
              className={css(
                item.page === currentPage && 'active',
                item.page === currentSiblingPage && 'active-sibling',
              )}
            >
              <PageLink
                only={only}
                href={item.url}
                isActive={item.page === currentPage}
                className={css(
                  key === 0 && 'rounded-l sm:rounded-l-none',
                  key === pages.length - 1 && 'rounded-r sm:rounded-r-none',
                  currentPage === item.page && 'bg-cgblue-dark border-cgblue-light',
                )}
              >
                <span className="block px-2">{item.page}</span>
              </PageLink>
            </li>
          ))}
        </ul>

        <PageLink
          only={only}
          href={nextPage.url}
          isActive={currentPage === lastPage}
          className="hidden sm:inline-flex rounded-r-full -ml-1 border-gray-700 bg-gray-800"
        >
          <span className="sr-only">Next</span>
          <svg
            className="w-[22px] h-[20px]"
            xmlns={'http:www.w3.org/2000/svg'}
            viewBox="0 0 20 20"
            fill="currentColor"
            aria-hidden="true"
          >
            <path
              fillRule="evenodd"
              d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
              clipRule="evenodd"
            />
          </svg>
        </PageLink>
      </div>
    </div>
  );
}, isEqual);

export default ListingsPagination;