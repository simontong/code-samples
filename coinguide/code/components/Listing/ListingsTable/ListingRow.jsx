import React, { useState } from 'react';
import isEqual from 'react-fast-compare';
import css from '../../../util/css';
import TableCell from '../../Table/TableCell';
import { Link } from '@inertiajs/inertia-react';
import ListingLogo from '../ListingLogo';
import ListingAmount from '../ListingAmount';
import ListingPriceDifference from '../ListingPriceDifference';
import ListingRelativeDate from '../ListingRelativeDate';
import ListingVoteButton from '../ListingVoteButton';
import { formatNumber } from '../../../util/formatting';
import { createEnrich } from '../../../util/enrich';
import {
  LISTING_ALLOWED_FILTERS,
  LISTING_ENRICH_SOURCES,
} from '../../../../code.0/isomorphic/consts/listingConsts';
import TableRow from '../../Table/TableRow';

const ListingRow = React.memo(({ listing, className }) => {
  const [voteCount, setVoteCount] = useState(listing.meta.listingVotesCount);

  const withCoinGecko = React.useMemo(
    () => createEnrich(LISTING_ENRICH_SOURCES.coinGecko, listing),
    [listing],
  );
  // const withCoinMarketCap = React.useMemo(
  //   () => createEnrich(LISTING_ENRICH_SOURCES.coinMarketCap, listing),
  //   [listing],
  // );


  const onVoteCasted = ({ listingVotesCount, listingVotesCountToday }) => {
    // this is hacky but we need to see if the filter is set for today
    // to decide if we should set vote count to today votes or total
    if (new URLSearchParams(location.search).get('filter') === LISTING_ALLOWED_FILTERS.today) {
      setVoteCount(listingVotesCountToday);
      return;
    }

    setVoteCount(listingVotesCount);
  };

  return (
    <TableRow className={css('hover:bg-gray-700 hover:bg-opacity-60', className)}>
      {/* rank */}
      <TableCell className="hidden sm:table-cell text-center">
        <span className="text-lg font-bold">{listing.rank}</span>
      </TableCell>

      {/* name */}
      <TableCell className="w-full">
        <Link
          href={`/coins/${listing.slug}`}
          className="flex sm:inline-flex items-start text-white hover:text-white"
        >
          <ListingLogo variant="sm" logo={listing.logo} alt={listing.name} className="w-12 h-12" />

          <span className="pl-2">
            <span className="sm:hidden flex flex-col space-y-1">
              <span className="inline-flex items-center space-x-2">
                <span className="px-2 py-1 text-xs font-bold bg-gray-700 rounded">
                  {listing.rank}
                </span>
                <span>{listing.symbol}</span>
              </span>
              <span className="text-gray-500">{listing.name}</span>
            </span>

            <span className="hidden sm:flex flex-col space-y-1">
              <span>{listing.name}</span>
              <span className="uppercase text-gray-500">{listing.symbol}</span>
            </span>
          </span>
        </Link>
      </TableCell>

      {/* price */}
      <TableCell className="hidden sm:table-cell text-right">
        <ListingAmount
          isPresaleActive={listing.isPresaleActive}
          amount={withCoinGecko('market_data.current_price.usd')}
        />
      </TableCell>

      {/* price difference */}
      <TableCell className="hidden sm:table-cell text-right">
        <ListingPriceDifference
          isPresaleActive={listing.isPresaleActive}
          percent={withCoinGecko('market_data.price_change_percentage_24h')}
        />
      </TableCell>

      {/* market cap */}
      <TableCell className="hidden sm:table-cell text-right">
        <ListingAmount
          isPresaleActive={listing.isPresaleActive}
          amount={withCoinGecko('market_data.market_cap.usd')}
        />
      </TableCell>

      {/* launch date */}
      <TableCell className="hidden sm:table-cell text-right whitespace-nowrap">
        <ListingRelativeDate date={listing.launched_at} />
      </TableCell>

      {/* vote button */}
      <TableCell className="text-center">
        <ListingVoteButton
          slug={listing.slug}
          label={formatNumber(voteCount)}
          onVoteCasted={onVoteCasted}
        />
      </TableCell>
    </TableRow>
  );
}, isEqual);

export default ListingRow;