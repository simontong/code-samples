import React from 'react';
import isEqual from 'react-fast-compare';
import TableCol from '../../Table/TableCol';
import ListingRow from './ListingRow';
import TableHeadRow from '../../Table/TableHeadRow';

/**
 * listing table
 */
const ListingsTable = React.memo(({ items }) => {
  /**
   * generate unique key for item (filter options change vote
   * count so we need to not use just the row.id)
   */
  const keyUnique = (item) => `${item.id}-${item.meta?.listingVotesCount}`;

  return (
    <div className="rounded bg-gray-800 shadow-lg scrollbar scrollbar-thin scrollbar-thumb-gray-600 scrollbar-track-gray-500">
      <table className="min-w-full">
        <thead>
          <TableHeadRow>
            <TableCol className="text-center">Rank</TableCol>
            <TableCol>Name</TableCol>
            <TableCol className="text-right">Price</TableCol>
            <TableCol className="text-right">24H %</TableCol>
            <TableCol className="text-right">Market Cap</TableCol>
            <TableCol className="text-right">Launch</TableCol>
            <TableCol className="text-center">Votes</TableCol>
          </TableHeadRow>
        </thead>

        <tbody>
          {items.length === 0 && (
            <tr>
              <td colSpan={100}>
                <div className="p-10 text-center font-bold bg-gray-700 bg-opacity-20">
                  No listings available to display
                </div>
              </td>
            </tr>
          )}

          {items.map((row, key) => (
            <ListingRow
              key={keyUnique(row)}
              listing={row}
              className={key % 2 ? 'bg-gray-800' : 'bg-gray-700 bg-opacity-20'}
            />
          ))}
        </tbody>
      </table>
    </div>
  );
}, isEqual);

export default ListingsTable;
