import React from 'react';
import { FaFire, FaClock, FaStarOfLife, FaStopwatch } from 'react-icons/fa';
import { Link } from '@inertiajs/inertia-react';
import css from '../../../util/css';
import PageTitle from '../../Layouts/MainLayout/PageTitle';
import isEqual from 'react-fast-compare';

const ListingsFilter = React.memo(({ filter, className = null }) => {
  const filters = [
    { id: 'top', icon: <FaFire />, label: 'Top', title: 'All Time Top Coins' },
    { id: 'today', icon: <FaClock />, label: 'Today', title: 'Top Coins Today' },
    { id: 'new', icon: <FaStarOfLife />, label: 'New', title: 'New Coins' },
    { id: 'presale', icon: <FaStopwatch />, label: 'Presale', title: 'Presale Coins' },
  ];

  const currentFilter = React.useMemo(() => filters.find((i) => i.id === filter), filters);

  return (
    <div className={css('text-center', className)}>
      {currentFilter && <PageTitle>{currentFilter?.title}</PageTitle>}

      <div className="overflow-auto">
        <div className="inline-flex items-center">
          {filters.map((filter, key) => {
            return (
              <Link
                key={key}
                only={['listings', 'filter']}
                href={`/?filter=${filter.id}`}
                preserveScroll
                preserveState
                // className={`relative inline-flex items-center px-3 py-2 rounded-l-full border border-gray-700 text-sm font-medium hover:bg-gray-700 ${
                className={css(
                  'relative inline-flex items-center px-3 py-2 border border-gray-700 text-sm font-medium hover:bg-gray-700 text-white hover:text-white',
                  key === 0 && 'rounded-l-full',
                  key === filters.length - 1 && 'rounded-r-full',
                  key > 0 && 'border-l-0',
                  currentFilter?.id === filter.id ? 'bg-gray-700' : 'bg-gray-800',
                )}
              >
                {filter.icon}
                <span className="pl-2">{filter.label}</span>
              </Link>
            );
          })}
        </div>
      </div>
    </div>
  );
}, isEqual);

export default ListingsFilter;
