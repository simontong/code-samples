import React, { useEffect, useState } from 'react';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { MdContentCopy } from 'react-icons/md';
import isEqual from 'react-fast-compare';
import Tooltip from "../Tooltip/Tooltip";

/**
 * listing contract
 */
const ListingContractAddress = React.memo(({ address, isPresaleActive, platform }) => {
  const [isCopiedTextVisible, setIsCopiedTextVisible] = useState(false);
  const platformImg = require(`../../../assets/images/platforms/${platform.logo}`);

  // no contract address or is presale
  if (!address || isPresaleActive) {
    const text = isPresaleActive ? '(contract hidden during presale)' : '(no contract yet)';
    return (
      <div className="flex items-center space-x-2">
        <div className="px-2 py-1 rounded font-bold shadow bg-gray-700">
          <span className="flex items-center">
            <img src={platformImg} alt={platform.name} className="object-contain h-5" />
            <span className="pl-1">{platform.shortname}</span>

            <span className="pl-2 max-w-48 sm:max-w-full font-bold text-center">
              <span>{text}</span>
            </span>
          </span>
        </div>
      </div>
    );
  }

  // split address in half
  let left = address;
  let right;
  if (address && address.length > 20) {
    const len = Math.ceil(address.length / 6);
    left = address.substr(0, len);
    right = address.substr(-len);
  }

  useEffect(() => {
    if (isCopiedTextVisible) {
      setTimeout(() => setIsCopiedTextVisible(false), 1500);
    }
  }, [isCopiedTextVisible]);

  return (
    <Tooltip
      visible={isCopiedTextVisible}
      animation="shift-away-subtle"
      content="Copied!"
      className="shadow-lg"
    >
      <div>
        <CopyToClipboard text={address} onCopy={() => setIsCopiedTextVisible(true)}>
          <button className="space-x-2 btn btn-gray flex items-center" title={address}>
            <span className="flex items-center text-gray-400">
              <img src={platformImg} alt={platform.name} className="object-contain h-5" />
              <span className="pl-1">{platform.shortname}:</span>
            </span>

            <span className="relative flex items-center font-bold max-w-48 sm:max-w-full text-center">
              <span>{left}</span>
              {left.length < address.length && (
                <>
                  <span>&hellip;</span>
                  <span>{right}</span>
                </>
              )}
            </span>

            <MdContentCopy size={18} />
          </button>
        </CopyToClipboard>
      </div>
    </Tooltip>
  );
}, isEqual);

export default ListingContractAddress;
