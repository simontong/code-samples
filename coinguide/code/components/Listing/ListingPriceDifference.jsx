import { formatPercent } from '../../util/formatting';
import { FaCaretUp, FaCaretDown } from 'react-icons/fa';
import css from '../../util/css';
import React from 'react';
import isEqual from 'react-fast-compare';
import Tooltip from "../Tooltip/Tooltip";

/**
 * price diff
 */
const ListingPriceDifference = React.memo(({ isPresaleActive, percent, tip = null, ...props }) => {
  if (isPresaleActive) {
    return null;
  }

  const value = Number(percent);

  if (Number.isNaN(value) || percent === null || typeof percent === 'undefined') {
    return null;
  }

  let percentRounded = formatPercent(value, 1, 2);

  let icon;
  let textColor = 'text-gray-600';
  if (+percentRounded > 0) {
    icon = <FaCaretUp />;
    textColor = 'text-green-600';
  } else if (+percentRounded < 0) {
    icon = <FaCaretDown />;
    textColor = 'text-red-600';
  }

  return (
    <Tooltip content={tip} className="shadow-lg" disabled={!tip}>
      <div className={css('inline-flex items-center text-bold', textColor)} {...props}>
        {icon}
        <span>{percentRounded}%</span>
      </div>
    </Tooltip>
  );
}, isEqual);

export default ListingPriceDifference;
