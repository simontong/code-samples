import css from '../../util/css';
import React from 'react';
import isEqual from 'react-fast-compare';

/**
 * listing stat
 */
const ListingStat = React.memo(
  ({ title, value, valueCss }) => (
    <div className="flex items-center sm:space-x-2">
      <div className="w-1/2 sm:w-auto font-bold">{title}</div>
      <div className={css('py-1 px-3 rounded bg-gray-900', valueCss)}>{value}</div>
    </div>
  ),
  isEqual,
);

export default ListingStat;
