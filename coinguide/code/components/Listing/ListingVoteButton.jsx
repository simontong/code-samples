import React from 'react';
import css from '../../util/css';
import { FaThumbsUp } from 'react-icons/fa';
import isEqual from 'react-fast-compare';
import securityVerification from '../Util/securityVerification';
import headsUpNotification from '../Util/headsUpNotification';
import { useAppContext } from '../../context/AppContext';
import { listingLogoUrl } from '../../util/url';
import Tooltip from "../Tooltip/Tooltip";

const ListingVoteButton = React.memo(
  ({ onVoteCasted, slug, label, className = null, ...props }) => {
    const { selectors, actions } = useAppContext();
    const hasVoted = selectors.votes.exists(slug);

    const onClickVote = async () => {
      // ignore if already voted
      if (hasVoted) {
        return;
      }

      // prettier-ignore
      const res = await securityVerification(`/vote/${slug}`)
      .catch((err) => console.error(err));

      // user clicked close on security
      if (res === false) {
        return;
      }

      const data = await res.json();
      const { listingVote } = data;

      if (typeof onVoteCasted === 'function') {
        onVoteCasted(data);
      }

      // add new vote to session
      actions.votes.add(listingVote);

      headsUpNotification({
        icon: listingLogoUrl(listingVote.listing.logo, 'sm'),
        title: `You voted for ${listingVote.listing.name} (${listingVote.listing.symbol})`,
        message: 'Your vote was successfully submitted',
      });
    };

    return (
      <Tooltip
        content="You can only vote once every 24 hours"
        disabled={!hasVoted}
        className="shadow-lg"
      >
        <button
          className={css(
            'w-full flex items-center justify-center py-2 px-3 sm:py-2 sm:px-4 space-x-2',
            'rounded-full font-bold shadow ring-2 ring-white hover:ring-cgblue-light',
            'hover:bg-cgblue-dark whitespace-nowrap transition-all',
            hasVoted && 'bg-cgblue-dark ring-cgblue-light',
            className,
          )}
          onClick={onClickVote}
          {...props}
        >
          <FaThumbsUp />
          <span>{label}</span>
        </button>
      </Tooltip>
    );
  },
  isEqual,
);

export default ListingVoteButton;
