import React from 'react';
import { formatCurrency, formatNumber } from '../../util/formatting';
import isEqual from 'react-fast-compare';
import css from '../../util/css';

/**
 * list price
 */
const ListingAmount = React.memo(
  ({
    amount,
    isPresaleActive,
    symbol,
    format = 'currency',
    hideZero = false,
    className,
    ...props
  }) => {
    if (isPresaleActive) {
      return (
        <div className={css('font-bold', className)} {...props}>
          PRESALE
        </div>
      );
    }

    const value = Number(amount);

    if (
      typeof amount === 'undefined' ||
      Number.isNaN(value) ||
      amount === null ||
      (hideZero && amount === 0)
    ) {
      return <div {...props}>-</div>;
    }

    const label = format === 'currency' ? formatCurrency(value) : formatNumber(value);

    let fontSize;
    const len = String(label).length;
    switch (true) {
      case len > 17:
        fontSize = '80%';
        break;

      case len > 14:
        fontSize = '90%';
        break;
    }

    return (
      <div style={{ fontSize }} {...props}>
        {label}
      </div>
    );
  },
  isEqual,
);

export default ListingAmount;
