import React from 'react';
import isEqual from 'react-fast-compare';

const ListingOverviewStat = React.memo(({ title, children, ...props }) => {
  return (
    <div {...props}>
      <div className="text-xs uppercase font-bold text-gray-400">{title}</div>
      <div>{children}</div>
    </div>
  );
}, isEqual);

export default ListingOverviewStat;
