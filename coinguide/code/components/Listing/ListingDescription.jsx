import React, { useEffect, useState } from 'react';
import isEqual from 'react-fast-compare';
import { FaCaretDown } from 'react-icons/fa';
import sanitizeHtml from 'sanitize-html';
import { LISTING_DESCRIPTION_ALLOWED_TAGS } from '../../../code.0/isomorphic/consts/listingConsts';

const clampMaxHeight = 440;

/**
 * listing description
 */
const ListingDescription = React.memo(({ text }) => {
  const [clampDescription, setClampDescription] = useState(true);
  const [showReadMoreDescBtn, setShowReadMoreDescBtn] = useState(false);
  const descriptionRef = React.createRef();

  const html = sanitizeHtml(text, {
    allowedTags: LISTING_DESCRIPTION_ALLOWED_TAGS,
  }).replace(/\n/, '<br><br>');

  const onClickShowMore = () => {
    setClampDescription(false);
    setShowReadMoreDescBtn(false);
  };

  useEffect(() => {
    const { offsetHeight, scrollHeight } = descriptionRef.current;
    if (offsetHeight > clampMaxHeight) {
      setClampDescription(true);
    }

    if (offsetHeight !== scrollHeight) {
      setShowReadMoreDescBtn(true);
    }
  }, []);

  return (
    <div className="relative enrich-defaults">
      <div
        ref={descriptionRef}
        className="overflow-hidden"
        style={{ maxHeight: clampDescription ? clampMaxHeight : '100%' }}
        dangerouslySetInnerHTML={{
          __html: html,
        }}
      />

      {showReadMoreDescBtn && (
        <div
          className="absolute w-full bottom-0 cursor-pointer bg-gray-800 hover:underline"
          onClick={onClickShowMore}
        >
          <div className="flex items-center justify-center -mt-10 h-10 absolute w-full cursor-pointer bg-gradient-to-b from-transparent to-gray-800" />
          <div className="flex items-center justify-center">
            <FaCaretDown />
            <span className="p-1">Show more</span>
          </div>
        </div>
      )}
    </div>
  );
}, isEqual);

export default ListingDescription;