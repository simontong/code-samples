import { FaSearch, FaTimesCircle } from 'react-icons/fa';
import css from '../../../util/css';
import React from 'react';
import isEqual from 'react-fast-compare';
import { CgSpinner } from 'react-icons/cg';

/**
 * search input
 */
const SearchInput = React.memo(
  ({ isOpen, keywords, isLoading, onFocus, setKeywords, onClickClear, onKeyDown, ...props }) => {
    return (
      <div className="relative z-30 flex items-center">
        {/* loading icon */}
        <div className="absolute left-3 mt-[-2px] text-gray-500">
          {!isLoading ? <FaSearch /> : <CgSpinner size={18} className="animate-spin" />}
        </div>

        {/* input field */}
        <input
          {...props}
          type="text"
          autoComplete="off"
          className={css(
            'form-input w-full px-10 !border-2 !ring-0',
            isOpen
              ? '!rounded-b-none !border-b-0 !border-gray-700 !shadow-none'
              : '!border-gray-800',
          )}
          value={keywords}
          onChange={(e) => setKeywords(e.target.value)}
          onFocus={onFocus}
          onKeyDown={onKeyDown}
        />

        {/* close */}
        {keywords && (
          <div
            className="absolute right-3 mt-[-2px] text-gray-500 hover:text-white cursor-pointer"
            onClick={onClickClear}
          >
            <FaTimesCircle />
          </div>
        )}
      </div>
    );
  },
  isEqual,
);

export default SearchInput;
