import React, { useEffect, useRef, useState } from 'react';
import css from '../../../util/css';
import isEqual from 'react-fast-compare';
import SearchResults from './SearchResults';
import SearchInput from './SearchInput';

/**
 * input
 */
const ListingSearch = React.memo(({ inputProps, handleClickItem, handleToggleOpen }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [selectedIndex, setSelectedIndex] = useState(0);
  const [items, setItems] = useState([]);
  const [keywords, setKeywords] = useState('');
  const wrapRef = useRef();
  const resultsRef = useRef();

  /**
   * handle toggling results open
   */
  const toggleOpen = (isOpen) => {
    if (handleToggleOpen) {
      handleToggleOpen(isOpen);
    }
    setIsOpen(isOpen);
  };

  /**
   * handle on focus function
   */
  const onFocusInput = () => {
    toggleOpen(true);
  };

  /**
   * handle clicking an item
   */
  const onClickItem = (item) => () => {
    if (handleClickItem) {
      handleClickItem(item)();
    }
    toggleOpen(false);
  };

  /**
   * handle blurring properly
   */
  useEffect(() => {
    const onClickOutside = (e) => {
      if (wrapRef.current && !wrapRef.current.contains(e.target)) {
        toggleOpen(false);
      }
    };

    if (isOpen) {
      document.addEventListener('mouseup', onClickOutside);
    } else {
      document.removeEventListener('mouseup', onClickOutside);
    }

    return () => {
      document.removeEventListener('mouseup', onClickOutside);
    };
  }, [isOpen]);

  /**
   * set scroll pos for results
   */
  const setScrollPos = (index) => {
    if (!resultsRef.current) {
      return;
    }
    const selectedItem = resultsRef.current.children[index];
    const resHeight = resultsRef.current.offsetHeight;
    const resTop = resultsRef.current.scrollTop;
    const resBottom = resHeight + resTop;
    // @ts-ignore
    const elTop = selectedItem.offsetTop;
    // @ts-ignore

    const elBottom = selectedItem.offsetHeight + elTop;
    if (elBottom > resBottom) {
      resultsRef.current.scrollTop = elBottom - resHeight;
      return;
    }
    if (elTop < resTop) {
      resultsRef.current.scrollTop = elTop;
    }
  };
  /**
   * on input key up
   */
  const onKeyDown = (e) => {
    setIsOpen(true);
    // move up an item
    if (e.key === 'ArrowUp' || (e.shiftKey && e.key === 'Tab')) {
      e.preventDefault();
      let index = Number(selectedIndex) - 1;
      if (index < 0) {
        index = items.length - 1;
      }
      setScrollPos(index);

      setSelectedIndex(index);
      return;
    }
    // move down an item
    if (e.key === 'ArrowDown' || (!e.shiftKey && e.key === 'Tab')) {
      e.preventDefault();
      let index = selectedIndex === null ? 0 : selectedIndex + 1;
      if (index >= items.length) index = 0;

      if (!isOpen) {
        index = 0;
      }
      setScrollPos(index);

      setSelectedIndex(index);
      return;
    }
    // on enter key pressed
    if (e.key === 'Enter') {
      e.preventDefault();
      const selectedItem = resultsRef.current?.children[selectedIndex];
      if (selectedItem) {
        selectedItem.click();
      }
      return;
    }
    // close search on escape
    if (e.key === 'Escape') {
      e.preventDefault();
      setIsOpen(false);
      setKeywords('');
      e.target.blur();
    }
  };
  /**
   * on clear button clicked
   */
  const onClickClear = () => {
    setKeywords('');
    setIsOpen(false);
  };

  return (
    <div className="relative" ref={wrapRef}>
      {/* search input */}
      <SearchInput
        {...inputProps}
        keywords={keywords}
        isOpen={isOpen}
        isLoading={isLoading}
        setKeywords={setKeywords}
        onKeyDown={onKeyDown}
        onFocus={onFocusInput}
        onClickClear={onClickClear}
      />

      {/* search results */}
      {isOpen && (
        <div
          className={css(
            'absolute z-20 left-0 right-0 max-h-60 overflow-auto scrollbar scrollbar-thin scrollbar-thumb-gray-700',
            'scrollbar-track-gray-800 scrollbar-track-rounded scrollbar-thumb-rounded',
            'border-2 border-t-0 border-gray-700 bg-gray-900 rounded-b shadow',
          )}
          ref={resultsRef}
        >
          <SearchResults
            onClickItem={onClickItem}
            setItems={setItems}
            selectedIndex={selectedIndex}
            setIsLoading={setIsLoading}
            keywords={keywords}
          />
        </div>
      )}
    </div>
  );
}, isEqual);

export default ListingSearch;
