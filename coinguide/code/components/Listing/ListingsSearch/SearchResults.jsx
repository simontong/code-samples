import React, { useEffect } from 'react';
import isEqual from 'react-fast-compare';
import useDebounce from '../../../hooks/useDebounce';
import useSWR from 'swr';
import css from '../../../util/css';
import ListingLogo from '../ListingLogo';
import ky from 'ky';

/**
 * post verification or init
 */
const fetchResults = (url) => {
  const opts = {
    throwHttpErrors: false,
    headers: {
      'accept': 'application/json',
      // 'x-xsrf-token': Cookies.get('XSRF-TOKEN'),
      'x-requested-with': 'XMLHttpRequest',
    },
  };

  return ky.get(url, opts);
};

/**
 * search message
 */
const SearchMessage = React.memo(
  ({ text }) => <div className="p-10 text-center font-bold">{text}</div>,
  isEqual,
);

/**
 * search results
 */
const SearchResults = React.memo(
  ({ keywords, selectedIndex, onClickItem, setIsLoading, setItems }) => {
    const keywordsTrimmed = keywords.trim();

    const debouncedKeywords = useDebounce(keywordsTrimmed, 500);
    const url = `/listing/search?keywords=${debouncedKeywords}`;
    const { data, error } = useSWR(url, () => {
      return !keywordsTrimmed ? null : fetchResults(url).then((res) => res.json());
    });

    const isLoading =
      keywordsTrimmed && ((!data && !error) || keywordsTrimmed !== debouncedKeywords);

    useEffect(() => {
      setIsLoading(isLoading);
    }, [isLoading]);

    useEffect(() => {
      if (data) {
        setItems(data.listings);
      }
    }, [data]);

    if (!keywordsTrimmed) {
      return <SearchMessage text="Search for coins" />;
    }

    if (isLoading) {
      return <SearchMessage text="Searching for coins ..." />;
    }

    if (error) {
      return <SearchMessage text="Something went wrong!" />;
    }

    if (data.listings.length === 0) {
      return <SearchMessage text="No coins found" />;
    }

    return data.listings.map((listing, key) => (
      <div
        key={listing.id}
        className={css(
          'flex items-center text-white hover:text-white py-3 px-3 cursor-pointer space-x-1',
          selectedIndex === key ? 'bg-gray-800' : 'hover:bg-gray-800 hover:bg-opacity-40',
        )}
        onClick={onClickItem(listing)}
      >
        {/* logo */}
        <ListingLogo variant="sm" logo={listing.logo} alt={listing.name} className="w-8 h-8" />

        {/* name */}
        <span className="flex space-x-1">
          <span>{listing.name}</span>
          <span className="inline-block uppercase px-1 bg-gray-700 rounded">
            {listing.symbol}
          </span>
        </span>
      </div>
    ));
  },
  isEqual,
);

export default SearchResults;
