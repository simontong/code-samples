import React, { useState } from 'react';
import css from '../../util/css';
import isEqual from 'react-fast-compare';
import { listingLogoUrl } from '../../util/url';

/**
 * logo
 */
const ListingLogo = React.memo(
  ({ logo, variant, className, alt = '', noFadeIn = false, ...props }) => {
    const url = listingLogoUrl(logo, variant);

    const [hasLoaded, setHasLoaded] = useState(noFadeIn);

    return (
      <img
        src={url}
        alt={alt}
        onLoad={() => setHasLoaded(true)}
        className={css(
          'object-contain rounded opacity-0 transition-opacity duration-700',
          hasLoaded && 'opacity-100',
          className,
        )}
        {...props}
      />
    );
  },
  isEqual,
);

export default ListingLogo;
