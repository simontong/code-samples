import React from 'react';
import isEqual from 'react-fast-compare';
import coingeckoImg from '../../../code.0/resources/assets/images/coingecko.png';
import coinmarketcapImg from '../../../code.0/resources/assets/images/coinmarketcap.png';
import css from '../../util/css';

const types = {
  coingecko: {
    name: 'CoinGecko',
    src: coingeckoImg,
    url: (slug) => `https://www.coingecko.com/en/coins/${slug}`,
  },

  coinmarketcap: {
    name: 'CoinMarketCap',
    src: coinmarketcapImg,
    url: (slug) => `https://coinmarketcap.com/currencies/${slug}`,
  },
};

/**
 * coinmarketcap / coingecko urls
 */
const ListingSource = React.memo(({ slug, type, className, ...props }) => {
  const item = types[type];

  if (!slug) {
    return null;
  }

  if (!item) {
    console.log(`URL group type '${type}' does not exist`);
    return null;
  }

  const url = item.url(slug);

  return (
    <div {...props}>
      <a
        className={css(
          'flex items-center space-x-1 text-white hover:text-white',
          'inline-flex items-center btn btn-round btn-gray',
          className,
        )}
        href={url}
        target="_blank"
        {...props}
      >
        <img
          src={item.src}
          alt={item.name}
          className={css('w-[20px] h-[20px] object-contain')}
          {...props}
        />

        <span>{item.name}</span>
      </a>
    </div>
  );
}, isEqual);

export default ListingSource;