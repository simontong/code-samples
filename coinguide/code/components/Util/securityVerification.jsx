import React, { useEffect, useRef, useState } from 'react';
import { create as createModal } from 'react-modal-promise';
import { IoMdCheckmark, IoMdClose } from 'react-icons/io';
import ReactSlider from 'react-slider';
import css from '../../util/css';
import ky from 'ky';
import isEqual from 'react-fast-compare';
import { StatusCodes } from 'http-status-codes';
import { FaChevronRight } from 'react-icons/fa';
import { ImSpinner8 } from 'react-icons/im';
import { defaultsDeep, set } from 'lodash';
import '../../../code.0/resources/assets/css/react-slider.css';
import { sleep } from '../../util/sleep';

const statuses = {
  loading: 'loading',
  ready: 'ready',
  submitting: 'submitting',
  failed: 'failed',
  success: 'success',
};

/**
 * post verification or init
 */
const postOrInitVerification = ({ url, kyOpts, angle, init }) => {
  const opts = defaultsDeep(kyOpts, {
    throwHttpErrors: false,
    headers: {
      'accept': 'application/json',
      // 'x-xsrf-token': Cookies.get('XSRF-TOKEN'),
      'x-requested-with': 'XMLHttpRequest',
    },
  });

  const secInit = init ? '1' : '';
  const secAngle = Number(angle);

  // if is a FormData post vs json post
  if (opts.body) {
    opts.body.set('!secInit', secInit);
    opts.body.set('!secAngle', secAngle);
  } else {
    set(opts, 'json.!secInit', secInit);
    set(opts, 'json.!secAngle', secAngle);
  }

  return ky.post(url, opts);
};

/**
 * security image
 */
const SecurityImage = ({ src, angle, className, onLoad, ...props }) => {
  const [hasLoaded, setHasLoaded] = useState(false);

  const handleOnLoad = (e) => {
    setHasLoaded(true);
    if (onLoad) {
      onLoad(e);
    }
  };

  return (
    <img
      src={src}
      alt=""
      style={{ transform: `rotate(${angle}deg)` }}
      onLoad={handleOnLoad}
      className={css(
        'object-contain opacity-0 transition-opacity',
        hasLoaded && 'opacity-100',
        className,
      )}
      {...props}
    />
  );
};

/**
 * modal
 */
const VerificationModal = React.memo(({ isOpen, onResolve, url, kyOpts }) => {
  const [slideEnded, setSlideEnded] = useState(false);
  const [fetchingImage, setFetchingImage] = useState(true);
  const [imageUrl, setImageUrl] = useState('');
  const [value, setValue] = useState(0);
  const [imageAngle, setImageAngle] = useState(0);
  const [status, setStatus] = useState(statuses.ready);
  const [message, setMessage] = useState('');
  const boxRef = useRef();

  // need to be this way so tailwind can parse it (should match number below)
  const sliderTransitionDurationCss = 'duration-300';
  const sliderTransitionDuration = 300;

  // pause displaying result (fail/success) before reverting back
  const resultPause = 700;

  /**
   * handle close
   */
  const handleClose = (ignoreBoxRef) => (e) => {
    if (status !== statuses.ready) {
      // todo maybe show wiggle first?
      return;
    }

    // if clicked inside box
    if (!ignoreBoxRef && boxRef.current.contains(e.target)) {
      return;
    }

    onResolve(false);
  };

  /**
   * on image loaded
   */
  const onImageLoaded = () => {
    setMessage('');
  };

  /**
   * on change
   */
  const onChange = (value) => {
    setValue(value);

    let angle = (value / 100) * 360;
    setImageAngle(angle);
  };

  /**
   * handle change
   */
  const onAfterChange = async () => {
    if (status === statuses.submitting) {
      return;
    }

    // convert value into angle
    const angle = (360 / 100) * value;

    // post data
    setStatus(statuses.submitting);
    setSlideEnded(true);

    // todo: TEST WHEN THIS FAILS, IT DOESN'T REVERT TO STATUS=READY

    let res;
    try {
      res = await postOrInitVerification({ url, kyOpts, angle });
    } catch (err) {
      setStatus(statuses.failed);
    }

    // if failed image verification
    if (res.status === StatusCodes.UNAUTHORIZED) {
      // show error on slider
      setStatus(statuses.failed);
      await sleep(resultPause);

      setStatus(statuses.loading);

      // load new image
      const blob = await res.blob();
      const src = URL.createObjectURL(blob);
      setImageUrl(src);

      // pause for slider transition to finish
      setValue(0);
      setImageAngle(0);
      await sleep(sliderTransitionDuration);
      setSlideEnded(false);
      setStatus(statuses.ready);

      return;
    }

    // success
    setStatus(statuses.success);
    await sleep(resultPause);
    onResolve(res);
  };

  // load image on start
  useEffect(() => {
    (async () => {
      setMessage('Fetching image ...');
      setFetchingImage(true);
      setStatus(statuses.loading);

      const res = await postOrInitVerification({ url, init: true });
      const blob = await res.blob();
      const src = URL.createObjectURL(blob);

      setImageUrl(src);
      setImageAngle(0);

      setMessage('');
      setFetchingImage(false);
      setStatus(statuses.ready);
    })();
  }, []);

  return (
    <div className={!isOpen ? 'hidden' : ''}>
      <div
        onClick={handleClose(false)}
        className="fixed z-[998] top-0 right-0 bottom-0 left-0 bg-black flex items-center justify-center bg-opacity-40"
      >
        <div ref={boxRef} className="w-[300px] p-2 bg-gray-800 rounded-lg shadow-lg">
          <div className="relative flex items-center text-gray-500">
            <div className="flex-1 text-center text-sm">Security Verification</div>
            <IoMdClose
              onClick={handleClose(true)}
              size={20}
              className="absolute right-0 cursor-pointer hover:text-white"
            />
          </div>

          <div className="py-8 px-4 flex flex-col items-center space-y-8">
            <div>Drag slider to straighten image</div>

            <div
              className={css(
                'w-[180px] h-[180px] relative flex items-center justify-center overflow-hidden bg-gray-900 ring-2 rounded-full',
                // prettier-ignore
                [statuses.loading, statuses.ready, statuses.submitting].includes(status) && 'ring-white',
                status === statuses.failed && 'ring-red-500',
                status === statuses.success && 'ring-green-500',
              )}
            >
              <div
                className={css(
                  'absolute z-10 w-full h-full flex items-center justify-center bg-gray-900',
                  status === statuses.ready ? 'bg-opacity-0' : 'bg-opacity-80',
                  status === statuses.failed && 'text-red-500',
                  status === statuses.success && 'text-green-500',
                )}
              >
                {status === statuses.submitting && (
                  <ImSpinner8 size={100} className="animate-spin" />
                )}
                {status === statuses.failed && <IoMdClose size={130} />}
                {status === statuses.success && <IoMdCheckmark size={130} />}

                {message && <div className="text-white p-2">{message}</div>}
              </div>

              {!fetchingImage && (
                <SecurityImage
                  src={imageUrl}
                  angle={imageAngle}
                  onLoad={onImageLoaded}
                  className={
                    slideEnded ? `transition-transform ${sliderTransitionDurationCss}` : ''
                  }
                />
              )}
            </div>

            <ReactSlider
              value={value}
              className={css(
                'react-slider w-full h-12 bg-gray-500  ring-gray-600 rounded-full shadow-inner',
                status === statuses.failed && 'animate-shake',
                (status !== statuses.ready || slideEnded) && 'pointer-events-none',
              )}
              thumbClassName={css(
                'w-12 h-full flex items-center justify-center cursor-pointer',
                'rounded-full hover:bg-white shadow bg-gray-200 text-gray-600',
                slideEnded && `transition-position ${sliderTransitionDurationCss}`,
                status !== statuses.ready && 'bg-opacity-60',
                // result === statuses.ready && 'bg-gray-300 text-gray-600',
                // result === statuses.submitting && 'bg-gray-800 text-white',
                // result === statuses.failed && 'bg-red-600 text-white',
              )}
              trackClassName={css(
                'track h-full rounded-full',
                slideEnded && `transition-position ${sliderTransitionDurationCss}`,
              )}
              renderThumb={(props) => (
                <div {...props}>
                  <FaChevronRight size={24} />
                </div>
              )}
              onChange={onChange}
              onAfterChange={onAfterChange}
              snapDragDisabled
            />
          </div>
        </div>
      </div>
    </div>
  );
}, isEqual);

/**
 * verification modal
 */
const securityVerification = async (url, kyOpts = {}) => {
  return await createModal(VerificationModal)({
    url,
    kyOpts,
    enterTimeout: 0,
    exitTimeout: 0,
  });
};

export default securityVerification;