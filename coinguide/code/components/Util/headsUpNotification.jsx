import { toast } from 'react-hot-toast';
import React from 'react';
import { FaTimes } from 'react-icons/fa';
import { Transition } from '@headlessui/react/dist/headlessui.cjs.production.min';

const headsUpNotification = ({ icon, title, message }) => {
  toast.custom((t) => (
    <Transition
      as="div"
      className="max-w-md w-full opacity-0 pointer-events-auto scale-50 bg-white shadow-lg rounded-lg flex ring-1 ring-black ring-opacity-5"
      show={t.visible}
      enter="transform transition duration-200"
      enterFrom="opacity-0 scale-50"
      enterTo="opacity-100 scale-100"
      leave="transform duration-200 transition ease-in-out"
      leaveFrom="opacity-100 scale-100"
      leaveTo="opacity-0 scale-50"
      onClick={() => toast.dismiss(t.id)}
    >
      <div className="flex-1 w-0 p-4">
        <div className="flex items-start">
          <div className="flex-shrink-0 pt-0.5">{icon && <img className="h-10 w-10 rounded" src={icon} alt="" />}</div>
          <div className="ml-3 flex-1">
            {title && <div className="font-bold text-gray-900">{title}</div>}
            {message && <div className="mt-1 text-gray-800">{message}</div>}
          </div>
        </div>
      </div>
      <div className="flex border-l flex items-start pt-2 px-2 border-gray-100">
        <button onClick={() => toast.dismiss(t.id)} className="text-gray-600">
          <FaTimes />
        </button>
      </div>
    </Transition>
  ));
};

export default headsUpNotification;
