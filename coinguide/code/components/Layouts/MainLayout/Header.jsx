import { Link, usePage } from '@inertiajs/inertia-react';
import React, { useEffect, useRef, useState } from 'react';
import css from '../../../util/css';
import isEqual from 'react-fast-compare';
import Search from './Search';
import Logo from './Logo';

/**
 * menu links
 */
const MenuLinks = React.memo(({ main, social }) => {
  return (
    <>
      {main?.map((link, key) => (
        <Link
          key={key}
          href={link.href}
          className={css(
            'sm:mx-1 px-3 py-2 rounded uppercase text-gray-300 hover:text-white hover:bg-gray-700',
            link.current && 'bg-gray-900 hover:bg-gray-900',
          )}
        >
          {link.text}
        </Link>
      ))}

      {social?.map((link, key) => (
        <a
          key={key}
          href={link.href}
          target="_blank"
          className="sm:mx-1 px-1 text-gray-400 hover:text-white"
        >
          {link.icon}
        </a>
      ))}
    </>
  );
}, isEqual);

/**
 * Header
 */
const Header = React.memo(({ links }) => {
  const { url } = usePage();
  const [menuOpen, setMenuOpen] = useState(false);
  const menuRef = useRef();
  const menuBtnRef = useRef();
  const menuWrapRef = useRef();

  /**
   * on click menu button
   */
  const onClickMenuBtn = () => {
    setMenuOpen(!menuOpen);
  };

  /**
   * close menu on url change
   */
  useEffect(() => {
    setMenuOpen(false);
  }, [url]);

  /**
   * on click inside / outside menu
   */
  useEffect(() => {
    const onClickOutsideMainNav = (e) => {
      // click outside button and menu
      if (!menuRef.current?.contains(e.target) && !menuBtnRef.current?.contains(e.target)) {
        setMenuOpen(false);
      }
    };

    if (menuOpen) {
      document.addEventListener('mouseup', onClickOutsideMainNav);
    } else {
      document.removeEventListener('mouseup', onClickOutsideMainNav);
    }

    return () => {
      document.removeEventListener('mouseup', onClickOutsideMainNav);
    };
  }, [menuOpen]);

  return (
    <>
      {/* placeholder because menu is fixed */}
      <div>
        <div style={{ height: 64 }}/>
      </div>

      <div ref={menuWrapRef} className="fixed top-0 left-0 right-0 z-40 shadow-lg bg-gray-800">
        <div className="container mx-auto max-w-screen-xl">
          <div className="flex flex-wrap items-center justify-center px-3 sm:px-9">
            {/* logo */}
            <div className="flex-shrink-0">
              <Logo className="px-3 -ml-3" />
            </div>

            {/* search */}
            <div className="flex-1 flex mx-2">
              <Search />
            </div>

            {/* links on right */}
            <div className="hidden sm:flex sm:items-center sm:ml-auto">
              <MenuLinks main={links.main} social={links.social} />
            </div>

            {/* mobile menu button */}
            <div ref={menuBtnRef} className="block sm:hidden">
              <button
                onClick={onClickMenuBtn}
                className={css('-mr-2 px-2 py-2 rounded focus:bg-gray-700')}
              >
                <svg
                  className="block h-6 w-6"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  aria-hidden="true"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d={!menuOpen ? 'M4 6h16M4 12h16M4 18h16' : 'M6 18L18 6M6 6l12 12'}
                  />
                </svg>
              </button>
            </div>
          </div>

          {/* mobile menu links */}
          <div ref={menuRef} className={css(!menuOpen && 'hidden', 'sm:hidden')}>
            <div className="flex flex-col px-1 space-y-1">
              <MenuLinks main={links.main} />
            </div>

            <div className="flex items-center justify-center py-3 space-x-2">
              <MenuLinks social={links.social} />
            </div>
          </div>
        </div>
      </div>
    </>
  );
});

export default Header;
