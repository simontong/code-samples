import { Link } from '@inertiajs/inertia-react';
import React from 'react';
import isEqual from 'react-fast-compare';
import css from '../../../util/css';
import coinguideImg from '../../../../code.0/resources/assets/images/coinguide.svg';

/**
 * logo
 */
const Logo = React.memo(({ className = '', ...props }) => {
  return (
    <Link
      href="/"
      className={css('block box-content h-10 py-3 hover:bg-gray-900', className)}
      {...props}
    >
      <img src={coinguideImg} alt="" className="h-full object-contain mt-[1px]" />
    </Link>
  );
}, isEqual);

export default Logo;