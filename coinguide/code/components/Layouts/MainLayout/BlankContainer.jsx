import React from 'react';
import css from '../../../util/css';

const BlankContainer = ({ children, className, width = 'max-w-screen-xl' }) => {
  // prettier-ignore
  return (
    <div className={css(
      className,
      width,
      `container mx-auto px-3 py-3 sm:px-10 sm:py-4`,
    )}>
      {children}
    </div>
  )
};

export default BlankContainer;
