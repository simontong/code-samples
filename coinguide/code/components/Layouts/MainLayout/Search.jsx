import React, { useState } from 'react';
import isEqual from 'react-fast-compare';
import ListingSearch from '../../Listing/ListingsSearch/ListingSearch';
import { Inertia } from '@inertiajs/inertia';
import css from '../../../util/css';

const Search = React.memo(() => {
  const [isOpen, setIsOpen] = useState(false);

  /**
   * handle focus blur
   * @param open
   */
  const handleToggleOpen = (open) => {
    setIsOpen(open);
  };

  /**
   * on click search item
   * @param item
   */
  const handleClickItem = (item) => () => {
    Inertia.visit(`/coins/${item.slug}`);
  };

  return (
    <div className="flex items-center">
      <div
        className={css(
          'w-full',
          isOpen ? 'absolute z-30 px-3 left-0 sm:static sm:px-0 sm:max-w-[400px]' : 'sm:w-[250px]',
        )}
      >
        <ListingSearch handleToggleOpen={handleToggleOpen} handleClickItem={handleClickItem} />
      </div>
    </div>
  );
}, isEqual);

export default Search;
