import React from 'react';
import { Link } from '@inertiajs/inertia-react';
import css from '../../../util/css';
import isEqual from 'react-fast-compare';
import Logo from './Logo';

/**
 * menu links
 */
const MenuLinks = React.memo(({ links }) => {
  return (
    <>
      {links.map((link, key) => (
        <Link
          key={key}
          href={link.href}
          className={css(
            'mx-1 px-2 py-1 uppercase rounded text-gray-400 hover:text-white hover:bg-gray-700',
            link.current && 'bg-gray-900 hover:bg-gray-900',
          )}
        >
          {link.text}
        </Link>
      ))}
    </>
  );
}, isEqual);

/**
 * footer
 */
const Footer = React.memo(({ links }) => {
  return (
    <div className="shadow bg-gray-800 border-t border-gray-900">
      <div className="container mx-auto max-w-screen-xl flex flex-col items-center justify-center py-10 space-y-4 bg-gray-800">
        {/* links */}
        <div className="flex flex-col sm:flex-row items-center space-y-1 sm:space-y-0">
          <MenuLinks links={links} />
        </div>

        {/* logo */}
        <Logo className="grayscale transition-opacity opacity-30 hover:opacity-100 hover:bg-transparent" />

        {/* copyright */}
        <div className="text-center text-gray-500">
          &copy; {new Date().getFullYear()} Coin.Guide
        </div>
      </div>
    </div>
  );
}, isEqual);

export default Footer;
