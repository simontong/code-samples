import React from 'react';
import RootComponent from '../../RootComponent';
import Header from './Header';
import Footer from './Footer';
import { FaTelegram, FaTwitter } from 'react-icons/fa';
import { usePage } from '@inertiajs/inertia-react';
import Banner from '../../Banner/Banner';
import Papercups from "../Papercups";

/**
 * main layout
 */
const MainLayout = ({ children }) => {
  const { url } = usePage();

  // prettier-ignore
  const headerLinks = React.useMemo(() => {
    const items = {
      main: [
        // todo: remove this once advertise is finished
        { href: window.coinguideConfig.env === 'development' ? '/advertise1' : '/advertise', text: 'Advertise' },
        { href: '/listing/create', text: 'Add Coin' },
      ],

      social: [
        { href: 'https://twitter.com/the_coin_guide', icon: <FaTwitter size={18}/> },
        { href: 'https://t.me/thecoinguide', icon: <FaTelegram size={18}/> },
      ]
    }

    items.main = items.main.map((item) => ({
      ...item,
      current: item.href === url,
    }));

    return items;
  }, [url]);

  const footerLinks = React.useMemo(() => {
    const items = [
      { href: '/advertise', text: 'Advertise With Us' },
      { href: '/disclaimer', text: 'Disclaimer' },
      { href: '/contact', text: 'Contact' },
      { href: '/privacy', text: 'Privacy' },
      { href: '/terms', text: 'Terms & Conditions' },
    ];

    return items.map((item) => ({
      ...item,
      current: item.href === url,
    }));
  }, [url]);

  // prettier-ignore
  return (
    <RootComponent>
      <Papercups />

      <Banner
        // className="fixed bottom-0 overflow-hidden mx-auto rounded-sm shadow"
        className="fixed z-40 bottom-0 left-2 right-2 overflow-hidden mx-auto rounded-sm rounded-b-none shadow"
        // style={{ maxWidth: 970, maxHeight: 90 }}
        style={{ maxWidth: 1000, maxHeight: 90 }}
        position="siteBottomFixed"
      />

      <div className="flex flex-col h-full">
        <Header links={headerLinks}/>
        <div className="flex-1 pb-24">{children}</div>
        <Footer links={footerLinks}/>

        {/* placeholder for fixed banner so footer is still visible */}
        <div>
          <div className="bg-gray-800 h-[25px] sm:h-[90px]"/>
        </div>
      </div>

    </RootComponent>
  );
};

export default MainLayout;
