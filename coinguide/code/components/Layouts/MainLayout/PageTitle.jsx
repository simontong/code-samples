import React from 'react';
import isEqual from 'react-fast-compare';

const PageTitle = React.memo(
  ({ children }) => <div className="pb-2 text-3xl text-center font-bold">{children}</div>,
  isEqual,
);

export default PageTitle;
