import React, { useEffect, useState } from 'react';
import useSWR from 'swr';
import css from '../../../util/css';
import useDebounce from '../../../hooks/useDebounce';
import isEqual from 'react-fast-compare';
import ListingLogo from '../../Listing/ListingLogo';
import ky from 'ky';
import { FaSearch } from 'react-icons/fa';
import { CgSpinner } from 'react-icons/cg';
import { IoIosCloseCircle } from 'react-icons/io';
import { Inertia } from '@inertiajs/inertia';

/**
 * loading icon
 */
const LoadingIcon = React.memo(
  ({ isLoading }) => (
    <div className="absolute left-3 flex items-center pointer-events-none text-gray-500">
      {!isLoading ? <FaSearch /> : <CgSpinner size={18} className="animate-spin" />}
    </div>
  ),
  isEqual,
);

/**
 * clear button
 */
const ClearButton = React.memo(
  ({ className, ...props }) => (
    <div
      className={css(
        'absolute flex items-center right-2 text-gray-500 hover:text-white cursor-pointer',
        className,
      )}
      {...props}
    >
      <IoIosCloseCircle />
    </div>
  ),
  isEqual,
);

/**
 * Search input
 */
const SearchInput = React.memo(({ isFocused, isResultsOpen, ...props }) => (
  <input
    // type="text"
    // ref={inputRef}
    className={css(
      'w-full px-9 py-2 bg-gray-900 outline-none rounded',
      !isFocused
        ? 'shadow-inner cursor-pointer border-transparent hover:bg-gray-900 hover:bg-opacity-60'
        : 'shadow-none border border-gray-700',
      isResultsOpen && 'rounded-b-none border-b-transparent',
    )}
    {...props}
  />
));

/**
 * search result message
 */
const SearchResultsMessage = React.memo(({ text }) => {
  return <div className="p-10 text-center font-bold">{text}</div>;
}, isEqual);

/**
 * post verification or init
 */
const fetchSearchResults = (url) => {
  const opts = {
    throwHttpErrors: false,
    headers: {
      'accept': 'application/json',
      // 'x-xsrf-token': Cookies.get('XSRF-TOKEN'),
      'x-requested-with': 'XMLHttpRequest',
    },
  };

  return ky.get(url, opts);
};

/**
 * Search results
 */
const SearchResults = React.memo(
  ({ keywords, selectedIndex, onClickItem, setIsLoading, setItems }) => {
    const keywordsTrimmed = keywords.trim();

    const debouncedKeywords = useDebounce(keywordsTrimmed, 500);
    const url = `/listing/search?keywords=${debouncedKeywords}`;
    const { data, error } = useSWR(url, () => {
      return !keywordsTrimmed ? null : fetchSearchResults(url).then((res) => res.json());
    });
    const isLoading =
      keywordsTrimmed && ((!data && !error) || keywordsTrimmed !== debouncedKeywords);

    useEffect(() => {
      setIsLoading(isLoading);
    }, [isLoading]);

    useEffect(() => {
      if (data) {
        setItems(data.listings);
      }
    }, [data]);

    // no keywords? then go no further
    if (!keywordsTrimmed) {
      return <SearchResultsMessage text="Search for coins" />;
    }

    if (isLoading) {
      return <SearchResultsMessage text="Searching for coins ..." />;
    }

    if (error) {
      return <SearchResultsMessage text="Something went wrong!" />;
    }

    if (data.listings.length === 0) {
      return <SearchResultsMessage text="No coins found" />;
    }

    return data.listings.map((item, key) => (
      <div
        key={key}
        onClick={onClickItem(item)}
        className={css(
          'flex items-center text-white hover:text-white py-3 px-3',
          selectedIndex === key ? 'bg-gray-800' : 'hover:bg-gray-800 hover:bg-opacity-40',
        )}
      >
        <ListingLogo variant="sm" logo={item.logo} alt={item.name} className="w-8 h-8" />

        <span className="pl-2 flex flex-col">
          <span className="">
            <span>{item.name}</span>
            <span className="inline-block uppercase ml-2 px-1 bg-gray-800 rounded">
              {item.symbol}
            </span>
          </span>
        </span>
      </div>
    ));
  },
  isEqual,
);

const Search = () => {
  const [isFocused, setIsFocused] = useState(false);
  const [keywords, setKeywords] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isResultsOpen, setIsResultsOpen] = useState(false);
  const [items, setItems] = useState([]);
  const [selectedIndex, setSelectedIndex] = useState(0);
  const formRef = React.useRef();
  const resultsRef = React.useRef();

  useEffect(() => {
    const onClickOutside = (e) => {
      if (formRef.current && !formRef.current.contains(e.target)) {
        // click outside
        setIsResultsOpen(false);
        setIsFocused(false);
      }
    };

    if (isResultsOpen) {
      document.addEventListener('mouseup', onClickOutside);
    } else {
      document.removeEventListener('mouseup', onClickOutside);
    }

    return () => {
      document.removeEventListener('mouseup', onClickOutside);
    };
  }, [isResultsOpen]);

  /**
   * on input focus
   */
  const onFocus = () => {
    setIsFocused(true);
    setIsResultsOpen(true);
  };

  /**
   * on input change
   */
  const onChange = (e) => {
    setKeywords(e.target.value);
  };

  /**
   * set scroll pos for results
   */
  const setScrollPos = (index) => {
    if (!resultsRef.current) {
      return;
    }

    const selectedItem = resultsRef.current.children[index];
    const resHeight = resultsRef.current.offsetHeight;
    const resTop = resultsRef.current.scrollTop;
    const resBottom = resHeight + resTop;
    // @ts-ignore
    const elTop = selectedItem.offsetTop;
    // @ts-ignore
    const elBottom = selectedItem.offsetHeight + elTop;

    if (elBottom > resBottom) {
      resultsRef.current.scrollTop = elBottom - resHeight;
      return;
    }

    if (elTop < resTop) {
      resultsRef.current.scrollTop = elTop;
    }
  };

  /**
   * on input key up
   */
  const onKeyDown = (e) => {
    setIsFocused(true);
    setIsResultsOpen(true);

    // move up an item
    if (e.key === 'ArrowUp' || (e.shiftKey && e.key === 'Tab')) {
      e.preventDefault();

      let index = Number(selectedIndex) - 1;
      if (index < 0) {
        index = items.length - 1;
      }

      setScrollPos(index);
      setSelectedIndex(index);

      return;
    }

    // move down an item
    if (e.key === 'ArrowDown' || (!e.shiftKey && e.key === 'Tab')) {
      e.preventDefault();

      let index = selectedIndex === null ? 0 : selectedIndex + 1;
      if (index >= items.length) index = 0;

      if (!isResultsOpen) {
        index = 0;
      }

      setScrollPos(index);
      setSelectedIndex(index);

      return;
    }

    // on enter key pressed
    if (e.key === 'Enter') {
      e.preventDefault();
      const selectedItem = resultsRef.current?.children[selectedIndex];
      if (selectedItem) {
        // @ts-ignore
        selectedItem.click();
      }
      return;
    }

    // close search on escape
    if (e.key === 'Escape') {
      e.preventDefault();
      setIsResultsOpen(false);
      setIsFocused(false);
      setKeywords('');
      e.target.blur();
    }
  };

  /**
   * on click selected item
   */
  const onClickItem = (item) => () => {
    setIsResultsOpen(false);
    setIsFocused(false);
    // setKeywords('');
    Inertia.visit(`/coins/${item.slug}`);
  };

  const onClickClearButton = () => {
    setIsResultsOpen(false);
    setIsFocused(false);
    setKeywords('');
  };

  return (
    <form
      ref={formRef}
      className={css(
        'flex items-center w-full h-full',
        !isFocused ? 'relative sm:max-w-[200px]' : 'sm:max-w-[400px]',
      )}
    >
      <div
        className={css(
          isFocused && 'absolute left-2 right-2 w-auto',
          'sm:relative sm:left-auto sm:right-auto w-full',
        )}
      >
        <div className={css('relative w-full flex items-center')}>
          {/* loading icon */}
          <LoadingIcon isLoading={isLoading} />

          {/* input field */}
          <SearchInput
            onFocus={onFocus}
            onChange={onChange}
            onKeyDown={onKeyDown}
            value={keywords}
            isFocused={isFocused}
            isResultsOpen={isResultsOpen}
          />

          {/* close button */}
          {keywords && <ClearButton onClick={onClickClearButton} />}
        </div>

        {/* search results */}
        {isResultsOpen && (
          <div
            ref={resultsRef}
            className="absolute z-30 w-full mt-3 mt-0 overflow-auto max-h-80 border border-t-0 border-gray-700 bg-gray-900 shadow-lg rounded-b scrollbar scrollbar-thin scrollbar-thumb-gray-700 scrollbar-track-gray-800 scrollbar-track-rounded scrollbar-thumb-rounded"
          >
            <SearchResults
              keywords={keywords}
              selectedIndex={selectedIndex}
              setIsLoading={setIsLoading}
              onClickItem={onClickItem}
              setItems={setItems}
            />
          </div>
        )}
      </div>
    </form>
  );
};

export default Search;
