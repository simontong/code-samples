import React from 'react';
import PageTitle from './PageTitle';
import BlankContainer from './BlankContainer';

const ContentContainer = ({ title = '', children }) => {
  // prettier-ignore
  return (
    <BlankContainer>
      {title && <PageTitle>{title}</PageTitle>}

      <div className="px-4 py-4 sm:px-9 sm:py-7 rounded bg-gray-800 shadow-lg">
        {children}
      </div>
    </BlankContainer>
  );
};

export default ContentContainer;
