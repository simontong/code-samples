import React from 'react';
import { ChatWidget } from '@papercups-io/chat-widget';
import config from '../../util/config';

/**
 * papercups script
 */
const Papercups = ({ ...props }) => {
  const { token, inbox, baseUrl } = config('papercups');

  return (
    <ChatWidget
      // `accountId` is used instead of `token` in older versions
      // of the @papercups-io/chat-widget package (before v1.2.x).
      // You can delete this line if you are on the latest version.
      // accountId="fb014e71-bee8-400a-a160-820aa73ee484"
      token={token}
      inbox={inbox}
      title="Welcome to Coin.Guide"
      subtitle="Ask us anything in the chat window below :)"
      primaryColor="#1890ff"
      newMessagePlaceholder="Start typing..."
      showAgentAvailability={false}
      agentAvailableText="We're online right now!"
      agentUnavailableText="We're away at the moment."
      requireEmailUpfront={false}
      iconVariant="filled"
      baseUrl={baseUrl}
      // Optionally include data about your customer here to identify them
      // customer={{
      //   name: __CUSTOMER__.name,
      //   email: __CUSTOMER__.email,
      //   external_id: __CUSTOMER__.id,
      //   metadata: {
      //     plan: "premium"
      //   }
      // }}
      {...props}
    />
  );
};

export default Papercups;
