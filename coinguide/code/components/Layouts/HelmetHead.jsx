import React from 'react';
import { Helmet } from 'react-helmet-async';
import { defaultsDeep } from 'lodash';
import { absoluteUrl, assetUrl } from '../../util/url';
import isEqual from 'react-fast-compare';
import config from '../../util/config';

const HelmetHead = React.memo(({ noAppTitle, title, meta }) => {
  const { appTitle } = config();

  if (!noAppTitle) {
    title = title ? `${title} - ${appTitle}` : `Welcome to ${appTitle}`;
  }

  const description =
    'Coin.Guide gives you the latest information and news to stay on top of a fast paced industry';

  const metaMerged = defaultsDeep(meta, {
    'description': description,

    // facebook og
    'og:title': `Welcome to ${appTitle}`,
    'og:site_name': appTitle,
    'og:image': assetUrl('/favicon/apple-touch-icon.png'),
    'og:description': description,
    'og:locale': 'en_US',
    'og:type': 'website',
    'og:url': absoluteUrl(),

    // twitter
    'twitter:site': '@the_coin_guide',
    'twitter:card': 'summary_large_image',
  });

  return (
    <Helmet>
      <title>{title}</title>

      {/* known meta tags */}
      {Object.entries(metaMerged).map(([name, content]) => (
        <meta key={name} name={name} content={content} />
      ))}
    </Helmet>
  );
}, isEqual);

export default HelmetHead;
