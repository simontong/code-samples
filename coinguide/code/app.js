import React from 'react';
import { render } from 'react-dom';
import { createInertiaApp } from '@inertiajs/inertia-react';
import { InertiaProgress } from '@inertiajs/progress';
import MainLayout from './components/Layouts/MainLayout/MainLayout';

import '../code.0/resources/assets/css/app.css';

InertiaProgress.init({
  showSpinner: true,
});

createInertiaApp({
  resolve: async (name) => {
    const { default: page } = await import(`./pages/${name}`);
    if (!page.layout) {
      page.layout = (page) => <MainLayout>{page}</MainLayout>;
    }
    return page;
  },
  setup: ({ el, App, props }) => render(<App {...props} />, el),
}).then();