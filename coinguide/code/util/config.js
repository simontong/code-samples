import { get } from 'lodash';

export default function config(key, defaultValue) {
  const coinguideConfig = window.coinguideConfig;

  if (!coinguideConfig) {
    throw new Error('Missing window.coinguideConfig');
  }

  return !key ? coinguideConfig : get(coinguideConfig, key, defaultValue);
}
