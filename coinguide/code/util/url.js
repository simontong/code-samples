import config from './config';
import { LISTING_LOGO_VARIANTS } from '../../code.0/isomorphic/consts/listingConsts';

/**
 * get listing logo
 * @param variant
 * @param logo
 * @returns {URL}
 */
export const listingLogoUrl = (logo, variant) => {
  const [suffix] = LISTING_LOGO_VARIANTS[variant] || [];
  const [, name, ext] = logo.match(/(.+)\.(.+)$/);
  const filename = `${name + (suffix ? `-${suffix}` : '')}.${ext}`;
  // const filename = logo;

  return assetUrl(`/listings/${filename}`);
};

/**
 * asset url
 */
export const assetUrl = (pathname) => {
  const { assetBaseUrl } = config();
  if (!assetBaseUrl) {
    return absoluteUrl({ pathname: pathname ?? '', search: '' });
  }

  const url = new URL(assetBaseUrl);
  if (pathname) {
    url.pathname = pathname;
  }

  return url;
};

/**
 * absolute url
 */
export const absoluteUrl = ({ pathname, search } = {}) => {
  const url = new URL(location.href);

  if (typeof search !== 'undefined') {
    url.search = search;
  }

  if (typeof pathname !== 'undefined') {
    url.pathname = pathname;
  }

  return url;
};