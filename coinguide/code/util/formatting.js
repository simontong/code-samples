/**
 * format percent
 */
export function formatPercent(amount, minDecimal = 2, maxDecimal = amount > 1 ? 3 : 20) {
  const formatter = new Intl.NumberFormat('en-US', {
    minimumFractionDigits: minDecimal,
    maximumFractionDigits: maxDecimal,
  });

  return formatter.format(amount);
}

/**
 * format currency
 */
export function formatCurrency(amount, minDecimal = 2, maxDecimal = amount > 1 ? 3 : 20) {
  const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: minDecimal,
    maximumFractionDigits: maxDecimal,
  });

  return formatter.format(amount);
}

/**
 * format number
 */
export function formatNumber(value, minDecimal = 0, maxDecimals = 2) {
  const formatter = new Intl.NumberFormat('en-US', {
    minimumFractionDigits: minDecimal,
    maximumFractionDigits: maxDecimals,
  });

  return formatter.format(value);
}

/**
 * abbreviate number
 */
export function abbreviateNumber(number) {
  const SI_SYMBOL = ['', 'k', 'M', 'G', 'T', 'P', 'E'];

  // what tier? (determines SI symbol)
  const tier = (Math.log10(Math.abs(number)) / 3) | 0;

  // if zero, we don't need a suffix
  if (tier === 0) {
    return number;
  }

  // get suffix and determine scale
  const suffix = SI_SYMBOL[tier];
  const scale = Math.pow(10, tier * 3);

  // scale the number
  const scaled = number / scale;

  // format number and add suffix
  return scaled.toFixed(1) + suffix;
}
