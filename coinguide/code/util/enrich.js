import { get } from 'lodash';

// export function hasSource(listing, source) {
//   return !!listing.listingEnriches.find((i) => i.source === source);
// }

export const createEnrich = (source, listing) => {
  const listingEnrich = listing.listingEnriches.find((i) => i.source === source);

  return (key = null, defaultValue) => {
    return key ? get(listingEnrich, `data.${key}`, defaultValue) : listingEnrich;
  };
};
