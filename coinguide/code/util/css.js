export default function css(...classNames) {
  return classNames.filter((i) => typeof i === 'string' && !!i.trim()).join(' ');
}
