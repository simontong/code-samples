// Courtesy of https://ricostacruz.com/til/state-management-with-react-hooks
import React, { useContext, useState, useMemo } from 'react';

const initialState = {
  appTitle: '',

  votes: [],

  cartItems: [],
};

// @ts-ignore
const AppContext = React.createContext({});

const useAppState = () => {
  // Manage the state using React.useState()
  const [state, setState] = useState(initialState);

  // Build our actions. We'll use useMemo() as an optimization,
  // so this will only ever be called once.
  const actions = useMemo(() => getActions(setState), [setState]);

  const selectors = useMemo(() => getSelectors(state), [state]);

  return { selectors, state, actions };
};

// Sub-components can use this function. It will pick up the
// `state` and `actions` given by useAppState() higher in the
// component tree.
const useAppContext = () => {
  return useContext(AppContext);
};

/**
 * state selectors
 */
const getSelectors = (state) => ({
  votes: {
    exists(slug) {
      return !!state.votes.find((i) => i.listing.slug === slug);
    },
  },

  cart: {
    get lineItems() {
      return state.cartItems;
    },

    get subTotal() {
      return state.cartItems.reduce((o, i) => o + i.price, 0);
    },

    get discount() {
      const percentOff = 0;
      return this.subTotal * (percentOff * 0.01);
    },

    get total() {
      return this.subTotal - this.discount;
    },
  },
});

/**
 * state mutators
 */
const getActions = (setState) => ({
  votes: {
    set: (votes) => {
      setState((state) => ({ ...state, votes }));
    },

    add: (vote) => {
      setState((state) => ({
        ...state,
        votes: state.votes.concat(vote),
      }));
    },
  },

  cart: {
    add: (item) => {
      setState((state) => {
        const exists = state.cartItems.find((i) => i.id === item.id);

        if (exists) {
          console.error('Cannot add to cart. Line item ID already exists: %s', item.id);
          return state;
        }

        const requiredProps = ['id', 'price'];
        for (const requiredProp of requiredProps) {
          if (!item[requiredProp]) {
            console.error('Cannot add to cart. Line item missing %s', requiredProp);
            return state;
          }
        }

        const cartItems = state.cartItems.concat(item);
        return {
          ...state,
          cartItems,
        };
      });
    },

    remove: (id) => {
      setState((state) => {
        const exists = state.cartItems.find((i) => i.id === id);

        if (!exists) {
          console.error('Cannot add to cart. Line item ID does not exist: %s', id);
          return state;
        }

        const cartItems = state.cartItems.filter((i) => i.id !== id);
        return {
          ...state,
          cartItems,
        };
      });
    },
  },
});

export { AppContext, useAppState, useAppContext };
