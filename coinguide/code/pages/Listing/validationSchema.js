import { object } from 'yup';
import { listingRules, ruleParams } from '../../../code.0/isomorphic/rules/listingRules';

/**
 * validate logo url
 */
const validateLogo = async (file) => {
  const { maxFilesize, allowTypes, maxDimensions } = ruleParams.logo;
  const dimensions = maxDimensions.join('x');
  const img = new Image();
  const objectUrl = URL.createObjectURL(file);

  // check file type
  if (!allowTypes.includes(file.type)) {
    const filetypes = allowTypes.map((i) => i.replace(/^.+?\/(.+)$/, '$1')).join(', ');
    return [null, `\${path} must be of type: ${filetypes}`];
  }

  // check file size
  if (file.size >= maxFilesize) {
    return [null, `\${path} file size must be less than ${Math.round(maxFilesize / 1024)}kb`];
  }

  // wait for image to load
  try {
    await new Promise((resolve, reject) => {
      img.onerror = () => reject(`\${path} failed to process`);
      img.onload = resolve;
      img.src = objectUrl;
    });
  } catch (err) {
    return [null, err];
  }

  // check if image is square
  if (img.width !== img.height) {
    return [null, `\${path} must be square with max dimensions of ${dimensions}`];
  }

  // check if image too big
  if (img.width > maxDimensions[0] || img.height > maxDimensions[1]) {
    return [null, `\${path} dimensions must be smaller than ${dimensions}`];
  }

  return [objectUrl, null];
};

/**
 * validation schema
 */
const validationSchema = (platforms) => {
  // prettier-ignore
  return object({
    logo: listingRules.logo
      .test('image', '', async function (value) {
        const file = value instanceof FileList ? value.item(0) : value;

        if (!file) {
          return true;
        }

        const [, error] = await validateLogo(file);

        // prettier-ignore
        return error
          ? this.createError({ message: error })
          : true;
      }),

    name: listingRules.name,

    symbol: listingRules.symbol,

    platform: listingRules.platform(platforms),

    is_presale: listingRules.is_presale,

    contract_address: listingRules.contract_address,

    description: listingRules.description,

    launched_at: object({
      date: listingRules.launched_at.date,
      time: listingRules.launched_at.time
    }),

    urls: object({
      homepage: listingRules.urls.homepage,
      presale: listingRules.urls.presale,
      twitter: listingRules.urls.twitter,
      reddit: listingRules.urls.reddit,
      telegram: listingRules.urls.telegram,
      discord: listingRules.urls.discord,
      dextools: listingRules.urls.dextools,
      poocoin: listingRules.urls.poocoin,
      bogged: listingRules.urls.bogged,
    }),

    contact: object({
      email: listingRules.contact.email,
      telegram: listingRules.contact.telegram,
    }),

    agree: listingRules.agree,
  });
};

export default validationSchema;