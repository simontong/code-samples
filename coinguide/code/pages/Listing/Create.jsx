import React, { useState } from 'react';
import ContentContainer from '../../components/Layouts/MainLayout/ContentContainer';
import { useForm, FormProvider } from 'react-hook-form';
import InputField from '../../components/Form/InputField';
import FormGroup from '../../components/Form/FormGroup';
import useYupValidationResolver from '../../hooks/useYupValidationResolver';
import validationSchema from './validationSchema';
import { isObjectLike, times } from 'lodash';
import InputRadioGroup from '../../components/Form/InputRadioGroup';
import InputLogoUpload from '../../components/Form/InputLogoUpload';
import InputDatePicker from '../../components/Form/DatePicker/InputDatePicker';
import InputSelectGroup from '../../components/Form/InputSelectGroup';
import InputHtmlTextarea from '../../components/Form/InputHtmlTextarea';
import securityVerification from '../../components/Util/securityVerification';
import { StatusCodes } from 'http-status-codes';
import headsUpNotification from '../../components/Util/headsUpNotification';
import setErrorsFromServer from '../../components/Form/setErrorsFromServer';
import { Inertia } from '@inertiajs/inertia';
import { ruleParams } from '../../../code.0/isomorphic/rules/listingRules';
import InputCheckbox from '../../components/Form/InputCheckbox';
import InputCheckboxGroup from '../../components/Form/InputCheckboxGroup';
import { useAppContext } from '../../context/AppContext';
import { BsGlobe2 } from 'react-icons/bs';
import {
  FaChartLine,
  FaDiscord,
  FaEnvelope,
  FaPoo,
  FaReddit,
  FaTelegram,
  FaTwitter,
} from 'react-icons/fa';
import HelmetHead from '../../components/Layouts/HelmetHead';
import { listingLogoUrl } from '../../util/url';
import SubmitButton from '../../components/Form/SubmitButton';

/**
 * initial values
 */
const defaultValues = {
  logo: null,
  name: '',
  symbol: '',
  platform: 'BSC',
  status: 'new',
  contract_address: '',
  description: '',
  launched_at: {
    date: '',
    time: ['00', '00'],
  },
  urls: {
    presale: '',
    bogged: '',
    dextools: '',
    discord: '',
    homepage: '',
    poocoin: '',
    reddit: '',
    telegram: '',
    twitter: '',
  },
  contact: {
    email: '',
    telegram: '',
  },
  agree: false,
};

if (new URLSearchParams(location.search).get('fill')) {
  Object.assign(defaultValues, {
    logo: null,
    name: 'FEG Token',
    symbol: 'FEG',
    platform: 'ETH',
    is_presale: false,
    contract_address: '',
    // contract_address: '0x389999216860ab8e0175387a0c90e5c52522c945',
    description: `FEG Token is redesigning the way decentralized finance (DeFi) is structured. FEG Token is focused on reshaping and evolving how DeFi works completely while delivering the most robust ecosystem ever designed for DeFi's much-needed advancements.
  FEG Token is a hyper-deflationary DeFi token built to succeed! The main idea behind FEG Token is to provide a decentralized transaction network, which operates on the Ethereum blockchain (ERC-20) and the Binance Smart Chain (BEP-20). The path forward for FEG Token is determined by market fluctuations, but the model it runs on begs FEG Token to succeed. FEG Token is a hyper-deflationary token with a maximum circulating supply of 100Q (quadrillion) on both the Ethereum blockchain and the Binance Smart Chain that involves an inaccessible burn wallet. Depending on each holder’s percentage of ownership, a 2% transaction tax is distributed among all holders—this includes the burn wallet, which is essentially a holder that collects more tokens over time as transactions occur. As the circulating supply decreases, the scarcity of the token increases. This inversely proportional relationship constitutes a supply and demand model. There is no limit as to how many tokens can be burnt. Without a burning limit, holders get to enjoy a never-ending cycle of passive income with positive price pressure!`,
    launched_at: {
      date: '2021-02-01',
      time: ['13', '15'],
    },
    urls: {
      presale: '',
      bogged: 'https://charts.bogged.finance/?token=0x389999216860ab8e0175387a0c90e5c52522c945',
      dextools: '',
      poocoin: 'https://poocoin.app/tokens/0x389999216860ab8e0175387a0c90e5c52522c945',
      chart: 'https://poocoin.app/tokens/0xbb4cdb9cbd36b01bd1cbaebf2de08d9173bc095c',
      discord: 'https://discord.gg/fegtoken',
      homepage: 'https://fegtoken.com/',
      reddit: 'https://www.reddit.com/',
      telegram: 'https://t.me/fegchat',
      twitter: 'https://twitter.com/FEGtoken',
    },
    contact: {
      email: 'admin@fegtoken.com',
      telegram: '@fegchat',
    },
    agree: true,
  });
}

/**
 * launch time values
 */
const launchTimeValues = [
  times(24, (n) => n.toString().padStart(2, '0')),
  times(60, (n) => n.toString().padStart(2, '0')),
];

/**
 * url fields
 */
const urlFields = [
  {
    icon: <BsGlobe2 />,
    name: 'urls.homepage',
    label: 'Homepage Link',
    placeholder: 'eg. https://www.binance.org/',
  },
  {
    icon: <FaTwitter />,
    name: 'urls.twitter',
    label: 'Twitter Link (optional)',
    placeholder: 'eg https://twitter.com/binance_dex',
  },
  {
    icon: <FaReddit />,
    name: 'urls.reddit',
    label: 'Reddit Link (optional)',
    placeholder: 'eg. https://www.reddit.com/r/binanceexchange',
  },
  {
    icon: <FaTelegram />,
    name: 'urls.telegram',
    label: 'Telegram Link (optional)',
    placeholder: 'eg. https://t.me/BinanceDEXchange',
  },
  {
    icon: <FaDiscord />,
    name: 'urls.discord',
    label: 'Discord Link (optional)',
    placeholder: 'eg. https://discord.gg/abcdef',
  },
  {
    icon: <FaChartLine />,
    name: 'urls.dextools',
    label: 'Dextools Link (optional)',
    placeholder: 'eg. https://www.dextools.io/app/ether/pair-explorer/0xa29fe6ef ...',
  },
  {
    icon: <FaPoo />,
    name: 'urls.poocoin',
    label: 'Poocoin Link (optional)',
    placeholder: 'eg. https://poocoin.app/tokens/0xbb4cdb9c6b ...',
  },
  {
    icon: <FaChartLine />,
    name: 'urls.bogged',
    label: 'Bogged Finance Link (optional)',
    placeholder: 'eg. https://charts.bogged.finance/0xB09FE161 ...',
  },
];

/**
 * create form
 * @constructor
 */
const Create = ({ platforms }) => {
  const { actions } = useAppContext();
  const methods = useForm({
    resolver: useYupValidationResolver(validationSchema(platforms)),
    mode: 'onChange',
    reValidateMode: 'onChange',
    defaultValues,
  });
  const {
    handleSubmit,
    watch,
    formState: { errors },
  } = methods;
  const [isLoading, setIsLoading] = useState(false);

  const platformValues = React.useMemo(
    () =>
      platforms.map((i) => ({
        value: i.shortname,
        label: i.name + ` (${i.shortname})`,
      })),
    [platforms],
  );

  /**
   * submit form
   */
  const onSubmit = async (input) => {
    setIsLoading(true);

    const body = new FormData();
    const { logo, ...fields } = input;

    body.append('logo', logo);
    for (const [name, value] of Object.entries(fields)) {
      body.append(name, isObjectLike(value) ? JSON.stringify(value) : value);
    }

    const res = await securityVerification('/listing/store', { body })
      .catch((err) => console.error(err))
      .finally(() => {
        setIsLoading(false);
      });

    // user clicked close on security
    if (res === false) {
      return;
    }

    const data = await res.json();

    // validation error
    if (res.status === StatusCodes.UNPROCESSABLE_ENTITY) {
      setErrorsFromServer(methods, data);
      return;
    }

    // todo check voting on frontend
    const { listingVote, listing } = data;

    // add new vote to session
    actions.votes.add(listingVote);

    // success
    headsUpNotification({
      icon: listingLogoUrl(listing.logo, 'sm'),
      // icon: assetUrl(`/listings/${listing.logo}`),
      title: 'New coin listing created!',
      message: `${listing.name} coin was created successfully`,
    });

    // redirect to page
    Inertia.visit(`/coins/${listing.slug}`);
  };

  return (
    <>
      <HelmetHead title="Submit a New Coin" />

      {/*<pre>{JSON.stringify(errors)}</pre>*/}

      <ContentContainer title="Submit a New Coin">
        <FormProvider {...methods}>
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="flex flex-col flex-wrap sm:flex-row items-center sm:items-start sm:space-x-10">
              {/* field: logo */}
              <FormGroup name="logo" className="text-center">
                <InputLogoUpload
                  name="logo"
                  label="Logo Upload"
                  accept={ruleParams.logo.allowTypes.join(',')}
                  wrapProps={{ style: { width: 200 } }}
                  innerWrapProps={{ style: { width: 200, height: 200 } }}
                  tip="Must be square and max 1000 x 1000"
                />
              </FormGroup>

              <div className="mt-6 sm:mt-0 flex-1 flex w-full">
                <div className="space-y-6 flex flex-col flex-1">
                  {/* field: name */}
                  <FormGroup name="name">
                    <InputField name="name" label="Name" placeholder="eg. Wrapped BNB" />
                  </FormGroup>

                  {/* field: symbol */}
                  <FormGroup name="symbol">
                    <InputField name="symbol" label="Symbol" placeholder="eg. WBNB" />
                  </FormGroup>

                  {/* field: platform */}
                  <FormGroup name="platform">
                    <InputRadioGroup name="platform" label="Platform" options={platformValues} />
                  </FormGroup>

                  {/* field: is_presale */}
                  <FormGroup className="flex-1" name="is_presale">
                    <InputCheckboxGroup
                      label="Presale"
                      checkboxes={[
                        {
                          name: 'is_presale',
                          label: 'This is a presale (contract address hidden until Launch Date)',
                        },
                      ]}
                    />
                  </FormGroup>

                  {/* presale link */}
                  {watch('is_presale') && (
                    <FormGroup name="urls.presale">
                      <InputField
                        name="urls.presale"
                        label="Presale Link (optional)"
                        placeholder="eg. https://dxsale.app/app/pages/defipresale?saleID=XXXX&chain=BSC"
                      />
                    </FormGroup>
                  )}

                  {/* field: contract address */}
                  <FormGroup name="contract_address">
                    <InputField
                      name="contract_address"
                      label={`${watch('platform')} Contract Address (optional)`}
                      placeholder="eg. 0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c"
                      tip={
                        watch('is_presale') && 'Contract address will be hidden until Launch Date'
                      }
                    />
                  </FormGroup>

                  {/* field: launch date + time */}
                  <div className="space-x-3 flex items-start justify-between">
                    {/* field: launch date */}
                    <FormGroup className="flex-1" name="launched_at.date">
                      <InputDatePicker name="launched_at.date" label="Launch Date" />
                    </FormGroup>

                    {/* field: launch time */}
                    <FormGroup className="flex-1" name="launched_at.time">
                      <InputSelectGroup
                        name="launched_at.time"
                        label="Launch Time"
                        tip="Timezone is UTC"
                        groupedOptions={launchTimeValues}
                      />
                    </FormGroup>
                  </div>

                  {/* field: description */}
                  <FormGroup name="description">
                    <InputHtmlTextarea
                      name="description"
                      label="Description"
                      // style={{ minHeight: 115 }}
                      tip={`Min: ${ruleParams.description.min}. Max: ${ruleParams.description.max}`}
                    />
                  </FormGroup>

                  {/* fields: urls */}
                  {urlFields.map((item, key) => (
                    <FormGroup key={key} name={item.name}>
                      <InputField {...item} />
                    </FormGroup>
                  ))}

                  {/* field: contacts */}
                  <div className="space-x-3 flex items-end justify-between">
                    {/* field: admin email */}
                    <FormGroup className="flex-1" name="contact.email">
                      <InputField
                        icon={<FaEnvelope />}
                        name="contact.email"
                        label="Admin Email (optional)"
                        placeholder="eg. john@mytoken.com"
                      />
                    </FormGroup>

                    {/* field: admin telegram */}
                    <FormGroup className="flex-1" name="contact.telegram">
                      <InputField
                        icon={<FaTelegram />}
                        name="contact.telegram"
                        label="Admin Telegram (optional)"
                        placeholder="eg. @TheCoinGuide or t.me/thecoinguide"
                      />
                    </FormGroup>
                  </div>

                  {/* field: agree */}
                  <FormGroup className="flex-1" name="agree">
                    <InputCheckbox
                      name="agree"
                      label={
                        <>
                          I have read and agree to the{' '}
                          <a href="/terms" target="_blank">
                            Terms and Conditions
                          </a>
                        </>
                      }
                    />
                  </FormGroup>

                  <div className="pt-2 self-center sm:self-start flex items-center space-x-4">
                    <SubmitButton isLoading={isLoading}>Submit Coin</SubmitButton>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </FormProvider>
      </ContentContainer>
    </>
  );
};

export default Create;