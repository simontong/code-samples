import React, { useState } from 'react';
import ListingLogo from '../../components/Listing/ListingLogo';
import ContentContainer from '../../components/Layouts/MainLayout/ContentContainer';
import ListingContractAddress from '../../components/Listing/ListingContractAddress';
import ListingUrls from '../../components/ListingUrls';
import ListingVoteButton from '../../components/Listing/ListingVoteButton';
import isEqual from 'react-fast-compare';
import ListingStat from '../../components/Listing/ListingStat';
import { Link, usePage } from '@inertiajs/inertia-react';
import css from '../../util/css';
import ListingDescription from '../../components/Listing/ListingDescription';
import ListingPriceDifference from '../../components/Listing/ListingPriceDifference';
import ListingAmount from '../../components/Listing/ListingAmount';
import { filter } from 'lodash';
import ListingOverviewStat from '../../components/Listing/ListingOverviewStat';
import ListingRelativeDate from '../../components/Listing/ListingRelativeDate';
import { abbreviateNumber } from '../../util/formatting';
import { DiscussionEmbed } from 'disqus-react';
import HelmetHead from '../../components/Layouts/HelmetHead';
import { assetUrl, absoluteUrl } from '../../util/url';
import config from '../../util/config';
import { createEnrich } from '../../util/enrich';
import ListingSource from '../../components/Listing/ListingSource';
import { LISTING_ENRICH_SOURCES } from '../../../code.0/isomorphic/consts/listingConsts';

/**
 * show listing
 */
const Show = React.memo(({ listing }) => {
  const { url } = usePage();
  const { disqusShortname } = config();
  const [voteCount, setVoteCount] = useState(Number(listing.meta.listingVotesCount));
  const [voteCountToday, setVoteCountToday] = useState(Number(listing.meta.listingVotesCountToday));

  // create enrich getters
  const withCoinGecko = React.useMemo(
    () => createEnrich(LISTING_ENRICH_SOURCES.coinGecko, listing),
    [listing],
  );
  const withCoinMarketCap = React.useMemo(
    () => createEnrich(LISTING_ENRICH_SOURCES.coinMarketCap, listing),
    [listing],
  );

  /**
   * set vote counters when voting
   */
  const setVoteCounters = ({ listingVotesCount, listingVotesCountToday }) => {
    setVoteCount(listingVotesCount);
    setVoteCountToday(listingVotesCountToday);
  };

  const tabs = [
    { id: 'overview', label: 'Overview' },
    // { id: 'discussion', label: 'Discussion' },
  ];

  const defaultTab = 'overview';
  const [, qs] = url.split('?');
  const activeTab = qs ? new URLSearchParams(qs).get('tab') : defaultTab;

  const socialStats = {
    twitterFollowers: withCoinGecko('community_data.twitter_followers'),
    telegramUserCount: withCoinGecko('community_data.telegram_channel_user_count'),
    redditSubscribers: withCoinGecko('community_data.reddit_subscribers'),
    facebookLikes: withCoinGecko('community_data.facebook_likes'),
  };
  const hasSocialStats = filter(socialStats).length > 0;

  return (
    <>
      <HelmetHead
        title={listing.name}
        meta={{
          'description': listing.description?.slice(0, 250),
          'og:title': listing.name,
          'og:image': assetUrl(`/listings/${listing.logo}`),
        }}
      />

      <ContentContainer>
        <div className="space-y-7">
          <div className="flex flex-col sm:flex-row items-center sm:items-start">
            <ListingLogo
              variant="md"
              logo={listing.logo}
              alt={listing.name}
              className="sm:w-24 sm:h-24 w-32 h-32"
            />

            <div className="flex flex-col space-y-2 sm:pl-4 mt-2 sm:mt-0 items-center sm:items-start">
              {/* name and symbol */}
              <div className="flex flex-wrap items-center justify-center space-x-2 text-center">
                <h2>{listing.name}</h2>
                <span className="inline-block px-2 py-1 font-bold bg-gray-900 rounded uppercase">
                  {listing.symbol}
                </span>
              </div>

              {/* contract address */}
              <ListingContractAddress
                address={listing.contract_address}
                isPresaleActive={listing.isPresaleActive}
                platform={listing.platform}
              />

              {/* links */}
              <div>
                <div className="flex flex-wrap justify-center sm:justify-start items-center -mt-2 sm:-ml-1">
                  <ListingSource
                    className="mt-2 mx-1"
                    type="coingecko"
                    slug={withCoinGecko('id')}
                  />
                  <ListingSource
                    className="mt-2 mx-1"
                    type="coinmarketcap"
                    slug={withCoinMarketCap('slug')}
                  />

                  {['homepage', 'presale', 'chat', 'community', 'chart'].map((type, key) => (
                    <ListingUrls key={key} urls={listing.urls} type={type} className="mt-2 mx-1" />
                  ))}
                </div>
              </div>
            </div>
          </div>

          {/* vote button */}
          <div>
            <ListingVoteButton
              slug={listing.slug}
              label="Vote Now"
              onVoteCasted={setVoteCounters}
            />
          </div>

          {/* stats */}
          <div className="py-3 flex flex-col sm:flex-row sm:flex-wrap border-t border-b border-gray-700">
            {/* stats: status */}
            <div className="sm:pr-5 py-2">
              <ListingStat
                title="Status"
                value={listing.isListed ? 'Listed' : 'New'}
                valueCss={listing.isListed ? 'bg-green-800' : 'bg-gray-900'}
              />
            </div>

            {/* stats: votes for listing */}
            <div className="sm:pr-5 py-2">
              <ListingStat
                title="Votes For Listing"
                value={
                  (listing.isListed ? listing.minListingVotes : voteCount) +
                  ' / ' +
                  listing.minListingVotes
                }
                valueCss={listing.isListed ? 'bg-green-800' : 'bg-gray-900'}
              />
            </div>

            {/* stats: total votes */}
            <div className="sm:pr-5 py-2">
              <ListingStat title="Total Votes" value={new Intl.NumberFormat().format(voteCount)} />
            </div>

            {/* stats: 24h votes */}
            <div className="sm:pr-5 py-2">
              <ListingStat
                title="24H Votes"
                value={new Intl.NumberFormat().format(voteCountToday)}
              />
            </div>
          </div>

          {/* tabs */}
          <div>
            <div className="flex items-center justify-center sm:justify-start">
              {tabs.map((tab, key) => (
                <Link
                  key={key}
                  href={`/coins/${listing.slug + (tab.id !== defaultTab ? `?tab=${tab.id}` : '')}`}
                  preserveScroll
                  preserveState
                  className={css(
                    'mr-2 btn btn-md btn-round',
                    activeTab === tab.id ? 'btn-blue' : 'btn-gray',
                  )}
                >
                  {tab.label}
                </Link>
              ))}
            </div>

            <div className="mt-4">
              {/* tab: overview */}
              {activeTab === 'overview' && (
                <div className="flex flex-col sm:flex-row sm:space-x-5">
                  <div className="flex-1">
                    {listing.description && <ListingDescription text={listing.description} />}
                  </div>

                  <hr className="sm:hidden border-gray-700 mt-7" />

                  <div className="sm:min-w-[250px] flex flex-col pt-8 sm:pt-0 sm:pl-5 space-y-6">
                    {/* overview stat: price */}
                    <ListingOverviewStat title="Price">
                      <div className="text-2xl">
                        <ListingAmount
                          isPresaleActive={listing.isPresaleActive}
                          amount={withCoinGecko('market_data.current_price.usd')}
                        />
                      </div>
                      <ListingPriceDifference
                        isPresaleActive={listing.isPresaleActive}
                        percent={withCoinGecko('market_data.price_change_percentage_24h')}
                      />
                    </ListingOverviewStat>

                    {/* overview stat: market cap */}
                    <ListingOverviewStat title="Market Cap">
                      <div className="text-2xl">
                        <ListingAmount
                          isPresaleActive={listing.isPresaleActive}
                          amount={withCoinGecko('market_data.market_cap.usd')}
                        />
                      </div>
                      <ListingPriceDifference
                        isPresaleActive={listing.isPresaleActive}
                        amount={withCoinGecko('market_data.market_cap_change_percentage_24h')}
                      />
                    </ListingOverviewStat>

                    {/* overview stat: 24h vol */}
                    {/*
                    <ListingOverviewStat title="24H Volume">
                      <div className="text-2xl">
                        <ListingPrice
                          isPresaleActive={listing.isPresaleActive}
                          amount={getEnrich(listing, 'market_data.total_volume.usd')}
                        />
                      </div>
                      <ListingPriceDifference
                        isPresaleActive={listing.isPresaleActive}
                        percent={getEnrich(listing, 'market_data.market_cap_change_percentage_24h')}
                      />
                    </ListingOverviewStat>
*/}

                    {/* overview stat: circ supply */}
                    <ListingOverviewStat title="Circulating Supply">
                      <div className="text-2xl">
                        <ListingAmount
                          isPresaleActive={listing.isPresaleActive}
                          amount={withCoinGecko('market_data.circulating_supply')}
                          format="number"
                          hideZero
                        />
                      </div>
                    </ListingOverviewStat>

                    {/* overview stat: total supply */}
                    <ListingOverviewStat title="Total Supply">
                      <div className="text-2xl">
                        <ListingAmount
                          isPresaleActive={listing.isPresaleActive}
                          amount={withCoinGecko('market_data.total_supply')}
                          format="number"
                          hideZero
                        />
                      </div>
                    </ListingOverviewStat>

                    {hasSocialStats && <hr className="border-gray-700" />}

                    {/* overview stat: twitter followers */}
                    {socialStats.twitterFollowers > 0 && (
                      <ListingOverviewStat title="Twitter Followers">
                        <div className="text-2xl">
                          {abbreviateNumber(socialStats.twitterFollowers)}
                        </div>
                      </ListingOverviewStat>
                    )}

                    {/* overview stat: telegram */}
                    {socialStats.telegramUserCount > 0 && (
                      <ListingOverviewStat title="Telegram users">
                        <div className="text-2xl">
                          {abbreviateNumber(socialStats.telegramUserCount)}
                        </div>
                      </ListingOverviewStat>
                    )}

                    {/* overview stat: reddit */}
                    {socialStats.redditSubscribers > 0 && (
                      <ListingOverviewStat title="Reddit Subscribers">
                        <div className="text-2xl">
                          {abbreviateNumber(socialStats.redditSubscribers)}
                        </div>
                      </ListingOverviewStat>
                    )}

                    {/* overview stat: facebook */}
                    {socialStats.facebookLikes > 0 && (
                      <ListingOverviewStat title="Facebook Likes">
                        <div className="text-2xl">
                          {abbreviateNumber(socialStats.facebookLikes)}
                        </div>
                      </ListingOverviewStat>
                    )}

                    <hr className="border-gray-700" />

                    {/* overview stat: added date */}
                    <ListingOverviewStat title="Added">
                      <div className="text-2xl">
                        <ListingRelativeDate date={listing.created_at} />
                      </div>
                    </ListingOverviewStat>

                    {/* overview stat: launched date */}
                    <ListingOverviewStat title="Launch">
                      <div className="text-2xl">
                        <ListingRelativeDate date={listing.launched_at} />
                      </div>
                    </ListingOverviewStat>
                  </div>
                </div>
              )}

              {/* tab: social */}
              {activeTab === 'discussion' && (
                <div>
                  <DiscussionEmbed
                    shortname={disqusShortname}
                    config={{
                      url: absoluteUrl().toString(),
                      identifier: listing.symbol,
                      title: listing.name,
                      // language: ''
                      // url: this.props.article.url,
                      // identifier: this.props.article.id,
                      // title: this.props.article.title,
                      // language: 'zh_TW' //e.g. for Traditional Chinese (Taiwan)
                    }}
                  />
                </div>
              )}
            </div>
          </div>
        </div>
      </ContentContainer>

      <div className="mt-2 text-center">
        <Link href="/contact" className="btn btn-dark-gray text-gray-400 btn-round">
          Coin info need updating?
        </Link>
      </div>
    </>
  );
}, isEqual);

export default Show;