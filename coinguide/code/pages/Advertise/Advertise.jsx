import React from 'react';
import isEqual from 'react-fast-compare';
import HelmetHead from '../../components/Layouts/HelmetHead';
import BlankContainer from '../../components/Layouts/MainLayout/BlankContainer';
import PageTitle from '../../components/Layouts/MainLayout/PageTitle';
import Basket from '../../components/Advertise/Basket/Basket';
import Products from '../../components/Advertise/Products/Products';

/**
 * advertise
 */
const Advertise = React.memo(({ products, promotedCoinOpts, promotedBannerOpts }) => {
  return (
    <>
      <HelmetHead title="Advertise With Us" />

      <BlankContainer width="max-w-screen-lg">
        <div className="flex flex-col sm:flex-row space-y-8 sm:space-x-5 sm:space-y-0">
          {/* shop */}
          <div className="flex-1">
            <PageTitle>Add Products</PageTitle>

            <div className="px-4 py-4 sm:px-9 sm:py-7 rounded bg-gray-800 shadow-lg">
              <Products
                products={products}
                promotedCoinOpts={promotedCoinOpts}
                promotedBannerOpts={promotedBannerOpts}
              />
            </div>
          </div>

          {/* basket */}
          <div className="flex-1">
            <PageTitle>Your Basket</PageTitle>
            <div className="flex flex-col rounded overflow-hidden bg-gray-800 shadow-lg">
              <Basket />
            </div>
          </div>
        </div>
      </BlankContainer>
    </>
  );
}, isEqual);

export default Advertise;
