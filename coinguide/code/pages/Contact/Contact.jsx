import React, { useState } from 'react';
import ContentContainer from '../../components/Layouts/MainLayout/ContentContainer';
import { useForm, FormProvider } from 'react-hook-form';
import InputField from '../../components/Form/InputField';
import FormGroup from '../../components/Form/FormGroup';
import useYupValidationResolver from '../../hooks/useYupValidationResolver';
import validationSchema from './validationSchema';
import InputTextarea from '../../components/Form/InputTextarea';
import securityVerification from '../../components/Util/securityVerification';
import { StatusCodes } from 'http-status-codes';
import headsUpNotification from '../../components/Util/headsUpNotification';
import isEqual from 'react-fast-compare';
import setErrorsFromServer from '../../components/Form/setErrorsFromServer';
import HelmetHead from '../../components/Layouts/HelmetHead';
import SubmitButton from '../../components/Form/SubmitButton';

/**
 * initial values
 */
const defaultValues = {
  // name: 'John Smith',
  // email: 'john@smith.com',
  // message: 'This is a message',

  name: '',
  email: '',
  message: '',
};

/**
 * contact
 */
const Contact = React.memo(() => {
  const methods = useForm({
    resolver: useYupValidationResolver(validationSchema()),
    mode: 'onChange',
    reValidateMode: 'onChange',
    defaultValues,
  });
  const { handleSubmit, reset } = methods;
  const [isLoading, setIsLoading] = useState(false);

  /**
   * submit form
   */
  const onSubmit = async (input) => {
    setIsLoading(true);

    const res = await securityVerification('/contact/send', { json: input })
      .catch((err) => console.error(err))
      .finally(() => {
        setIsLoading(false);
      });

    // user clicked close on security
    if (res === false) {
      return;
    }

    const data = await res.json();

    // validation error
    if (res.status === StatusCodes.UNPROCESSABLE_ENTITY) {
      setErrorsFromServer(methods, data);
      return;
    }

    // success
    headsUpNotification({
      title: 'Message Sent!',
      message: `Thank you. We will be in contact shortly`,
    });
    reset();
  };

  return (
    <>
      <HelmetHead title="Contact Us" />

      <ContentContainer title="Contact Us">
        <FormProvider {...methods}>
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="space-y-6 flex flex-col">
              {/* field: name */}
              <FormGroup name="name">
                <InputField name="name" label="Name" />
              </FormGroup>

              {/* field: email */}
              <FormGroup name="email">
                <InputField name="email" label="Email" />
              </FormGroup>

              {/* field: message */}
              <FormGroup name="message">
                <InputTextarea name="message" label="Message" style={{ minHeight: 115 }} />
              </FormGroup>

              <div className="pt-2 self-center sm:self-start">
                <SubmitButton isLoading={isLoading}>Send Message</SubmitButton>
              </div>
            </div>
          </form>
        </FormProvider>
      </ContentContainer>
    </>
  );
}, isEqual);

export default Contact;
