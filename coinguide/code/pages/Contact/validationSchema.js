import { object } from 'yup';
import { contactRules } from '../../../code.0/isomorphic/rules/contactRules';

const validationSchema = () => {
  return object({
    name: contactRules.name,
    email: contactRules.email,
    message: contactRules.message,
  });
};

export default validationSchema;