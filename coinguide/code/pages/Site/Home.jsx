import React from 'react';
import ListingsTable from '../../components/Listing/ListingsTable/ListingsTable';
import ListingsPagination from '../../components/Listing/ListingsTable/ListingsPagination';
import PageTitle from '../../components/Layouts/MainLayout/PageTitle';
import ListingsFilter from '../../components/Listing/ListingsTable/ListingsFilter';
import BlankContainer from '../../components/Layouts/MainLayout/BlankContainer';
import Banner from '../../components/Banner/Banner';
import HelmetHead from '../../components/Layouts/HelmetHead';

const Home = ({ filter, listings, promotedListings }) => {
  return (
    <>
      <HelmetHead title="Welcome to Coin.Guide" noAppTitle />

      <BlankContainer>
        <div className="space-y-8">
          <Banner
            className="overflow-hidden mx-auto rounded-sm shadow"
            style={{ maxWidth: 970, maxHeight: 90 }}
            // style={{ maxWidth: 1000, maxHeight: 90 }}
            position="homeTop"
          />

          <div>
            <PageTitle>Promoted Coins</PageTitle>
            <ListingsTable items={promotedListings} />
          </div>

          <div className="space-y-3">
            <ListingsFilter filter={filter} />
            <ListingsTable items={listings.data} />
            <ListingsPagination only={['listings']} meta={listings.meta} />
          </div>
        </div>
      </BlankContainer>
    </>
  );
};

export default Home;
