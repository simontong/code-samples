import React from 'react';
import ContentContainer from '../../components/Layouts/MainLayout/ContentContainer';
import { FaEnvelope } from 'react-icons/fa';
import isEqual from 'react-fast-compare';
import HelmetHead from '../../components/Layouts/HelmetHead';
import config from '../../util/config';

const Advertise = React.memo(() => {
  const { appTitle, advertiseEmailTo } = config();

  return (
    <>
      <HelmetHead title="Advertise" />

      <ContentContainer title="Advertise With Us">
        <div className="text-center space-y-9 text-xl">
          <div>
            Advertise on {appTitle} to help promote your brand and gain exposure through {appTitle}
            's website, Twitter and Telegram group.
          </div>

          <div className="space-y-1">
            <div className="pb-1">To promote your project, send us an email at:</div>

            <a
              href={`mailto:${advertiseEmailTo}`}
              className="flex-inline items-center space-x-2 text-cgblue-dark hover:text-cgblue-light"
            >
              <FaEnvelope className="inline-block" />
              <span>{advertiseEmailTo}</span>
            </a>
          </div>

          <div>
            Unless you have received an email directly from us at {appTitle}, do not pay anyone for
            advertising on our website.
          </div>
        </div>
      </ContentContainer>
    </>
  );
}, isEqual);

export default Advertise;
