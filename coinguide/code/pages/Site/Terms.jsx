import React from 'react';
import ContentContainer from '../../components/Layouts/MainLayout/ContentContainer';
import isEqual from 'react-fast-compare';
import { Link } from '@inertiajs/inertia-react';
import config from '../../util/config';
import HelmetHead from '../../components/Layouts/HelmetHead';

const Terms = React.memo(() => {
  const { appTitle } = config();

  return (
    <>
      <HelmetHead title="Terms &amp; Conditions" />

      <ContentContainer title="Terms &amp; Conditions">
        <h2>We reserve the right at any time to:</h2>
        <p>
          Any changes we make to the ToU will be effective immediately after we post the modified
          ToU on {appTitle}.
        </p>
        <p>
          All of the information and other content displayed on, transmitted through, or used in
          connection with the Coin Guide website and its other services, including for example,
          advertising, directories, guides, opinions, reviews, text, photographs, images, html,
          source and object code, software, data, the selection and arrangement of the
          aforementioned and the “look and feel” of the {appTitle} site (collectively, the
          “Content”), are protected under applicable copyrights and other proprietary (including but
          not limited to intellectual property) rights and are the intellectual property of{' '}
          {appTitle} and its affiliated companies, licensors and suppliers.
          {appTitle} actively protects its rights to the Content to the fullest extent of the law.
        </p>
        <p>
          You may use the Content online and solely for your personal, non-commercial use, and you
          may download or print a single copy of any portion of the Content for your personal,
          non-commercial use, provided you do not remove any trademark, copyright or other notice
          contained in such Content. You may not, for example, republish the Content on any
          Internet, Intranet or Extranet site or incorporate the Content in any database,
          compilation, archive or cache or store the Content in electronic form on your computer or
          mobile device unless otherwise expressly permitted by {appTitle}. You may not distribute
          any of the Content to others, whether or not for payment or other consideration, and you
          may not modify, copy, frame, reproduce, sell, publish, transmit, display or otherwise use
          any portion of the Content, except as permitted by the ToU or by securing the prior
          written consent of {appTitle}.
        </p>
        <p>
          The Content includes logotypes, trademarks and service marks (collectively “Marks”) owned
          by {appTitle}, and Marks owned by other information providers and third parties.
        </p>
        <p>
          Requests to use the Content for any purpose other than as permitted in the ToU should be
          submitted via our <Link href="/contact">Contact Us page</Link>.
        </p>
        <p>
          {appTitle} respects the intellectual property of others. If you believe that your work has
          been copied in a way that constitutes copyright infringement or are aware of any
          infringing material placed by any third party on our website, please contact our
          designated copyright agent, in writing, either by email at legal@{appTitle}.net.
        </p>
        <p>
          The information, products and services on the {appTitle} website and its services are
          provided on a strictly “as is,” “where is” and “where available” basis. {appTitle} does
          not provide any warranties (either express or implied) with respect to the information
          provided on any {appTitle} website and its services for any particular purpose. {appTitle}{' '}
          expressly disclaims any implied warranties, including but not limited to, warranties of
          title, non-infringement, merchantability or fitness for a particular purpose. {appTitle}{' '}
          will not be responsible for any loss or damage that could result from interception by
          third parties of any information made available to you via the {appTitle} Sites or any of
          them. Although the information provided to you on this website is obtained or compiled
          from sources we believe to be reliable, {appTitle} cannot and does not guarantee the
          accuracy, validity, timeliness, or completeness of any information or data made available
          to you for any particular purpose. Neither {appTitle}, nor any of its affiliates,
          directors, officers or employees, nor any third party providers of content, software
          and/or technology (collectively, the “{appTitle} parties”), will be liable or have any
          responsibility of any kind for any loss or damage that you incur in the event of any
          failure or interruption of any {appTitle} site, or resulting from the act or omission of
          any other party involved in making any {appTitle} site, the data contained therein or the
          products or services offered thereby available to you, or from any other cause relating to
          your access to, inability to access, or use of any {appTitle} site or the materials
          contained therein, whether or not the circumstances giving rise to such cause may have
          been within the control of {appTitle} or of any vendor providing software or services.
        </p>
        <p>
          {appTitle} strongly recommends that you perform your own independent research and/or speak
          with a qualified investment professional before making any financial decisions.
        </p>
        <p>
          Certain links, including hypertext links, in our site will take you to external websites.
          These are provided for your convenience and inclusion of any link does not imply
          endorsement or approval by {appTitle} of the linked site, its operator or its content.
          Each of those websites have their own “Terms and Conditions.” We are not responsible for
          the content of any website outside of the {appTitle} website and its services. We do not
          monitor and assume no duty to monitor the content of any such third-party websites.
        </p>
        <p>
          Cookies are small text files that are placed on your computer by websites that you visit.
          They are widely used in order to make websites work, or work more efficiently, as well as
          to provide information to the owners of the site.
        </p>
        <p>
          These cookies are used to collect information about how visitors use our site. We use the
          information to compile reports and to help us improve the site. The cookies collect
          information in an anonymous form, including the number of visitors to the site, where
          visitors have come to the site from and the pages they visited.
        </p>
        <p>
          By using our website, you agree that we can place these types of cookies on your device.
        </p>
        <p>Any rights not expressly granted herein are reserved.</p>
      </ContentContainer>
    </>
  );
}, isEqual);

export default Terms;
