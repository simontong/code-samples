import React from 'react';
import ContentContainer from '../../components/Layouts/MainLayout/ContentContainer';
import isEqual from 'react-fast-compare';
import HelmetHead from '../../components/Layouts/HelmetHead';
import config from '../../util/config';

const Disclaimer = React.memo(() => {
  const { appTitle } = config();

  return (
    <>
      <HelmetHead title="Disclaimer" />

      <ContentContainer title="Disclaimer">
        <h2>No Investment Advice</h2>
        <p>
          The information provided on this website does not constitute investment advice, financial
          advice, trading advice, or any other sort of advice and you should not treat any of the
          website's content as such. {appTitle}
          does not recommend that any cryptocurrency should be bought, sold, or held by you. Do
          conduct your own due diligence and consult your financial advisor before making any
          investment decisions.
        </p>

        <h2 className="pt-4">Accuracy of Information</h2>
        <p>
          {appTitle} will strive to ensure accuracy of information listed on this website although
          it will not hold any responsibility for any missing or wrong information. {appTitle}{' '}
          provides all information as is. You understand that you are using any and all information
          available here at your own risk.
        </p>

        <h2 className="pt-4">Non Endorsement</h2>
        <p>
          The appearance of third party advertisements and hyperlinks on {appTitle} does not
          constitute an endorsement, guarantee, warranty, or recommendation by {appTitle}. Do
          conduct your own due diligence before deciding to use any third party services.
        </p>
      </ContentContainer>
    </>
  );
}, isEqual);

export default Disclaimer;
