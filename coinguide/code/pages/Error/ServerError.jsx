import React from 'react';
import isEqual from 'react-fast-compare';
import HelmetHead from '../../components/Layouts/HelmetHead';
import ContentContainer from '../../components/Layouts/MainLayout/ContentContainer';

const NotFound = React.memo(() => {
  return (
    <>
      <HelmetHead title="Something Went Wrong!" />

      <ContentContainer title="Something Went Wrong!">
        <div className="p-10 text-center font-bold">
          Something went wrong
        </div>
      </ContentContainer>
    </>
  );
}, isEqual);

export default NotFound;
