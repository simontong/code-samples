import React from 'react';
import isEqual from 'react-fast-compare';
import HelmetHead from '../../components/Layouts/HelmetHead';
import ContentContainer from '../../components/Layouts/MainLayout/ContentContainer';

const NotFound = React.memo(() => {
  return (
    <>
      <HelmetHead title="Unauthorized" />

      <ContentContainer title="Unauthorized">
        <div className="p-10 text-center font-bold">
          You are not authorized to view this page
        </div>
      </ContentContainer>
    </>
  );
}, isEqual);

export default NotFound;
