import React from 'react';
import isEqual from 'react-fast-compare';
import HelmetHead from '../../components/Layouts/HelmetHead';
import ContentContainer from '../../components/Layouts/MainLayout/ContentContainer';

const NotFound = React.memo(() => {
  return (
    <>
      <HelmetHead title="Page Not Found" />

      <ContentContainer title="Page Not Found">
        <div className="p-10 text-center font-bold">
          Sorry, we couldn't find the page you were looking for.
        </div>
      </ContentContainer>
    </>
  );
}, isEqual);

export default NotFound;
