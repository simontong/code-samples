#### DONATELLO
This is a side project (code name Donatello - favourite ninja turtle when I was younger, in case you were wondering!) -- It is a tool for managing your personal finances and investment portfolios.

I've completed quite a large portion of the API backend that uses Koa (NodeJS library) web server and Postgres. There is pretty decent test coverage and I've written a handful of commands for interacting with the API directly from the command line. It should give you a good idea of my coding style with JavaScript/Node though.

#### COIN.GUIDE
The second example is a platform that informs users about new cryptocurrencies. A user will submit their newly launched crypto project to the platform with basic details and once they get listed on the main exchanges (CoinMarketCap and Coingecko) the platform will automatically detect the listing and fill in more details and poll for current price data (the price is 1 minute from real-time I think) via a separate queue (bull queue JS package).

NOTE: I've only included the frontend in the zip but the backend is using AdonisJS (https://adonisjs.com) coupled with InertiaJS (inertiajs.com) and the frontend is written in React.

The live site for Coin.Guide lives here - https://thecoin.guide 

There's also an advertise form that is quite interesting (its not fully complete hence the "/advertise1" URL) that I'd like you to see - https://thecoin.guide/advertise1

