import { AppError } from '@/src/lib/errors';
import container from '@/src/app/container';
import { makeAuthLoginRequest } from '@/test/utils/requests/authRequest';
import User from '@/src/models/User';
import { StatusCodes } from 'http-status-codes';
import supertest, { SuperAgentTest } from 'supertest';
import app from '@/src/app/app';
import { factoryUser } from '@/database/factory';

type AuthenticatedRequest = {
  user: User;
  request: SuperAgentTest;
};

export default async function createUserAndAuthenticate(confirmEmail = false): Promise<AuthenticatedRequest> {
  const input = await factoryUser.build();
  // use container.cradle.userService.createUser here because we need to hash password
  const user = await container.cradle.userService.createUser(input);

  // confirm email address after creation
  if (confirmEmail) {
    await container.cradle.userService.confirmEmail(user);
  }

  const data = await makeAuthLoginRequest({
    email: input.email,
    password: input.password,
  });
  const request = supertest.agent(app.callback());

  // prettier-ignore
  const res = await request
    .post('/auth/login')
    .send(data);

  // using `expect` here previously was causing issues
  if (res.status !== StatusCodes.OK) {
    const info = { data };
    throw new AppError('Authentication failed').setInfo(info);
  }

  return { user, request };
}
