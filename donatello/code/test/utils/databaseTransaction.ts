import container from '@/src/app/container';
import { Model, Transaction } from 'objection';

let trx: Transaction = null;

export const getDatabaseTransaction = (): Transaction => trx;

export const startDatabaseTransaction = async (): Promise<Transaction> => {
  trx = await new Promise((resolve, reject) => container.cradle.db.transaction(resolve).catch(reject));
  Model.knex(trx);
};

export const rollbackDatabaseTransaction = async (): Promise<void> => {
  await trx.rollback();
  trx = null;
  Model.knex(container.cradle.db);
};

export const commitDatabaseTransaction = async (): Promise<void> => {
  await trx.commit();
  trx = null;
  Model.knex(container.cradle.db);
};
