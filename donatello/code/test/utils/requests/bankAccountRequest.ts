import { factoryBankAccount } from '@/database/factory';
import { BankAccountCreateRequestDto } from '@/src/dtos/BankAccountCreateRequestDto';
import { ankAccountUpdateRequestDto } from '@/src/dtos/BankAccountUpdateRequestDto';
import _ from 'lodash';

// prettier-ignore
export const makeBankAccountCreateRequest = async (attrs?: Partial<BankAccountCreateRequestDto>): Promise<BankAccountCreateRequestDto> => {
  const input = await factoryBankAccount.build();

  const defaults: BankAccountCreateRequestDto = {
    name: input.name,
  };

  return _.defaultsDeep(attrs, defaults);
};

// prettier-ignore
export const makeBankAccountUpdateRequest = async (attrs?: Partial<ankAccountUpdateRequestDto>): Promise<ankAccountUpdateRequestDto> => {
  const input = await factoryBankAccount.build();

  const defaults: ankAccountUpdateRequestDto = {
    name: input.name,
  };

  return _.defaultsDeep(attrs, defaults);
};
