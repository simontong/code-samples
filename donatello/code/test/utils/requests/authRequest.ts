import { AuthConfirmEmailRequestDto } from '@/src/dtos/AuthConfirmEmailRequestDto';
import { AuthLoginRequestDto } from '@/src/dtos/AuthLoginRequestDto';
import { AuthLostPasswordRequestDto } from '@/src/dtos/AuthLostPasswordRequestDto';
import { AuthLostPasswordResetRequestDto } from '@/src/dtos/AuthLostPasswordResetRequestDto';
import { AuthRegisterRequestDto } from '@/src/dtos/AuthRegisterRequestDto';
import * as faker from 'faker';
import _ from 'lodash';
import { factoryUser } from '@/database/factory';

// prettier-ignore
export const makeAuthLoginRequest = async (attrs?: Partial<AuthLoginRequestDto>): Promise<AuthLoginRequestDto> => {
  const input = await factoryUser.build();

  const defaults: AuthLoginRequestDto = {
    email: input.email,
    password: input.password,
  };

  return _.defaultsDeep(attrs, defaults);
};

// prettier-ignore
export const makeAuthRegisterRequest = async (attrs?: Partial<AuthRegisterRequestDto>): Promise<AuthRegisterRequestDto> => {
  const input = await factoryUser.build();

  const defaults: AuthRegisterRequestDto = {
    name: input.name,
    email: input.email,
    password: input.password,
    passwordConfirm: input.password,
  };

  return _.defaultsDeep(attrs, defaults);
};

// prettier-ignore
export const makeAuthConfirmEmailRequest = async (attrs?: Partial<AuthConfirmEmailRequestDto>): Promise<AuthConfirmEmailRequestDto> => {
  const defaults: AuthConfirmEmailRequestDto = {
    token: faker.datatype.uuid(),
  };

  return _.defaultsDeep(attrs, defaults);
};

// prettier-ignore
export const makeAuthLostPasswordRequest = async (attrs?: Partial<AuthLostPasswordRequestDto>): Promise<AuthLostPasswordRequestDto> => {
  const input = await factoryUser.build();

  const defaults: AuthLostPasswordRequestDto = {
    email: input.email,
  };

  return _.defaultsDeep(attrs, defaults);
};

// prettier-ignore
export const makeAuthLostPasswordResetRequest = async (attrs?: Partial<AuthLostPasswordResetRequestDto>): Promise<AuthLostPasswordResetRequestDto> => {
  const input = await factoryUser.build();

  const defaults: AuthLostPasswordResetRequestDto = {
    token: faker.datatype.uuid(),
    password: input.password,
    passwordConfirm: input.password,
  };

  return _.defaultsDeep(attrs, defaults);
};
