import { factoryBankTransaction } from '@/database/factory';
import { BankTransactionCreateRequestDto } from '@/src/dtos/BankTransactionCreateRequestDto';
import { BankTransactionUpdateRequestDto } from '@/src/dtos/BankTransactionUpdateRequestDto';
import _ from 'lodash';

// prettier-ignore
export const makeBankTransactionCreateRequest = async (attrs?: Partial<BankTransactionCreateRequestDto>): Promise<BankTransactionCreateRequestDto> => {
  const input = await factoryBankTransaction.build();

  const defaults: BankTransactionCreateRequestDto = {
    bank_account_id: input.bank_account_id,
    currency_code: input.currency_code,
    date: input.date,
    payee: input.payee,
    amount: input.amount,
    note: input.note,
  };

  return _.defaultsDeep(attrs, defaults);
};

// prettier-ignore
export const makeBankTransactionUpdateRequest = async (attrs?: Partial<BankTransactionUpdateRequestDto>): Promise<BankTransactionUpdateRequestDto> => {
  const input = await factoryBankTransaction.build();

  const defaults: BankTransactionUpdateRequestDto = {
    bank_account_id: input.bank_account_id,
    currency_code: input.currency_code,
    date: input.date,
    payee: input.payee,
    amount: input.amount,
    note: input.note,
  };

  return _.defaultsDeep(attrs, defaults);
};
