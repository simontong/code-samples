import { factoryBankCategory } from '@/database/factory';
import { BankCategoryCreateRequestDto } from '@/src/dtos/BankCategoryCreateRequestDto';
import { BankCategoryUpdateRequestDto } from '@/src/dtos/BankCategoryUpdateRequestDto';
import _ from 'lodash';

// prettier-ignore
export const makeBankCategoryCreateRequest = async (attrs?: Partial<BankCategoryCreateRequestDto>): Promise<BankCategoryCreateRequestDto> => {
  const input = await factoryBankCategory.build();

  const defaults: BankCategoryCreateRequestDto = {
    name: input.name,
  };

  return _.defaultsDeep(attrs, defaults);
};

// prettier-ignore
export const makeBankCategoryUpdateRequest = async (attrs?: Partial<BankCategoryUpdateRequestDto>): Promise<BankCategoryUpdateRequestDto> => {
  const input = await factoryBankCategory.build();

  const defaults: BankCategoryUpdateRequestDto = {
    name: input.name,
  };

  return _.defaultsDeep(attrs, defaults);
};
