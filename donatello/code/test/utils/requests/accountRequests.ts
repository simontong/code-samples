import { AccountUpdateProfileRequestDto } from '@/src/dtos/AccountUpdateProfileRequestDto';
import _ from 'lodash';
import { factoryUser } from '@/database/factory';

// prettier-ignore
export const makeAccountUpdateProfileRequest = async (attrs?: Partial<AccountUpdateProfileRequestDto>): Promise<AccountUpdateProfileRequestDto> => {
  const input = await factoryUser.build();

  const defaults: AccountUpdateProfileRequestDto = {
    name: input.name,
    email: input.email,
  };

  return _.defaultsDeep(attrs, defaults);
};
