import { factoryBankTag } from '@/database/factory';
import { BankTagCreateRequestDto } from '@/src/dtos/BankTagCreateRequestDto';
import { BankTagUpdateRequestDto } from '@/src/dtos/BankTagUpdateRequestDto';
import _ from 'lodash';

// prettier-ignore
export const makeBankTagCreateRequest = async (attrs?: Partial<BankTagCreateRequestDto>): Promise<BankTagCreateRequestDto> => {
  const input = await factoryBankTag.build();

  const defaults: BankTagCreateRequestDto = {
    name: input.name,
  };

  return _.defaultsDeep(attrs, defaults);
};

// prettier-ignore
export const makeBankTagUpdateRequest = async (attrs?: Partial<BankTagUpdateRequestDto>): Promise<BankTagUpdateRequestDto> => {
  const input = await factoryBankTag.build();

  const defaults: BankTagUpdateRequestDto = {
    name: input.name,
  };

  return _.defaultsDeep(attrs, defaults);
};
