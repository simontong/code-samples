import { factoryBankInstitution } from '@/database/factory';
import { BankInstitutionCreateRequestDto } from '@/src/dtos/BankInstitutionCreateRequestDto';
import { BankInstitutionUpdateRequestDto } from '@/src/dtos/BankInstitutionUpdateRequestDto';
import _ from 'lodash';

// prettier-ignore
export const makeBankInstitutionCreateRequest = async (attrs?: Partial<BankInstitutionCreateRequestDto>): Promise<BankInstitutionCreateRequestDto> => {
  const input = await factoryBankInstitution.build();

  const defaults: BankInstitutionCreateRequestDto = {
    name: input.name,
  };

  return _.defaultsDeep(attrs, defaults);
};

// prettier-ignore
export const makeBankInstitutionUpdateRequest = async (attrs?: Partial<BankInstitutionUpdateRequestDto>): Promise<BankInstitutionUpdateRequestDto> => {
  const input = await factoryBankInstitution.build();

  const defaults: BankInstitutionUpdateRequestDto = {
    name: input.name,
  };

  return _.defaultsDeep(attrs, defaults);
};
