import container from '@/src/app/container';
import * as bull from 'bullmq';
import BaseJob from '@/src/lib/queues/BaseJob';
import QueueManager, { QueueManagerOpts } from '@/src/lib/queues/QueueManager';
import { makeDispatcher } from '@/src/lib/queues/utils';

describe('lib: queues/QueueManager', () => {
  describe('adding jobs', () => {
    /**
     * test
     */
    it('uses one connection for multiple job adds on the same queues', async () => {
      class Jobly implements BaseJob {
        static queueName = 'jobly';

        constructor(readonly data: { foo: string }) {}

        static async handle(job: bull.Job, token?: string): Promise<void> {
          await Promise.resolve();
        }
      }

      // @ts-ignore
      const spy = jest.spyOn(bull, 'Queue').mockImplementation(() => {
        return new (class Queue {
          add = () => ({});
          waitUntilReady = jest.fn();
          close = jest.fn();
        })();
      });

      const opts = container.cradle.config.getOrThrow<QueueManagerOpts>('queues');
      const queueManagers = [new QueueManager(Jobly.queueName, opts)];
      const dispatch = makeDispatcher(queueManagers);

      await dispatch(new Jobly({ foo: 'bar' }));
      await dispatch(new Jobly({ foo: 'world' }));
      await dispatch(new Jobly({ foo: 'three' }));

      expect(spy).toBeCalledTimes(1);

      await Promise.all(queueManagers.map((qm) => qm.close()));
    });

    /**
     * test
     */
    it('should add jobs to the queue', async () => {
      class Jobbo implements BaseJob {
        static queueName = 'jobbo-items';

        constructor(readonly data: { foo: string }) {}

        static async handle(job: bull.Job, token?: string): Promise<void> {
          await Promise.resolve();
        }
      }

      // @ts-ignore
      const spy = jest.spyOn(bull.Queue.prototype, 'add').mockImplementation(() => ({}));

      const opts = container.cradle.config.getOrThrow<QueueManagerOpts>('queues');
      const queueManagers = [new QueueManager(Jobbo.queueName, opts)];
      const dispatch = makeDispatcher(queueManagers);

      await dispatch(new Jobbo({ foo: 'hello' }));
      await dispatch(new Jobbo({ foo: 'there' }));

      expect(spy).toBeCalledTimes(2);

      await Promise.all(queueManagers.map((qm) => qm.close()));
    });
  });
});
