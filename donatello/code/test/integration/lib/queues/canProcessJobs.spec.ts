import container from '@/src/app/container';
import BaseJob from '@/src/lib/queues/BaseJob';
import * as bull from 'bullmq';
import QueueManager, { QueueManagerOpts } from '@/src/lib/queues/QueueManager';
import { makeDispatcher, makeProcessor } from '@/src/lib/queues/utils';
import { Worker } from 'bullmq';

type OpenHandles = {
  queueManagers: QueueManager[];
  workers: Worker[];
};

const openHandles: OpenHandles = {
  queueManagers: [],
  workers: [],
};

beforeEach(() => {
  openHandles.queueManagers = [];
  openHandles.workers = [];
});
afterEach(async () => {
  await Promise.all(openHandles.queueManagers.map((qm) => qm.close()));
  await Promise.all(openHandles.workers.map((w) => w.close()));
});

describe('lib: queues/QueueManager', () => {
  describe('processing jobs', () => {
    /**
     * test
     */
    it('should throw if job class is missing handle() method', async () => {
      class SendAnEmail implements BaseJob {
        static queueName = 'my-jobs';

        constructor(readonly data: { foo: string }) {}
      }

      const opts = container.cradle.config.getOrThrow<QueueManagerOpts>('queues');
      const queueManagers = [new QueueManager(SendAnEmail.queueName, opts)];
      openHandles.queueManagers.push(...queueManagers);
      const dispatch = makeDispatcher(queueManagers);

      await expect(dispatch(new SendAnEmail({ foo: 'bar' }))).rejects.toThrow(/missing static handle/i);
    });

    /**
     * test
     */
    it('should process a job', () => {
      // eslint-disable-next-line no-async-promise-executor
      return new Promise(async (resolve, reject) => {
        // eslint-disable-next-line no-async-promise-executor
        class BillClient implements BaseJob {
          static queueName = 'my-queue';

          constructor(readonly data: { foo: string }) {}

          static async handle(): Promise<void> {
            await Promise.resolve();
          }
        }

        const spyHandle = jest.spyOn(BillClient, 'handle');

        const opts = container.cradle.config.getOrThrow<QueueManagerOpts>('queues');
        const queueManagers = [new QueueManager(BillClient.queueName, opts)];
        openHandles.queueManagers.push(...queueManagers);

        // @ts-ignore
        const jobs: typeof BaseJob[] = [BillClient];
        const dispatch = makeDispatcher(queueManagers);
        const process = makeProcessor(queueManagers, jobs);

        // eslint-disable-next-line prefer-const
        let newJob: bull.Job;
        const worker = await process('my-queue');
        openHandles.workers.push(worker);

        worker.on('completed', (job: bull.Job) => {
          try {
            expect(spyHandle).toBeCalledTimes(1);
            expect(job).toBeObject();
            expect(newJob).toBeObject();
            expect(newJob.id).toEqual(job.id);
            resolve(null);
          } catch (err) {
            reject(err);
          }
        });

        newJob = await dispatch(new BillClient({ foo: 'bar' }));
      });
    });
  });
});
