import container from '@/src/app/container';
import {
  factoryBankAccount,
  factoryBankCategory,
  factoryBankTag,
  factoryCurrency,
  factoryUser,
} from '@/database/factory';

export type RecordPresenceSpies = {
  findBankAccount: jest.SpyInstance;
  findBankCategory: jest.SpyInstance;
  findBankTag: jest.SpyInstance;
  findCurrency: jest.SpyInstance;
  findUser: jest.SpyInstance;
};

export const createRecordPresenceSpies = (uniqueRecords = false): RecordPresenceSpies => {
  return {
    // @ts-ignore
    findBankAccount: jest.spyOn(container.cradle.bankAccountService, 'findBankAccount').mockImplementation(() => {
      return uniqueRecords ? null : factoryBankAccount.build();
    }),

    // @ts-ignore
    findBankCategory: jest.spyOn(container.cradle.bankCategoryService, 'findBankCategory').mockImplementation(() => {
      return uniqueRecords ? null : factoryBankCategory.build();
    }),

    // @ts-ignore
    findBankTag: jest.spyOn(container.cradle.bankTagService, 'findBankTag').mockImplementation(() => {
      return uniqueRecords ? null : factoryBankTag.build();
    }),

    // @ts-ignore
    findCurrency: jest.spyOn(container.cradle.currencyService, 'findCurrency').mockImplementation(() => {
      return uniqueRecords ? null : factoryCurrency.build();
    }),

    // @ts-ignore
    findUser: jest.spyOn(container.cradle.userService, 'findUser').mockImplementation(() => {
      return uniqueRecords ? null : factoryUser.build();
    }),
  };
};
