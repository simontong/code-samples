import { createMockContext } from '@shopify/jest-koa-mocks';
import { applyJsendToContext } from '@/src/app/middlewares/jsend';
import { KoaContext } from '@/src/types/KoaContext';

export default (opts?: unknown): KoaContext => {
  // @ts-ignore
  const ctx = createMockContext(opts) as KoaContext;

  applyJsendToContext(ctx);

  return ctx;
};
