/* eslint-disable no-undef */
import dotenv from 'dotenv';
import path from 'path';

const env = dotenv.config({
  path: path.join(__dirname, '../.env.testing'),
});

// jest sets NODE_ENV automatically and the `dotenv` bootstrap doesn't override it, so we forcefully do it here
process.env.NODE_ENV = env.parsed?.NODE_ENV;
if (!process.env.NODE_ENV) {
  throw new Error('NODE_ENV is empty');
}
