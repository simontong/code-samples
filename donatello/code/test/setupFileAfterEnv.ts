import '@/src/locale/yup';
import 'jest-extended';
import container from '@/src/app/container';
import { queueManagers } from '@/src/lib/queues';
import { isDatabaseConnectionLive } from '@/src/lib/database/utils';

beforeAll(async () => {
  // todo: should this be running on every single test?
  await container.cradle.redis.client.flushdb();
});

afterAll(async () => {
  // close redis
  if (container.cradle.redis.isConnected) {
    container.cradle.redis.client.disconnect(false);
  }

  // close database
  if (isDatabaseConnectionLive(container.cradle.db)) {
    await container.cradle.db.destroy();
  }

  // close queue manager handles
  const openQueueManagers = queueManagers.filter((qm) => qm.hasOpenHandles());
  if (openQueueManagers.length) {
    await Promise.all(openQueueManagers.map((qm) => qm.close()));
  }
});
