import env from '@/src/lib/env';

export default {
  dsn: env.get('SENTRY_DSN'),

  // prettier-ignore
  // won't report if NODE_ENV is these environments
  skipEnvironments: [
    'testing',
    'development',
  ],
};
