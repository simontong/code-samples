import env from '@/src/lib/env';
import smtpTransport from 'nodemailer/lib/smtp-transport';
import mgTransport from 'nodemailer-mailgun-transport';

// set up smtp (if used)
const smtp: smtpTransport.Options = {
  host: env.get('MAILER_SMTP_HOST'),
  port: Number(env.get('MAILER_SMTP_PORT')),
  auth: {
    user: env.get('MAILER_SMTP_USER'),
    pass: env.get('MAILER_SMTP_PASSWORD'),
  },
};

// if no auth provided then unset (some emails fail if this is set)
if (!smtp.auth?.user) {
  smtp.auth = undefined;
}

// set up mailgun (if used)
const mailgun: mgTransport.Options = {
  auth: {
    api_key: env.get('MAILGUN_API_KEY'),
    domain: env.get('MAILGUN_DOMAIN'),
  },
};

export default {
  mailer: 'smtp',

  mailers: {
    smtp,
    mailgun,
  },
};
