import { RedisOptions } from 'ioredis';
import env from '@/src/lib/env';

const config: RedisOptions = {
  host: env.get('REDIS_HOST', '127.0.0.1'),
  port: Number(env.get('REDIS_PORT', 6379)),
  showFriendlyErrorStack: true,

  // reconnect after 2 seconds
  retryStrategy: (): number => 2000,
};

export default config;
