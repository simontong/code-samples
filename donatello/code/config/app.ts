import env from '@/src/lib/env';

export default {
  env: env.get('NODE_ENV'),
  id: 'donatello',

  keys: [env.get('APP_SECRET')],
  encryptionKey: env.get('APP_CRYPTO_KEY'),

  host: env.get('HOST'),
  port: env.get('PORT'),
  webClientOrigin: env.get('WEB_CLIENT_ORIGIN'),

  // prettier-ignore
  // used with isDevEnv()
  devEnvironments: [
    'testing',
    'development',
  ],

  emails: {
    transactional: env.get('EMAIL_TRANSACTIONAL'),
    support: env.get('EMAIL_SUPPORT'),
  },

  company: {
    name: 'Donatello',
    address: ['Donatello Ltd', '1234 Street', 'Suite 1234'],
  },
};
