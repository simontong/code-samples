import env from '@/src/lib/env';
import { ConnectionOptions } from 'bullmq';
import { QueueManagerOpts } from '@/src/lib/queues/QueueManager';

const connection: ConnectionOptions = {
  host: env.get('REDIS_HOST', '127.0.0.1'),
  port: Number(env.get('REDIS_PORT', 6379)),
  showFriendlyErrorStack: true,

  // reconnect after 2 seconds
  retryStrategy: (): number => 2000,
};

const config: QueueManagerOpts = {
  queue: { connection },
  scheduler: { connection },
  events: { connection },
  worker: { connection },
};

export default config;
