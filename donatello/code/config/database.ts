import path from 'path';
import env from '@/src/lib/env';
import { Knex } from 'knex';
import { projectRoot } from '@/src/lib/paths';

const config: Knex.Config = {
  client: 'pg',
  connection: {
    host: env.get('DB_HOST', '127.0.0.1'),
    port: Number(env.get('DB_PORT', 5432)),
    database: env.get('DB_DATABASE'),
    user: env.get('DB_USER'),
    password: env.get('DB_PASSWORD'),
    dateStrings: true,
  },
  pool: {
    min: 2,
    max: 10,
  },
  migrations: {
    tableName: 'knex_migrations',
    directory: path.join(__dirname, '../database/migrations'),
  },
  seeds: {
    directory: projectRoot(env.get('DB_SEED_DIR', './database/seeds/development')),
  },
};

export default config;
