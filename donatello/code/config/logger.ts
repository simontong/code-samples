import env from '@/src/lib/env';
import { projectRoot } from '@/src/lib/paths';

export default {
  logDir: projectRoot('storage/logs'),
  stdoutLevel: env.get('LOG_LEVEL', 'trace'),
};
