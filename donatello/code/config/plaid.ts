import env from '@/src/lib/env';

export default {
  env: env.get('PLAID_ENV'),
  clientId: env.get('PLAID_CLIENT_ID'),
  secret: env.get('PLAID_SECRET'),
  redirectUri: env.get('PLAID_REDIRECT_URI'),
  products: env.get('PLAID_PRODUCTS'),
  countryCodes: env.get('PLAID_COUNTRY_CODES'),
};
