import BaseQueryBuilder from '@/src/lib/database/BaseQueryBuilder';
import { IDatasource } from '@/src/lib/datasource';
import { ExportableOptions } from '@/src/lib/datasource/exportable';
import { FilterableOptions } from '@/src/lib/datasource/filterable';
import { SearchableOptions } from '@/src/lib/datasource/searchable';
import { SortableOptions } from '@/src/lib/datasource/sortable';
import BankTransaction from '@/src/models/BankTransaction';

/**
 * TODO:
 *  need to figure out how to include bank tags without duplicate rows!
 */

export class BankTransactionDatasource implements IDatasource<BankTransaction> {
  sortables: SortableOptions<BankTransaction> = {
    columns: {
      bankAccount: { column: 'bankAccount.name' },
      bankCategory: { column: 'bankCategory.name' },
      date: { column: 'bank_transactions.date' },
      payee: { column: 'bank_transactions.payee' },
      amount: { column: 'bank_transactions.amount' },
    },
  };

  filterables: FilterableOptions<BankTransaction> = {
    columns: {
      bankCategory: {
        type: 'text',
        column: 'bankCategory.name',
        operators: ['ts', 'eq', 'in'],
      },

      // bankTags: {
      //   type: 'text',
      //   column: 'bankTags.name',
      //   operators: ['eq', 'in'],
      // },

      date: {
        type: 'datetime',
        column: 'bank_transactions.date',
        operators: ['eq', 'gt', 'gte', 'lt', 'lte', 'between'],
      },

      payee: {
        type: 'text',
        column: 'bank_transactions.payee',
        operators: ['ts', 'eq'],
      },

      amount: {
        type: 'integer',
        column: 'bank_transactions.amount',
        operators: ['eq', 'gt', 'gte', 'lt', 'lte', 'between'],
      },

      currency: {
        type: 'text',
        column: 'currency.code',
        operators: ['eq'],
      },
    },
  };

  searchable: SearchableOptions<BankTransaction> = {
    columns: {
      bankCategory: { column: 'bankCategory.name' },
      payee: { column: 'bank_transactions.payee' },
    },
  };

  exportable: ExportableOptions<BankTransaction> = {
    select: [
      'bank_transactions.id as ID',
      'bankAccount.name as Account',
      'bankCategory.name as Category',
      'bank_transactions.date as Date',
      'bank_transactions.payee as Payee',
      'bank_transactions.amount as Amount',
      // container.cradle.db.raw(`string_agg(??, ',') over (partition by ??) as ??`, ['bankTags.name', 'bank_transactions.id', 'Tags']),
    ],
  };

  constructor(readonly userId: string) {
  }

  query(): BaseQueryBuilder<BankTransaction> {
    const where = {
      // 'bank_transactions__REMOVE__.user_id': this.userId,
      'bank_transactions.user_id': this.userId,
    };

    // prettier-ignore
    return BankTransaction.query()
      .distinctOn('bank_transactions.id')
      .orderBy('bank_transactions.id')
      .select('bank_transactions.*')
      .leftJoinRelated('bankAccount')
      .leftJoinRelated('bankCategory')
      // .leftJoinRelated('bankTags')
      .leftJoinRelated('currency')
      .withGraphFetched('[bankAccount,bankCategory,bankTags,currency]')
      .where(where);
  }
}
