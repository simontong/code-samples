import BaseQueryBuilder from '@/src/lib/database/BaseQueryBuilder';
import { IDatasource } from '@/src/lib/datasource';
import { FilterableOptions } from '@/src/lib/datasource/filterable';
import { SearchableOptions } from '@/src/lib/datasource/searchable';
import { SortableOptions } from '@/src/lib/datasource/sortable';
import BankInstitution from '@/src/models/BankInstitution';

export class BankInstitutionDatasource implements IDatasource<BankInstitution> {
  sortables: SortableOptions<BankInstitution> = {
    columns: {
      name: { column: 'bank_institutions.name' },
    },
  };

  filterables: FilterableOptions<BankInstitution> = {
    columns: {
      name: {
        type: 'text',
        column: 'bank_institutions.name',
        operators: ['ts', 'eq'],
      },
    },
  };

  searchable: SearchableOptions<BankInstitution> = {
    columns: {
      name: { column: 'bank_institutions.name' },
    },
  };

  constructor(readonly userId: string) {}

  query(): BaseQueryBuilder<BankInstitution> {
    const where = {
      'bank_institutions.user_id': this.userId,
    };

    // prettier-ignore
    return BankInstitution.query()
      .where(where);
  }
}
