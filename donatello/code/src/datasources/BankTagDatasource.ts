import BaseQueryBuilder from '@/src/lib/database/BaseQueryBuilder';
import { IDatasource } from '@/src/lib/datasource';
import { FilterableOptions } from '@/src/lib/datasource/filterable';
import { SearchableOptions } from '@/src/lib/datasource/searchable';
import { SortableOptions } from '@/src/lib/datasource/sortable';
import BankTag from '@/src/models/BankTag';

export class BankTagDatasource implements IDatasource<BankTag> {
  sortables: SortableOptions<BankTag> = {
    columns: {
      name: { column: 'bank_tags.name' },
    },
  };

  filterables: FilterableOptions<BankTag> = {
    columns: {
      name: {
        type: 'text',
        column: 'bank_tags.name',
        operators: ['ts', 'eq'],
      },
    },
  };

  searchable: SearchableOptions<BankTag> = {
    columns: {
      name: { column: 'bank_tags.name' },
    },
  };

  constructor(readonly userId: string) {}

  query(): BaseQueryBuilder<BankTag> {
    const where = {
      'bank_tags.user_id': this.userId,
    };

    // prettier-ignore
    return BankTag.query()
      .where(where);
  }
}
