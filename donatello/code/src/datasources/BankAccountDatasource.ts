import container from '@/src/app/container';
import BaseQueryBuilder from '@/src/lib/database/BaseQueryBuilder';
import { IDatasource } from '@/src/lib/datasource';
import { ExportableOptions } from '@/src/lib/datasource/exportable';
import { FilterableOptions } from '@/src/lib/datasource/filterable';
import { SearchableOptions } from '@/src/lib/datasource/searchable';
import { SortableOptions } from '@/src/lib/datasource/sortable';
import BankAccount from '@/src/models/BankAccount';

export class BankAccountDatasource implements IDatasource<BankAccount> {
  sortables: SortableOptions<BankAccount> = {
    columns: {
      name: { column: 'bank_accounts.name' },
      bankInstitution: { column: 'bankInstitution.name' },
    },
  };

  filterables: FilterableOptions<BankAccount> = {
    columns: {
      name: {
        type: 'text',
        column: 'bank_accounts.name',
        operators: ['ts', 'eq'],
      },

      bankInstitution: {
        type: 'text',
        column: 'bankInstitution.name',
        operators: ['ts', 'eq'],
      },
    },
  };

  searchable: SearchableOptions<BankAccount> = {
    columns: {
      name: { column: 'bank_accounts.name' },
    },
  };

  exportable: ExportableOptions<BankAccount> = {
    select: [
      'bank_accounts.id as ID',
      'bankInstitution.name as Institution',
      'bank_accounts.name as Account',
    ],
  };

  constructor(readonly userId: string) {
  }

  query(): BaseQueryBuilder<BankAccount> {
    const where = {
      'bank_accounts.user_id': this.userId,
    };

    // prettier-ignore
    return BankAccount.query()
      .leftJoinRelated('bankInstitution')
      .withGraphFetched('[bankInstitution]')
      .where(where);
  }
}
