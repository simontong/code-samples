import BaseQueryBuilder from '@/src/lib/database/BaseQueryBuilder';
import { IDatasource } from '@/src/lib/datasource';
import { FilterableOptions } from '@/src/lib/datasource/filterable';
import { SearchableOptions } from '@/src/lib/datasource/searchable';
import { SortableOptions } from '@/src/lib/datasource/sortable';
import BankCategory from '@/src/models/BankCategory';

export class BankCategoryDatasource implements IDatasource<BankCategory> {
  sortables: SortableOptions<BankCategory> = {
    columns: {
      name: { column: 'bank_categories.name' },
    },
  };

  filterables: FilterableOptions<BankCategory> = {
    columns: {
      name: {
        type: 'text',
        column: 'bank_categories.name',
        operators: ['ts', 'eq'],
      },
    },
  };

  searchable: SearchableOptions<BankCategory> = {
    columns: {
      name: { column: 'bank_categories.name' },
    },
  };

  constructor(readonly userId: string) {}

  query(): BaseQueryBuilder<BankCategory> {
    const where = {
      'bank_categories.user_id': this.userId,
    };

    // prettier-ignore
    return BankCategory.query()
      .where(where);
  }
}
