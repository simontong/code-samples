import container from '@/src/app/container';
import { webClientUrl } from '@/src/utils/webClientUrl';

describe('webClientUrl', () => {
  /**
   * test
   */
  it('should return web client URL', () => {
    const baseUrl = 'https://donatello.com';
    const path = '/auth/login';
    const variants = ['auth/login', '//auth/login', './auth/login'];

    container.cradle.config.set('app.webClientOrigin', baseUrl);

    for (const variant of variants) {
      expect(webClientUrl(variant)).toEqual(baseUrl + path);
    }
  });

  /**
   * test
   */
  it('should include querystring', () => {
    const baseUrl = 'https://example.com';
    const path = '/user/profile?id=123&sort=date';

    container.cradle.config.set('app.webClientOrigin', baseUrl);

    expect(webClientUrl(path)).toEqual(baseUrl + path);
  });

  /**
   * test
   */
  it('should throw on missing baseUrl', () => {
    const baseUrl = '';
    container.cradle.config.set('app.webClientOrigin', baseUrl);

    // @ts-ignore
    expect(() => webClientUrl('/about')).toThrow(/invalid prop keys: app\.webClientOrigin/i);
  });

  /**
   * test
   */
  it('should throw if pathname is not a string', () => {
    const baseUrl = 'https://google.com';
    container.cradle.config.set('app.webClientOrigin', baseUrl);

    // @ts-ignore
    expect(() => webClientUrl(false)).toThrow(/expected `pathname` to be of type `string`/i);
  });

  /**
   * test
   */
  it('should throw if search is not a string', () => {
    const baseUrl = 'https://google.com';
    container.cradle.config.set('app.webClientOrigin', baseUrl);

    // @ts-ignore
    expect(() => webClientUrl('/reset-password', null)).toThrow(/expected `search` to be of type `string`/i);
  });
});
