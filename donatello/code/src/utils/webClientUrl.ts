import container from '@/src/app/container';
import ow from 'ow';
import { URL } from 'url';

export const webClientUrl = (pathname = '', search = ''): string => {
  ow(pathname, 'pathname', ow.string);
  ow(search, 'search', ow.string);

  const baseUrl = container.cradle.config.getOrThrow<string>('app.webClientOrigin');

  const input = [];
  input.push(pathname.replace(/(^\/+|\/$)/g, '/'));
  if (search) {
    input.push(search);
  }

  const url = new URL(input.join('?'), baseUrl);
  return url.href;
};
