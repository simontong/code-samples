import _ from 'lodash';
import { ArgError } from '@/src/lib/errors';

/**
 * get single prop
 */
export const getProp = <T>(props: unknown, key: string, defaultValue?: unknown): T => {
  return _.get(props, key, defaultValue) as T;
};

/**
 * get single prop or throw
 */
export const getPropOrThrow = <T>(props: Record<string, unknown>, key: string, allowEmpty = false): T => {
  const [prop] = getManyPropsOrThrow<[T]>(props, [key], allowEmpty);
  return prop;
};

/**
 * get array of props or throw
 */
export const getManyPropsOrThrow = <T extends Array<unknown>>(
  props: Record<string, unknown>,
  keys: string[],
  allowEmpty = false,
): T => {
  const keysMissing = [];
  const values = [];

  for (const key of keys) {
    const value = _.get(props, key);

    // if value is undefined or we don't allow empty and its not a number and its an empty object/string
    if (_.isUndefined(value) || (!allowEmpty && !_.isNumber(value) && (!value || _.isEmpty(value)))) {
      keysMissing.push(key);
    }

    values.push(value);
  }

  // throw if any keys missing
  if (keysMissing.length > 0) {
    throw new ArgError(`Invalid prop keys: %s`, keysMissing.join(', '));
  }

  return values as T;
};

/**
 * get array of props
 */
export const getManyProps = <T extends Array<unknown>>(props: Record<string, unknown>, keys: string[]): T => {
  return _.at(props, keys) as T;
};
