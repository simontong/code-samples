import { getManyPropsOrThrow, getPropOrThrow, getProp, getManyProps } from '@/src/utils/getProp';

describe('utils: getProps', () => {
  describe('getPropOrThrow, getManyPropsOrThrow', () => {
    /**
     * test
     */
    it('should throw on invalid key', () => {
      const props = {};
      expect(() => getPropOrThrow(props, 'foobar')).toThrow(/invalid prop keys: foobar/i);
      expect(() => getManyPropsOrThrow(props, ['host', 'port'])).toThrow(/invalid prop keys: host, port/i);
    });

    /**
     * test
     */
    it('should not throw on valid key with allow empty = true', () => {
      const props = { logger: '', host: '', name: '' };
      expect(getPropOrThrow(props, 'logger', true)).toEqual(props.logger);
      expect(getManyPropsOrThrow(props, ['host', 'name'], true)).toEqual([props.host, props.name]);
    });

    /**
     * test
     */
    it('should return value', () => {
      const props = { host: 'localhost', name: 'test' };
      expect(getPropOrThrow(props, 'host')).toEqual(props.host);
      expect(getManyPropsOrThrow(props, ['host', 'name'], true)).toEqual([props.host, props.name]);
    });
  });

  describe('getProp, getManyProps', () => {
    /**
     * test
     */
    it('should return value', () => {
      const props = { host: 'localhost', port: 6999 };
      expect(getProp(props, 'host')).toEqual(props.host);
      expect(getManyProps(props, ['host', 'port'])).toEqual([props.host, props.port]);
    });
  });
});
