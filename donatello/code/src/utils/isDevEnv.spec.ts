import container from '@/src/app/container';
import { isDevEnv } from '@/src/utils/isDevEnv';

describe('isDevMode', () => {
  /**
   * test
   */
  it('should return true when in development environment', () => {
    const devEnvs = ['testing', 'development'];

    for (const env of devEnvs) {
      container.cradle.config.set('app.env', env);
      expect(isDevEnv()).toEqual(true);
    }
  });

  /**
   * test
   */
  it('should return false when in production environment', () => {
    const devEnvs = ['production', 'staging'];

    for (const env of devEnvs) {
      container.cradle.config.set('app.env', env);
      expect(isDevEnv()).toEqual(false);
    }
  });
});
