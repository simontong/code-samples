import container from '@/src/app/container';

export const isDevEnv = (): boolean => {
  const env = container.cradle.config.getOrThrow<string>('app.env');
  const devEnvs = container.cradle.config.getOrThrow<Array<string>>('app.devEnvironments');

  return devEnvs.includes(env);
};
