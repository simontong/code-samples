import '@/src/locale/yup';
import container from '@/src/app/container';
import exitHook from 'exit-hook';
import { AppError } from '@/src/lib/errors';
import { captureError, sentryInit } from '@/src/lib/errors/errorHandling';
import { initRedisConnection } from '@/src/lib/redis/utils';
import { initDatabaseConnection } from '@/src/lib/database/utils';
import app from './app';

async function main() {
  const cradle = container.cradle;

  sentryInit();
  await initDatabaseConnection(cradle.db, cradle.logger);
  await initRedisConnection(cradle.redis, cradle.logger);

  const host = cradle.config.getOrThrow<string>('app.host');
  const port = cradle.config.getOrThrow<number>('app.port');

  const server = app.listen(port, host, () => {
    cradle.logger.debug('* Serving on http://%s:%s', host, port);
  });

  // close server gracefully on exit
  exitHook(() => {
    cradle.logger.debug('* Closing server on http://%s:%s ...', host, port);
    server.close((err) => {
      if (err) {
        captureError(new AppError(err, 'Failed to close server on http://%s:%s', host, port));
      }
    });
  });

  // force exit after 10s if we still haven't exited
  exitHook(() => setTimeout(() => process.exit(), 10000).unref());
}

// handle processes
process.on('uncaughtException', (err: Error) => captureError(err));
process.on('unhandledRejection', (err: Error) => captureError(err));

// start
main().catch((err: Error) => captureError(err));
