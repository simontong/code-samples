import attachContainer from '@/src/app/middlewares/attachContainer';
import streamResponse from '@/src/app/middlewares/stream';
import { isDevEnv } from '@/src/utils/isDevEnv';
import Koa from 'koa';
import koaQs from 'koa-qs';
import compose from 'koa-compose';
import helmet from 'koa-helmet';
import cors from '@koa/cors';
import session from 'koa-session';
import { captureError } from '@/src/lib/errors/errorHandling';
import jsend from '@/src/app/middlewares/jsend';
import appErrorHandler from '@/src/app/middlewares/appErrorHandler';
import validationErrorHandler from '@/src/app/middlewares/validationErrorHandler';
import requestLogger from '@/src/app/middlewares/requestLogger';
import routes from '@/src/app/routes';
import container from '@/src/app/container';

const app = new Koa();

app.on('error', (err, ctx) => captureError(err, ctx));

app.proxy = true;

app.keys = container.cradle.config.getOrThrow<Array<string>>('app.keys');

// attach container to app
attachContainer(app);

app.use(jsend(app));
app.use(streamResponse(app));
app.use(appErrorHandler);
app.use(validationErrorHandler);
app.use(requestLogger);
app.use(helmet());
app.use(
  cors({
    origin: container.cradle.config.getOrThrow<string>('app.webClientOrigin'),
    credentials: true,
    allowMethods: ['GET', 'PUT', 'POST', 'DELETE'],
    allowHeaders: ['Content-Type'],
    keepHeadersOnError: false,
  }),
);
app.use(
  session(
    {
      key: 'don:sess',
      // maxAge: 900000, // 15 min session
      maxAge: 3600000, // 60 min session
      overwrite: true,
      httpOnly: true,
      signed: true,
      rolling: false,
      renew: true,
      secure: !isDevEnv(),
    },
    app,
  ),
);
koaQs(app);
app.use(compose([routes.allowedMethods(), routes.routes()]));

export default app;
