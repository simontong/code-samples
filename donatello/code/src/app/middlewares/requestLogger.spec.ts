import container from '@/src/app/container';
import requestLogger from './requestLogger';
import createMockContext from '@/test/mock/createMockContext';

describe('middleware: logger', () => {
  describe('logs on request', () => {
    /**
     * test
     */
    it('should write log', async () => {
      const ctx = createMockContext();
      const next = jest.fn();
      const spy = jest.spyOn(container.cradle.logger, 'trace').mockImplementation();

      await expect(requestLogger(ctx, next)).resolves.toBeUndefined();
      expect(spy).toHaveBeenCalledTimes(1);
    });
  });
});
