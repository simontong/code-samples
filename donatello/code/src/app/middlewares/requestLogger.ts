import container from '@/src/app/container';
import { KoaContext } from '@/src/types/KoaContext';
import { Next } from 'koa';

let requestCount = 0;

const makeOnResFinished = (ctx: KoaContext, requestNum: number) => {
  // prettier-ignore
  // incoming
  container.cradle.logger.trace(
    '[req-%d] < %s - "%s %s" "%s"',
    requestNum,
    ctx.ip,
    ctx.method,
    ctx.href,
    ctx.get('user-agent'),
  );

  // begin time
  const start = process.hrtime.bigint();

  return () => {
    // calc time
    const end = process.hrtime.bigint();
    // const time = Number(end - start); // nano
    // const time = Number(end - start) / 1000; // micro
    const time = Number(end - start) / 1000000; // milli

    // prettier-ignore
    // outgoing
    container.cradle.logger.trace(
      '[req-%d] > %s - "%s %s" %d "%s" +%dms',
      requestNum,
      ctx.ip,
      ctx.method,
      ctx.href,
      ctx.status,
      ctx.get('user-agent'),
      time,
    );
  };
};

export default async function requestLogger(ctx: KoaContext, next: Next): Promise<Next> {
  const onResFinished = makeOnResFinished(ctx, requestCount);
  requestCount += 1;
  ctx.res.on('finish', onResFinished);
  ctx.res.on('error', onResFinished);

  return next();
}
