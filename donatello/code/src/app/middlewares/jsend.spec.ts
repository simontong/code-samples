import { ErrorJson } from '@/src/lib/validators/validateOrThrow';
import Application from 'koa';
import createMockContext from '@/test/mock/createMockContext';
import jsend, { Jsend } from './jsend';
import { StatusCodes } from 'http-status-codes';
import * as faker from 'faker';

describe('middleware: jsend', () => {
  describe('successful responses', () => {
    /**
     * test
     */
    it('should wrap successful responses', async () => {
      const app = { context: {} } as Application;
      const middleware = jsend(app);
      const ctx = createMockContext();
      const data = {
        id: faker.datatype.uuid(),
        name: faker.name.findName(),
        email: faker.internet.email(),
      };

      // pretend route applying jsend
      const next = async () => {
        ctx.jsend().data(data);
      };

      await expect(middleware(ctx, next)).resolves.toBeUndefined();
      expect(ctx.jsend).toBeFunction();
      expect(ctx.status).toEqual(StatusCodes.OK);
      expect(ctx.body).toStrictEqual({ data });
    });

    /**
     * test
     */
    it('should not modify bodyParser or status unless explicitly called to', async () => {
      const app = { context: {} } as Application;
      const ctx = createMockContext();
      const next = jest.fn();
      const middleware = jsend(app);
      const spy = jest.spyOn(Jsend.prototype, 'modifyContext');

      await expect(middleware(ctx, next)).resolves.toBeUndefined();
      expect(spy).toBeCalledTimes(0);
      expect(ctx.body).toBeUndefined();
    });
  });

  describe('failed responses', () => {
    /**
     * test
     */
    it('should wrap failed responses', async () => {
      const app = { context: {} } as Application;
      const ctx = createMockContext();
      const middleware = jsend(app);
      const errors: ErrorJson[] = [{ path: 'password', error: 'password is a required field' }];

      // pretend route applying jsend
      const next = async () => {
        ctx.jsend().badRequest(errors);
      };

      await expect(middleware(ctx, next)).resolves.toBeUndefined();
      expect(ctx.jsend).toBeFunction();
      expect(ctx.status).toEqual(StatusCodes.BAD_REQUEST);
      expect(ctx.body).toBeObject();
      expect(ctx.body.error).toStrictEqual(errors);
    });
  });
});
