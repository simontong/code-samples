import { Jsend } from '@/src/app/middlewares/jsend';
import { KoaContext } from '@/src/types/KoaContext';
import csv from 'csv';
import Application, { Next } from 'koa';
import mime from 'mime-types';
import { PassThrough } from 'stream';

export type MainCtxStream = {
  stream(stream: PassThrough): void;
};

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export default function streamResponse(app: Application) {
  app.context.stream = null;

  return async (ctx: KoaContext, next: Next): Promise<Next> => {
    ctx.stream = (stream: PassThrough) => {
      // bind request close to stream close
      ctx.req.on('close', () => stream.end.bind(stream));

      // handle stream error
      stream.on('error', (err) => ctx.onerror(err));

      const opts = {
        header: true,
      };

      // todo: improve this
      ctx.type = mime.lookup('csv') || 'text/csv; charset=utf-8';

      // @ts-ignore
      ctx.body = stream.pipe(csv.stringify(opts));
    };

    return next();
  };
}
