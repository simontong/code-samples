import container from '@/src/app/container';
import { Next } from 'koa';
import { KoaContext } from '@/src/types/KoaContext';
import { UserShape } from '@/src/models/User';
import { HttpErrorUnauthorized } from '@/src/lib/errors';

export type StateCtxAttachUser = {
  user: UserShape;
};

export type MainCtxAttachUser = {
  session: { userId?: string | null };
};

export default async function attachUserOrThrow(ctx: KoaContext, next: Next): Promise<Next | void> {
  let user;

  // attempt to find user by session id
  const userId = ctx.session?.userId;
  if (userId) {
    user = await container.cradle.userService.findUserById(userId);
  }

  // no user found, then bail
  if (!user) {
    throw new HttpErrorUnauthorized();
  }

  // add user to state
  ctx.state.user = user;

  return next();
}
