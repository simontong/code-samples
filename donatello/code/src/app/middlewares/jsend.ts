import _ from 'lodash';
import Application, { Next } from 'koa';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { KoaContext } from '@/src/types/KoaContext';

type JsendResponse = {
  status?: number;
  code?: string | null;
  error?: unknown | null;
  data?: unknown | null;
};

export type MainCtxJsend = {
  body: JsendResponse;
  jsend(body?: JsendResponse): Jsend;
};

export class Jsend {
  private ctx: KoaContext;

  constructor(ctx: KoaContext) {
    this.ctx = ctx;
  }

  modifyContext(response?: JsendResponse): this {
    const ctxBody: JsendResponse = typeof this.ctx.body === 'object' ? this.ctx.body : {};
    const status = _.get(response, 'status');
    const code = _.get(response, 'code', ctxBody.code);
    const error = _.get(response, 'error', ctxBody.error);
    const data = _.get(response, 'data', ctxBody.data);

    // set up new body, remove any properties with undefined values
    const newBody = _.omitBy({ code, error, data }, _.isUndefined);

    // if newBody isn't an empty object then modify this.ctx.bodyParser
    if (!_.isEmpty(newBody)) {
      this.ctx.body = newBody;
    }

    // set status code
    if (status) {
      this.ctx.status = status;
    }

    return this;
  }

  code(code: string): this {
    this.modifyContext({ code });
    return this;
  }

  error(error: unknown): this {
    this.modifyContext({ error });
    return this;
  }

  data(data: unknown): this {
    this.modifyContext({ data });
    return this;
  }

  status(status: StatusCodes): this {
    this.modifyContext({ status });
    return this;
  }

  statusWithMessage = (status: StatusCodes) => {
    return (message?: unknown): this => {
      this.status(status);

      // if `message` passed then change ctx.bodyParser to reflect based on status code
      if (message) {
        if (status >= 400) {
          this.error(message);
        } else {
          this.data(message);
        }
      }

      // if this is an error and no bodyParser has been set then set it
      // 400-500 and over = client error, 500-561 = server error
      if (status >= 400 && typeof _.get(this.ctx.body, 'error') === 'undefined') {
        this.error(getReasonPhrase(status));
      }

      return this;
    };
  };

  accepted = this.statusWithMessage(StatusCodes.ACCEPTED);
  badRequest = this.statusWithMessage(StatusCodes.BAD_REQUEST);
  forbidden = this.statusWithMessage(StatusCodes.FORBIDDEN);
  internalServerError = this.statusWithMessage(StatusCodes.INTERNAL_SERVER_ERROR);
  noContent = this.statusWithMessage(StatusCodes.NO_CONTENT);
  notAcceptable = this.statusWithMessage(StatusCodes.NOT_ACCEPTABLE);
  notFound = this.statusWithMessage(StatusCodes.NOT_FOUND);
  ok = this.statusWithMessage(StatusCodes.OK);
  tooManyRequests = this.statusWithMessage(StatusCodes.TOO_MANY_REQUESTS);
  unauthorized = this.statusWithMessage(StatusCodes.UNAUTHORIZED);
}

export const applyJsendToContext = (ctx: KoaContext): void => {
  const res = new Jsend(ctx);
  ctx.jsend = res.modifyContext.bind(res);
};

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export default function jsend(app: Application) {
  app.context.jsend = null;

  return async (ctx: KoaContext, next: Next): Promise<Next> => {
    applyJsendToContext(ctx);
    return next();
  };
}
