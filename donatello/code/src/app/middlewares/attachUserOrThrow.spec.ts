import createMockContext from '@/test/mock/createMockContext';
import * as faker from 'faker';
import User from '@/src/models/User';
import { QueryBuilder } from 'objection';
import container from '@/src/app/container';
import attachUserOrThrow from './attachUserOrThrow';
import { HttpErrorUnauthorized } from '@/src/lib/errors';

describe('middleware: attachUserOrThrow', () => {
  describe('user is authorized', () => {
    /**
     * test
     */
    it('should attach the logged in user', async () => {
      const user = {
        id: faker.datatype.uuid(),
        name: faker.name.findName(),
        email: faker.internet.email(),
        password: faker.internet.password(),
      };
      const ctx = createMockContext({ session: { userId: user.id } });
      const next = jest.fn();

      // @ts-ignore
      const spy = jest.spyOn(container.cradle.userService, 'findUserById').mockImplementation(async () => {
        return QueryBuilder.forClass(User).resolve(user);
      });

      await expect(attachUserOrThrow(ctx, next)).resolves.toBeUndefined();
      expect(spy).toHaveBeenCalledTimes(1);
      expect(ctx.state.user).toEqual(user);
    });
  });

  describe('user is unauthorized', () => {
    /**
     * test
     */
    it('should return unauthorized status', async () => {
      const user = null;
      const ctx = createMockContext();
      const next = jest.fn();

      // @ts-ignore
      const spy = jest.spyOn(container.cradle.userService, 'findUserById').mockImplementation(async () => {
        return QueryBuilder.forClass(User).resolve(user);
      });

      await expect(attachUserOrThrow(ctx, next)).rejects.toThrow(HttpErrorUnauthorized);
      expect(spy).toHaveBeenCalledTimes(0); // should not reach
      expect(ctx.state.user).toBeUndefined();
    });
  });
});
