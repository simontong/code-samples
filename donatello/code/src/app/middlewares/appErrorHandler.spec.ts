import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import appErrorHandler from './appErrorHandler';
import createMockContext from '@/test/mock/createMockContext';
import { KoaContext } from '@/src/types/KoaContext';

describe('middleware: jsonError', () => {
  describe('thrown errors', () => {
    /**
     * test
     */
    it('should format an error response with json', async () => {
      const ctx = createMockContext();
      const next = async () => {
        throw new Error('This is an error');
      };

      // make sure error emitted
      const onError = jest.fn().mockImplementation((err: Error, _ctx: KoaContext) => {
        expect(err).toBeInstanceOf(Error);
        expect(_ctx).toStrictEqual(ctx);
      });
      ctx.app.on('error', onError);

      await expect(appErrorHandler(ctx, next)).resolves.toBeUndefined();
      expect(onError).toBeCalledTimes(1);
      expect(ctx.status).toEqual(StatusCodes.INTERNAL_SERVER_ERROR);
      expect(ctx.body).toBeObject();
      expect(ctx.body.error).toEqual(getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR));
    });

    /**
     * test
     */
    it('should display message if error.expose true', async () => {
      const ctx = createMockContext();
      const msg = 'Explosive kaboom error';
      const next = async () => {
        const err = new Error(msg);
        // @ts-ignore
        err.expose = true;
        throw err;
      };

      // make sure error emitted
      const onError = jest.fn().mockImplementation();
      ctx.app.on('error', onError);

      await expect(appErrorHandler(ctx, next)).resolves.toBeUndefined();
      expect(onError).toBeCalledTimes(0);
      expect(ctx.status).toEqual(StatusCodes.INTERNAL_SERVER_ERROR);
      expect(ctx.body).toBeObject();
      expect(ctx.body.error).toEqual(msg);
    });

    /**
     * test
     */
    it('should set custom status', async () => {
      const ctx = createMockContext();
      const msg = 'Custom error here';
      const next = async () => {
        const err = new Error(msg);
        // @ts-ignore
        err.expose = true;
        // @ts-ignore
        err.status = StatusCodes.FORBIDDEN;
        throw err;
      };

      // make sure error emitted
      const onError = jest.fn().mockImplementation();
      ctx.app.on('error', onError);

      await expect(appErrorHandler(ctx, next.bind(null, ctx))).resolves.toBeUndefined();
      expect(onError).toBeCalledTimes(0);
      expect(ctx.status).toEqual(StatusCodes.FORBIDDEN);
      expect(ctx.body).toBeObject();
      expect(ctx.body.error).toEqual(msg);
    });
  });
});
