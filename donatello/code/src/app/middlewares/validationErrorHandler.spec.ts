import validateOrThrow from '@/src/lib/validators/validateOrThrow';
import * as yup from 'yup';
import createMockContext from '@/test/mock/createMockContext';
import validationErrorHandler from './validationErrorHandler';
import { StatusCodes } from 'http-status-codes';

describe('middleware: validationErrorHandler', () => {
  describe('successful validation', () => {
    /**
     * test
     */
    it('should continue to next middleware', async () => {
      const schema = yup.object({
        name: yup.string().required(),
        age: yup.number().required(),
      });

      const ctx = createMockContext({
        requestBody: {
          name: 'john smith',
          age: 17,
        },
      });
      const statusOrig = ctx.status;
      const next = async () => {
        await validateOrThrow(schema, ctx.request.body);
      };

      // @ts-ignore
      await expect(validationErrorHandler(ctx, next)).resolves.toBeUndefined();
      expect(ctx.status).toEqual(statusOrig);
      expect(ctx.body).toBeUndefined();
    });
  });

  describe('failed validation', () => {
    /**
     * test
     */
    it('should respond with validation errors', async () => {
      const schema = yup.object({
        name: yup.string().label('Name').required(),
      });

      const ctx = createMockContext({
        requestBody: {
          name: '',
        },
      });

      const next = async () => {
        await validateOrThrow(schema, ctx.request.body);
      };

      // @ts-ignore
      await expect(validationErrorHandler(ctx, next)).resolves.toBeUndefined();
      expect(ctx.status).toEqual(StatusCodes.BAD_REQUEST);
      expect(ctx.body).toBeObject();
      expect(ctx.body.error).toBeArray();
      expect(ctx.body.error).toEqual(
        // prettier-ignore
        expect.arrayContaining([{ path: 'name', error: 'Name is a required field' }]),
      );
    });
  });
});
