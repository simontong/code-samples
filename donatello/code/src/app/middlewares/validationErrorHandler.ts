import { formatErrors } from '@/src/lib/validators/validateOrThrow';
import { Next } from 'koa';
import { ValidationError } from 'yup';
import { KoaContext } from '@/src/types/KoaContext';
import appCodes from '@/src/consts/appCodes';

/**
 * handle validation error
 */
export default async function validationErrorHandler(ctx: KoaContext, next: Next): Promise<void> {
  try {
    await next();
  } catch (err) {
    if (!ValidationError.isError(err)) {
      throw err;
    }

    // format errors and output
    const errors = formatErrors(err);
    if (errors.length > 0) {
      ctx.jsend().badRequest(errors).code(appCodes.E_VALIDATION_FAILED);
      return;
    }
  }
}
