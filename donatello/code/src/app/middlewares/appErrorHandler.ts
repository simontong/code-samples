import { Next } from 'koa';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { KoaContext } from '@/src/types/KoaContext';

export default async function appErrorHandler(ctx: KoaContext, next: Next): Promise<void> {
  try {
    await next();
  } catch (err) {
    const expose = err?.expose || false;
    const code = expose ? err?.code : undefined;
    const status = err?.status || err?.statusCode || StatusCodes.INTERNAL_SERVER_ERROR;
    const msg = expose && err?.message ? err?.message : getReasonPhrase(status);

    const type = ctx.accepts(['json', 'text']); // order matters here, accept json as default
    switch (type) {
      case 'text': {
        ctx.status = status;
        ctx.body = msg;
        break;
      }

      default:
      case 'json': {
        // make sure jsend available on ctx object, might not be in the instance
        // where `jsend` itself threw an error
        if (typeof ctx.jsend === 'function') {
          ctx.jsend({ status, code, error: msg });
        } else {
          ctx.status = status;
          ctx.body = { code, error: msg };
        }
        break;
      }
    }

    // emit the error
    if (!expose) {
      ctx.app.emit('error', err, ctx);
    }
  }
}
