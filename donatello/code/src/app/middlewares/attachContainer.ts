import Application from 'koa';
import { AwilixContainer } from 'awilix';
import container, { Cradle } from '@/src/app/container';

export type MainCtxAttachContainer = {
  di: AwilixContainer<Cradle>
  deps: Cradle
};

export default function attachContainer(app: Application): void {
  app.context.di = container;
  app.context.deps = container.cradle;
}
