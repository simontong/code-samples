import { asClass, asFunction, asValue, createContainer } from 'awilix';
import { Logger } from 'pino';
import { Knex } from 'knex';
import { Transaction } from 'objection';
import configProps from '@/config';
import Cache from '@/src/lib/cache/Cache';
import Config from '@/src/lib/config/Config';
import createDbConnection from '@/src/lib/database';
import logger from '@/src/lib/logger';
import RedisConnection from '@/src/lib/redis/RedisConnection';
import BankAccountRepository from '@/src/repositories/BankAccountRepository';
import BankAccountService from '@/src/services/BankAccountService';
import BankCategoryRepository from '@/src/repositories/BankCategoryRepository';
import BankCategoryService from '@/src/services/BankCategoryService';
import BankInstitutionRepository from '@/src/repositories/BankInstitutionRepository';
import BankInstitutionService from '@/src/services/BankInstitutionService';
import BankTagRepository from '@/src/repositories/BankTagRepository';
import BankTagService from '@/src/services/BankTagService';
import BankTransactionRepository from '@/src/repositories/BankTransactionRepository';
import BankTransactionService from '@/src/services/BankTransactionService';
import CurrencyRepository from '@/src/repositories/CurrencyRepository';
import CurrencyService from '@/src/services/CurrencyService';
import PlaidAccountRepository from '@/src/repositories/PlaidAccountRepository';
import PlaidAccountService from '@/src/services/PlaidAccountService';
import PlaidApiService from '@/src/services/PlaidApiService';
import PlaidItemRepository from '@/src/repositories/PlaidItemRepository';
import PlaidItemService from '@/src/services/PlaidItemService';
import PlaidTransactionRepository from '@/src/repositories/PlaidTransactionRepository';
import PlaidTransactionService from '@/src/services/PlaidTransactionService';
import UserRepository from '@/src/repositories/UserRepository';
import UserService from '@/src/services/UserService';

export type Cradle = {
  config: Config;

  cache: Cache;
  db: Knex;
  logger: Logger;
  redis: RedisConnection;

  trx: Transaction | undefined;

  // services
  bankAccountService: BankAccountService;
  bankCategoryService: BankCategoryService;
  bankInstitutionService: BankInstitutionService;
  bankTagService: BankTagService;
  bankTransactionService: BankTransactionService;
  currencyService: CurrencyService;
  plaidAccountService: PlaidAccountService;
  plaidApiService: PlaidApiService;
  plaidItemService: PlaidItemService;
  plaidTransactionService: PlaidTransactionService;
  userService: UserService;

  // repositories
  bankAccountRepository: BankAccountRepository;
  bankCategoryRepository: BankCategoryRepository;
  bankInstitutionRepository: BankInstitutionRepository;
  bankTagRepository: BankTagRepository;
  bankTransactionRepository: BankTransactionRepository;
  currencyRepository: CurrencyRepository;
  plaidAccountRepository: PlaidAccountRepository;
  plaidItemRepository: PlaidItemRepository;
  plaidTransactionRepository: PlaidTransactionRepository;
  userRepository: UserRepository;
}

const container = createContainer<Cradle>();
container.register({
  config: asFunction(() => new Config(configProps)).singleton(),

  cache: asClass(Cache),
  db: asFunction(createDbConnection).singleton(),
  logger: asFunction(logger).singleton(),
  redis: asClass(RedisConnection).singleton(),

  // store transactions for scoped
  trx: asValue(undefined),

  // services: apis
  plaidApiService: asClass(PlaidApiService).singleton(),

  // services
  bankAccountService: asClass(BankAccountService).scoped(),
  bankCategoryService: asClass(BankCategoryService).scoped(),
  bankInstitutionService: asClass(BankInstitutionService).scoped(),
  bankTagService: asClass(BankTagService).scoped(),
  bankTransactionService: asClass(BankTransactionService).scoped(),
  currencyService: asClass(CurrencyService).scoped(),
  plaidAccountService: asClass(PlaidAccountService).scoped(),
  plaidItemService: asClass(PlaidItemService).scoped(),
  plaidTransactionService: asClass(PlaidTransactionService).scoped(),
  userService: asClass(UserService).scoped(),

  // repos
  bankAccountRepository: asClass(BankAccountRepository).scoped(),
  bankCategoryRepository: asClass(BankCategoryRepository).scoped(),
  bankInstitutionRepository: asClass(BankInstitutionRepository).scoped(),
  bankTagRepository: asClass(BankTagRepository).scoped(),
  bankTransactionRepository: asClass(BankTransactionRepository).scoped(),
  currencyRepository: asClass(CurrencyRepository).scoped(),
  plaidAccountRepository: asClass(PlaidAccountRepository).scoped(),
  plaidItemRepository: asClass(PlaidItemRepository).scoped(),
  plaidTransactionRepository: asClass(PlaidTransactionRepository).scoped(),
  userRepository: asClass(UserRepository).scoped(),
});

export default container;
