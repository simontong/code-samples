import Router from 'koa-router';
import { DefaultState } from 'koa';
import { KoaContext } from '@/src/types/KoaContext';
import appPing from './app/ping';
import appCatchAll from './app/catch-all';
import account from './account/routes';
import auth from './auth/routes';
import bankAccount from './bank-account/routes';
import bankCategory from './bank-category/routes';
import bankInstitution from './bank-institution/routes';
import bankTag from './bank-tag/routes';
import bankTransaction from './bank-transaction/routes';
import plaid from './plaid/routes';
// bulk-create cryptoWallet from './crypto-wallet';
// bulk-create stockPortfolio from './stock-portfolio';

const router = new Router<DefaultState, KoaContext>();

// app endpoints
router.use('/account', account.routes(), account.allowedMethods());
router.use('/auth', auth.routes(), auth.allowedMethods());
router.use('/bank-account', bankAccount.routes(), bankAccount.allowedMethods());
router.use('/bank-category', bankCategory.routes(), bankCategory.allowedMethods());
router.use('/bank-institution', bankInstitution.routes(), bankInstitution.allowedMethods());
router.use('/bank-tag', bankTag.routes(), bankTag.allowedMethods());
router.use('/bank-transaction', bankTransaction.routes(), bankTransaction.allowedMethods());
router.use('/plaid', plaid.routes(), plaid.allowedMethods());
// index.use('/crypto-wallet', cryptoWallet.routes(), cryptoWallet.allowedMethods());
// index.use('/stock-portfolio', stockPortfolio.routes(), stockPortfolio.allowedMethods());

// helper endpoints
router.get('ping', '/ping', appPing);

// catch all route (MUST be the final route on all routes)
router.all('catch-all', '/(.*)', appCatchAll);

export default router;
