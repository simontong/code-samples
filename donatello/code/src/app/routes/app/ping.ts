import { KoaContext } from '@/src/types/KoaContext';

export default async (ctx: KoaContext): Promise<void> => {
  // @ts-ignore
  ctx.body = 'pong';
};
