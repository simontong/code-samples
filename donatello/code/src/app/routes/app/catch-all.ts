import { HttpErrorNotFound } from '@/src/lib/errors';
export default async (): Promise<void> => {
  throw new HttpErrorNotFound();
};
