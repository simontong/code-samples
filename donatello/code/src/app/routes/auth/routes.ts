import Router from 'koa-router';
import { DefaultState } from 'koa';
import { KoaContext } from '@/src/types/KoaContext';
import koaBody from 'koa-body';
import attachUserOrThrow from '@/src/app/middlewares/attachUserOrThrow';
import * as authRegister from './register';
import * as authConfirmEmail from './confirm-email';
import * as authLogin from './login';
import * as authLogout from './logout';
import * as authLostPassword from './lost-password';
import * as authLostPasswordReset from './lost-password-reset';
import * as authTotp from './totp';

const router = new Router<DefaultState, KoaContext>();

// prettier-ignore
router.post(
  'auth.register',
  '/register',
  koaBody(),
  authRegister.route,
);

// prettier-ignore
router.post(
  'auth.confirm-email',
  '/confirm-email',
  koaBody(),
  authConfirmEmail.route,
);

// prettier-ignore
router.post(
  'auth.login',
  '/login',
  koaBody(),
  authLogin.route,
);

// prettier-ignore
router.post(
  'auth.totp',
  '/totp',
  koaBody(),
  authTotp.route,
);

// prettier-ignore
router.post(
  'auth.logout',
  '/logout',
  attachUserOrThrow,
  authLogout.route,
);

// prettier-ignore
router.post(
  'auth.lost-password',
  '/lost-password',
  koaBody(),
  authLostPassword.route,
);

// prettier-ignore
router.post(
  'auth.lost-password-reset',
  '/lost-password-reset',
  koaBody(),
  authLostPasswordReset.route,
);

export default router;
