import app from '@/src/app/app';
import createUserAndAuthenticate from '../../../../../test/utils/createUserAndAuthenticate';
import supertest from 'supertest';
import { StatusCodes } from 'http-status-codes';
import { rollbackDatabaseTransaction, startDatabaseTransaction } from '@/test/utils/databaseTransaction';

beforeEach(async () => {
  await startDatabaseTransaction();
});

afterEach(async () => {
  await rollbackDatabaseTransaction();
});

describe('routes: POST /auth/logout', () => {
  describe('failed logout', () => {
    /**
     * test
     */
    it('should return unauthorised on logout when not logged in', async () => {
      // prettier-ignore
      const res = await supertest(app.callback())
        .post('/auth/logout')
        .send();

      expect(res.status).toEqual(StatusCodes.UNAUTHORIZED);
      expect(res.body).toBeObject();
      // expect(res.bodyParser.code).toEqual(appCodes.E_USER_INVALID);
      expect(res.body.error).toMatch(/unauthorized/i);
    });
  });

  describe('successful logout', () => {
    /**
     * test
     */
    it('should return empty response and clear cookies', async () => {
      const { request } = await createUserAndAuthenticate();

      // prettier-ignore
      const res = await request
        .post('/auth/logout')
        .send();

      expect(res.status).toEqual(StatusCodes.NO_CONTENT);
      expect(res.body).toBeEmpty();
      expect(res.headers['set-cookies']).toBeUndefined();
    });
  });
});
