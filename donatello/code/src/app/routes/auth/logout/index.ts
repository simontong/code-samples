import { KoaContext } from '@/src/types/KoaContext';

export const route = async (ctx: KoaContext): Promise<void> => {
  ctx.deps.logger.info('User signed out: %s', ctx.session.userId);
  ctx.session.userId = null;

  ctx.jsend().noContent();
};
