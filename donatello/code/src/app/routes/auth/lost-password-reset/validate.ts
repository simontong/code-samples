import { AuthLostPasswordResetRequestDto } from '@/src/dtos/AuthLostPasswordResetRequestDto';
import validateOrThrow from '@/src/lib/validators/validateOrThrow';
import { userRules } from '@/src/rules/userRules';
import { KoaContext } from '@/src/types/KoaContext';
import { object, SchemaOf } from 'yup';

export const validate = (ctx: KoaContext): Promise<AuthLostPasswordResetRequestDto> => {
  const schema: SchemaOf<AuthLostPasswordResetRequestDto> = object({
    token: userRules.password_reset_token,
    password: userRules.password,
    passwordConfirm: userRules.passwordConfirm,
  });

  return validateOrThrow(schema, ctx.request.body);
};
