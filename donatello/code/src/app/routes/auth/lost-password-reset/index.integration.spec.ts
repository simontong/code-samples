import { factoryUser } from '@/database/factory';
import container from '@/src/app/container';
import app from '@/src/app/app';
import { makeAuthLostPasswordResetRequest } from '@/test/utils/requests/authRequest';
import supertest from 'supertest';
import { StatusCodes } from 'http-status-codes';
import appCodes from '@/src/consts/appCodes';
import { rollbackDatabaseTransaction, startDatabaseTransaction } from '@/test/utils/databaseTransaction';
import { verifyPassword } from '@/src/lib/crypto';

beforeEach(async () => {
  await startDatabaseTransaction();
});

afterEach(async () => {
  await rollbackDatabaseTransaction();
});

describe('routes: POST /auth/lost-password-reset', () => {
  describe('token validation', () => {
    /**
     * test
     */
    it('should fail on invalid token', async () => {
      const row = await factoryUser.create();
      // modify user to have password reset token
      await container.cradle.userService.updateUserWithPasswordResetToken(row);
      const data = await makeAuthLostPasswordResetRequest({
        token: 'bogus-token',
      });

      const spy = jest.spyOn(container.cradle.userService, 'updatePassword');

      // prettier-ignore
      const res = await supertest(app.callback())
        .post('/auth/lost-password-reset')
        .send(data);

      expect(res.status).toEqual(StatusCodes.BAD_REQUEST);
      expect(spy).toBeCalledTimes(0); // validation should catch it before it gets to run
      expect(res.body).toBeObject();
      expect(res.body.code).toEqual(appCodes.E_VALIDATION_FAILED);
      expect(res.body.error).toBeArray();
      expect(res.body.error).toEqual(expect.arrayContaining([{ path: 'token', error: 'Token is invalid' }]));
    });

    /**
     * test
     */
    it('should fail on expired token', async () => {
      jest.useFakeTimers();
      jest.setSystemTime(10);
      const row = await factoryUser.create();
      // modify user to have password reset token
      const newRow = await container.cradle.userService.updateUserWithPasswordResetToken(row);
      jest.useRealTimers();

      const data = await makeAuthLostPasswordResetRequest({
        // @ts-ignore - will always have password_reset_token from calling `reuseOrCreatePasswordResetToken`
        token: newRow.password_reset_token.trim(),
      });
      const spy = jest.spyOn(container.cradle.userService, 'updatePassword');

      // prettier-ignore
      const res = await supertest(app.callback())
        .post('/auth/lost-password-reset')
        .send(data);

      expect(res.status).toEqual(StatusCodes.UNAUTHORIZED);
      expect(spy).toBeCalledTimes(0); // validation should catch it before it gets to run
      expect(res.body).toBeObject();
      expect(res.body.code).toEqual(appCodes.E_TOKEN_EXPIRED);
      expect(res.body.error).toMatch(/token has expired/i);
    });
  });

  describe('successful password reset', () => {
    /**
     * test
     */
    it('should update their password', async () => {
      const row = await factoryUser.create();
      // modify user to have password reset token
      const newRow = await container.cradle.userService.updateUserWithPasswordResetToken(row);
      const data = await makeAuthLostPasswordResetRequest({
        // @ts-ignore - will always have password_reset_token from calling `reuseOrCreatePasswordResetToken`
        token: newRow.password_reset_token.trim(),
      });

      const spy = jest.spyOn(container.cradle.userService, 'updatePassword');

      // prettier-ignore
      const res = await supertest(app.callback())
        .post('/auth/lost-password-reset')
        .send(data);

      expect(res.status).toEqual(StatusCodes.NO_CONTENT);
      expect(res.body).toBeEmpty();
      expect(spy).toBeCalledTimes(1);
      // expect(res.body).toBeObject();
      // expect(res.body.data).toEqual(row.toJSON());

      const updatedRow = await container.cradle.userService.findUserById(row.id);
      await expect(verifyPassword(updatedRow.password, data.password)).resolves.toBeTrue();

      // should be set to null so it can't be re-used
      expect(updatedRow.password_reset_token).toBeNull();
    });
  });
});
