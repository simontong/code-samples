import { KoaContext } from '@/src/types/KoaContext';
import appCodes from '@/src/consts/appCodes';
import { HttpErrorUnauthorized } from '@/src/lib/errors';
import { validate } from './validate';

export const route = async (ctx: KoaContext): Promise<void> => {
  const { token, password } = await validate(ctx);

  // verify token hasn't expired
  if (ctx.deps.userService.hasPasswordResetTokenExpired(token)) {
    throw new HttpErrorUnauthorized('Token has expired').setCode(appCodes.E_TOKEN_EXPIRED);
  }

  // lookup user by reset token
  const user = await ctx.deps.userService.findUserByPasswordResetToken(token);
  if (!user) {
    throw new HttpErrorUnauthorized('Token does not match user').setCode(appCodes.E_TOKEN_INVALID);
  }

  // update password
  await ctx.deps.userService.updatePassword(user.id, password);
  ctx.deps.logger.info('User password updated: %s', user.id);

  // if we got here means token is valid
  ctx.jsend().noContent();
};
