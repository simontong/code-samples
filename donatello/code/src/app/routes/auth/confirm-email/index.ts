import { KoaContext } from '@/src/types/KoaContext';
import appCodes from '@/src/consts/appCodes';
import { HttpErrorUnauthorized } from '@/src/lib/errors';
import { validate } from './validate';

export const route = async (ctx: KoaContext): Promise<void> => {
  const { token } = await validate(ctx);

  // verify token hasn't expired
  if (ctx.deps.userService.hasEmailConfirmTokenExpired(token)) {
    throw new HttpErrorUnauthorized('Token has expired').setCode(appCodes.E_TOKEN_EXPIRED);
  }

  // lookup user by token
  const user = await ctx.deps.userService.findUserByEmailConfirmToken(token);
  if (!user) {
    throw new HttpErrorUnauthorized('Token does not match user').setCode(appCodes.E_TOKEN_INVALID);
  }

  // confirm email
  const updatedUser = await ctx.deps.userService.confirmEmail(user);

  ctx.jsend().data(updatedUser);
};
