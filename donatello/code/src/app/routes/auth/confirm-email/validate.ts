import { AuthConfirmEmailRequestDto } from '@/src/dtos/AuthConfirmEmailRequestDto';
import validateOrThrow from '@/src/lib/validators/validateOrThrow';
import { userRules } from '@/src/rules/userRules';
import { KoaContext } from '@/src/types/KoaContext';
import { object, SchemaOf } from 'yup';

export const validate = (ctx: KoaContext): Promise<AuthConfirmEmailRequestDto> => {
  const schema: SchemaOf<AuthConfirmEmailRequestDto> = object({
    token: userRules.email_confirm_token,
  });

  return validateOrThrow(schema, ctx.request.body);
};
