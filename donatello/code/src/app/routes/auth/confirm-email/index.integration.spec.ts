import container from '@/src/app/container';
import app from '@/src/app/app';
import { makeAuthConfirmEmailRequest, makeAuthRegisterRequest } from '@/test/utils/requests/authRequest';
import supertest from 'supertest';
import { StatusCodes } from 'http-status-codes';
import appCodes from '@/src/consts/appCodes';
import { factoryUser } from '@/database/factory';
import { rollbackDatabaseTransaction, startDatabaseTransaction } from '@/test/utils/databaseTransaction';

beforeEach(async () => {
  await startDatabaseTransaction();
});

afterEach(async () => {
  await rollbackDatabaseTransaction();
});

describe('routes: POST /auth/confirm-email', () => {
  describe('token validation', () => {
    /**
     * test
     */
    it('should fail on invalid token', async () => {
      const row = await factoryUser.create();
      const data = await makeAuthConfirmEmailRequest({
        token: 'bogus-' + row.email_confirm_token,
      });

      const spy = jest.spyOn(container.cradle.userService, 'confirmEmail');

      // prettier-ignore
      const res = await supertest(app.callback())
        .post('/auth/confirm-email')
        .send(data);

      expect(res.status).toEqual(StatusCodes.BAD_REQUEST);
      expect(spy).toBeCalledTimes(0); // validation should catch it before it gets to run
      expect(res.body).toBeObject();
      expect(res.body.code).toEqual(appCodes.E_VALIDATION_FAILED);
      expect(res.body.error).toBeArray();
      // prettier-ignore
      expect(res.body.error).toEqual(
        expect.arrayContaining([{ path: 'token', error: 'Token is invalid' }]),
      );
    });

    /**
     * test
     */
    it('should fail on expired token', async () => {
      jest.useFakeTimers();
      jest.setSystemTime(10);
      // use container.cradle.userService.createUser here because we need to create expiry token
      const row = await container.cradle.userService.createUser(await makeAuthRegisterRequest());
      jest.useRealTimers();

      const data = await makeAuthConfirmEmailRequest({
        // @ts-ignore - will always have token when using `container.cradle.userService.createUser`
        // we need to trim here because postgres stores full length of string and the
        // tiny expiry (10) will make padding on the right side of the token string
        token: row.email_confirm_token.trim(),
      });
      const spy = jest.spyOn(container.cradle.userService, 'confirmEmail');

      // prettier-ignore
      const res = await supertest(app.callback())
        .post('/auth/confirm-email')
        .send(data);

      expect(res.status).toEqual(StatusCodes.UNAUTHORIZED);
      expect(spy).toBeCalledTimes(0); // validation should catch it before it gets to run
      expect(res.body).toBeObject();
      expect(res.body.code).toEqual(appCodes.E_TOKEN_EXPIRED);
      expect(res.body.error).toMatch(/token has expired/i);
    });
  });

  describe('successful confirmation', () => {
    /**
     * test
     */
    it('should confirm their email', async () => {
      // use container.cradle.userService.createUser here because we need to create expiry token
      const row = await container.cradle.userService.createUser(await makeAuthRegisterRequest());
      const data = await makeAuthConfirmEmailRequest({
        // @ts-ignore - will always have token when using `container.cradle.userService.createUser`
        token: row.email_confirm_token,
      });

      const spy = jest.spyOn(container.cradle.userService, 'confirmEmail');

      // prettier-ignore
      const res = await supertest(app.callback())
        .post('/auth/confirm-email')
        .send(data);

      expect(res.status).toEqual(StatusCodes.OK);
      expect(spy).toBeCalledTimes(1);
      expect(res.body).toBeObject();
      expect(res.body.data).toEqual({
        ...row.toJSON(),
        new_email: null,
        email_confirm_token: null,
      });

      const updatedRow = await container.cradle.userService.findUserById(row.id);

      // should be set to null so it can't be re-used
      expect(updatedRow.email_confirm_token).toBeNull();
    });
  });
});
