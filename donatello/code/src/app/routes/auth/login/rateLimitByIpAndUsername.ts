import { HttpErrorTooManyRequest } from '@/src/lib/errors';
import { createHash } from 'crypto';
import { KoaContext } from '@/src/types/KoaContext';
import { makeRateLimiter } from '@/src/lib/rateLimiter';
import container from '@/src/app/container';

const rateLimiter = makeRateLimiter({
  storeClient: container.cradle.redis.client,
  keyPrefix: 'rateLimit:auth.login.byIpAndUsername',
  points: 10,
  duration: 60 * 60 * 24 * 90, // store number for 90 days since first fail
  blockDuration: 60 * 60, // block for 1 hour
});

export const rateLimitByIpAndUsername = async (ctx: KoaContext): Promise<void> => {
  const email = String(ctx.request.body.email);
  // prettier-ignore
  const parts = [
    ctx.request.ip.replace(/(:+|\.)/g, '_'),
    createHash('md5').update(email).digest('hex'),
  ];
  const key = parts.join(':');
  const isRateLimited = await rateLimiter(key);

  if (isRateLimited) {
    throw new HttpErrorTooManyRequest();
  }
};
