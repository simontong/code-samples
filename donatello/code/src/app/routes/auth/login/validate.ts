import { AuthLoginRequestDto } from '@/src/dtos/AuthLoginRequestDto';
import validateOrThrow from '@/src/lib/validators/validateOrThrow';
import { userRules } from '@/src/rules/userRules';
import { KoaContext } from '@/src/types/KoaContext';
import { object, SchemaOf } from 'yup';

export const validate = (ctx: KoaContext): Promise<AuthLoginRequestDto> => {
  const schema: SchemaOf<AuthLoginRequestDto> = object({
    email: userRules.email,
    password: userRules.password,
  });

  return validateOrThrow(schema, ctx.request.body);
};
