import { AuthLoginRequestDto } from '@/src/dtos/AuthLoginRequestDto';
import { makeAuthLoginRequest, makeAuthRegisterRequest } from '@/test/utils/requests/authRequest';
import container from '@/src/app/container';
import app from '@/src/app/app';
import supertest from 'supertest';
import { StatusCodes } from 'http-status-codes';
import { rollbackDatabaseTransaction, startDatabaseTransaction } from '@/test/utils/databaseTransaction';

beforeEach(async () => {
  await startDatabaseTransaction();
});

afterEach(async () => {
  await rollbackDatabaseTransaction();
});

describe('routes: POST /auth/login', () => {
  describe('validation failures', () => {
    /**
     * test
     */
    it('should fail on required fields', async () => {
      const data: AuthLoginRequestDto = {
        email: '',
        password: '',
      };

      const spy = jest.spyOn(container.cradle.userService, 'verifyCredentials');

      // prettier-ignore
      const res = await supertest(app.callback())
        .post('/auth/login')
        .send(data);

      expect(res.status).toEqual(StatusCodes.BAD_REQUEST);
      expect(spy).toBeCalledTimes(0); // validation should catch it before it gets to run
      expect(res.body).toBeObject();
      expect(res.body.error).toBeArray();
      expect(res.body.error).toEqual(expect.arrayContaining([{ path: 'email', error: 'Email is a required field' }]));
      expect(res.body.error).toEqual(
        expect.arrayContaining([{ path: 'password', error: 'Password is a required field' }]),
      );
    });

    /**
     * test
     */
    it('should fail on invalid email', async () => {
      const data = await makeAuthLoginRequest({ email: 'bogus-email.address' });
      const spy = jest.spyOn(container.cradle.userService, 'verifyCredentials');

      // prettier-ignore
      const res = await supertest(app.callback())
        .post('/auth/login')
        .send(data);

      expect(res.status).toEqual(StatusCodes.BAD_REQUEST);
      expect(spy).toBeCalledTimes(0); // validation should catch it before it gets to run
      expect(res.body).toBeObject();
      expect(res.body.error).toBeArray();
      expect(res.body.error).toEqual(
        // prettier-ignore
        expect.arrayContaining([{ path: 'email', error: 'Email must be a valid email' }]),
      );
    });

    /**
     * test
     */
    it('should return authentication failed for invalid credentials', async () => {
      const data = await makeAuthLoginRequest();
      const spy = jest.spyOn(container.cradle.userService, 'verifyCredentials');

      // prettier-ignore
      const res = await supertest(app.callback())
        .post('/auth/login')
        .send(data);

      expect(res.status).toEqual(StatusCodes.UNAUTHORIZED);
      expect(spy).toBeCalledTimes(1);
      expect(res.body.error).toMatch(/invalid credentials/i);
    });
  });

  describe('routes/auth success', () => {
    /**
     * test
     */
    it('should successfully login', async () => {
      const input = await makeAuthRegisterRequest();
      // use container.cradle.userService.createUser here because we need to hash password
      await container.cradle.userService.createUser(input);

      const data: AuthLoginRequestDto = {
        email: input.email,
        password: input.password,
      };

      const spy = jest.spyOn(container.cradle.userService, 'verifyCredentials');

      // prettier-ignore
      const res = await supertest(app.callback())
        .post('/auth/login')
        .send(data);

      expect(res.status).toEqual(StatusCodes.OK);
      expect(spy).toBeCalledTimes(1);
      expect(res.body).toBeObject();
      expect(res.body.data.password).toBeUndefined();
      expect(res.body.data).toEqual(
        expect.objectContaining({
          name: input.name,
          email: input.email,
        }),
      );

      expect(res.headers['set-cookie']).toBeArray();
      const cookie1 = res.headers['set-cookie'].find((i: string) => i.match(/^don:sess=.+/));
      const cookie2 = res.headers['set-cookie'].find((i: string) => i.match(/^don:sess\.sig=.+/));
      expect(cookie1).toBeString();
      expect(cookie2).toBeString();
    });
  });
});
