import { KoaContext } from '@/src/types/KoaContext';
import { HttpErrorUnauthorized } from '@/src/lib/errors';
import { validate } from './validate';
// import { rateLimitByIpAndUsername } from './rateLimitByIpAndUsername';

export const route = async (ctx: KoaContext): Promise<void> => {
  // await rateLimitByIpAndUsername(ctx); // todo: this needs to be revisited and enabled on live ...
  const { email, password } = await validate(ctx);

  // verify user
  const user = await ctx.deps.userService.verifyCredentials(email, password);
  if (!user) {
    throw new HttpErrorUnauthorized('Invalid credentials');
  }

  ctx.deps.logger.info('User signed in: %s', user.id);
  ctx.session.userId = user.id;

  ctx.jsend().data(user);
};
