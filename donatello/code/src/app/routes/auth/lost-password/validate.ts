import { AuthLostPasswordRequestDto } from '@/src/dtos/AuthLostPasswordRequestDto';
import validateOrThrow from '@/src/lib/validators/validateOrThrow';
import { userRules } from '@/src/rules/userRules';
import { KoaContext } from '@/src/types/KoaContext';
import { object, SchemaOf } from 'yup';

export const validate = (ctx: KoaContext): Promise<AuthLostPasswordRequestDto> => {
  const schema: SchemaOf<AuthLostPasswordRequestDto> = object({
    email: userRules.email,
  });

  return validateOrThrow(schema, ctx.request.body);
};
