import { factoryUser } from '@/database/factory';
import app from '@/src/app/app';
import { AuthLostPasswordRequestDto } from '@/src/dtos/AuthLostPasswordRequestDto';
import { SendResetPasswordEmail } from '@/src/jobs';
import { SendResetPasswordEmailJob } from '@/src/jobs/SendResetPasswordEmail';
import mailer from '@/src/lib/mail';
import User from '@/src/models/User';
import container from '@/src/app/container';
import { webClientUrl } from '@/src/utils/webClientUrl';
import { rollbackDatabaseTransaction, startDatabaseTransaction } from '@/test/utils/databaseTransaction';
import { makeAuthLostPasswordRequest } from '@/test/utils/requests/authRequest';
import { Queue } from 'bullmq';
import { StatusCodes } from 'http-status-codes';
import _ from 'lodash';
import Mustache from 'mustache';
import Mail from 'nodemailer/lib/mailer';
import supertest from 'supertest';

beforeEach(async () => {
  await startDatabaseTransaction();
});

afterEach(async () => {
  await rollbackDatabaseTransaction();
});

describe('routes: POST /auth/lost-password', () => {
  describe('validation failures', () => {
    /**
     * test
     */
    it('should fail on required fields', async () => {
      const data: AuthLostPasswordRequestDto = {
        email: '',
      };

      const spy = jest.spyOn(container.cradle.userService, 'updateUserWithPasswordResetToken');

      // prettier-ignore
      const res = await supertest(app.callback())
        .post('/auth/lost-password')
        .send(data);

      expect(res.status).toEqual(StatusCodes.BAD_REQUEST);
      expect(spy).toBeCalledTimes(0);
      expect(res.body).toBeObject();
      expect(res.body.error).toBeArray();
      expect(res.body.error).toEqual(expect.arrayContaining([{ path: 'email', error: 'Email is a required field' }]));
    });

    /**
     * test
     */
    it('should fail on invalid email', async () => {
      const data = await makeAuthLostPasswordRequest({
        email: 'bogus.email-address',
      });

      const spy = jest.spyOn(container.cradle.userService, 'updateUserWithPasswordResetToken');

      // prettier-ignore
      const res = await supertest(app.callback())
        .post('/auth/lost-password')
        .send(data);

      expect(res.status).toEqual(StatusCodes.BAD_REQUEST);
      expect(spy).toBeCalledTimes(0);
      expect(res.body).toBeObject();
      expect(res.body.error).toBeArray();
      expect(res.body.error).toEqual(expect.arrayContaining([{ path: 'email', error: 'Email must be a valid email' }]));
    });
  });

  describe('successful password reset request', () => {
    /**
     * test
     */
    it('should silently ignore request when email does not exist', async () => {
      const data = await makeAuthLostPasswordRequest();

      const spy = jest.spyOn(container.cradle.userService, 'updateUserWithPasswordResetToken');

      // prettier-ignore
      const res = await supertest(app.callback())
        .post('/auth/lost-password')
        .send(data);

      expect(res.status).toEqual(StatusCodes.NO_CONTENT);
      expect(spy).toBeCalledTimes(0); // does not get called because silently ignore
    });

    /**
     * test
     */
    it('should send user email with password reset link', async () => {
      const row = await factoryUser.create();
      const data = await makeAuthLostPasswordRequest({
        email: row.email,
      });

      // set up spies
      const spies = {
        reuseOrCreatePasswordResetToken: jest.spyOn(container.cradle.userService, 'updateUserWithPasswordResetToken'),

        // spy to make sure job added to queue
        addEmailToQueue: jest.spyOn(Queue.prototype, 'add'),

        // @ts-ignore
        sendMail: jest.spyOn(mailer, 'sendMail').mockImplementation(),
      };

      // prettier-ignore
      const res = await supertest(app.callback())
        .post('/auth/lost-password')
        .send(data);

      expect(res.status).toEqual(StatusCodes.NO_CONTENT);
      expect(spies.reuseOrCreatePasswordResetToken).toBeCalledTimes(1);

      expect(spies.addEmailToQueue).toBeCalledTimes(1);
      const mockJob = (await spies.addEmailToQueue.mock.results[0].value) as SendResetPasswordEmailJob;

      // trigger mock-send email
      await expect(SendResetPasswordEmail.handle(mockJob)).resolves.toBeUndefined();

      // check email was mock-sent and grab it
      expect(spies.sendMail).toBeCalledTimes(1);
      const email = _.get(spies, 'sendMail.mock.calls.0.0') as Mail.Options;

      expect(email).toBeObject();
      expect(email.to).toEqual(data.email);

      const user = await container.cradle.userService.findUserByEmail(String(email.to));
      expect(user).toBeInstanceOf(User);

      // check email bodyParser
      const bodies = [email.text, email.html];
      for (const body of bodies) {
        const { name, password_reset_token: token } = user;

        expect(body).toMatch(Mustache.escape(`Hi ${name}`));
        expect(body).toMatch('You recently requested to reset your password');
        expect(body).toMatch(webClientUrl(`/auth/lost-password-reset/${token}`));
      }
    });
  });
});
