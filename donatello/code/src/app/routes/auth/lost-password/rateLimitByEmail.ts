import container from '@/src/app/container';
import { HttpErrorTooManyRequest } from '@/src/lib/errors';
import { KoaContext } from '@/src/types/KoaContext';
import { makeRateLimiter } from '@/src/lib/rateLimiter';

const rateLimiter = makeRateLimiter({
  storeClient: container.cradle.redis.client,
  keyPrefix: 'rateLimit:auth.lost-password',
  points: 3,
  duration: 60 * 2,
});

export const rateLimitByEmail = async (ctx: KoaContext): Promise<void> => {
  const key = String(ctx.request.body.email);
  const isRateLimited = await rateLimiter(key);

  if (isRateLimited) {
    throw new HttpErrorTooManyRequest();
  }
};
