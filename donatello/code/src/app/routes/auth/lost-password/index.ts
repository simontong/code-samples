import { KoaContext } from '@/src/types/KoaContext';
import { dispatchJob } from '@/src/lib/queues';
import { SendResetPasswordEmail } from '@/src/jobs';
import { validate } from './validate';
import { rateLimitByEmail } from './rateLimitByEmail';

export const route = async (ctx: KoaContext): Promise<void> => {
  await rateLimitByEmail(ctx);
  const { email } = await validate(ctx);

  const user = await ctx.deps.userService.findUserByEmail(email);
  if (!user) {
    // silently ignore (response = 204) - avoids leaking registered users
    ctx.jsend().noContent();
    return;
  }

  // generate password token / reuse previous one
  await ctx.deps.userService.updateUserWithPasswordResetToken(user);

  // dispatch reset-password email to job queue
  await dispatchJob(
    new SendResetPasswordEmail({
      userId: user.id,
      userAgent: ctx.get('user-agent'),
    }),
  );

  // return user
  ctx.jsend().noContent();
};
