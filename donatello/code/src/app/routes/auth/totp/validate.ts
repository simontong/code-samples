import { AuthTotpRequestDto } from '@/src/dtos/AuthTotpRequestDto';
import validateOrThrow from '@/src/lib/validators/validateOrThrow';
import { userRules } from '@/src/rules/userRules';
import { KoaContext } from '@/src/types/KoaContext';
import { object, SchemaOf } from 'yup';

export const validate = (ctx: KoaContext): Promise<AuthTotpRequestDto> => {
  const schema: SchemaOf<AuthTotpRequestDto> = object({
    token: userRules.totpToken,
  });

  return validateOrThrow(schema, ctx.request.body);
};
