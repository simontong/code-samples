import { KoaContext } from '@/src/types/KoaContext';
import { dispatchJob } from '@/src/lib/queues';
import { SendWelcomeEmail } from '@/src/jobs';
import { validate } from './validate';

export const route = async (ctx: KoaContext): Promise<void> => {
  const input = await validate(ctx);

  // create new user
  const user = await ctx.deps.userService.createUser(input);
  ctx.deps.logger.info('User created: %s', user.id);

  // dispatch welcome email to job queue
  await dispatchJob(
    new SendWelcomeEmail({
      userId: user.id,
      userAgent: ctx.get('user-agent'),
    }),
  );

  // return user
  ctx.jsend().data(user);
};
