import { AuthRegisterRequestDto } from '@/src/dtos/AuthRegisterRequestDto';
import { userUnique } from '@/src/lib/validators/rules/recordUnique';
import validateOrThrow from '@/src/lib/validators/validateOrThrow';
import { userRules } from '@/src/rules/userRules';
import { KoaContext } from '@/src/types/KoaContext';
import { object, SchemaOf } from 'yup';

export const validate = (ctx: KoaContext): Promise<AuthRegisterRequestDto> => {
  const schema: SchemaOf<AuthRegisterRequestDto> = object({
    name: userRules.name,
    email: userRules.email.test('userUnique', 'Email already taken', userUnique('email')),
    password: userRules.password,
    passwordConfirm: userRules.passwordConfirm,
  });

  return validateOrThrow(schema, ctx.request.body);
};
