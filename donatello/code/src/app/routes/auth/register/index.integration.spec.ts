import { AuthRegisterRequestDto } from '@/src/dtos/AuthRegisterRequestDto';
import { webClientUrl } from '@/src/utils/webClientUrl';
import { makeAuthRegisterRequest } from '@/test/utils/requests/authRequest';
import Mail from 'nodemailer/lib/mailer';
import Mustache from 'mustache';
import User from '@/src/models/User';
import container from '@/src/app/container';
import _ from 'lodash';
import app from '@/src/app/app';
import mailer from '@/src/lib/mail';
import supertest from 'supertest';
import { Queue } from 'bullmq';
import { SendWelcomeEmail } from '@/src/jobs';
import { SendWelcomeEmailJob } from '@/src/jobs/SendWelcomeEmail';
import { StatusCodes } from 'http-status-codes';
import { factoryUser } from '@/database/factory';
import { rollbackDatabaseTransaction, startDatabaseTransaction } from '@/test/utils/databaseTransaction';

beforeEach(async () => {
  await startDatabaseTransaction();
});

afterEach(async () => {
  await rollbackDatabaseTransaction();
});

describe('routes: POST /auth/register', () => {
  describe('validation', () => {
    /**
     * test
     */
    it('should fail on required fields', async () => {
      // data object with empty fields and missing fields (password,passwordConfirm
      const data: Partial<AuthRegisterRequestDto> = {
        name: '',
        email: '',
      };

      const spy = jest.spyOn(container.cradle.userService, 'createUser');

      // prettier-ignore
      const res = await supertest(app.callback())
        .post('/auth/register')
        .send(data);

      expect(res.status).toEqual(StatusCodes.BAD_REQUEST);
      expect(spy).toBeCalledTimes(0); // validation should catch it before it gets to run
      expect(res.body).toBeObject();
      expect(res.body.error).toBeArray();
      expect(res.body.error).toEqual(
        // prettier-ignore
        expect.arrayContaining([{ path: 'name', error: 'Name is a required field' }]),
      );
      expect(res.body.error).toEqual(
        // prettier-ignore
        expect.arrayContaining([{ path: 'email', error: 'Email is a required field' }]),
      );
      expect(res.body.error).toEqual(
        // prettier-ignore
        expect.arrayContaining([{ path: 'password', error: 'Password is a required field' }]),
      );
    });

    /**
     * test
     */
    it('should fail on invalid email', async () => {
      const data = await makeAuthRegisterRequest({
        email: 'bogus.email-address',
      });

      const spy = jest.spyOn(container.cradle.userService, 'createUser');

      // prettier-ignore
      const res = await supertest(app.callback())
        .post('/auth/register')
        .send(data);

      expect(res.status).toEqual(StatusCodes.BAD_REQUEST);
      expect(spy).toBeCalledTimes(0); // validation should catch it before it gets to run
      expect(res.body).toBeObject();
      expect(res.body.error).toBeArray();
      expect(res.body.error).toEqual(
        // prettier-ignore
        expect.arrayContaining([{ path: 'email', error: 'Email must be a valid email' }]),
      );
    });

    /**
     * test
     */
    it('should fail on existing email', async () => {
      const user = await factoryUser.create();
      const data = await makeAuthRegisterRequest({
        email: user.email,
      });

      const spy = jest.spyOn(container.cradle.userService, 'createUser');

      // prettier-ignore
      const res = await supertest(app.callback())
        .post('/auth/register')
        .send(data);

      expect(res.status).toEqual(StatusCodes.BAD_REQUEST);
      expect(spy).toBeCalledTimes(0); // validation should catch it before it gets to run
      expect(res.body).toBeObject();
      expect(res.body.error).toBeArray();
      expect(res.body.error).toEqual(expect.arrayContaining([{ path: 'email', error: 'Email already taken' }]));
    });

    /**
     * test
     */
    it('should fail on passwords not matching', async () => {
      const data = await makeAuthRegisterRequest({
        password: 'password123xyz!',
        passwordConfirm: '$random123abc',
      });

      const spy = jest.spyOn(container.cradle.userService, 'createUser');

      // prettier-ignore
      const res = await supertest(app.callback())
        .post('/auth/register')
        .send(data);

      expect(res.status).toEqual(StatusCodes.BAD_REQUEST);
      expect(spy).toBeCalledTimes(0); // validation should catch it before it gets to run
      expect(res.body).toBeObject();
      expect(res.body.error).toBeArray();
      expect(res.body.error).toEqual(
        expect.arrayContaining([{ path: 'passwordConfirm', error: 'Passwords must match' }]),
      );
    });
  });

  describe('successful registration', () => {
    /**
     * test
     */
    it('should create a new user and send confirmation email', async () => {
      const data = await makeAuthRegisterRequest();

      // set up spies
      const spies = {
        createUser: jest.spyOn(container.cradle.userService, 'createUser'),

        // spy to make sure job added to queue
        addEmailToQueue: jest.spyOn(Queue.prototype, 'add'),

        // @ts-ignore
        sendMail: jest.spyOn(mailer, 'sendMail').mockImplementation(),
      };

      // prettier-ignore
      const res = await supertest(app.callback())
        .post('/auth/register')
        .send(data);

      expect(res.status).toEqual(StatusCodes.OK);
      expect(spies.createUser).toBeCalledTimes(1);
      expect(res.body.data).toBeObject();
      expect(res.body.data.password).toBeUndefined();
      expect(res.body.data).toEqual(
        expect.objectContaining({
          id: expect.toBeString(),
          name: data.name,
          email: data.email,
        }),
      );

      expect(spies.addEmailToQueue).toBeCalledTimes(1);
      const mockJob = (await spies.addEmailToQueue.mock.results[0].value) as SendWelcomeEmailJob;

      // trigger mock-send email
      await expect(SendWelcomeEmail.handle(mockJob)).resolves.toBeUndefined();

      // check email was mock-sent and grab it
      expect(spies.sendMail).toBeCalledTimes(1);
      const email = _.get(spies, 'sendMail.mock.calls.0.0') as Mail.Options;

      expect(email).toBeObject();
      expect(email.to).toEqual(data.email);

      const user = await container.cradle.userService.findUserByEmail(String(email.to));
      expect(user).toBeInstanceOf(User);

      // check email bodyParser
      const bodies = [email.text, email.html];
      for (const body of bodies) {
        // @ts-ignore
        const { name, email_confirm_token: token } = user;

        expect(body).toMatch(Mustache.escape(`Hi ${name}`));
        expect(body).toMatch(`Thank you for signing up with`);
        expect(body).toMatch(`verify your email`);
        expect(body).toMatch(webClientUrl(`/auth/confirm-email/${token}`));
      }
    });
  });
});
