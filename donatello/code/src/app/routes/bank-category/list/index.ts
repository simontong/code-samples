import { KoaContext } from '@/src/types/KoaContext';

export const route = async (ctx: KoaContext): Promise<void> => {
  const userId = ctx.state.user.id;
  const input = ctx.request.query;

  const bankCategories = await ctx.deps.bankCategoryService.paginateBankCategories(userId, input);

  ctx.jsend().data(bankCategories);
};
