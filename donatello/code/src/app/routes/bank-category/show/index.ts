import { KoaContext } from '@/src/types/KoaContext';

export const route = async (ctx: KoaContext): Promise<void> => {
  const bankCategory = await ctx.deps.bankCategoryService.findBankCategoryOrThrowNotFound(ctx.state.user.id, ctx.params.id);

  ctx.jsend().data(bankCategory);
};
