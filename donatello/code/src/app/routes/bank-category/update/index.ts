import { KoaContext } from '@/src/types/KoaContext';
import { validate } from './validate';

export const route = async (ctx: KoaContext): Promise<void> => {
  const id = ctx.params.id;
  const userId = ctx.state.user.id;

  // throw 404 if this resource doesn't belong to requesting user
  await ctx.deps.bankCategoryService.findBankCategoryOrThrowNotFound(userId, id);

  // validate
  const input = await validate(ctx);

  // update
  const bankCategory = await ctx.deps.bankCategoryService.updateBankCategory(userId, id, input);
  ctx.deps.logger.info('Bank category updated: %s', bankCategory.id);

  ctx.jsend().data(bankCategory);
};
