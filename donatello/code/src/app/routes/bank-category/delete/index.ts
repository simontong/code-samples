import { KoaContext } from '@/src/types/KoaContext';

export const route = async (ctx: KoaContext): Promise<void> => {
  const userId = ctx.state.user.id;
  const id = ctx.params.id;

  // throw 404 if this resource doesn't belong to requesting user
  await ctx.deps.bankCategoryService.findBankCategoryOrThrowNotFound(userId, id);

  const bankCategory = await ctx.deps.bankCategoryService.deleteBankCategory(userId, id);
  ctx.deps.logger.info('Bank category deleted: %s', bankCategory.id);

  ctx.jsend().noContent();
};
