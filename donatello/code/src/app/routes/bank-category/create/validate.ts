import { BankCategoryCreateRequestDto } from '@/src/dtos/BankCategoryCreateRequestDto';
import validateOrThrow from '@/src/lib/validators/validateOrThrow';
import { bankCategoryRules } from '@/src/rules/bankCategoryRules';
import { KoaContext } from '@/src/types/KoaContext';
import { object, SchemaOf } from 'yup';

export const validate = (ctx: KoaContext): Promise<BankCategoryCreateRequestDto> => {
  const schema: SchemaOf<BankCategoryCreateRequestDto> = object({
    name: bankCategoryRules.name,
  });

  return validateOrThrow(schema, ctx.request.body);
};
