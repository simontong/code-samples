import appCodes from '@/src/consts/appCodes';
import container from '@/src/app/container';
import app from '@/src/app/app';
import { BankCategoryCreateRequestDto } from '@/src/dtos/BankCategoryCreateRequestDto';
import { makeBankCategoryCreateRequest } from '@/test/utils/requests/bankCategoryRequest';
import createUserAndAuthenticate from '../../../../../test/utils/createUserAndAuthenticate';
import supertest from 'supertest';
import { StatusCodes } from 'http-status-codes';
import { rollbackDatabaseTransaction, startDatabaseTransaction } from '@/test/utils/databaseTransaction';

beforeEach(async () => {
  await startDatabaseTransaction();
});

afterEach(async () => {
  await rollbackDatabaseTransaction();
});

describe('routes: POST /bank-category/create', () => {
  describe('authorization', () => {
    /**
     * test
     */
    it('should fail on guest access', async () => {
      const data = await makeBankCategoryCreateRequest();

      // prettier-ignore
      const res = await supertest(app.callback())
        .post('/bank-category/create')
        .send(data);

      expect(res.status).toEqual(StatusCodes.UNAUTHORIZED);
      expect(res.body).toBeObject();
      expect(res.body.error).toMatch(/unauthorized/i);
    });
  });

  describe('validation', () => {
    /**
     * test
     */
    it('should fail on missing required fields', async () => {
      const { request } = await createUserAndAuthenticate();
      const data: Partial<BankCategoryCreateRequestDto> = {
        name: '',
      };

      // prettier-ignore
      const res = await request
        .post('/bank-category/create')
        .send(data);

      expect(res.status).toEqual(StatusCodes.BAD_REQUEST);
      expect(res.body).toBeObject();
      expect(res.body.code).toEqual(appCodes.E_VALIDATION_FAILED);
      expect(res.body.error).toBeArray();
      expect(res.body.error).toEqual(
        // prettier-ignore
        expect.arrayContaining([{ path: 'name', error: 'Name is a required field' }]),
      );
    });
  });

  describe('successful bank category creation', () => {
    /**
     * test
     */
    it('should create a new bank category', async () => {
      const { request } = await createUserAndAuthenticate();
      const data = await makeBankCategoryCreateRequest();

      const spy = jest.spyOn(container.cradle.bankCategoryService, 'createBankCategory');

      // prettier-ignore
      const res = await request
        .post('/bank-category/create')
        .send(data);

      expect(res.status).toEqual(StatusCodes.OK);
      expect(spy).toBeCalledTimes(1);
      expect(res.body.data).toBeObject();
      expect(res.body.data).toEqual({
        id: expect.toBeString(),
        name: data.name,
      });
    });
  });
});
