import { KoaContext } from '@/src/types/KoaContext';
import { validate } from './validate';

export const route = async (ctx: KoaContext): Promise<void> => {
  const userId = ctx.state.user.id;

  // validate
  const input = await validate(ctx);

  // create
  const bankCategory = await ctx.deps.bankCategoryService.createBankCategory(userId, input);
  ctx.deps.logger.info('Bank category created: %s', bankCategory.id);

  ctx.jsend().data(bankCategory);
};
