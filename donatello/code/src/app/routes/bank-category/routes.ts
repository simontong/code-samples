import Router from 'koa-router';
import { DefaultState } from 'koa';
import { KoaContext } from '@/src/types/KoaContext';
import koaBody from 'koa-body';
import regexes from '@/src/consts/regexes';
import attachUserOrThrow from '@/src/app/middlewares/attachUserOrThrow';
import * as bankCategoryList from './list';
import * as bankCategoryShow from './show';
import * as bankCategoryCreate from './create';
import * as bankCategoryUpdate from './update';
import * as bankCategoryDelete from './delete';

const router = new Router<DefaultState, KoaContext>();

// prettier-ignore
router.get(
  'bank-category.list',
  '/list',
  attachUserOrThrow,
  bankCategoryList.route,
);

// prettier-ignore
router.get(
  'bank-category.show',
  `/show/:id(${regexes.uuid})`,
  attachUserOrThrow,
  bankCategoryShow.route,
);

// prettier-ignore
router.post(
  'bank-category.create',
  '/create',
  attachUserOrThrow,
  koaBody(),
  bankCategoryCreate.route,
);

// prettier-ignore
router.post(
  'bank-category.update',
  `/update/:id(${regexes.uuid})`,
  attachUserOrThrow,
  koaBody(),
  bankCategoryUpdate.route,
);

// prettier-ignore
router.post(
  'bank-category.delete',
  `/delete/:id(${regexes.uuid})`,
  attachUserOrThrow,
  bankCategoryDelete.route,
);

export default router;
