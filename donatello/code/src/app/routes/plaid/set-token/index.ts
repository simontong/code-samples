import { KoaContext } from '@/src/types/KoaContext';
import { validate } from './validate';

export const route = async (ctx: KoaContext): Promise<void> => {
  const userId = ctx.state.user.id;
  const { publicToken } = await validate(ctx);

  // fetch token and item information for db storage
  await ctx.deps.plaidItemService.exchangeTokenAndCreatePlaidItem(userId, publicToken);

  ctx.jsend().noContent();
};
