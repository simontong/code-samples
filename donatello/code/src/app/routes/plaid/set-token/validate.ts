import { PlaidSetTokenRequestDto } from '@/src/dtos/PlaidSetTokenRequestDto';
import validateOrThrow from '@/src/lib/validators/validateOrThrow';
import { plaidRules } from '@/src/rules/plaidRules';
import { KoaContext } from '@/src/types/KoaContext';
import { object, SchemaOf } from 'yup';

export const validate = (ctx: KoaContext): Promise<PlaidSetTokenRequestDto> => {
  const schema: SchemaOf<PlaidSetTokenRequestDto> = object({
    publicToken: plaidRules.publicToken,
  });

  return validateOrThrow(schema, ctx.request.body);
};
