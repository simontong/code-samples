import { HttpErrorUnauthorized } from '@/src/lib/errors';
import { KoaContext } from '@/src/types/KoaContext';

/**
 * allow ip to trigger webhook
 */
export async function authorize(ctx: KoaContext): Promise<void> {
  const ip = ctx.request.ip;

  // https://plaid.com/docs/api/webhooks/
  // Note that these IP addresses are subject to change.
  // validate the webhook came from these ips
  const webhookIps = [
    '52.21.26.131',
    '52.21.47.157',
    '52.41.247.19',
    '52.88.82.239',
  ];

  // if ip in allowed ips then go no futher
  if (webhookIps.includes(ip)) {
    return;
  }

  throw new HttpErrorUnauthorized('IP not in allowed IPs');
}
