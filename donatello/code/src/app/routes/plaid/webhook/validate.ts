import { PlaidWebhookRequestDto } from '@/src/dtos/PlaidWebhookRequestDto';
import validateOrThrow from '@/src/lib/validators/validateOrThrow';
import { plaidRules } from '@/src/rules/plaidRules';
import { KoaContext } from '@/src/types/KoaContext';
import { object, SchemaOf } from 'yup';

export const validate = (ctx: KoaContext): Promise<PlaidWebhookRequestDto> => {
  const schema: SchemaOf<PlaidWebhookRequestDto> = object({
    webhook_type: plaidRules.webhookType,
    webhook_code: plaidRules.webhookCode,
  });

  return validateOrThrow(schema, ctx.request.body);
};
