import { KoaContext } from '@/src/types/KoaContext';
import { authorize } from './authorize';
import { validate } from './validate';


export const route = async (ctx: KoaContext): Promise<void> => {
  // authorize access
  await authorize(ctx);

  // validate
  const input = await validate(ctx);

  // handle incoming webhook
  const { pushData } = await ctx.deps.plaidApiService.handleWebhook(input);

// const { webhook_type: webhookType } = req.body;
  // const { io } = req;
  // const type = webhookType.toLowerCase();
  // // There are five types of webhooks: AUTH, TRANSACTIONS, ITEM, INCOME, and ASSETS.
  // // @TODO implement handling for remaining webhook types.
  // const webhookHandlerMap = {
  //   transactions: handleTransactionsWebhook,
  //   item: handleItemWebhook,
  // };
  // const webhookHandler = webhookHandlerMap[type] || unhandledWebhook;
  // webhookHandler(req.body, io);
  // res.json({ status: 'ok' });
};
