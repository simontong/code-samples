import Router from 'koa-router';
import { DefaultState } from 'koa';
import { KoaContext } from '@/src/types/KoaContext';
import koaBody from 'koa-body';
import attachUserOrThrow from '@/src/app/middlewares/attachUserOrThrow';
import * as plaidCreateLinkToken from './create-link-token';
import * as plaidSetToken from './set-token';

const router = new Router<DefaultState, KoaContext>();

// prettier-ignore
router.post(
  'plaid.create-link-token',
  '/create-link-token',
  attachUserOrThrow,
  plaidCreateLinkToken.route,
);

// prettier-ignore
router.post(
  'plaid.set-token',
  '/set-token',
  attachUserOrThrow,
  koaBody(),
  plaidSetToken.route,
);

export default router;
