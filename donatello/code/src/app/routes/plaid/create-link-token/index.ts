import _ from 'lodash';
import { KoaContext } from '@/src/types/KoaContext';

export const route = async (ctx: KoaContext): Promise<void> => {
  const userId = ctx.state.user.id;

  const res = await ctx.deps.plaidApiService.linkTokenCreate(userId);

  const data = _.pick(res.data, [
    'expiration',
    'link_token',
    'request_id',
  ]);

  ctx.jsend().data(data);
};
