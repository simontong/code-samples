import * as bankTransactionExport from '@/src/app/routes/bank-transaction/export';
import Router from 'koa-router';
import { DefaultState } from 'koa';
import { KoaContext } from '@/src/types/KoaContext';
import koaBody from 'koa-body';
import regexes from '@/src/consts/regexes';
import attachUserOrThrow from '@/src/app/middlewares/attachUserOrThrow';
import * as bankAccountExport from './export';
import * as bankAccountList from './list';
import * as bankAccountShow from './show';
import * as bankAccountCreate from './create';
import * as bankAccountUpdate from './update';
import * as bankAccountDelete from './delete';

const router = new Router<DefaultState, KoaContext>();

// prettier-ignore
router.get(
  'bank-account.list',
  '/list',
  attachUserOrThrow,
  bankAccountList.route,
);

// prettier-ignore
router.get(
  'bank-account.export',
  '/export',
  attachUserOrThrow,
  bankAccountExport.route,
);

// prettier-ignore
router.get(
  'bank-account.show',
  `/show/:id(${regexes.uuid})`,
  attachUserOrThrow,
  bankAccountShow.route,
);

// prettier-ignore
router.post(
  'bank-account.create',
  '/create',
  attachUserOrThrow,
  koaBody(),
  bankAccountCreate.route,
);

// prettier-ignore
router.post(
  'bank-account.update',
  `/update/:id(${regexes.uuid})`,
  attachUserOrThrow,
  koaBody(),
  bankAccountUpdate.route,
);

// prettier-ignore
router.post(
  'bank-account.delete',
  `/delete/:id(${regexes.uuid})`,
  attachUserOrThrow,
  bankAccountDelete.route,
);

export default router;
