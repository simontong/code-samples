import { KoaContext } from '@/src/types/KoaContext';

export const route = async (ctx: KoaContext): Promise<void> => {
  const userId = ctx.state.user.id;
  const id = ctx.params.id;

  // throw 404 if this resource doesn't belong to requesting user
  await ctx.deps.bankAccountService.findBankAccountOrThrowNotFound(userId, id);

  const bankAccount = await ctx.deps.bankAccountService.deleteBankAccount(userId, id);
  ctx.deps.logger.info('Bank account deleted: %s', bankAccount.id);

  ctx.jsend().noContent();
};
