import { KoaContext } from '@/src/types/KoaContext';
import { validate } from './validate';

export const route = async (ctx: KoaContext): Promise<void> => {
  const id = ctx.params.id;
  const userId = ctx.state.user.id;

  // throw 404 if this resource doesn't belong to requesting user
  await ctx.deps.bankAccountService.findBankAccountOrThrowNotFound(userId, id);

  // validate
  const input = await validate(ctx);

  // update
  const bankAccount = await ctx.deps.bankAccountService.updateBankAccount(userId, id, input);
  ctx.deps.logger.info('Bank account updated: %s', bankAccount.id);

  // load relations
  await ctx.deps.bankAccountService.loadRelations(bankAccount);

  ctx.jsend().data(bankAccount);
};
