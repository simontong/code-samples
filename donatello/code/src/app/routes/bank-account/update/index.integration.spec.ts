import container from '@/src/app/container';
import appCodes from '@/src/consts/appCodes';
import app from '@/src/app/app';
import { ankAccountUpdateRequestDto } from '@/src/dtos/BankAccountUpdateRequestDto';
import { makeBankAccountUpdateRequest } from '@/test/utils/requests/bankAccountRequest';
import createUserAndAuthenticate from '../../../../../test/utils/createUserAndAuthenticate';
import supertest from 'supertest';
import { StatusCodes } from 'http-status-codes';
import { factoryBankAccount } from '@/database/factory';
import { rollbackDatabaseTransaction, startDatabaseTransaction } from '@/test/utils/databaseTransaction';

beforeEach(async () => {
  await startDatabaseTransaction();
});

afterEach(async () => {
  await rollbackDatabaseTransaction();
});

describe('routes: POST /bank-account/update/:id', () => {
  describe('authorization', () => {
    /**
     * test
     */
    it('should fail on guest access', async () => {
      const { user } = await createUserAndAuthenticate();
      const row = await factoryBankAccount.create({ user_id: user.id });
      const data = await makeBankAccountUpdateRequest();

      // prettier-ignore
      const res = await supertest(app.callback())
        .post(`/bank-account/update/${row.id}`)
        .send(data);

      expect(res.status).toEqual(StatusCodes.UNAUTHORIZED);
      expect(res.body).toBeObject();
      expect(res.body.error).toMatch(/unauthorized/i);
    });

    /**
     * test
     */
    it('should fail on attempting access to another users resource', async () => {
      const { user: otherUser } = await createUserAndAuthenticate();
      const { request } = await createUserAndAuthenticate();
      const row = await factoryBankAccount.create({ user_id: otherUser.id });
      const data = await makeBankAccountUpdateRequest();

      // prettier-ignore
      const res = await request
        .post(`/bank-account/update/${row.id}`)
        .send(data);

      expect(res.status).toEqual(StatusCodes.NOT_FOUND);
      expect(res.body).toBeObject();
      expect(res.body.error).toMatch(/not found/i);
    });
  });

  describe('validation', () => {
    /**
     * test
     */
    it('should fail on missing required fields', async () => {
      const { request, user } = await createUserAndAuthenticate();
      const row = await factoryBankAccount.create({ user_id: user.id });
      const data: Partial<ankAccountUpdateRequestDto> = {
        name: '',
      };

      // prettier-ignore
      const res = await request
        .post(`/bank-account/update/${row.id}`)
        .send(data);

      expect(res.status).toEqual(StatusCodes.BAD_REQUEST);
      expect(res.body).toBeObject();
      expect(res.body.code).toEqual(appCodes.E_VALIDATION_FAILED);
      expect(res.body.error).toBeArray();
      expect(res.body.error).toEqual(
        // prettier-ignore
        expect.arrayContaining([{ path: 'name', error: 'Name is a required field' }]),
      );
    });
  });

  describe('successful bank account update', () => {
    /**
     * test
     */
    it('should update a bank account', async () => {
      const { request, user } = await createUserAndAuthenticate();
      const row = await factoryBankAccount.create({
        user_id: user.id,
        name: 'Old Bank Account Name',
      });
      const data = await makeBankAccountUpdateRequest({
        bankInstitution: 'HSBC Bank',
      });

      const spy = jest.spyOn(container.cradle.bankAccountService, 'updateBankAccount');

      // prettier-ignore
      const res = await request
        .post(`/bank-account/update/${row.id}`)
        .send(data);

      expect(res.status).toEqual(StatusCodes.OK);
      expect(spy).toBeCalledTimes(1);
      expect(res.body.data).toBeObject();
      expect(res.body.data).toEqual(
        expect.objectContaining({
          id: row.id,
          name: data.name,

          // relations
          bankInstitution: expect.objectContaining({ name: data.bankInstitution }),
        }),
      );
    });
  });
});
