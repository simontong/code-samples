import { ankAccountUpdateRequestDto } from '@/src/dtos/BankAccountUpdateRequestDto';
import validateOrThrow from '@/src/lib/validators/validateOrThrow';
import { bankAccountRules } from '@/src/rules/bankAccountRules';
import { KoaContext } from '@/src/types/KoaContext';
import { object, SchemaOf } from 'yup';

export const validate = (ctx: KoaContext): Promise<ankAccountUpdateRequestDto> => {
  const schema: SchemaOf<ankAccountUpdateRequestDto> = object({
    name: bankAccountRules.name,
    bankInstitution: bankAccountRules.bankInstitution,
  });

  return validateOrThrow(schema, ctx.request.body);
};
