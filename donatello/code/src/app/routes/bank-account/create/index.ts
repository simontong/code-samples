import { KoaContext } from '@/src/types/KoaContext';
import { validate } from './validate';

export const route = async (ctx: KoaContext): Promise<void> => {
  const userId = ctx.state.user.id;

  // validate
  const input = await validate(ctx);

  // create
  const bankAccount = await ctx.deps.bankAccountService.createBankAccount(userId, input);
  ctx.deps.logger.info('Bank account created: %s', bankAccount.id);

  // load relations
  await ctx.deps.bankAccountService.loadRelations(bankAccount);

  ctx.jsend().data(bankAccount);
};
