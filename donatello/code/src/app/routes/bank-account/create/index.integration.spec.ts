import container from '@/src/app/container';
import appCodes from '@/src/consts/appCodes';
import app from '@/src/app/app';
import { BankAccountCreateRequestDto } from '@/src/dtos/BankAccountCreateRequestDto';
import { makeBankAccountCreateRequest } from '@/test/utils/requests/bankAccountRequest';
import createUserAndAuthenticate from '../../../../../test/utils/createUserAndAuthenticate';
import supertest from 'supertest';
import { StatusCodes } from 'http-status-codes';
import { rollbackDatabaseTransaction, startDatabaseTransaction } from '@/test/utils/databaseTransaction';

beforeEach(async () => {
  await startDatabaseTransaction();
});

afterEach(async () => {
  await rollbackDatabaseTransaction();
});

describe('routes: POST /bank-account/create', () => {
  describe('authorization', () => {
    /**
     * test
     */
    it('should fail on guest access', async () => {
      const data = await makeBankAccountCreateRequest();

      // prettier-ignore
      const res = await supertest(app.callback())
        .post('/bank-account/create')
        .send(data);

      expect(res.status).toEqual(StatusCodes.UNAUTHORIZED);
      expect(res.body).toBeObject();
      expect(res.body.error).toMatch(/unauthorized/i);
    });
  });

  describe('validation', () => {
    /**
     * test
     */
    it('should fail on missing required fields', async () => {
      const { request } = await createUserAndAuthenticate();
      const data: Partial<BankAccountCreateRequestDto> = {
        name: '',
      };

      // prettier-ignore
      const res = await request
        .post('/bank-account/create')
        .send(data);

      expect(res.status).toEqual(StatusCodes.BAD_REQUEST);
      expect(res.body).toBeObject();
      expect(res.body.code).toEqual(appCodes.E_VALIDATION_FAILED);
      expect(res.body.error).toBeArray();
      expect(res.body.error).toEqual(
        // prettier-ignore
        expect.arrayContaining([{ path: 'name', error: 'Name is a required field' }]),
      );
    });
  });

  describe('successful bank account creation', () => {
    /**
     * test
     */
    it('should create a new bank account', async () => {
      const { request } = await createUserAndAuthenticate();
      const data = await makeBankAccountCreateRequest({
        bankInstitution: 'Chase Bank',
      });

      const spy = jest.spyOn(container.cradle.bankAccountService, 'createBankAccount');

      // prettier-ignore
      const res = await request
        .post('/bank-account/create')
        .send(data);

      expect(res.status).toEqual(StatusCodes.OK);
      expect(spy).toBeCalledTimes(1);
      expect(res.body.data).toBeObject();
      expect(res.body.data).toEqual(
        expect.objectContaining({
          id: expect.toBeString(),
          name: data.name,

          // relations
          bankInstitution: expect.objectContaining({ name: data.bankInstitution }),
        }),
      );
    });
  });
});
