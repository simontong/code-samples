import { factoryBankAccount } from '@/database/factory';
import { BankAccountShape } from '@/src/models/BankAccount';
import { BankTransactionShape } from '@/src/models/BankTransaction';
import _ from 'lodash';
import app from '@/src/app/app';
import createUserAndAuthenticate from '../../../../../test/utils/createUserAndAuthenticate';
import supertest from 'supertest';
import { StatusCodes } from 'http-status-codes';
import { rollbackDatabaseTransaction, startDatabaseTransaction } from '@/test/utils/databaseTransaction';

beforeEach(async () => {
  await startDatabaseTransaction();
});

afterEach(async () => {
  await rollbackDatabaseTransaction();
});

describe('routes: GET /bank-account/list', () => {
  describe('authorization', () => {
    /**
     * test
     */
    it('should fail on guest access', async () => {
      const { user } = await createUserAndAuthenticate();
      const count = 3;
      await Promise.all(_.times(count, () => factoryBankAccount.create({ user_id: user.id })));

      // prettier-ignore
      const res = await supertest(app.callback())
        .get(`/bank-account/list`);

      expect(res.status).toEqual(StatusCodes.UNAUTHORIZED);
      expect(res.body).toBeObject();
      expect(res.body.error).toMatch(/unauthorized/i);
    });
  });

  describe('successful list bank accounts', () => {
    /**
     * test
     */
    it('should return bank accounts', async () => {
      const { request, user } = await createUserAndAuthenticate();
      const count = 10;
      const rows = await Promise.all(_.times(count, () => factoryBankAccount.create({ user_id: user.id })));
      const rowsJson = rows.map((i) => i.toJSON());

      // prettier-ignore
      const res = await request
        .get(`/bank-account/list`);

      expect(res.status).toEqual(StatusCodes.OK);
      expect(res.body).toBeObject();
      expect(res.body.data).toBeObject();
      expect(res.body.data.total).toEqual(count);
      expect(res.body.data.currentPage).toEqual(1);
      expect(res.body.data.lastPage).toEqual(1);
      expect(res.body.data.results).toHaveLength(count);

      const differences = [];
      for (const row of rowsJson) {
        const find = res.body.data.results.find((k: BankAccountShape) => k.id === row.id);

        // @ts-ignore
        const diff = _.reduce(
          row,
          // @ts-ignore
          (result, value, key) => (_.isEqual(value, find[key]) ? result : result.concat(key)),
          [],
        );

        if (diff.length) {
          differences.push({ find, row, diff });
        }
      }

      expect(differences).toHaveLength(0);

    });

    /**
     * test
     */
    it('should return bank accounts sorted', async () => {
      const { request, user } = await createUserAndAuthenticate();
      const count = 20;
      const rows = await Promise.all(_.times(count, () => factoryBankAccount.create({ user_id: user.id })));

      // prettier-ignore
      const res = await request
        .get(`/bank-account/list?sort=name`);

      expect(res.status).toEqual(StatusCodes.OK);
      expect(res.body).toBeObject();
      expect(res.body.data).toBeObject();
      expect(res.body.data.results).toHaveLength(count);

      const unsortedIds = _.map(rows, 'id');
      const sortedIds = _.chain(rows).sortBy('name').map('id').value();
      const resultIds = _.map(res.body.data.results, 'id');

      expect(unsortedIds).not.toEqual(sortedIds);
      expect(resultIds).toEqual(sortedIds);
    });

    /**
     * test
     */
    it('should return bank accounts filtered', async () => {
      const { request, user } = await createUserAndAuthenticate();
      const count = 20;
      await Promise.all(_.times(count, () => factoryBankAccount.create({ user_id: user.id })));

      // add our unique rows
      const resultCount = 3;
      // prettier-ignore
      await Promise.all(
        _.times(resultCount, n => factoryBankAccount.create({ user_id: user.id, name: `myUniqName abc${n}` })),
      );

      // prettier-ignore
      const res = await request
        .get(`/bank-account/list?filter=name@ts:myUniqName`);

      expect(res.status).toEqual(StatusCodes.OK);
      expect(res.body).toBeObject();
      expect(res.body.data).toBeObject();
      expect(res.body.data.results).toHaveLength(resultCount);
    });

    /**
     * test
     */
    it('should return bank accounts searched', async () => {
      const { request, user } = await createUserAndAuthenticate();
      const count = 20;
      await Promise.all(_.times(count, () => factoryBankAccount.create({ user_id: user.id })));

      // add our unique rows
      const resultCount = 10;
      // prettier-ignore
      await Promise.all(
        _.times(resultCount, n => factoryBankAccount.create({ user_id: user.id, name: `anotherName abc${n}` })),
      );

      // prettier-ignore
      const res = await request
        .get(`/bank-account/list?search=anotherName`);

      expect(res.status).toEqual(StatusCodes.OK);
      expect(res.body).toBeObject();
      expect(res.body.data).toBeObject();
      expect(res.body.data.results).toHaveLength(resultCount);
    });

    /**
     * test
     */
    it('should return bank accounts next page', async () => {
      const { request, user } = await createUserAndAuthenticate();
      const count = 40;
      const pageSize = 10;
      const pageCount = Math.ceil(count / pageSize);
      await Promise.all(_.times(count, () => factoryBankAccount.create({ user_id: user.id })));

      // prettier-ignore
      let resultIdsPrev = [];
      for (let page = 0; page < pageCount; page++) {
        const res = await request.get(`/bank-account/list?pageSize=${pageSize}&page=${page}`);

        expect(res.status).toEqual(StatusCodes.OK);
        expect(res.body).toBeObject();
        expect(res.body.data).toBeObject();
        expect(res.body.data.total).toEqual(count);
        expect(res.body.data.lastPage).toEqual(pageCount);
        expect(res.body.data.results.length).toBeLessThanOrEqual(pageSize);

        // compare previous pages ids
        const resultIds = _.map(res.body.data.results, 'id');
        expect(resultIds).not.toEqual(resultIdsPrev);

        resultIdsPrev = resultIds;
      }
    });
  });
});
