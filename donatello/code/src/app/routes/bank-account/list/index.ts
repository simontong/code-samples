import { KoaContext } from '@/src/types/KoaContext';

export const route = async (ctx: KoaContext): Promise<void> => {
  const userId = ctx.state.user.id;
  const input = ctx.request.query;

  // paginate
  const bankAccounts = await ctx.deps.bankAccountService.paginateBankAccounts(userId, input);

  ctx.jsend().data(bankAccounts);
};
