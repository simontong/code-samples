import { KoaContext } from '@/src/types/KoaContext';

export const route = async (ctx: KoaContext): Promise<void> => {
  const userId = ctx.state.user.id;
  const input = ctx.request.query;

  // create stream
  const stream = await ctx.deps.bankAccountService.exportBankAccounts(userId, input);

  // stream body
  ctx.stream(stream);
};
