import { KoaContext } from '@/src/types/KoaContext';

export const route = async (ctx: KoaContext): Promise<void> => {
  const bankAccount = await ctx.deps.bankAccountService.findBankAccountOrThrowNotFound(ctx.state.user.id, ctx.params.id);

  // load relations
  await ctx.deps.bankAccountService.loadRelations(bankAccount);

  ctx.jsend().data(bankAccount);
};
