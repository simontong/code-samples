import app from '@/src/app/app';
import createUserAndAuthenticate from '@/test/utils/createUserAndAuthenticate';
import supertest from 'supertest';
import { StatusCodes } from 'http-status-codes';
import { rollbackDatabaseTransaction, startDatabaseTransaction } from '@/test/utils/databaseTransaction';

beforeEach(async () => {
  await startDatabaseTransaction();
});

afterEach(async () => {
  await rollbackDatabaseTransaction();
});

describe('routes: GET /account/profile', () => {
  describe('get profile', () => {
    /**
     * test
     */
    it('should return profile', async () => {
      const { request, user } = await createUserAndAuthenticate();

      // prettier-ignore
      const res = await request
        .get('/account/profile');

      expect(res.status).toEqual(StatusCodes.OK);
      expect(res.status).toBe(StatusCodes.OK);
      expect(res.body.data).toEqual(user.toJSON());
    });

    /**
     * test
     */
    it('should return unauthorized without cookies', async () => {
      // prettier-ignore
      const res = await supertest(app.callback())
        .get('/account/profile');

      expect(res.status).toEqual(StatusCodes.UNAUTHORIZED);
      expect(res.body).toBeObject();
      // expect(res.bodyParser.code).toEqual(appCodes.E_USER_INVALID);
      expect(res.body.error).toMatch(/unauthorized/i);
    });
  });
});
