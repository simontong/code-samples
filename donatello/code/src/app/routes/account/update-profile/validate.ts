import { AccountUpdateProfileRequestDto } from '@/src/dtos/AccountUpdateProfileRequestDto';
import { userUnique } from '@/src/lib/validators/rules/recordUnique';
import validateOrThrow from '@/src/lib/validators/validateOrThrow';
import { userRules } from '@/src/rules/userRules';
import { KoaContext } from '@/src/types/KoaContext';
import { object, SchemaOf } from 'yup';

export const validate = (ctx: KoaContext): Promise<AccountUpdateProfileRequestDto> => {
  const userId = ctx.state.user.id;

  const schema: SchemaOf<AccountUpdateProfileRequestDto> = object({
    name: userRules.name,
    email: userRules.email.test('userUnique', 'Email already taken', userUnique('email', ['id', userId])),
  });

  return validateOrThrow(schema, ctx.request.body);
};
