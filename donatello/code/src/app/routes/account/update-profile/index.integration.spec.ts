import { factoryUser } from '@/database/factory';
import { AccountUpdateProfileRequestDto } from '@/src/dtos/AccountUpdateProfileRequestDto';
import { SendEmailConfirmEmail, SendEmailConfirmEmailJob } from '@/src/jobs/SendEmailConfirmEmail';
import mailer from '@/src/lib/mail';
import User from '@/src/models/User';
import container from '@/src/app/container';
import { webClientUrl } from '@/src/utils/webClientUrl';
import createUserAndAuthenticate from '@/test/utils/createUserAndAuthenticate';
import { rollbackDatabaseTransaction, startDatabaseTransaction } from '@/test/utils/databaseTransaction';
import { makeAccountUpdateProfileRequest } from '@/test/utils/requests/accountRequests';
import { Queue } from 'bullmq';
import { StatusCodes } from 'http-status-codes';
import _ from 'lodash';
import Mustache from 'mustache';
import Mail from 'nodemailer/lib/mailer';

beforeEach(async () => {
  await startDatabaseTransaction();
});

afterEach(async () => {
  await rollbackDatabaseTransaction();
});

describe('routes: POST /account/update-profile', () => {
  describe('validation fails', () => {
    /**
     * test
     */
    it('should fail on required fields missing', async () => {
      const { request } = await createUserAndAuthenticate(true);
      const data: Partial<AccountUpdateProfileRequestDto> = {
        name: '',
      };

      const spy = jest.spyOn(container.cradle.userService, 'updateUser');

      // prettier-ignore
      const res = await request
        .post('/account/update-profile')
        .send(data);

      expect(res.status).toEqual(StatusCodes.BAD_REQUEST);
      expect(spy).toBeCalledTimes(0);
      expect(res.body).toBeObject();
      expect(res.body.error).toBeArray();
      expect(res.body.error).toEqual(expect.arrayContaining([{ path: 'name', error: 'Name is a required field' }]));
      expect(res.body.error).toEqual(expect.arrayContaining([{ path: 'email', error: 'Email is a required field' }]));
    });

    /**
     * test
     */
    it('should fail on change email to another existing account with the same email', async () => {
      const otherUser = await factoryUser.create();
      const { request } = await createUserAndAuthenticate(true);
      const data = await makeAccountUpdateProfileRequest({
        email: otherUser.email,
      });

      const spy = jest.spyOn(container.cradle.userService, 'updateUser');

      // prettier-ignore
      const res = await request
        .post('/account/update-profile')
        .send(data);

      expect(res.status).toEqual(StatusCodes.BAD_REQUEST);
      expect(spy).toBeCalledTimes(0);
      expect(res.body).toBeObject();
      expect(res.body.error).toBeArray();
      expect(res.body.error).toEqual(expect.arrayContaining([{ path: 'email', error: 'Email already taken' }]));
    });
  });

  describe('successful profile update', () => {
    /**
     * test
     */
    it('should not dispatch confirm email unless email changed', async () => {
      const { request, user } = await createUserAndAuthenticate(true);
      const data = await makeAccountUpdateProfileRequest({
        email: user.email,
      });

      // set up spies
      const spies = {
        updateUser: jest.spyOn(container.cradle.userService, 'updateUser'),

        // spy to make sure job added to queue
        addEmailToQueue: jest.spyOn(Queue.prototype, 'add'),

        // @ts-ignore
        sendMail: jest.spyOn(mailer, 'sendMail').mockImplementation(),
      };

      // prettier-ignore
      const res = await request
        .post('/account/update-profile')
        .send(data);

      expect(res.status).toEqual(StatusCodes.OK);
      expect(spies.updateUser).toBeCalledTimes(1);
      expect(res.body).toBeObject();
      expect(res.body.data).toBeObject();
      expect(res.body.data.password).toBeUndefined();
      expect(res.body.data).toEqual({
        id: expect.toBeString(),
        name: data.name,
        email: data.email,
        new_email: null,
        email_confirm_token: null,
      });

      // make sure add to queue or add email was NOT called
      expect(spies.addEmailToQueue).toBeCalledTimes(0);
      expect(spies.sendMail).toBeCalledTimes(0);
    });

    /**
     * test
     */
    it('should send email confirmation if email address was changed', async () => {
      const { request, user } = await createUserAndAuthenticate(true);
      const data = await makeAccountUpdateProfileRequest({
        name: 'Jack',
        email: 'jack@gmail.com',
      });

      // set up spies
      const spies = {
        updateUser: jest.spyOn(container.cradle.userService, 'updateUser'),

        // spy to make sure job added to queue
        addEmailToQueue: jest.spyOn(Queue.prototype, 'add'),

        // @ts-ignore
        sendMail: jest.spyOn(mailer, 'sendMail').mockImplementation(),
      };

      // prettier-ignore
      const res = await request
        .post('/account/update-profile')
        .send(data);

      expect(res.status).toEqual(StatusCodes.OK);
      expect(spies.updateUser).toBeCalledTimes(1);
      expect(res.body).toBeObject();
      expect(res.body.data).toBeObject();
      expect(res.body.data.password).toBeUndefined();
      expect(res.body.data).toEqual({
        id: user.id,
        name: data.name,
        email: user.email, // this email should not have changed yet
        new_email: data.email, // this email should be pending new email
        email_confirm_token: expect.toBeString(),
      });

      const updatedUser = await container.cradle.userService.findUserById(user.id as string);
      expect(updatedUser).toBeInstanceOf(User);

      expect(spies.addEmailToQueue).toBeCalledTimes(1);
      const mockJob = (await spies.addEmailToQueue.mock.results[0].value) as SendEmailConfirmEmailJob;

      // trigger mock-send email
      await expect(SendEmailConfirmEmail.handle(mockJob)).resolves.toBeUndefined();

      // check email was mock-sent and grab it
      expect(spies.sendMail).toBeCalledTimes(1);
      const email = _.get(spies, 'sendMail.mock.calls.0.0') as Mail.Options;

      expect(email).toBeObject();
      expect(email.to).toEqual(data.email);

      // check email bodyParser
      const bodies = [email.text, email.html];
      for (const body of bodies) {
        // @ts-ignore
        const { name, email_confirm_token: token } = updatedUser;

        expect(body).toMatch(Mustache.escape(`Hi ${name}`));
        expect(body).toMatch(`requested to change your email`);
        expect(body).toMatch(webClientUrl(`/auth/confirm-email/${token}`));
      }

      /**
       * a user may update their profile while their email is still unconfirmed, we don't want the
       * confirm email to be sent out on every update, only if they change their email address
       */
      {
        // create request with same email but diff name, this should not trigger an email confirmation
        const data = await makeAccountUpdateProfileRequest({
          name: 'John',
          email: user.email,
        });

        // prettier-ignore
        const res = await request
          .post('/account/update-profile')
          .send(data);

        expect(res.status).toEqual(StatusCodes.OK);
        expect(spies.updateUser).toBeCalledTimes(2); // called twice now since previous call
        expect(res.body).toBeObject();
        expect(res.body.data).toBeObject();
        expect(res.body.data.password).toBeUndefined();
        expect(res.body.data).toEqual({
          id: user.id,
          name: data.name,
          email: user.email, // should not have changed
          new_email: updatedUser.new_email, // should contain previous requests's email change
          email_confirm_token: updatedUser.email_confirm_token, // should contain previous request's token
        });

        // check job was NOT created (spy should still be 1 from previous call)
        expect(spies.addEmailToQueue).toBeCalledTimes(1);
        expect(spies.sendMail).toBeCalledTimes(1);
      }
    });
  });
});
