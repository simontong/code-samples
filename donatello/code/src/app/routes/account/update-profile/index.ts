import { SendEmailConfirmEmail } from '@/src/jobs';
import { dispatchJob } from '@/src/lib/queues';
import { KoaContext } from '@/src/types/KoaContext';
import { validate } from './validate';

export const route = async (ctx: KoaContext): Promise<void> => {
  const input = await validate(ctx);
  const currentUser = ctx.state.user;

  // update user
  const updatedUser = await ctx.deps.userService.updateUser(currentUser, input);

  // dispatch confirm email if email changed
  if (updatedUser.new_email && currentUser.new_email !== updatedUser.new_email) {
    await dispatchJob(
      new SendEmailConfirmEmail({
        userId: currentUser.id,
        userAgent: ctx.get('user-agent'),
      }),
    );
  }

  ctx.jsend().data(updatedUser);
};
