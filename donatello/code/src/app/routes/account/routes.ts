import Router from 'koa-router';
import { DefaultState } from 'koa';
import { KoaContext } from '@/src/types/KoaContext';
import koaBody from 'koa-body';
import attachUserOrThrow from '@/src/app/middlewares/attachUserOrThrow';
import * as accountProfile from './profile';
import * as accountUpdateProfile from './update-profile';

const router = new Router<DefaultState, KoaContext>();

// prettier-ignore
router.get(
  'account.profile',
  '/profile',
  attachUserOrThrow,
  accountProfile.route,
);

// prettier-ignore
router.post(
  'account.update-profile',
  '/update-profile',
  attachUserOrThrow,
  koaBody(),
  accountUpdateProfile.route,
);

export default router;
