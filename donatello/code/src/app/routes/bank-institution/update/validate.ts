import { BankInstitutionUpdateRequestDto } from '@/src/dtos/BankInstitutionUpdateRequestDto';
import validateOrThrow from '@/src/lib/validators/validateOrThrow';
import { bankInstitutionRules } from '@/src/rules/bankInstitutionRules';
import { KoaContext } from '@/src/types/KoaContext';
import { object, SchemaOf } from 'yup';

export const validate = (ctx: KoaContext): Promise<BankInstitutionUpdateRequestDto> => {
  const schema: SchemaOf<BankInstitutionUpdateRequestDto> = object({
    name: bankInstitutionRules.name,
  });

  return validateOrThrow(schema, ctx.request.body);
};
