import Router from 'koa-router';
import { DefaultState } from 'koa';
import { KoaContext } from '@/src/types/KoaContext';
import koaBody from 'koa-body';
import regexes from '@/src/consts/regexes';
import attachUserOrThrow from '@/src/app/middlewares/attachUserOrThrow';
import * as bankInstitutionList from './list';
import * as bankInstitutionShow from './show';
import * as bankInstitutionCreate from './create';
import * as bankInstitutionUpdate from './update';
import * as bankInstitutionDelete from './delete';

const router = new Router<DefaultState, KoaContext>();

// prettier-ignore
router.get(
  'bank-institution.list',
  '/list',
  attachUserOrThrow,
  bankInstitutionList.route,
);

// prettier-ignore
router.get(
  'bank-institution.show',
  `/show/:id(${regexes.uuid})`,
  attachUserOrThrow,
  bankInstitutionShow.route,
);

// prettier-ignore
router.post(
  'bank-institution.create',
  '/create',
  attachUserOrThrow,
  koaBody(),
  bankInstitutionCreate.route,
);

// prettier-ignore
router.post(
  'bank-institution.update',
  `/update/:id(${regexes.uuid})`,
  attachUserOrThrow,
  koaBody(),
  bankInstitutionUpdate.route,
);

// prettier-ignore
router.post(
  'bank-institution.delete',
  `/delete/:id(${regexes.uuid})`,
  attachUserOrThrow,
  bankInstitutionDelete.route,
);

export default router;
