import { KoaContext } from '@/src/types/KoaContext';
import { validate } from './validate';

export const route = async (ctx: KoaContext): Promise<void> => {
  const userId = ctx.state.user.id;

  // validate
  const input = await validate(ctx);

  // create
  const bankInstitution = await ctx.deps.bankInstitutionService.createBankInstitution(userId, input);
  ctx.deps.logger.info('Bank institution created: %s', bankInstitution.id);

  ctx.jsend().data(bankInstitution);
};
