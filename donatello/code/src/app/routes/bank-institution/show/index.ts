import { KoaContext } from '@/src/types/KoaContext';

export const route = async (ctx: KoaContext): Promise<void> => {
  const bankInstitution = await ctx.deps.bankInstitutionService.findBankInstitutionOrThrowNotFound(ctx.state.user.id, ctx.params.id);

  ctx.jsend().data(bankInstitution);
};
