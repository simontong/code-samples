import app from '@/src/app/app';
import createUserAndAuthenticate from '../../../../../test/utils/createUserAndAuthenticate';
import supertest from 'supertest';
import { StatusCodes } from 'http-status-codes';
import { factoryBankInstitution } from '@/database/factory';
import { rollbackDatabaseTransaction, startDatabaseTransaction } from '@/test/utils/databaseTransaction';

beforeEach(async () => {
  await startDatabaseTransaction();
});

afterEach(async () => {
  await rollbackDatabaseTransaction();
});

describe('routes: GET /bank-institution/show/:id', () => {
  describe('authorization', () => {
    /**
     * test
     */
    it('should fail on guest access', async () => {
      const { user } = await createUserAndAuthenticate();
      const row = await factoryBankInstitution.create({ user_id: user.id });

      // prettier-ignore
      const res = await supertest(app.callback())
        .get(`/bank-institution/show/${row.id}`);

      expect(res.status).toEqual(StatusCodes.UNAUTHORIZED);
      expect(res.body).toBeObject();
      expect(res.body.error).toMatch(/unauthorized/i);
    });

    /**
     * test
     */
    it('should fail on attempting access to another users resource', async () => {
      const { user: otherUser } = await createUserAndAuthenticate();
      const { request } = await createUserAndAuthenticate();
      const row = await factoryBankInstitution.create({ user_id: otherUser.id });

      // prettier-ignore
      const res = await request
        .get(`/bank-institution/show/${row.id}`);

      expect(res.status).toEqual(StatusCodes.NOT_FOUND);
      expect(res.body).toBeObject();
      expect(res.body.error).toMatch(/not found/i);
    });
  });

  describe('successful bank institution fetch', () => {
    /**
     * test
     */
    it('should return bank institution', async () => {
      const { request, user } = await createUserAndAuthenticate();
      const row = await factoryBankInstitution.create({ user_id: user.id });

      // prettier-ignore
      const res = await request
        .get(`/bank-institution/show/${row.id}`);

      expect(res.status).toEqual(StatusCodes.OK);
      expect(res.body.data).toBeObject();
      expect(res.body.data).toEqual(row.toJSON());
    });
  });
});
