import { KoaContext } from '@/src/types/KoaContext';

export const route = async (ctx: KoaContext): Promise<void> => {
  const userId = ctx.state.user.id;
  const id = ctx.params.id;

  // throw 404 if this resource doesn't belong to requesting user
  await ctx.deps.bankInstitutionService.findBankInstitutionOrThrowNotFound(userId, id);

  const bankInstitution = await ctx.deps.bankInstitutionService.deleteBankInstitution(userId, id);
  ctx.deps.logger.info('Bank institution deleted: %s', bankInstitution.id);

  ctx.jsend().noContent();
};
