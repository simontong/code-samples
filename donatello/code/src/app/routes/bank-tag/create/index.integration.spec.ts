import appCodes from '@/src/consts/appCodes';
import container from '@/src/app/container';
import app from '@/src/app/app';
import { BankTagCreateRequestDto } from '@/src/dtos/BankTagCreateRequestDto';
import { makeBankTagCreateRequest } from '@/test/utils/requests/bankTagRequest';
import createUserAndAuthenticate from '../../../../../test/utils/createUserAndAuthenticate';
import supertest from 'supertest';
import { StatusCodes } from 'http-status-codes';
import { rollbackDatabaseTransaction, startDatabaseTransaction } from '@/test/utils/databaseTransaction';

beforeEach(async () => {
  await startDatabaseTransaction();
});

afterEach(async () => {
  await rollbackDatabaseTransaction();
});

describe('routes: POST /bank-tag/create', () => {
  describe('authorization', () => {
    /**
     * test
     */
    it('should fail on guest access', async () => {
      const data = await makeBankTagCreateRequest();

      // prettier-ignore
      const res = await supertest(app.callback())
        .post('/bank-tag/create')
        .send(data);

      expect(res.status).toEqual(StatusCodes.UNAUTHORIZED);
      expect(res.body).toBeObject();
      expect(res.body.error).toMatch(/unauthorized/i);
    });
  });

  describe('validation', () => {
    /**
     * test
     */
    it('should fail on missing required fields', async () => {
      const { request } = await createUserAndAuthenticate();
      const data: Partial<BankTagCreateRequestDto> = {
        name: '',
      };

      // prettier-ignore
      const res = await request
        .post('/bank-tag/create')
        .send(data);

      expect(res.status).toEqual(StatusCodes.BAD_REQUEST);
      expect(res.body).toBeObject();
      expect(res.body.code).toEqual(appCodes.E_VALIDATION_FAILED);
      expect(res.body.error).toBeArray();
      expect(res.body.error).toEqual(
        // prettier-ignore
        expect.arrayContaining([{ path: 'name', error: 'Name is a required field' }]),
      );
    });
  });

  describe('successful bank tag creation', () => {
    /**
     * test
     */
    it('should create a new bank tag', async () => {
      const { request } = await createUserAndAuthenticate();
      const data = await makeBankTagCreateRequest();

      const spy = jest.spyOn(container.cradle.bankTagService, 'createBankTag');

      // prettier-ignore
      const res = await request
        .post('/bank-tag/create')
        .send(data);

      expect(res.status).toEqual(StatusCodes.OK);
      expect(spy).toBeCalledTimes(1);
      expect(res.body.data).toBeObject();
      expect(res.body.data).toEqual({
        id: expect.toBeString(),
        name: data.name,
      });
    });
  });
});
