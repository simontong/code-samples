import { KoaContext } from '@/src/types/KoaContext';
import { validate } from './validate';

export const route = async (ctx: KoaContext): Promise<void> => {
  const userId = ctx.state.user.id;

  // validate
  const input = await validate(ctx);

  // create
  const bankTag = await ctx.deps.bankTagService.createBankTag(userId, input);
  ctx.deps.logger.info('Bank tag created: %s', bankTag.id);

  ctx.jsend().data(bankTag);
};
