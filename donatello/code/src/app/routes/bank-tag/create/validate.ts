import { BankTagCreateRequestDto } from '@/src/dtos/BankTagCreateRequestDto';
import validateOrThrow from '@/src/lib/validators/validateOrThrow';
import { bankTagRules } from '@/src/rules/bankTagRules';
import { KoaContext } from '@/src/types/KoaContext';
import { object, SchemaOf } from 'yup';

export const validate = (ctx: KoaContext): Promise<BankTagCreateRequestDto> => {
  const schema: SchemaOf<BankTagCreateRequestDto> = object({
    name: bankTagRules.name,
  });

  return validateOrThrow(schema, ctx.request.body);
};
