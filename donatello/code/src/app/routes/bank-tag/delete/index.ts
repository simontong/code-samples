import { KoaContext } from '@/src/types/KoaContext';

export const route = async (ctx: KoaContext): Promise<void> => {
  const userId = ctx.state.user.id;
  const id = ctx.params.id;

  // throw 404 if this resource doesn't belong to requesting user
  await ctx.deps.bankTagService.findBankTagOrThrowNotFound(userId, id);

  const bankTag = await ctx.deps.bankTagService.deleteBankTag(userId, id);
  ctx.deps.logger.info('Bank tag deleted: %s', bankTag.id);

  ctx.jsend().noContent();
};
