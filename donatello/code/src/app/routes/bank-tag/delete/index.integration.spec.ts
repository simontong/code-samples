import container from '@/src/app/container';
import app from '@/src/app/app';
import createUserAndAuthenticate from '../../../../../test/utils/createUserAndAuthenticate';
import supertest from 'supertest';
import { StatusCodes } from 'http-status-codes';
import { factoryBankTag } from '@/database/factory';
import { rollbackDatabaseTransaction, startDatabaseTransaction } from '@/test/utils/databaseTransaction';

beforeEach(async () => {
  await startDatabaseTransaction();
});

afterEach(async () => {
  await rollbackDatabaseTransaction();
});

describe('routes: POST /bank-tag/delete/:id', () => {
  describe('authorization', () => {
    /**
     * test
     */
    it('should fail on guest access', async () => {
      const { user } = await createUserAndAuthenticate();
      const row = await factoryBankTag.create({ user_id: user.id });

      // prettier-ignore
      const res = await supertest(app.callback())
        .post(`/bank-tag/delete/${row.id}`);

      expect(res.status).toEqual(StatusCodes.UNAUTHORIZED);
      expect(res.body).toBeObject();
      expect(res.body.error).toMatch(/unauthorized/i);
    });

    /**
     * test
     */
    it('should fail on attempting access to another users resource', async () => {
      const { user: otherUser } = await createUserAndAuthenticate();
      const { request } = await createUserAndAuthenticate();
      const row = await factoryBankTag.create({ user_id: otherUser.id });

      // prettier-ignore
      const res = await request
        .post(`/bank-tag/delete/${row.id}`);

      expect(res.status).toEqual(StatusCodes.NOT_FOUND);
      expect(res.body).toBeObject();
      expect(res.body.error).toMatch(/not found/i);
    });
  });

  describe('successful bank tag delete', () => {
    /**
     * test
     */
    it('should delete a bank tag', async () => {
      const { request, user } = await createUserAndAuthenticate();
      const row = await factoryBankTag.create({
        user_id: user.id,
        name: 'Old Bank Tag Name',
      });

      const spy = jest.spyOn(container.cradle.bankTagService, 'deleteBankTag');

      // prettier-ignore
      const res = await request
        .post(`/bank-tag/delete/${row.id}`);

      expect(res.status).toEqual(StatusCodes.NO_CONTENT);
      expect(spy).toBeCalledTimes(1);
      expect(res.body.data).toBeUndefined();
    });
  });
});
