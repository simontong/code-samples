import { BankTagUpdateRequestDto } from '@/src/dtos/BankTagUpdateRequestDto';
import validateOrThrow from '@/src/lib/validators/validateOrThrow';
import { bankTagRules } from '@/src/rules/bankTagRules';
import { KoaContext } from '@/src/types/KoaContext';
import { object, SchemaOf } from 'yup';

export const validate = (ctx: KoaContext): Promise<BankTagUpdateRequestDto> => {
  const schema: SchemaOf<BankTagUpdateRequestDto> = object({
    name: bankTagRules.name,
  });

  return validateOrThrow(schema, ctx.request.body);
};
