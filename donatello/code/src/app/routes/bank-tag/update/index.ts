import { KoaContext } from '@/src/types/KoaContext';
import { validate } from './validate';

export const route = async (ctx: KoaContext): Promise<void> => {
  const id = ctx.params.id;
  const userId = ctx.state.user.id;

  // throw 404 if this resource doesn't belong to requesting user
  await ctx.deps.bankTagService.findBankTagOrThrowNotFound(userId, id);

  // validate
  const input = await validate(ctx);

  // update
  const bankTag = await ctx.deps.bankTagService.updateBankTag(userId, id, input);
  ctx.deps.logger.info('Bank tag updated: %s', bankTag.id);

  ctx.jsend().data(bankTag);
};
