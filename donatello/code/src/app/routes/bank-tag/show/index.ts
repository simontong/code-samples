import { KoaContext } from '@/src/types/KoaContext';

export const route = async (ctx: KoaContext): Promise<void> => {
  const bankTag = await ctx.deps.bankTagService.findBankTagOrThrowNotFound(ctx.state.user.id, ctx.params.id);

  ctx.jsend().data(bankTag);
};
