import { factoryBankTag } from '@/database/factory';
import _ from 'lodash';
import app from '@/src/app/app';
import createUserAndAuthenticate from '../../../../../test/utils/createUserAndAuthenticate';
import supertest from 'supertest';
import { StatusCodes } from 'http-status-codes';
import { rollbackDatabaseTransaction, startDatabaseTransaction } from '@/test/utils/databaseTransaction';

beforeEach(async () => {
  await startDatabaseTransaction();
});

afterEach(async () => {
  await rollbackDatabaseTransaction();
});

describe('routes: GET /bank-tag/list', () => {
  describe('authorization', () => {
    /**
     * test
     */
    it('should fail on guest access', async () => {
      const { user } = await createUserAndAuthenticate();
      const count = 3;
      await Promise.all(_.times(count, () => factoryBankTag.create({ user_id: user.id })));

      // prettier-ignore
      const res = await supertest(app.callback())
        .get(`/bank-tag/list`);

      expect(res.status).toEqual(StatusCodes.UNAUTHORIZED);
      expect(res.body).toBeObject();
      expect(res.body.error).toMatch(/unauthorized/i);
    });
  });

  describe('successful list bank tags', () => {
    /**
     * test
     */
    it('should return bank tags', async () => {
      const { request, user } = await createUserAndAuthenticate();
      const count = 3;
      const rows = await Promise.all(_.times(count, () => factoryBankTag.create({ user_id: user.id })));

      // prettier-ignore
      const res = await request
        .get(`/bank-tag/list`);

      expect(res.status).toEqual(StatusCodes.OK);
      expect(res.body).toBeObject();
      expect(res.body.data).toBeObject();
      expect(res.body.data.total).toEqual(count);
      expect(res.body.data.currentPage).toEqual(1)
      expect(res.body.data.lastPage).toEqual(1)
      expect(res.body.data.results).toHaveLength(count);

      const rowsJson = rows.map((i) => i.toJSON());
      expect(res.body.data.results).toIncludeSameMembers(rowsJson);
    });

    /**
     * test
     */
    it('should return bank tags sorted', async () => {
      const { request, user } = await createUserAndAuthenticate();
      const count = 20;
      const rows = await Promise.all(_.times(count, () => factoryBankTag.create({ user_id: user.id })));

      // prettier-ignore
      const res = await request
        .get(`/bank-tag/list?sort=name`);

      expect(res.status).toEqual(StatusCodes.OK);
      expect(res.body).toBeObject();
      expect(res.body.data).toBeObject();
      expect(res.body.data.results).toHaveLength(count);

      const unsortedIds = _.map(rows, 'id');
      const sortedIds = _.chain(rows).sortBy('name').map('id').value();
      const resultIds = _.map(res.body.data.results, 'id');

      expect(unsortedIds).not.toEqual(sortedIds);
      expect(resultIds).toEqual(sortedIds);
    });

    /**
     * test
     */
    it('should return bank tags filtered', async () => {
      const { request, user } = await createUserAndAuthenticate();
      const count = 20;
      await Promise.all(_.times(count, () => factoryBankTag.create({ user_id: user.id })));

      // add our unique rows
      const resultCount = 3;
      // prettier-ignore
      await Promise.all(
        _.times(resultCount, n => factoryBankTag.create({ user_id: user.id, name: `myUniqName a${n}` })),
      );

      // prettier-ignore
      const res = await request
        .get(`/bank-tag/list?filter=name@ts:myUniqName`);

      expect(res.status).toEqual(StatusCodes.OK);
      expect(res.body).toBeObject();
      expect(res.body.data).toBeObject();
      expect(res.body.data.results).toHaveLength(resultCount);
    });

    /**
     * test
     */
    it('should return bank tags searched', async () => {
      const { request, user } = await createUserAndAuthenticate();
      const count = 20;
      await Promise.all(_.times(count, () => factoryBankTag.create({ user_id: user.id })));

      // add our unique rows
      const resultCount = 10;
      // prettier-ignore
      await Promise.all(
        _.times(resultCount, n => factoryBankTag.create({ user_id: user.id, name: `Takeout xyz${n}` })),
      );

      // prettier-ignore
      const res = await request
        .get(`/bank-tag/list?search=Takeout`);

      expect(res.status).toEqual(StatusCodes.OK);
      expect(res.body).toBeObject();
      expect(res.body.data).toBeObject();
      expect(res.body.data.results).toHaveLength(resultCount);
    });

    /**
     * test
     */
    it('should return bank tags next page', async () => {
      const { request, user } = await createUserAndAuthenticate();
      const count = 40;
      const pageSize = 10;
      const pageCount = Math.ceil(count / pageSize);
      await Promise.all(_.times(count, () => factoryBankTag.create({ user_id: user.id })));

      // prettier-ignore
      let resultIdsPrev = [];
      for (let page = 0; page < pageCount; page++) {
        const res = await request.get(`/bank-tag/list?pageSize=${pageSize}&page=${page}`);

        expect(res.status).toEqual(StatusCodes.OK);
        expect(res.body).toBeObject();
        expect(res.body.data).toBeObject();
        expect(res.body.data.total).toEqual(count);
        expect(res.body.data.lastPage).toEqual(pageCount);
        expect(res.body.data.results.length).toBeLessThanOrEqual(pageSize);

        // compare previous pages ids
        const resultIds = _.map(res.body.data.results, 'id');
        expect(resultIds).not.toEqual(resultIdsPrev);

        resultIdsPrev = resultIds;
      }
    });
  });
});
