import Router from 'koa-router';
import { DefaultState } from 'koa';
import { KoaContext } from '@/src/types/KoaContext';
import koaBody from 'koa-body';
import regexes from '@/src/consts/regexes';
import attachUserOrThrow from '@/src/app/middlewares/attachUserOrThrow';
import * as bankTagList from './list';
import * as bankTagShow from './show';
import * as bankTagCreate from './create';
import * as bankTagUpdate from './update';
import * as bankTagDelete from './delete';

const router = new Router<DefaultState, KoaContext>();

// prettier-ignore
router.get(
  'bank-tag.list',
  '/list',
  attachUserOrThrow,
  bankTagList.route,
);

// prettier-ignore
router.get(
  'bank-tag.show',
  `/show/:id(${regexes.uuid})`,
  attachUserOrThrow,
  bankTagShow.route,
);

// prettier-ignore
router.post(
  'bank-tag.create',
  '/create',
  attachUserOrThrow,
  koaBody(),
  bankTagCreate.route,
);

// prettier-ignore
router.post(
  'bank-tag.update',
  `/update/:id(${regexes.uuid})`,
  attachUserOrThrow,
  koaBody(),
  bankTagUpdate.route,
);

// prettier-ignore
router.post(
  'bank-tag.delete',
  `/delete/:id(${regexes.uuid})`,
  attachUserOrThrow,
  bankTagDelete.route,
);

export default router;
