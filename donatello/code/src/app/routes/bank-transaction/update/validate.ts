import { BankTransactionUpdateRequestDto } from '@/src/dtos/BankTransactionUpdateRequestDto';
import validateOrThrow from '@/src/lib/validators/validateOrThrow';
import { bankTransactionRules } from '@/src/rules/bankTransactionRules';
import { KoaContext } from '@/src/types/KoaContext';
import { object, SchemaOf } from 'yup';

export const validate = (ctx: KoaContext): Promise<BankTransactionUpdateRequestDto> => {
  const userId = ctx.state.user.id;

  const schema: SchemaOf<BankTransactionUpdateRequestDto> = object({
    bank_account_id: bankTransactionRules.bank_account_id(userId),
    bankCategory: bankTransactionRules.bankCategory,
    bankTags: bankTransactionRules.bankTags,
    currency_code: bankTransactionRules.currency_code,
    date: bankTransactionRules.date,
    payee: bankTransactionRules.payee,
    amount: bankTransactionRules.amount,
    note: bankTransactionRules.note,
  });

  return validateOrThrow(schema, ctx.request.body);
};
