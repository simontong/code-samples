import { KoaContext } from '@/src/types/KoaContext';
import { validate } from './validate';

export const route = async (ctx: KoaContext): Promise<void> => {
  const id = ctx.params.id;
  const userId = ctx.state.user.id;

  // throw 404 if this resource doesn't belong to requesting user
  await ctx.deps.bankTransactionService.findBankTransactionOrThrowNotFound(userId, id);

  // validate
  const input = await validate(ctx);

  // update
  const bankTransaction = await ctx.deps.bankTransactionService.updateBankTransaction(userId, id, input);
  ctx.deps.logger.info('Bank transaction updated: %s', bankTransaction.id);

  // load relations
  await ctx.deps.bankTransactionService.loadRelations(bankTransaction);

  ctx.jsend().data(bankTransaction);
};
