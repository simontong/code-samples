import { factoryBankTransaction, factoryBankAccount } from '@/database/factory';
import appCodes from '@/src/consts/appCodes';
import { BankTransactionUpdateRequestDto } from '@/src/dtos/BankTransactionUpdateRequestDto';
import { makeBankTransactionUpdateRequest } from '@/test/utils/requests/bankTransactionRequest';
import { StatusCodes } from 'http-status-codes';
import supertest from 'supertest';
import app from '@/src/app/app';
import container from '@/src/app/container';
import createUserAndAuthenticate from '../../../../../test/utils/createUserAndAuthenticate';
import { rollbackDatabaseTransaction, startDatabaseTransaction } from '@/test/utils/databaseTransaction';

beforeEach(async () => {
  await startDatabaseTransaction();
});

afterEach(async () => {
  await rollbackDatabaseTransaction();
});

describe('routes: POST /bank-transaction/update/:id', () => {
  describe('authorization', () => {
    /**
     * test
     */
    it('should fail on guest access', async () => {
      const { user } = await createUserAndAuthenticate();
      const row = await factoryBankTransaction.create({ user_id: user.id });
      const data = await makeBankTransactionUpdateRequest();

      // prettier-ignore
      const res = await supertest(app.callback())
        .post(`/bank-transaction/update/${row.id}`)
        .send(data);

      expect(res.status).toEqual(StatusCodes.UNAUTHORIZED);
      expect(res.body).toBeObject();
      expect(res.body.error).toMatch(/unauthorized/i);
    });

    /**
     * test
     */
    it('should fail on attempting access to another users resource', async () => {
      const { user } = await createUserAndAuthenticate();
      const { request } = await createUserAndAuthenticate();
      const row = await factoryBankTransaction.create({ user_id: user.id });
      const data = await makeBankTransactionUpdateRequest();

      // prettier-ignore
      const res = await request
        .post(`/bank-transaction/update/${row.id}`)
        .send(data);

      expect(res.status).toEqual(StatusCodes.NOT_FOUND);
      expect(res.body).toBeObject();
      expect(res.body.error).toMatch(/not found/i);
    });
  });

  describe('validation', () => {
    /**
     * test
     */
    it('should fail on missing required fields', async () => {
      const { request, user } = await createUserAndAuthenticate();
      const row = await factoryBankTransaction.create({ user_id: user.id });
      const data: Partial<BankTransactionUpdateRequestDto> = {
        payee: '',
      };

      // prettier-ignore
      const res = await request
        .post(`/bank-transaction/update/${row.id}`)
        .send(data);

      expect(res.status).toEqual(StatusCodes.BAD_REQUEST);
      expect(res.body).toBeObject();
      expect(res.body.code).toEqual(appCodes.E_VALIDATION_FAILED);
      expect(res.body.error).toBeArray();
      expect(res.body.error).toEqual(
        // prettier-ignore
        expect.arrayContaining([{ path: 'payee', error: 'Payee is a required field' }]),
      );
    });

    /**
     * test
     */
    it('should fail on attempting to set bank account for transaction to another users', async () => {
      const { user: otherUser } = await createUserAndAuthenticate();
      const { request, user } = await createUserAndAuthenticate();
      const otherUserBankAccount = await factoryBankAccount.create({ user_id: otherUser.id });
      const row = await factoryBankTransaction.create({ user_id: user.id });
      const data = await makeBankTransactionUpdateRequest({
        bank_account_id: otherUserBankAccount.id,
      });

      // prettier-ignore
      const res = await request
        .post(`/bank-transaction/update/${row.id}`)
        .send(data);

      expect(res.status).toEqual(StatusCodes.BAD_REQUEST);
      expect(res.body).toBeObject();
      expect(res.body.code).toEqual(appCodes.E_VALIDATION_FAILED);
      expect(res.body.error).toBeArray();
      expect(res.body.error).toEqual(
        // prettier-ignore
        expect.arrayContaining([{ path: 'bank_account_id', error: 'Bank account does not exist' }]),
      );
    });
  });

  describe('successful bank transaction update', () => {
    /**
     * test
     */
    it('should update a bank transaction', async () => {
      const { request, user } = await createUserAndAuthenticate();
      const row = await factoryBankTransaction.create({ user_id: user.id });
      const bankAccount = await factoryBankAccount.create({ user_id: user.id });
      const data = await makeBankTransactionUpdateRequest({
        bank_account_id: bankAccount.id,
      });

      const spy = jest.spyOn(container.cradle.bankTransactionService, 'updateBankTransaction');

      // prettier-ignore
      const res = await request
        .post(`/bank-transaction/update/${row.id}`)
        .send(data);

      expect(res.status).toEqual(StatusCodes.OK);
      expect(spy).toBeCalledTimes(1);
      expect(res.body.data).toBeObject();
      expect(res.body.data).toEqual(
        expect.objectContaining({
          id: row.id,
          date: data.date,
          payee: data.payee,
          amount: data.amount,
          note: data.note,

          // relations
          bankAccount: expect.objectContaining({ id: data.bank_account_id }),
          bankCategory: null,
          bankTags: [],
          currency: expect.objectContaining({ code: data.currency_code }),
        }),
      );
    });
  });
});
