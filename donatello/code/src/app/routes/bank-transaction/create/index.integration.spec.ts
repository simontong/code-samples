import { BankTransactionCreateRequestDto } from '@/src/dtos/BankTransactionCreateRequestDto';
import { makeBankTransactionCreateRequest } from '@/test/utils/requests/bankTransactionRequest';
import _ from 'lodash';
import { factoryBankAccount } from '@/database/factory';
import appCodes from '@/src/consts/appCodes';
import { StatusCodes } from 'http-status-codes';
import supertest from 'supertest';
import * as faker from 'faker';
import app from '@/src/app/app';
import container from '@/src/app/container';
import createUserAndAuthenticate from '../../../../../test/utils/createUserAndAuthenticate';
import { rollbackDatabaseTransaction, startDatabaseTransaction } from '@/test/utils/databaseTransaction';

beforeEach(async () => {
  await startDatabaseTransaction();
});

afterEach(async () => {
  await rollbackDatabaseTransaction();
});

describe('routes: POST /bank-transaction/create', () => {
  describe('authorization', () => {
    /**
     * test
     */
    it('should fail on guest access', async () => {
      const data = await makeBankTransactionCreateRequest();

      // prettier-ignore
      const res = await supertest(app.callback())
        .post('/bank-transaction/create')
        .send(data);

      expect(res.status).toEqual(StatusCodes.UNAUTHORIZED);
      expect(res.body).toBeObject();
      expect(res.body.error).toMatch(/unauthorized/i);
    });
  });

  describe('validation', () => {
    /**
     * test
     */
    it('should fail on missing required fields', async () => {
      const { request } = await createUserAndAuthenticate();
      const data: Partial<BankTransactionCreateRequestDto> = {
        payee: '',
      };

      // prettier-ignore
      const res = await request
        .post('/bank-transaction/create')
        .send(data);

      expect(res.status).toEqual(StatusCodes.BAD_REQUEST);
      expect(res.body).toBeObject();
      expect(res.body.code).toEqual(appCodes.E_VALIDATION_FAILED);
      expect(res.body.error).toBeArray();
      expect(res.body.error).toEqual(
        // prettier-ignore
        expect.arrayContaining([{ path: 'currency_code', error: 'Currency is a required field' }]),
      );
      expect(res.body.error).toEqual(
        // prettier-ignore
        expect.arrayContaining([{ path: 'payee', error: 'Payee is a required field' }]),
      );
      expect(res.body.error).toEqual(
        // prettier-ignore
        expect.arrayContaining([{ path: 'amount', error: 'Amount is a required field' }]),
      );
      expect(res.body.error).toEqual(
        // prettier-ignore
        expect.arrayContaining([{ path: 'date', error: 'Date is a required field' }]),
      );
    });

    /**
     * test
     */
    it('should fail on attempting to create transaction on another users resource', async () => {
      const { user: otherUser } = await createUserAndAuthenticate();
      const { request } = await createUserAndAuthenticate();
      const bankAccount = await factoryBankAccount.create({ user_id: otherUser.id });
      const data = await makeBankTransactionCreateRequest({
        bank_account_id: bankAccount.id,
        payee: faker.company.companyName(),
      });

      // prettier-ignore
      const res = await request
        .post('/bank-transaction/create')
        .send(data);

      expect(res.status).toEqual(StatusCodes.BAD_REQUEST);
      expect(res.body).toBeObject();
      expect(res.body.code).toEqual(appCodes.E_VALIDATION_FAILED);
      expect(res.body.error).toBeArray();
      expect(res.body.error).toEqual(
        // prettier-ignore
        expect.arrayContaining([{ path: 'bank_account_id', error: 'Bank account does not exist' }]),
      );
    });
  });

  describe('successful bank transaction creation', () => {
    /**
     * test
     */
    it('should create a new bank transaction', async () => {
      const { request, user } = await createUserAndAuthenticate();
      const bankAccount = await factoryBankAccount.create({ user_id: user.id });
      const data = await makeBankTransactionCreateRequest({
        bank_account_id: bankAccount.id,
        bankTags: _.times(3, () => faker.lorem.word()),
        bankCategory: faker.commerce.department(),
      });

      const spy = jest.spyOn(container.cradle.bankTransactionService, 'createBankTransaction');

      // prettier-ignore
      const res = await request
        .post('/bank-transaction/create')
        .send(data);

      expect(res.status).toEqual(StatusCodes.OK);
      expect(spy).toBeCalledTimes(1);
      expect(res.body.data).toBeObject();
      expect(res.body.data).toEqual(
        expect.objectContaining({
          id: expect.toBeString(),
          date: data.date,
          payee: data.payee,
          amount: data.amount,
          note: data.note,

          // relations
          bankAccount: expect.objectContaining({ id: data.bank_account_id }),
          bankCategory: expect.objectContaining({ id: expect.toBeString() }),
          bankTags: expect.arrayContaining([expect.objectContaining({ id: expect.toBeString() })]),
          currency: expect.objectContaining({ code: data.currency_code }),
        }),
      );
    });
  });
});
