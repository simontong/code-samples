import { KoaContext } from '@/src/types/KoaContext';
import { validate } from './validate';

export const route = async (ctx: KoaContext): Promise<void> => {
  const userId = ctx.state.user.id;

  // validate
  const input = await validate(ctx);

  // create
  const bankTransaction = await ctx.deps.bankTransactionService.createBankTransaction(userId, input);
  ctx.deps.logger.info('Bank transaction created: %s', bankTransaction.id);

  // load relations
  await ctx.deps.bankTransactionService.loadRelations(bankTransaction);

  ctx.jsend().data(bankTransaction);
};
