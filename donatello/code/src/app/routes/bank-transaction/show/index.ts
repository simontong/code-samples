import { KoaContext } from '@/src/types/KoaContext';

export const route = async (ctx: KoaContext): Promise<void> => {
  const bankTransaction = await ctx.deps.bankTransactionService.findBankTransactionOrThrowNotFound(ctx.state.user.id, ctx.params.id);

  // load relations
  await ctx.deps.bankTransactionService.loadRelations(bankTransaction);

  ctx.jsend().data(bankTransaction);
};
