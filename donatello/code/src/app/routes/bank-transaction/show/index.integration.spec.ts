import { factoryBankAccount, factoryBankCategory, factoryBankTransaction } from '@/database/factory';
import supertest from 'supertest';
import { StatusCodes } from 'http-status-codes';
import createUserAndAuthenticate from '../../../../../test/utils/createUserAndAuthenticate';
import app from '@/src/app/app';
import { rollbackDatabaseTransaction, startDatabaseTransaction } from '@/test/utils/databaseTransaction';

beforeEach(async () => {
  await startDatabaseTransaction();
});

afterEach(async () => {
  await rollbackDatabaseTransaction();
});

describe('routes: GET /bank-transaction/show/:id', () => {
  describe('authorization', () => {
    /**
     * test
     */
    it('should fail on guest access', async () => {
      const { user } = await createUserAndAuthenticate();
      const row = await factoryBankTransaction.create({ user_id: user.id });

      // prettier-ignore
      const res = await supertest(app.callback())
        .get(`/bank-transaction/show/${row.id}`);

      expect(res.status).toEqual(StatusCodes.UNAUTHORIZED);
      expect(res.body).toBeObject();
      expect(res.body.error).toMatch(/unauthorized/i);
    });

    /**
     * test
     */
    it('should fail on attempting access to another users resource', async () => {
      const { user: otherUser } = await createUserAndAuthenticate();
      const { request } = await createUserAndAuthenticate();
      const row = await factoryBankTransaction.create({ user_id: otherUser.id });

      // prettier-ignore
      const res = await request
        .get(`/bank-transaction/show/${row.id}`);

      expect(res.status).toEqual(StatusCodes.NOT_FOUND);
      expect(res.body).toBeObject();
      expect(res.body.error).toMatch(/not found/i);
    });
  });

  describe('successful bank transaction fetch', () => {
    /**
     * test
     */
    it('should return bank transaction', async () => {
      const { request, user } = await createUserAndAuthenticate();
      const bankAccount = await factoryBankAccount.create({ user_id: user.id });
      const bankCategory = await factoryBankCategory.create({ user_id: user.id });
      const row = await factoryBankTransaction.create({
        user_id: user.id,
        bank_account_id: bankAccount.id,
        bank_category_id: bankCategory.id,
      });

      // prettier-ignore
      const res = await request
        .get(`/bank-transaction/show/${row.id}`);

      expect(res.status).toEqual(StatusCodes.OK);
      expect(res.body.data).toBeObject();
      expect(res.body.data).toEqual(
        expect.objectContaining({
          id: row.id,
          date: row.date,
          payee: row.payee,
          amount: row.amount,
          note: row.note,

          // relations
          bankAccount: expect.objectContaining({ id: row.bank_account_id }),
          bankCategory: expect.objectContaining({ id: row.bank_category_id }),
          bankTags: expect.arrayContaining([]),
          currency: expect.objectContaining({ code: row.currency_code }),
        }),
      );
    });
  });
});
