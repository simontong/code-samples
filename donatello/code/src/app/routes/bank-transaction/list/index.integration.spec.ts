import { factoryBankTransaction } from '@/database/factory';
import { BankTransactionShape } from '@/src/models/BankTransaction';
import _ from 'lodash';
import supertest from 'supertest';
import app from '@/src/app/app';
import createUserAndAuthenticate from '../../../../../test/utils/createUserAndAuthenticate';
import { StatusCodes } from 'http-status-codes';
import { rollbackDatabaseTransaction, startDatabaseTransaction } from '@/test/utils/databaseTransaction';

beforeEach(async () => {
  await startDatabaseTransaction();
});

afterEach(async () => {
  await rollbackDatabaseTransaction();
});

describe('routes: GET /bank-transaction/list', () => {
  describe('authorization', () => {
    /**
     * test
     */
    it('should fail on guest access', async () => {
      const { user } = await createUserAndAuthenticate();
      const count = 3;
      await Promise.all(_.times(count, () => factoryBankTransaction.create({ user_id: user.id })));

      // prettier-ignore
      const res = await supertest(app.callback())
        .get(`/bank-transaction/list`);

      expect(res.status).toEqual(StatusCodes.UNAUTHORIZED);
      expect(res.body).toBeObject();
      expect(res.body.error).toMatch(/unauthorized/i);
    });
  });

  describe('successful list bank transactions', () => {
    /**
     * test
     */
    it('should return bank transactions', async () => {
      const { request, user } = await createUserAndAuthenticate();
      const count = 5;
      const rows = await Promise.all(_.times(count, () => factoryBankTransaction.create({ user_id: user.id })));
      const rowsJson = rows.map((i) => i.toJSON());

      // prettier-ignore
      const res = await request
        .get(`/bank-transaction/list`);

      expect(res.status).toEqual(StatusCodes.OK);
      expect(res.body).toBeObject();
      expect(res.body.data).toBeObject();
      expect(res.body.data.total).toEqual(count);
      expect(res.body.data.currentPage).toEqual(1);
      expect(res.body.data.lastPage).toEqual(1);
      expect(res.body.data.results).toHaveLength(count);

      // when the ms time ends in zero for timestamps, toJSON() is rounding it off ,
      // eg.: "2021-07-20 12:57:32.750" -> "2021-07-20 12:57:32.75"
      // here we are checking to make sure there isn't any differences, if there is
      // it will be output to console
      const differences = [];
      for (const row of rowsJson) {
        const find = res.body.data.results.find((k: BankTransactionShape) => k.id === row.id);

        // @ts-ignore
        const diff = _.reduce(
          row,
          // @ts-ignore
          (result, value, key) => (_.isEqual(value, find[key]) ? result : result.concat(key)),
          [],
        );

        if (diff.length) {
          differences.push({ find, row, diff });
        }
      }

      expect(differences).toHaveLength(0);
    });

    /**
     * test
     */
    it('should return bank transactions sorted', async () => {
      const { request, user } = await createUserAndAuthenticate();
      const count = 20;
      const rows = await Promise.all(_.times(count, () => factoryBankTransaction.create({ user_id: user.id })));

      // prettier-ignore
      const res = await request
        .get(`/bank-transaction/list?sort=payee`);

      expect(res.status).toEqual(StatusCodes.OK);
      expect(res.body).toBeObject();
      expect(res.body.data).toBeObject();
      expect(res.body.data.results).toHaveLength(count);

      const unsortedIds = _.map(rows, 'id');
      const sortedIds = _.chain(rows).sortBy('payee').map('id').value();
      const resultIds = _.map(res.body.data.results, 'id');

      expect(unsortedIds).not.toEqual(sortedIds);
      expect(resultIds).toEqual(sortedIds);
    });

    /**
     * test
     */
    it('should return bank transactions filtered', async () => {
      const { request, user } = await createUserAndAuthenticate();
      const count = 20;
      await Promise.all(_.times(count, () => factoryBankTransaction.create({ user_id: user.id })));

      // add our unique rows
      const resultCount = 3;
      // prettier-ignore
      await Promise.all(
        _.times(resultCount, n => factoryBankTransaction.create({ user_id: user.id, payee: `myUniqPayee abc${n}` })),
      );

      // prettier-ignore
      const res = await request
        .get(`/bank-transaction/list?filter=payee@ts:myUniqPayee`);

      expect(res.status).toEqual(StatusCodes.OK);
      expect(res.body).toBeObject();
      expect(res.body.data).toBeObject();
      expect(res.body.data.results).toHaveLength(resultCount);
    });

    /**
     * test
     */
    it('should return bank transactions searched', async () => {
      const { request, user } = await createUserAndAuthenticate();
      const count = 20;
      await Promise.all(_.times(count, () => factoryBankTransaction.create({ user_id: user.id })));

      // add our unique rows
      const resultCount = 6;
      // prettier-ignore
      await Promise.all(
        _.times(resultCount, n => factoryBankTransaction.create({ user_id: user.id, payee: `zzzzzgggg xyz${n}` })),
      );

      // prettier-ignore
      const res = await request
        .get(`/bank-transaction/list?search=zzzzzgggg`);

      expect(res.status).toEqual(StatusCodes.OK);
      expect(res.body).toBeObject();
      expect(res.body.data).toBeObject();
      expect(res.body.data.results).toHaveLength(resultCount);
    });

    /**
     * test
     */
    it('should return bank transactions next page', async () => {
      const { request, user } = await createUserAndAuthenticate();
      const count = 40;
      const pageSize = 10;
      const pageCount = Math.ceil(count / pageSize);
      await Promise.all(_.times(count, () => factoryBankTransaction.create({ user_id: user.id })));

      // prettier-ignore
      let resultIdsPrev = [];
      for (let page = 0; page < pageCount; page++) {
        const res = await request.get(`/bank-transaction/list?pageSize=${pageSize}&page=${page}`);

        expect(res.status).toEqual(StatusCodes.OK);
        expect(res.body).toBeObject();
        expect(res.body.data).toBeObject();
        expect(res.body.data.total).toEqual(count);
        expect(res.body.data.lastPage).toEqual(pageCount);
        expect(res.body.data.results.length).toBeLessThanOrEqual(pageSize);

        // compare previous pages ids
        const resultIds = _.map(res.body.data.results, 'id');
        expect(resultIds).not.toEqual(resultIdsPrev);

        resultIdsPrev = resultIds;
      }
    });
  });
});
