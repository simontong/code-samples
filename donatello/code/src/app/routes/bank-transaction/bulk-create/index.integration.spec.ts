import { BankTransactionBulkCreateRequestDto } from '@/src/dtos/BankTransactionBulkCreateRequestDto';
import _ from 'lodash';
import appCodes from '@/src/consts/appCodes';
import { makeBankTransactionCreateRequest } from '@/test/utils/requests/bankTransactionRequest';
import { factoryBankAccount } from '@/database/factory';
import { StatusCodes } from 'http-status-codes';
import supertest from 'supertest';
import * as faker from 'faker';
import app from '@/src/app/app';
import container from '@/src/app/container';
import createUserAndAuthenticate from '../../../../../test/utils/createUserAndAuthenticate';
import { rollbackDatabaseTransaction, startDatabaseTransaction } from '@/test/utils/databaseTransaction';

beforeEach(async () => {
  await startDatabaseTransaction();
});

afterEach(async () => {
  await rollbackDatabaseTransaction();
});

describe('routes: POST /bank-transaction/bulk-create', () => {
  describe('authorization', () => {
    /**
     * test
     */
    it('should fail on guest access', async () => {
      const data = await makeBankTransactionCreateRequest();

      // prettier-ignore
      const res = await supertest(app.callback())
        .post('/bank-transaction/bulk-create')
        .send(data);

      expect(res.status).toEqual(StatusCodes.UNAUTHORIZED);
      expect(res.body).toBeObject();
      expect(res.body.error).toMatch(/unauthorized/i);
    });
  });

  describe('validation', () => {
    /**
     * test
     */
    it('should fail on missing required fields', async () => {
      const { request, user } = await createUserAndAuthenticate();
      const bankAccount = await factoryBankAccount.create({ user_id: user.id });
      const count = 5;
      const data: BankTransactionBulkCreateRequestDto = {
        bank_account_id: bankAccount.id,
        // @ts-ignore
        bankTransactions: await Promise.all(
          _.times(count, () => ({
            payee: '',
          })),
        ),
      };

      // prettier-ignore
      const res = await request
        .post('/bank-transaction/bulk-create')
        .send(data);

      expect(res.status).toEqual(StatusCodes.BAD_REQUEST);
      expect(res.body).toBeObject();
      expect(res.body.code).toEqual(appCodes.E_VALIDATION_FAILED);
      expect(res.body.error).toBeArray();

      for (let i = 0; i < count; i++) {
        expect(res.body.error).toEqual(
          // prettier-ignore
          expect.arrayContaining([{
            path: `bankTransactions[${i}].currency_code`,
            error: 'Currency is a required field',
          }]),
        );
        expect(res.body.error).toEqual(
          // prettier-ignore
          expect.arrayContaining([{ path: `bankTransactions[${i}].payee`, error: 'Payee is a required field' }]),
        );
        expect(res.body.error).toEqual(
          // prettier-ignore
          expect.arrayContaining([{ path: `bankTransactions[${i}].amount`, error: 'Amount is a required field' }]),
        );
        expect(res.body.error).toEqual(
          // prettier-ignore
          expect.arrayContaining([{ path: `bankTransactions[${i}].date`, error: 'Date is a required field' }]),
        );
      }
    });

    /**
     * test
     */
    it('should fail on attempting to bulk create transactions on another users resource', async () => {
      const { user: otherUser } = await createUserAndAuthenticate();
      const { request } = await createUserAndAuthenticate();
      const bankAccount = await factoryBankAccount.create({ user_id: otherUser.id });
      const count = 5;
      const data: BankTransactionBulkCreateRequestDto = {
        bank_account_id: bankAccount.id,
        bankTransactions: await Promise.all(
          _.times(count, () =>
            makeBankTransactionCreateRequest({
              payee: faker.company.companyName(),
            }),
          ),
        ),
      };

      // prettier-ignore
      const res = await request
        .post('/bank-transaction/bulk-create')
        .send(data);

      expect(res.status).toEqual(StatusCodes.BAD_REQUEST);
      expect(res.body).toBeObject();
      expect(res.body.code).toEqual(appCodes.E_VALIDATION_FAILED);
      expect(res.body.error).toBeArray();
      expect(res.body.error).toEqual(
        // prettier-ignore
        expect.arrayContaining([{ path: `bank_account_id`, error: 'Bank account does not exist' }]),
      );
    });
  });

  describe('successful bulk bank transaction creation', () => {
    /**
     * test
     */
    it('should bulk create bank transactions', async () => {
      const { request, user } = await createUserAndAuthenticate();
      const bankAccount = await factoryBankAccount.create({ user_id: user.id });
      const count = 5;
      const data: BankTransactionBulkCreateRequestDto = {
        bank_account_id: bankAccount.id,
        bankTransactions: await Promise.all(
          _.times(count, () =>
            makeBankTransactionCreateRequest({
              bankTags: _.times(3, () => faker.lorem.word()),
              bankCategory: faker.commerce.department(),
            }),
          ),
        ),
      };

      const spy = jest.spyOn(container.cradle.bankTransactionService, 'bulkCreateBankTransactions');

      // prettier-ignore
      const res = await request
        .post('/bank-transaction/bulk-create')
        .send(data);

      expect(res.status).toEqual(StatusCodes.OK);
      expect(spy).toBeCalledTimes(1);
      expect(res.body.data).toBeObject();
      expect(res.body.data.bankTransactions).toBeArray();

      for (let i = 0; i < count; i++) {
        const dataBankTransaction = data.bankTransactions[i];

        expect(res.body.data.bankTransactions[i]).toEqual(
          expect.objectContaining({
            id: expect.toBeString(),
            bank_account_id: data.bank_account_id,
            currency_code: dataBankTransaction.currency_code,
            date: dataBankTransaction.date,
            payee: dataBankTransaction.payee,
            amount: dataBankTransaction.amount,
            note: dataBankTransaction.note,

            // relations
            // bankAccount: expect.objectContaining({ id: data.bank_account_id }),
            // bankCategory: expect.objectContaining({ id: expect.toBeString() }),
            // bankTags: expect.arrayContaining([expect.objectContaining({ id: expect.toBeString() })]),
            // currency: expect.objectContaining({ id: dataBankTransaction.currency_code }),
          }),
        );
      }
    });
  });
});
