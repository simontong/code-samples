import { BankTransactionBulkCreateRequestDto } from '@/src/dtos/BankTransactionBulkCreateRequestDto';
import validateOrThrow from '@/src/lib/validators/validateOrThrow';
import { bankTransactionRules } from '@/src/rules/bankTransactionRules';
import { KoaContext } from '@/src/types/KoaContext';
import { array, object, SchemaOf } from 'yup';

export const validate = (ctx: KoaContext): Promise<BankTransactionBulkCreateRequestDto> => {
  const userId = ctx.state.user.id;

  // prettier-ignore
  const schema: SchemaOf<BankTransactionBulkCreateRequestDto> = object({
    bank_account_id: bankTransactionRules.bank_account_id(userId),
    bankTransactions: array()
      .label('Bank Transactions')
      .of(
        object({
          bankCategory: bankTransactionRules.bankCategory,
          bankTags: bankTransactionRules.bankTags,
          currency_code: bankTransactionRules.currency_code,
          date: bankTransactionRules.date,
          payee: bankTransactionRules.payee,
          amount: bankTransactionRules.amount,
          note: bankTransactionRules.note,
        }),
      )
      .required()
      .ensure()
      .compact()
      .min(1),
  });

  return validateOrThrow(schema, ctx.request.body);
};
