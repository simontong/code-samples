import { KoaContext } from '@/src/types/KoaContext';
import { validate } from './validate';

export const route = async (ctx: KoaContext): Promise<void> => {
  const userId = ctx.state.user.id;

  // validate -- todo need to work on performance here, this takes massive perf hit
  const input = await validate(ctx);

  const bankTransactions = await ctx.deps.bankTransactionService.bulkCreateBankTransactions(userId, input);
  ctx.deps.logger.info('Bank transaction bulk created %d rows for user %s', bankTransactions.length, userId);

  ctx.jsend().data({ bankTransactions });
};
