import { KoaContext } from '@/src/types/KoaContext';

export const route = async (ctx: KoaContext): Promise<void> => {
  const userId = ctx.state.user.id;
  const id = ctx.params.id;

  // throw 404 if this resource doesn't belong to requesting user
  await ctx.deps.bankTransactionService.findBankTransactionOrThrowNotFound(userId, id);

  const bankTransaction = await ctx.deps.bankTransactionService.deleteBankTransaction(userId, id);
  ctx.deps.logger.info('Bank transaction deleted: %s', bankTransaction.id);

  ctx.jsend().noContent();
};
