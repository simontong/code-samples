import Router from 'koa-router';
import { DefaultState } from 'koa';
import { KoaContext } from '@/src/types/KoaContext';
import koaBody from 'koa-body';
import regexes from '@/src/consts/regexes';
import attachUserOrThrow from '@/src/app/middlewares/attachUserOrThrow';
import * as bankTransactionBulkCreate from './bulk-create';
import * as bankTransactionExport from './export';
import * as bankTransactionList from './list';
import * as bankTransactionShow from './show';
import * as bankTransactionCreate from './create';
import * as bankTransactionUpdate from './update';
import * as bankTransactionDelete from './delete';

const router = new Router<DefaultState, KoaContext>();

// prettier-ignore
router.get(
  'bank-transaction.list',
  '/list',
  attachUserOrThrow,
  bankTransactionList.route,
);

// prettier-ignore
router.get(
  'bank-transaction.export',
  '/export',
  attachUserOrThrow,
  bankTransactionExport.route,
);

// prettier-ignore
router.get(
  'bank-transaction.show',
  `/show/:id(${regexes.uuid})`,
  attachUserOrThrow,
  bankTransactionShow.route,
);

// prettier-ignore
router.post(
  'bank-transaction.create',
  '/create',
  attachUserOrThrow,
  koaBody(),
  bankTransactionCreate.route,
);

// prettier-ignore
router.post(
  'bank-transaction.update',
  `/update/:id(${regexes.uuid})`,
  attachUserOrThrow,
  koaBody(),
  bankTransactionUpdate.route,
);

// prettier-ignore
router.post(
  'bank-transaction.delete',
  `/delete/:id(${regexes.uuid})`,
  attachUserOrThrow,
  bankTransactionDelete.route,
);

// prettier-ignore
router.post(
  'bank-transaction.bulk-create',
  `/bulk-create`,
  attachUserOrThrow,
  koaBody({
    formLimit: '500kb',
    jsonLimit: '500kb',
    textLimit: '500kb',
  }),
  bankTransactionBulkCreate.route,
);

export default router;
