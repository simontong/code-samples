type ValidationTestRule = (value: unknown) => Promise<boolean>;

export default ValidationTestRule;
