type ValuesOf<T extends readonly unknown[]> = T[number];

export default ValuesOf;
