import { KoaContext } from '@/src/types/KoaContext';
import { Next } from 'koa';

type KoaMiddleware = (ctx: KoaContext, next: Next) => Promise<Next | void>;

export default KoaMiddleware;
