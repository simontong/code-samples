import BaseModel from '@/src/lib/database/BaseModel';
import { ColumnRef, Expression, PartialModelObject, Operator, PrimitiveValue } from 'objection';

export default interface IRepository<T extends BaseModel> {
  find(obj: PartialModelObject<T>): Promise<T[]>;
  find(col: keyof PartialModelObject<T>, expr: Expression<PrimitiveValue>): Promise<T[]>;
  find(col: ColumnRef, op?: Operator, expr?: Expression<PrimitiveValue>): Promise<T[]>;

  findOne(obj: PartialModelObject<T>): Promise<T>;
  findOne(col: keyof PartialModelObject<T>, expr: Expression<PrimitiveValue>): Promise<T>;
  findOne(col: ColumnRef, op?: Operator, expr?: Expression<PrimitiveValue>): Promise<T>;

  create(data: PartialModelObject<T>): Promise<T>;

  update(where: PartialModelObject<T>, data: PartialModelObject<T>): Promise<T>;

  delete(where: PartialModelObject<T>): Promise<T>;
}
