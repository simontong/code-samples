import { MainCtxAttachContainer } from '@/src/app/middlewares/attachContainer';
import { MainCtxStream } from '@/src/app/middlewares/stream';
import { DefaultState, ExtendableContext, ParameterizedContext } from 'koa';
import { ContextSession } from 'koa-session';
import { MainCtxJsend } from '@/src/app/middlewares/jsend';
import { StateCtxAttachUser, MainCtxAttachUser } from '@/src/app/middlewares/attachUserOrThrow';
import { RouterContext } from 'koa-router';

type CtxState = DefaultState & StateCtxAttachUser;
type CtxBody = MainCtxJsend;
type CtxMain =
  ExtendableContext
  & RouterContext
  & ContextSession
  & MainCtxAttachContainer
  & MainCtxAttachUser
  & MainCtxStream
  & CtxBody;

export type KoaContext = ParameterizedContext<CtxState, CtxMain>;
