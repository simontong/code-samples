import { Cradle } from '@/src/app/container';
import { AccountUpdateProfileRequestDto } from '@/src/dtos/AccountUpdateProfileRequestDto';
import { AuthRegisterRequestDto } from '@/src/dtos/AuthRegisterRequestDto';
import UserRepository from '@/src/repositories/UserRepository';
import { DateTime } from 'luxon';
import { v4 as uuidv4 } from 'uuid';
import User, { UserShape } from '@/src/models/User';
import { hashPassword, verifyPassword } from '@/src/lib/crypto';
import { canReuseExpiryToken, createExpiryToken, getTokenExpiry, hasTokenExpired } from '@/src/lib/expiryToken';

export const PASSWORD_RESET_TOKEN_EXPIRES_AFTER_MS = 1000 * 60 * 60 * 24;
export const EMAIL_CONFIRM_TOKEN_EXPIRES_AFTER_MS = 1000 * 60 * 60 * 24;

export default class UserService {
  private userRepo: UserRepository;

  constructor({ userRepository }: Cradle) {
    this.userRepo = userRepository;
  }

  /**
   * find user by whatever
   */
  async findUser(obj: Partial<UserShape>): Promise<User> {
    return await this.userRepo.findOne(obj);
  }

  /**
   * find user by id
   */
  async findUserById(id: string): Promise<User> {
    const where: Partial<UserShape> = {
      id,
    };
    return await this.userRepo.findOne(where);
  }

  /**
   * find user by email
   */
  async findUserByEmail(email: string): Promise<User> {
    const where: Partial<UserShape> = {
      email,
    };
    return await this.userRepo.findOne(where);
  }

  /**
   * find user by email confirm token
   */
  async findUserByEmailConfirmToken(token: string): Promise<User> {
    const where: Partial<UserShape> = {
      email_confirm_token: token,
    };
    return await this.userRepo.findOne(where);
  }

  /**
   * find user by password reset token
   */
  async findUserByPasswordResetToken(token: string): Promise<User> {
    const where: Partial<UserShape> = {
      password_reset_token: token,
    };
    return await this.userRepo.findOne(where);
  }

  /**
   * register new user
   */
  async createUser(input: Omit<AuthRegisterRequestDto, 'passwordConfirm'>): Promise<User> {
    const data: Partial<UserShape> = {
      id: uuidv4(),
      name: input.name,
      email: input.email,
      password: await hashPassword(input.password),
      email_confirm_token: createExpiryToken(),
    };
    return await this.userRepo.create(data);
  }

  /**
   * update a users profile
   */
  async updateUser(user: User, input: AccountUpdateProfileRequestDto): Promise<User> {
    const data: Partial<UserShape> = {
      name: input.name,
    };

    const where: Partial<UserShape> = {
      id: user.id,
    };

    // if DB email doesn't match input email then the user must be changing their email
    if (user.email !== input.email) {
      data.new_email = input.email;

      // if this token still has XXX seconds left then reuse else create new one
      if (!canReuseExpiryToken(user.email_confirm_token, EMAIL_CONFIRM_TOKEN_EXPIRES_AFTER_MS)) {
        data.email_confirm_token = createExpiryToken();
      }
    }

    return await this.userRepo.update(where, data);
  }

  /**
   * confirm users email
   */
  async confirmEmail(user: User): Promise<User> {
    const data: Partial<UserShape> = {
      email_confirm_token: null,
      new_email: null,

      // if newly registered user then `newEmail` is their email, if its an email confirmation after a
      // changed email then we pass in the `new_email`
      email: user.new_email || user.email,
    };

    const where: Partial<UserShape> = {
      id: user.id,
    };

    return await this.userRepo.update(where, data);
  }

  /**
   * get email confirm token
   */
  getEmailConfirmTokenExpiry(token: string): DateTime {
    return getTokenExpiry(token, EMAIL_CONFIRM_TOKEN_EXPIRES_AFTER_MS);
  }

  /**
   * check if email confirm token has expired
   */
  hasEmailConfirmTokenExpired(token: string): boolean {
    return hasTokenExpired(token, EMAIL_CONFIRM_TOKEN_EXPIRES_AFTER_MS);
  }

  /**
   * create password reset token if we don't already have an existing one in play
   */
  async updateUserWithPasswordResetToken(user: User): Promise<User> {
    // if this token still has XXX seconds left then reuse it rather than creating a new one
    if (canReuseExpiryToken(user.password_reset_token, PASSWORD_RESET_TOKEN_EXPIRES_AFTER_MS)) {
      return user;
    }

    const data: Partial<UserShape> = {
      password_reset_token: createExpiryToken(),
    };

    const where: Partial<UserShape> = {
      id: user.id,
    };

    return await this.userRepo.update(where, data);
  }

  /**
   * get password reset token
   */
  getPasswordResetTokenExpiry(token: string): DateTime {
    return getTokenExpiry(token, PASSWORD_RESET_TOKEN_EXPIRES_AFTER_MS);
  }

  /**
   * check if password reset token has expired
   */
  hasPasswordResetTokenExpired(token: string): boolean {
    return hasTokenExpired(token, PASSWORD_RESET_TOKEN_EXPIRES_AFTER_MS);
  }

  /**
   * change account password
   */
  async updatePassword(userId: string, password: string): Promise<User> {
    const data: Partial<UserShape> = {
      password_reset_token: null,
      password: await hashPassword(password),
    };

    const where: Partial<UserShape> = {
      id: userId,
    };

    return await this.userRepo.update(where, data);
  }

  /**
   *  verify user account credentials
   */
  async verifyCredentials(email: string, password: string): Promise<User | null> {
    const user = await this.findUserByEmail(email);
    if (!user) {
      return null;
    }

    const passwordMatched = await verifyPassword(user.password, password);
    if (!passwordMatched) {
      return null;
    }

    return user;
  }
}

// export default new UserService();
