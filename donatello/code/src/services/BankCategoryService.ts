import { Cradle } from '@/src/app/container';
import { BankCategoryCreateRequestDto } from '@/src/dtos/BankCategoryCreateRequestDto';
import { BankCategoryUpdateRequestDto } from '@/src/dtos/BankCategoryUpdateRequestDto';
import { BankTransactionShape } from '@/src/models/BankTransaction';
import BankCategoryRepository from '@/src/repositories/BankCategoryRepository';
import { v4 as uuidv4 } from 'uuid';
import { BankCategoryDatasource } from '@/src/datasources/BankCategoryDatasource';
import { buildDatasource, IDatasourceInput } from '@/src/lib/datasource';
import BankCategory, { BankCategoryShape } from '@/src/models/BankCategory';
import { HttpErrorNotFound } from '@/src/lib/errors';

export default class BankCategoryService {
  private bankCategoryRepo: BankCategoryRepository;

  constructor({ bankCategoryRepository }: Cradle) {
    this.bankCategoryRepo = bankCategoryRepository;
  }

  /**
   * find bank category by whatever
   */
  async findBankCategory(userId: string, obj: Partial<BankCategoryShape>): Promise<BankCategory> {
    const where = {
      ...obj,
      user_id: userId,
    };
    return await this.bankCategoryRepo.findOne(where);
  }

  /**
   * Find bank account or throw not found error
   */
  async findBankCategoryOrThrowNotFound(userId: string, id: string): Promise<BankCategory> {
    const where: Partial<BankTransactionShape> = {
      user_id: userId,
      id,
    };

    const row = await this.bankCategoryRepo.findOne(where);
    if (!row) {
      throw new HttpErrorNotFound();
    }

    return row;
  }

  /**
   * paginate bank categories
   */
  async paginateBankCategories(userId: string, input: IDatasourceInput) {
    return await buildDatasource(new BankCategoryDatasource(userId)).paginate(input);
  }

  /**
   * create bank category
   */
  async createBankCategory(userId: string, input: BankCategoryCreateRequestDto): Promise<BankCategory> {
    const data: Partial<BankCategoryShape> = {
      id: uuidv4(),
      user_id: userId,
      name: input.name,
    };
    return await this.bankCategoryRepo.create(data);
  }

  /**
   * find or create bank category
   */
  async findByNameOrCreateBankCategory(userId: string, input: BankCategoryCreateRequestDto): Promise<BankCategory> {
    const where: Partial<BankCategoryShape> = {
      user_id: userId,
      name: input.name,
    };
    let bankCategory = await this.bankCategoryRepo.findOne(where);

    if (!bankCategory) {
      bankCategory = await this.createBankCategory(userId, input);
    }

    return bankCategory;
  }

  /**
   * update bank category
   */
  async updateBankCategory(userId: string, id: string, input: BankCategoryUpdateRequestDto): Promise<BankCategory> {
    const data: Partial<BankCategoryShape> = {
      name: input.name,
    };

    const where: Partial<BankCategoryShape> = {
      user_id: userId,
      id,
    };

    return await this.bankCategoryRepo.update(where, data);
  }

  /**
   * delete bank category
   */
  async deleteBankCategory(userId: string, id: string): Promise<BankCategory> {
    const where: Partial<BankCategoryShape> = {
      user_id: userId,
      id,
    };
    return await this.bankCategoryRepo.delete(where);
  }
}

// export default new BankCategoryService();
