import { Cradle } from '@/src/app/container';
import { BankAccountCreateRequestDto } from '@/src/dtos/BankAccountCreateRequestDto';
import { ankAccountUpdateRequestDto } from '@/src/dtos/BankAccountUpdateRequestDto';
import BankAccountRepository from '@/src/repositories/BankAccountRepository';
import BankInstitutionService from '@/src/services/BankInstitutionService';
import { PassThrough } from 'stream';
import { v4 as uuidv4 } from 'uuid';
import { BankAccountDatasource } from '@/src/datasources/BankAccountDatasource';
import { buildDatasource, IDatasourceInput } from '@/src/lib/datasource';
import BankAccount, { BankAccountShape } from '@/src/models/BankAccount';
import { DatabaseQueryError, HttpErrorNotFound } from '@/src/lib/errors';

export default class BankAccountService {
  private bankAccountRepo: BankAccountRepository;
  private bankInstitutionService: BankInstitutionService;

  constructor({ bankAccountRepository, bankInstitutionService }: Cradle) {
    this.bankAccountRepo = bankAccountRepository;
    this.bankInstitutionService = bankInstitutionService;
  }

  /**
   * find bank account by whatever
   */
  async findBankAccount(userId: string, obj: Partial<BankAccountShape>): Promise<BankAccount> {
    const where = {
      ...obj,
      user_id: userId,
    };
    return await this.bankAccountRepo.findOne(where);
  }

  /**
   * Find bank account or throw not found error
   */
  async findBankAccountOrThrowNotFound(userId: string, id: string): Promise<BankAccount> {
    const where: Partial<BankAccountShape> = {
      user_id: userId,
      id,
    };

    const row = await this.bankAccountRepo.findOne(where);
    if (!row) {
      throw new HttpErrorNotFound();
    }

    return row;
  }

  /**
   * paginate bank account
   */
  async paginateBankAccounts(userId: string, input: IDatasourceInput) {
    return await buildDatasource(new BankAccountDatasource(userId)).paginate(input);
  }

  /**
   * export bank accounts
   */
  async exportBankAccounts(userId: string, input: IDatasourceInput): Promise<PassThrough> {
    return await buildDatasource(new BankAccountDatasource(userId)).exportStream(input);
  }

  /**
   * load default bank account relations
   */
  async loadRelations(bankAccount: BankAccount): Promise<void> {
    try {
      await bankAccount.$fetchGraph('[bankInstitution]');
    } catch (err) {
      throw new DatabaseQueryError(err, 'Failed to load relations');
    }
  }

  /**
   * create bank account
   */
  async createBankAccount(userId: string, input: BankAccountCreateRequestDto): Promise<BankAccount> {
    const data: Partial<BankAccountShape> = {
      id: uuidv4(),
      user_id: userId,
      name: input.name,
    };

    if (input.bankInstitution) {
      const bankInstitutionInput = {
        name: input.bankInstitution,
      };

      const bankInstitution = await this.bankInstitutionService.findByNameOrCreateBankInstitution(userId, bankInstitutionInput);
      data.bank_institution_id = bankInstitution.id;
    }

    return await this.bankAccountRepo.create(data);
  }

  /**
   * update bank account
   */
  async updateBankAccount(userId: string, id: string, input: ankAccountUpdateRequestDto): Promise<BankAccount> {
    const data: Partial<BankAccountShape> = {
      name: input.name,
    };

    const where: Partial<BankAccountShape> = {
      user_id: userId,
      id,
    };

    if (input.bankInstitution) {
      const bankInstitutionInput = {
        name: input.bankInstitution,
      };

      const bankInstitution = await this.bankInstitutionService.findByNameOrCreateBankInstitution(userId, bankInstitutionInput);
      data.bank_institution_id = bankInstitution.id;
    }

    return await this.bankAccountRepo.update(where, data);
  }

  /**
   * delete bank account
   */
  async deleteBankAccount(userId: string, id: string): Promise<BankAccount> {
    const where: Partial<BankAccountShape> = {
      user_id: userId,
      id,
    };
    return await this.bankAccountRepo.delete(where);
  }
}

// export default new BankAccountService();
