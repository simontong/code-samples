import { Cradle } from '@/src/app/container';
import { BankTagCreateRequestDto } from '@/src/dtos/BankTagCreateRequestDto';
import { BankTagUpdateRequestDto } from '@/src/dtos/BankTagUpdateRequestDto';
import BankTagRepository from '@/src/repositories/BankTagRepository';
import { v4 as uuidv4 } from 'uuid';
import { BankTagDatasource } from '@/src/datasources/BankTagDatasource';
import { buildDatasource, IDatasourceInput } from '@/src/lib/datasource';
import BankTag, { BankTagShape } from '@/src/models/BankTag';
import { HttpErrorNotFound } from '@/src/lib/errors';

export default class BankTagService {
  private bankTagRepo: BankTagRepository;

  constructor({ bankTagRepository }: Cradle) {
    this.bankTagRepo = bankTagRepository;
  }

  /**
   * find bank tag by whatever
   */
  async findBankTag(userId: string, obj: Partial<BankTagShape>): Promise<BankTag> {
    const where = {
      ...obj,
      user_id: userId,
    };
    return await this.bankTagRepo.findOne(where);
  }

  /**
   * Find bank account or throw not found error
   */
  async findBankTagOrThrowNotFound(userId: string, id: string): Promise<BankTag> {
    const row = await this.bankTagRepo.findOne({ user_id: userId, id });
    if (!row) {
      throw new HttpErrorNotFound();
    }

    return row;
  }

  /**
   * paginate bank tags
   */
  async paginateBankTags(userId: string, input: IDatasourceInput) {
    return await buildDatasource(new BankTagDatasource(userId)).paginate(input);
  }

  /**
   * create bank tag
   */
  async createBankTag(userId: string, input: BankTagCreateRequestDto): Promise<BankTag> {
    const data: Partial<BankTagShape> = {
      id: uuidv4(),
      user_id: userId,
      name: input.name,
    };
    return await this.bankTagRepo.create(data);
  }

  /**
   * find or create bank tag
   */
  async findByNameOrCreateBankTag(userId: string, input: BankTagCreateRequestDto): Promise<BankTag> {
    const where: Partial<BankTagShape> = {
      user_id: userId,
      name: input.name,
    };
    let bankTag = await this.bankTagRepo.findOne(where);

    if (!bankTag) {
      bankTag = await this.createBankTag(userId, input);
    }

    return bankTag;
  }

  /**
   * update bank tag
   */
  async updateBankTag(userId: string, id: string, input: BankTagUpdateRequestDto): Promise<BankTag> {
    const data: Partial<BankTagShape> = {
      name: input.name,
    };

    const where: Partial<BankTagShape> = {
      user_id: userId,
      id,
    };

    return this.bankTagRepo.update(where, data);
  }

  /**
   * delete bank tag
   */
  async deleteBankTag(userId: string, id: string): Promise<BankTag> {
    const where: Partial<BankTagShape> = {
      user_id: userId,
      id,
    };
    return await this.bankTagRepo.delete(where);
  }
}

// export default new BankTagService();
