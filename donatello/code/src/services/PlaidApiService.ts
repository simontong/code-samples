import { PlaidWebhookItemRequestDto } from '@/src/dtos/PlaidWebhookItemRequestDto';
import { PlaidWebhookRequestDto } from '@/src/dtos/PlaidWebhookRequestDto';
import { PlaidWebhookResponseDto } from '@/src/dtos/PlaidWebhookResponseDto';
import { PlaidWebhookTransactionRequestDto } from '@/src/dtos/PlaidWebhookTransactionRequestDto';
import handleItemWebhook from '@/src/lib/plaid/handleItemWebhook';
import handleTransactionsWebhook from '@/src/lib/plaid/handleTransactionWebhook';
import _ from 'lodash';
import {
  Configuration, ItemGetRequest,
  ItemPublicTokenExchangeRequest, ItemWebhookUpdateRequest,
  LinkTokenCreateRequest,
  PlaidApi,
  PlaidEnvironments,
  Products,
} from 'plaid';
import { CountryCode } from 'plaid/api';
import { PlaidApiError, PlaidWebhookError } from '@/src/lib/errors';
import { Cradle } from '@/src/app/container';

type PlaidApiConfig = {
  clientName: string;
  language: string;
  env: string;
  clientId: string;
  secret: string;
  redirectUri: string;
  countryCodes: Array<CountryCode>;
  products: Array<Products>;
}

export default class PlaidApiService {
  readonly config: PlaidApiConfig;
  readonly client: PlaidApi;

  constructor({ config }: Cradle) {
    // prettier-ignore
    const [clientName, env, clientId, secret, redirectUri, countryCodes, products] = config
      .getManyOrThrow([
        'app.company.name',
        'plaid.env',
        'plaid.clientId',
        'plaid.secret',
        'plaid.redirectUri',
        'plaid.countryCodes',
        'plaid.products',
      ]);

    this.config = {
      clientName,
      language: 'en',
      env,
      clientId,
      secret,
      redirectUri,
      countryCodes: countryCodes.split(','),
      products: products.split(','),
    };

    const configuration = new Configuration({
      basePath: PlaidEnvironments[this.config.env],
      baseOptions: {
        headers: {
          'PLAID-CLIENT-ID': this.config.clientId,
          'PLAID-SECRET': this.config.secret,
          'Plaid-Version': '2020-09-14',
        },
      },
    });

    this.client = new PlaidApi(configuration);
  }

  /**
   * create link token
   */
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  async linkTokenCreate(userId: string, accessToken?: string, webhookUrl?: string) {
    const data: LinkTokenCreateRequest = {
      user: {
        // This should correspond to a unique id for the current user.
        client_user_id: userId,
      },
      language: this.config.language,
      client_name: this.config.clientName,
      products: this.config.products,
      country_codes: this.config.countryCodes,
      redirect_uri: this.config.redirectUri,
    };

    // if we are doing update mode then add access token
    if (accessToken) {
      data.access_token = accessToken;
    }

    // if webhook passed
    if (webhookUrl) {
      data.webhook = webhookUrl;
    }

    try {
      return await this.client.linkTokenCreate(data);
    } catch (err) {
      const info = { userId, data: _.get(err, 'response.data') };
      throw new PlaidApiError(err, 'Plaid failed to create link token').setInfo(info);
    }
  }

  /**
   * exchange link token for access token
   */
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  async itemPublicTokenExchange(publicToken: string) {
    const data: ItemPublicTokenExchangeRequest = {
      public_token: publicToken,
    };

    try {
      return await this.client.itemPublicTokenExchange(data);
    } catch (err) {
      const info = { data: _.get(err, 'response.data') };
      throw new PlaidApiError(err, 'Plaid failed to exchange public token').setInfo(info);
    }
  }

  /**
   * exchange link token for access token
   */
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  async itemGet(accessToken: string) {
    const data: ItemGetRequest = {
      access_token: accessToken,
    };

    try {
      return await this.client.itemGet(data);
    } catch (err) {
      const info = { data: _.get(err, 'response.data') };
      throw new PlaidApiError(err, 'Plaid failed to get item').setInfo(info);
    }
  }

  /**
   * update webhook url
   * @param accessToken
   * @param webhookUrl
   */
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  async itemWebhookUpdate(accessToken: string, webhookUrl: string) {
    const data: ItemWebhookUpdateRequest = {
      access_token: accessToken,
      webhook: webhookUrl,
    };

    try {
      return await this.client.itemWebhookUpdate(data);
    } catch (err) {
      const info = { data: _.get(err, 'response.data') };
      throw new PlaidApiError(err, 'Plaid failed to update webhook').setInfo(info);
    }
  }

  /**
   * handle incoming webhook
   */
  async handleWebhook(input: PlaidWebhookRequestDto): Promise<PlaidWebhookResponseDto> {
    switch (input.webhook_type) {
      case 'ITEM': {
        return await handleItemWebhook(input as PlaidWebhookItemRequestDto);
      }

      case 'TRANSACTIONS': {
        return await handleTransactionsWebhook(input as PlaidWebhookTransactionRequestDto);
      }
    }

    const info = { input };
    throw new PlaidWebhookError('Unhandled webhook type: %s', input.webhook_type).setInfo(info);
  }
}
