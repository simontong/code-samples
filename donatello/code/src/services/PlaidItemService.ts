import PlaidApiService from '@/src/services/PlaidApiService';
import { v4 as uuidv4 } from 'uuid';
import { Cradle } from '@/src/app/container';
import Config from '@/src/lib/config/Config';
import { CreatePlaidItemDto } from '@/src/dtos/CreatePlaidItemDto';
import { encrypt } from '@/src/lib/crypto';
import PlaidItem, { PlaidItemShape } from '@/src/models/PlaidItem';
import PlaidItemRepository from '@/src/repositories/PlaidItemRepository';

export default class PlaidItemService {
  private config: Config;
  private plaidItemRepo: PlaidItemRepository;
  private plaidApiService: PlaidApiService;

  constructor({ config, plaidItemRepository, plaidApiService }: Cradle) {
    this.config = config;
    this.plaidItemRepo = plaidItemRepository;
    this.plaidApiService = plaidApiService;
  }

  /**
   * create plaid item
   */
  async createPlaidItem(userId: string, input: CreatePlaidItemDto): Promise<PlaidItem> {
    const data: Partial<PlaidItemShape> = {
      id: uuidv4(),
      user_id: userId,
      item_eid: input.item_eid,
      institution_eid: input.institution_eid || null,
      access_token: encrypt(input.access_token, this.config.getOrThrow('app.encryptionKey')),
    };

    return await this.plaidItemRepo.create(data);
  }

  /**
   * exchange plaid item token and create new
   */
  async exchangeTokenAndCreatePlaidItem(userId: string, publicToken: string): Promise<PlaidItem> {
    const tokenRes = await this.plaidApiService.itemPublicTokenExchange(publicToken);
    const itemRes = await this.plaidApiService.itemGet(tokenRes.data.access_token);

    // store plaid item
    const input: CreatePlaidItemDto = {
      item_eid: itemRes.data.item.item_id,
      institution_eid: itemRes.data.item.institution_id,
      access_token: tokenRes.data.access_token,
    };
    return await this.createPlaidItem(userId, input);
  }
}
