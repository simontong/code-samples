import { Cradle } from '@/src/app/container';
import { BankInstitutionDatasource } from '@/src/datasources/BankInstitutionDatasource';
import { BankInstitutionCreateRequestDto } from '@/src/dtos/BankInstitutionCreateRequestDto';
import { BankInstitutionUpdateRequestDto } from '@/src/dtos/BankInstitutionUpdateRequestDto';
import { BankTransactionShape } from '@/src/models/BankTransaction';
import BankInstitutionRepository from '@/src/repositories/BankInstitutionRepository';
import { v4 as uuidv4 } from 'uuid';
import { buildDatasource, IDatasourceInput } from '@/src/lib/datasource';
import BankInstitution, { BankInstitutionShape } from '@/src/models/BankInstitution';
import { HttpErrorNotFound } from '@/src/lib/errors';

export default class BankInstitutionService {
  private bankInstitutionRepo: BankInstitutionRepository;

  constructor({ bankInstitutionRepository }: Cradle) {
    this.bankInstitutionRepo = bankInstitutionRepository;
  }

  /**
   * find bank institution by whatever
   */
  async findBankInstitution(userId: string, obj: Partial<BankInstitutionShape>): Promise<BankInstitution> {
    const where = {
      ...obj,
      user_id: userId,
    };
    return await this.bankInstitutionRepo.findOne(where);
  }

  /**
   * Find bank account or throw not found error
   */
  async findBankInstitutionOrThrowNotFound(userId: string, id: string): Promise<BankInstitution> {
    const where: Partial<BankTransactionShape> = {
      user_id: userId,
      id,
    };

    const row = await this.bankInstitutionRepo.findOne(where);
    if (!row) {
      throw new HttpErrorNotFound();
    }

    return row;
  }

  /**
   * paginate bank institutions
   */
  async paginateBankInstitutions(userId: string, input: IDatasourceInput) {
    return await buildDatasource(new BankInstitutionDatasource(userId)).paginate(input);
  }

  /**
   * create bank institution
   */
  async createBankInstitution(userId: string, input: BankInstitutionCreateRequestDto): Promise<BankInstitution> {
    const data: Partial<BankInstitutionShape> = {
      id: uuidv4(),
      user_id: userId,
      name: input.name,
    };
    return await this.bankInstitutionRepo.create(data);
  }

  /**
   * find or create bank institution
   */
  async findByNameOrCreateBankInstitution(userId: string, input: BankInstitutionCreateRequestDto): Promise<BankInstitution> {
    const where: Partial<BankInstitutionShape> = {
      user_id: userId,
      name: input.name,
    };
    let bankInstitution = await this.bankInstitutionRepo.findOne(where);

    if (!bankInstitution) {
      bankInstitution = await this.createBankInstitution(userId, input);
    }

    return bankInstitution;
  }

  /**
   * update bank institution
   */
  async updateBankInstitution(userId: string, id: string, input: BankInstitutionUpdateRequestDto): Promise<BankInstitution> {
    const data: Partial<BankInstitutionShape> = {
      name: input.name,
    };

    const where: Partial<BankInstitutionShape> = {
      user_id: userId,
      id,
    };

    return await this.bankInstitutionRepo.update(where, data);
  }

  /**
   * delete bank institution
   */
  async deleteBankInstitution(userId: string, id: string): Promise<BankInstitution> {
    const where: Partial<BankInstitutionShape> = {
      user_id: userId,
      id,
    };
    return await this.bankInstitutionRepo.delete(where);
  }
}

// export default new BankInstitutionService();
