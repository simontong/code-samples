import container, { Cradle } from '@/src/app/container';
import { BankCategoryCreateRequestDto } from '@/src/dtos/BankCategoryCreateRequestDto';
import { BankTagCreateRequestDto } from '@/src/dtos/BankTagCreateRequestDto';
import { BankTransactionBulkCreateRequestDto } from '@/src/dtos/BankTransactionBulkCreateRequestDto';
import { BankTransactionCreateRequestDto } from '@/src/dtos/BankTransactionCreateRequestDto';
import { BankTransactionUpdateRequestDto } from '@/src/dtos/BankTransactionUpdateRequestDto';
import { BasePage } from '@/src/lib/database/BasePage';
import BankTransactionRepository from '@/src/repositories/BankTransactionRepository';
import BankCategoryService from '@/src/services/BankCategoryService';
import BankTagService from '@/src/services/BankTagService';
import { asValue } from 'awilix';
import { PassThrough } from 'stream';
import { v4 as uuidv4 } from 'uuid';
import { BankTransactionDatasource } from '@/src/datasources/BankTransactionDatasource';
import { buildDatasource, IDatasourceInput } from '@/src/lib/datasource';
import BankCategory from '@/src/models/BankCategory';
import BankTag from '@/src/models/BankTag';
import BankTagBankTransaction from '@/src/models/BankTagBankTransaction';
import BankTransaction, { BankTransactionShape } from '@/src/models/BankTransaction';
import { DatabaseQueryError, HttpErrorNotFound } from '@/src/lib/errors';

export default class BankTransactionService {
  private bankCategoryService: BankCategoryService;
  private bankTagService: BankTagService;
  private bankTransactionRepo: BankTransactionRepository;

  constructor(opts: Cradle) {
    this.bankCategoryService = opts.bankCategoryService;
    this.bankTagService = opts.bankTagService;
    this.bankTransactionRepo = opts.bankTransactionRepository;
  }

  /**
   * Find bank account or throw not found error
   */
  async findBankTransactionOrThrowNotFound(userId: string, id: string): Promise<BankTransaction> {
    const where: Partial<BankTransactionShape> = {
      user_id: userId,
      id,
    };

    const row = await this.bankTransactionRepo.findOne(where);
    if (!row) {
      throw new HttpErrorNotFound();
    }

    return row;
  }

  /**
   * paginate bank transactions
   */
  async paginateBankTransactions(userId: string, input: IDatasourceInput): Promise<BasePage<BankTransaction>> {
    return await buildDatasource(new BankTransactionDatasource(userId)).paginate(input);
  }

  /**
   * export bank transactions
   */
  async exportBankTransactions(userId: string, input: IDatasourceInput): Promise<PassThrough> {
    return await buildDatasource(new BankTransactionDatasource(userId)).exportStream(input);
  }

  /**
   * load default bank transaction relations
   */
  async loadRelations(bankTransaction: BankTransaction): Promise<void> {
    try {
      await bankTransaction.$fetchGraph('[bankAccount,bankCategory,bankTags,currency]');
    } catch (err) {
      throw new DatabaseQueryError(err, 'Failed to load relations');
    }
  }

  /**
   * create bank transaction
   */
  async createBankTransaction(userId: string, input: BankTransactionCreateRequestDto): Promise<BankTransaction> {
    // prepare input data
    const data = await this.prepareBankTransactionUpsert(userId, null, input);

    // create bank category
    if (input.bankCategory) {
      const bankCategoryInput = {
        name: input.bankCategory,
      };

      const bankCategory = await this.bankCategoryService.findByNameOrCreateBankCategory(userId, bankCategoryInput);
      data.bank_category_id = bankCategory.id;
    }

    // create bank transaction
    const bankTransaction = await this.bankTransactionRepo.create(data);

    // create / find bank tags
    if (input.bankTags && input.bankTags.length > 0) {
      const bankTags = await Promise.all(
        input.bankTags.map((name) => {
          const bankTagInput = {
            name,
          };
          return this.bankTagService.findByNameOrCreateBankTag(userId, bankTagInput);
        }),
      );

      // get or create and attach bank tags
      const bankTagIds = bankTags.map((i) => i.id);
      await this.attachBankTagIds(bankTransaction.id, bankTagIds);
    }

    return bankTransaction;
  }

  /**
   * update bank transaction
   */
  async updateBankTransaction(
    userId: string,
    id: string,
    input: BankTransactionUpdateRequestDto,
  ): Promise<BankTransaction> {
    // prepare input data
    const data = await this.prepareBankTransactionUpsert(userId, id, input);

    // prep where for update
    const where: Partial<BankTransactionShape> = {
      user_id: userId,
      id,
    };

    // create bank category
    if (input.bankCategory) {
      const bankCategoryInput = {
        name: input.bankCategory,
      };

      const bankCategory = await this.bankCategoryService.findByNameOrCreateBankCategory(userId, bankCategoryInput);
      data.bank_category_id = bankCategory.id;
    }

    // create transaction
    const bankTransaction = await this.bankTransactionRepo.update(where, data);

    // create / find bank tags
    let bankTagIds: string[] = [];
    if (input.bankTags && input.bankTags.length > 0) {
      const bankTags = await Promise.all(
        input.bankTags.map((name) => {
          const bankTagInput = {
            name,
          };
          return this.bankTagService.findByNameOrCreateBankTag(userId, bankTagInput);
        }),
      );

      // get or create and attach bank tags
      bankTagIds = bankTags.map((i) => i.id);
      await this.attachBankTagIds(bankTransaction.id, bankTagIds);
    }

    // detach bank tags not in `bankTagIds`
    await this.detachBankTagNotInIds(bankTransaction.id, bankTagIds);

    return bankTransaction;
  }

  /**
   * bulk insert a bunch of transactions at once
   */
  async bulkCreateBankTransactions(
    userId: string,
    input: BankTransactionBulkCreateRequestDto,
  ): Promise<BankTransaction[]> {
    // start transaction
    let trx;
    try {
      trx = await BankTransaction.startTransaction();
    } catch (err) {
      const info = { userId };
      throw new DatabaseQueryError(err, 'Create bank transaction bulk failed to start transaction').setInfo(info);
    }

    // create a scoped container with a transaction so all calls to
    // containing dependencies will use the transaction
    const scopedCradle = container.createScope()
      .register('trx', asValue(trx))
      .cradle;

    // cache created / found categories and bank tags to avoid duplicate db lookups
    const storedBankCategories: BankCategory[] = [];
    const storedBankTags: BankTag[] = [];

    // prepare batch insert items
    const batchBankTransactionInput = [];
    const batchBankTagBankTransactionInput = [];

    // prepare the bank transactions
    for (const item of input.bankTransactions) {
      const data = await this.prepareBankTransactionUpsert(userId, null, {
        ...item,
        bank_account_id: input.bank_account_id, // only allow single bank account per bulk transaction create
      });

      // add bank category
      if (item.bankCategory) {
        let bankCategory = storedBankCategories.find(i => i.name === item.bankCategory);

        if (!bankCategory) {
          const bankCategoryInput: BankCategoryCreateRequestDto = {
            name: item.bankCategory,
          };

          // attempt find / create
          try {
            bankCategory = await scopedCradle.bankCategoryService.findByNameOrCreateBankCategory(userId, bankCategoryInput);
          } catch (err) {
            await trx.rollback();

            const info = { userId };
            throw new DatabaseQueryError(err, 'Create bank transaction bulk failed to batch insert').setInfo(info);
          }

          // stored so we don't do multiple db lookups
          storedBankCategories.push(bankCategory);
        }

        // add bank category to data for transaction
        data.bank_category_id = bankCategory.id;
      }

      // lookup / create bank tags for bulk adding later in function
      if (item.bankTags && item.bankTags.length > 0) {
        for (const bankTagName of item.bankTags) {
          let bankTag = storedBankTags.find(i => i.name === bankTagName);

          if (!bankTag) {
            const bankTagInput: BankTagCreateRequestDto = {
              name: bankTagName,
            };

            // attempt find / create
            try {
              bankTag = await scopedCradle.bankTagService.findByNameOrCreateBankTag(userId, bankTagInput);
            } catch (err) {
              await trx.rollback();

              const info = { userId };
              throw new DatabaseQueryError(err, 'Create bank transaction bulk failed to batch insert').setInfo(info);
            }

            // stored so we don't do multiple db lookups
            storedBankTags.push(bankTag);
          }

          // add to array for bank_tag_bank_transactions insert
          batchBankTagBankTransactionInput.push({
            bank_tag_id: bankTag.id,
            bank_transaction_id: data.id,
          });
        }
      }

      // add to array for batch bank_transactions insert
      batchBankTransactionInput.push(data);
    }

    // run batch insert
    let rows;
    try {
      rows = await scopedCradle.bankTransactionRepository.bulkCreate(batchBankTransactionInput);
    } catch (err) {
      await trx.rollback();

      const info = { userId };
      throw new DatabaseQueryError(err, 'Create bank transaction bulk failed to batch insert').setInfo(info);
    }

    // run batch insert for bank_tag_bank_transactions
    try {
      await BankTagBankTransaction.query(trx)
        .insert(batchBankTagBankTransactionInput)
        .onConflict(['bank_tag_id', 'bank_transaction_id'])
        .ignore();
    } catch (err) {
      await trx.rollback();

      const info = { userId };
      throw new DatabaseQueryError(err, 'Create bank transaction bulk failed to commit transaction').setInfo(info);
    }

    // commit transaction
    try {
      await trx.commit();
    } catch (err) {
      await trx.rollback();

      const info = { userId };
      throw new DatabaseQueryError(err, 'Create bank transaction bulk failed to commit transaction').setInfo(info);
    }

    return rows;
  }

  /**
   * prepare upsert
   */
  async prepareBankTransactionUpsert(
    userId: string,
    id: string | null,
    input: BankTransactionCreateRequestDto | BankTransactionUpdateRequestDto,
  ): Promise<Partial<BankTransactionShape>> {
    return {
      id: id || uuidv4(),
      user_id: userId,
      bank_account_id: input.bank_account_id || null,
      bank_category_id: null,
      currency_code: input.currency_code,
      date: input.date,
      payee: input.payee,
      amount: input.amount,
      note: input.note || null,
    };
  }

  /**
   * delete bank transaction
   */
  async deleteBankTransaction(userId: string, id: string): Promise<BankTransaction> {
    const where: Partial<BankTransactionShape> = {
      user_id: userId,
      id,
    };
    return await this.bankTransactionRepo.delete(where);
  }

  /**
   * Attach bank transaction to bank tag
   */
  async attachBankTagIds(bankTransactionId: string, bankTagIds: string[]): Promise<void> {
    const data = bankTagIds.map((id) => {
      return {
        bank_transaction_id: bankTransactionId,
        bank_tag_id: id,
      };
    });

    try {
      await BankTagBankTransaction.query().insert(data).onConflict(['bank_tag_id', 'bank_transaction_id']).ignore();
    } catch (err) {
      const info = { bankTransactionId, bankTagIds };
      throw new DatabaseQueryError(err, 'Failed attaching bank tags to bank transaction').setInfo(info);
    }
  }

  /**
   * Detach bank transaction to bank tag
   */
  async detachBankTagNotInIds(bankTransactionId: string, bankTagIds: string[]): Promise<void> {
    try {
      await BankTagBankTransaction.query()
        .where('bank_transaction_id', bankTransactionId)
        .whereNotIn('bank_tag_id', bankTagIds)
        .delete();
    } catch (err) {
      const info = { bankTransactionId, bankTagIds };
      throw new DatabaseQueryError(err, 'Failed attaching bank tags to bank transaction').setInfo(info);
    }
  }
}

// export default new BankTransactionService();
