import { createHash } from 'crypto';
import { Cradle } from '@/src/app/container';
import Cache from '@/src/lib/cache/Cache';
import Currency, { CurrencyShape } from '@/src/models/Currency';
import CurrencyRepository from '@/src/repositories/CurrencyRepository';

export default class CurrencyService {
  private currencyRepo: CurrencyRepository;
  private cache: Cache;

  constructor({ currencyRepository, cache }: Cradle) {
    this.currencyRepo = currencyRepository;
    this.cache = cache;
  }

  /**
   * find currency by whatever
   */
  async findCurrency(obj: Partial<CurrencyShape>): Promise<Currency> {
    const where = {
      ...obj,
    };
    return await this.currencyRepo.findOne(where);
  }

  /**
   * find cached currency
   */
  async findCurrencyCached(obj: Partial<CurrencyShape>): Promise<Currency> {
    // hash args
    const hash = createHash('md5')
      .update(JSON.stringify(obj))
      .digest('hex');

    const cacheKey = [CurrencyService.name, 'findCurrencyCached', hash].join(':');
    const cacheTtl = 60;

    return this.cache.remember(cacheKey, cacheTtl, () => this.findCurrency(obj));
  }

  /**
   * find currency by code
   */
  findCurrencyByCode = async (code: string): Promise<Currency> => {
    const where: Partial<CurrencyShape> = {
      code,
    };
    return await this.currencyRepo.findOne(where);
  };

  /**
   * Return currencies
   */
  async findCurrencies(): Promise<Currency[]> {
    const where: Partial<CurrencyShape> = {};
    return await this.currencyRepo.find(where);
  }
}

// export default new CurrencyService();
