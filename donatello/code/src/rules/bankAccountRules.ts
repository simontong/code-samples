import { string } from 'yup';

export const bankAccountRules = {
  // prettier-ignore
  name: string()
    .label('Name')
    .trim()
    .required(),

  // prettier-ignore
  bankInstitution: string()
    .label('Bank Institution')
    .nullable()
    .trim(),
};
