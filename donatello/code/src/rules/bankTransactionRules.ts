import dateDatabaseValid from '@/src/lib/validators/rules/dateDatabaseValid';
import { bankAccountExists, currencyExists } from '@/src/lib/validators/rules/recordExists';
import { array, number, string, StringSchema } from 'yup';

// largest min and max for amount
const amountMin = -9999999999999;
const amountMax = 9999999999999;

// largest amount of bank tags
const bankTagsMax = 20;

export const bankTransactionRules = {
  // prettier-ignore
  bank_account_id: (userId: string): StringSchema<string | null | undefined> => {
    return string()
      .label('Bank account')
      .nullable()
      .trim()
      .test('bankAccountExists', '${path} does not exist', bankAccountExists(userId));
  },

  // prettier-ignore
  bankCategory: string()
    .label('Category')
    .nullable()
    .trim(),

  // prettier-ignore
  bankTags: array()
    .label('Tags')
    .of(
      string()
        .required()
        .trim(),
    )
    .notRequired()
    .ensure()
    .compact()
    .max(bankTagsMax),

  // prettier-ignore
  currency_code: string()
    .label('Currency')
    .required()
    .test('currencyExists', '${path} does not exist', currencyExists()),

  // prettier-ignore
  date: string()
    .label('Date')
    .required()
    .trim()
    .test('dateIsValid', '${path} is an invalid date', dateDatabaseValid),

  // prettier-ignore
  payee: string()
    .label('Payee')
    .required()
    .trim(),

  // prettier-ignore
  amount: number()
    .label('Amount')
    .required()
    .integer('${path} must be an integer')
    .min(amountMin, '${path} is too small')
    .max(amountMax, '${path} is too big'),

  // prettier-ignore
  note: string()
    .label('Note')
    .nullable()
    .trim(),
};
