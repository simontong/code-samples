import { string } from 'yup';

export const bankTagRules = {
  // prettier-ignore
  name: string()
    .label('Name')
    .required()
    .trim(),
};
