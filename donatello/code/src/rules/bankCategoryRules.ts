import { string } from 'yup';

export const bankCategoryRules = {
  // prettier-ignore
  name: string()
    .label('Name')
    .required()
    .trim(),
};
