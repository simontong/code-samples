import { string } from 'yup';

export const bankInstitutionRules = {
  // prettier-ignore
  name: string()
    .label('Name')
    .required()
    .trim(),
};
