import { string } from 'yup';
import regexes from '@/src/consts/regexes';

export const plaidRules = {
  // prettier-ignore
  publicToken: string()
    .label('Public Token')
    .required()
    .matches(
      new RegExp(`^public-(sandbox|development|production)-${regexes.uuid}$`, 'i'),
      '${path} is invalid',
    ),

  // prettier-ignore
  webhookType: string()
    .label('Webhook Type')
    .required(),

  // prettier-ignore
  webhookCode: string()
    .label('Webhook Code')
    .required(),
};
