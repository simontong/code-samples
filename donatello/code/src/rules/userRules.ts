import { EXPIRY_TOKEN_REGEXP } from '@/src/lib/expiryToken';
import { ref, string } from 'yup';

// min password length
const passwordMin = 8;

export const userRules = {
  // prettier-ignore
  name: string()
    .label('Name')
    .required()
    .trim(),

  // prettier-ignore
  email: string()
    .label('Email')
    .required()
    .trim()
    .email(),

  // prettier-ignore
  new_email: string()
    .label('New Email')
    .required()
    .trim(),

  // prettier-ignore
  email_confirm_token: string()
    .label('Token')
    .required()
    .trim()
    .matches(EXPIRY_TOKEN_REGEXP, '${path} is invalid'),

  // prettier-ignore
  password: string()
    .label('Password')
    .required()
    .matches(/[a-z]/, '${path} must contain at least one lowercase letter')
    .matches(/[0-9]/, '${path} must contain least one number')
    .min(passwordMin),

  // prettier-ignore
  password_reset_token: string()
    .label('Token')
    .required()
    .matches(EXPIRY_TOKEN_REGEXP, '${path} is invalid'),

  // prettier-ignore
  passwordConfirm: string()
    .label('Confirm Password')
    .required()
    .oneOf([ref('password')], 'Passwords must match'),

  // prettier-ignore
  totpToken: string()
    .label('TOTP Token')
    .required(),
};
