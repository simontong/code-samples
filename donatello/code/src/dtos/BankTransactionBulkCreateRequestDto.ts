export type BankTransactionBulkCreateRequestDto = {
  bank_account_id?: string | null;
  bankTransactions: BankTransactionLineBulkCreateRequestDto[];
};

// single line in bankTransaction request (note the absent bank_account_id)
export type BankTransactionLineBulkCreateRequestDto = {
  bankCategory?: string | null;
  bankTags?: string[];
  currency_code: string;
  date: string;
  payee: string;
  amount: number;
  note?: string | null;
};
