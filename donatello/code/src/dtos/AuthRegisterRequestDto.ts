export type AuthRegisterRequestDto = {
  name: string;
  email: string;
  password: string;
  passwordConfirm: string;
};
