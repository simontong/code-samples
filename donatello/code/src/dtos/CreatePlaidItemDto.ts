import { encrypt } from '@/src/lib/crypto';

export type CreatePlaidItemDto = {
  item_eid: string;
  institution_eid?: string | null;
  access_token: string;
}
