export type ankAccountUpdateRequestDto = {
  name: string;
  bankInstitution?: string | null;
};
