export type AuthLostPasswordResetRequestDto = {
  token: string;
  password: string;
  passwordConfirm: string;
};
