import { PlaidWebhookError } from '@/src/dtos/PlaidWebhookErrorDto';
import { PlaidWebhookTypes } from '@/src/dtos/PlaidWebhookRequestDto';

type PlaidWebhookItemCodes = 'ERROR' | 'PENDING_EXPIRATION' | 'USER_PERMISSION_REVOKED' | 'WEBHOOK_UPDATE_ACKNOWLEDGED';

export type PlaidWebhookItemRequestDto = {
  webhook_type: PlaidWebhookTypes;
  webhook_code: PlaidWebhookItemCodes;
  item_id: string;
  error: PlaidWebhookError | null;
  new_webhook_url?: string;
}
