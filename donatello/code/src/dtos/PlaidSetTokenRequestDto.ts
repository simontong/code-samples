export type PlaidSetTokenRequestDto = {
  publicToken: string;
};
