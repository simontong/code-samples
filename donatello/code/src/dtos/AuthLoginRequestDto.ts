export type AuthLoginRequestDto = {
  email: string;
  password: string;
};
