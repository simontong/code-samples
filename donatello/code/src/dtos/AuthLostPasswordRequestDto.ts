export type AuthLostPasswordRequestDto = {
  email: string;
};
