export type BankCategoryUpdateRequestDto = {
  name: string;
};
