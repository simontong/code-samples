export type PlaidWebhookError = {
  error_type: string;
  error_code: string;
  display_message: string;
  request_id: string;
  causes: Array<string>;
  status: number;
  documentation_url: string;
  suggested_action: string;
}
