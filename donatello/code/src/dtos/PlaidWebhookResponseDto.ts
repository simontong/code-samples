import { WebsocketResponseDto } from '@/src/dtos/WebsocketResponseDto';

export type PlaidWebhookResponseDto = {
  pushData?: WebsocketResponseDto
}
