import { PlaidWebhookError } from '@/src/dtos/PlaidWebhookErrorDto';
import { PlaidWebhookTypes } from '@/src/dtos/PlaidWebhookRequestDto';

type PlaidWebhookTransactionCodes = 'INITIAL_UPDATE' | 'HISTORICAL_UPDATE' | 'DEFAULT_UPDATE' | 'TRANSACTIONS_REMOVED';

export type PlaidWebhookTransactionRequestDto = {
  webhook_type: PlaidWebhookTypes;
  webhook_code: PlaidWebhookTransactionCodes;
  item_id: string;
  error: PlaidWebhookError | null;
  new_webhook_url?: string;
  new_transactions?: number,
  removed_transactions?: Array<number>,
}
