import { PlaidWebhookItemRequestDto } from '@/src/dtos/PlaidWebhookItemRequestDto';
import { PlaidWebhookTransactionRequestDto } from '@/src/dtos/PlaidWebhookTransactionRequestDto';

export type PlaidWebhookTypes =
  'ASSETS'
  | 'AUTH'
  | 'BANK_TRANSFERS'
  | 'DEPOSIT_SWITCH'
  | 'HOLDINGS'
  | 'INCOME'
  | 'ITEM'
  | 'LIABILITIES'
  | 'PAYMENT_INITIATION'
  | 'TRANSACTIONS';

export type PlaidWebhookRequestDto = PlaidWebhookItemRequestDto | PlaidWebhookTransactionRequestDto;
