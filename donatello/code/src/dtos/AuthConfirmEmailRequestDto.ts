export type AuthConfirmEmailRequestDto = {
  token: string;
};
