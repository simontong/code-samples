export type BankAccountCreateRequestDto = {
  name: string;
  bankInstitution?: string | null;
};
