export type WebsocketResponseDto = {
  eventName: string;
  data: unknown;
}
