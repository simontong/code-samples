export type AccountUpdateProfileRequestDto = {
  name: string;
  email: string;
};
