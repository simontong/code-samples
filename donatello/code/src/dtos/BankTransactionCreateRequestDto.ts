export type BankTransactionCreateRequestDto = {
  bank_account_id?: string | null;
  bankCategory?: string | null;
  bankTags?: string[];
  currency_code: string;
  date: string;
  payee: string;
  amount: number;
  note?: string | null;
};
