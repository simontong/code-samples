import '@/src/locale/yup';
import yargs from 'yargs';
import { captureError } from '@/src/lib/errors/errorHandling';
import commands from './commands';

async function main() {
  yargs
    .scriptName('./cli')
    .alias('V', 'version')
    .alias('h', 'help')
    .showHelpOnFail(false)
    .demandCommand()
    .recommendCommands()
    .strict()
    .wrap(130)
    // @ts-ignore - no types for array of args yet
    .command(commands)
    .parse();
}

// handle processes
process.on('uncaughtException', (err: Error) => captureError(err));
process.on('unhandledRejection', (err: Error) => captureError(err));

// start
main().catch((err: Error) => captureError(err));
