import util from 'util';
import { Arguments } from 'yargs';
import { ValidationError } from 'yup';
import { formatErrors } from '@/src/lib/validators/validateOrThrow';

type CommandHandler = (args: Arguments) => void;

/**
 * wrap handlers
 */
export default function commandHandler(handler: CommandHandler) {
  return async (args: Arguments): Promise<void> => {
    let exitCode = 0;

    try {
      await handler(args);
    } catch (err) {
      handleError(err);
      exitCode = 1;
    }

    process.exit(exitCode);
  };
}

/**
 * handle error
 */
const handleError = (err: Error) => {
  // if not a validation error
  if (!ValidationError.isError(err)) {
    throw err;
  }

  // format errors and output
  const errors = formatErrors(err);
  if (errors.length > 0) {
    console.log(util.inspect(errors, { depth: null, compact: 1, colors: true }));
  }
};
