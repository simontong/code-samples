import Table from 'cli-table';
import router from '@/src/app/routes';
import Router from 'koa-router';

export default {
  command: 'api:routes',
  describe: 'List API routes',
  handler,
};

async function handler(): Promise<void> {
  const table = new Table({
    head: ['Method', 'URI', 'Name', 'Middleware'],
  });

  // @ts-ignore
  const availableMethods: Array<string> = new Router().methods;

  for (const stack of router.stack) {
    // get methods for route, check if this route uses all methods
    let methods = stack.methods.filter((i) => !!i).join('|');
    const hasAllMethods = availableMethods.every((i) => methods.includes(i));
    if (hasAllMethods) {
      methods = '- ALL -';
    }

    const middleware = [...stack.stack]
      .filter((i) => !!i.name && i.name !== 'route')
      .map((i) => i.name)
      .join(',');

    // ignore routes without method (they are the routes that prefix and mount the
    // actual routes, eg. /auth and /user and /bank-account etc.)
    if (!methods) {
      continue;
    }

    // prettier-ignore
    table.push([
      methods,
      stack.path,
      stack.name || '',
      middleware,
    ]);
  }

  console.log(table.toString());
}
