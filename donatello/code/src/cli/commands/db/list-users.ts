import User from '@/src/models/User';
import _ from 'lodash';
import Table from 'cli-table';

export default {
  command: 'db:list-users',
  describe: 'List users',
  handler,
};

/**
 * handle command
 */
async function handler(): Promise<void> {
  // todo: eventually use XxxService here when figure out way to where/order/etc.
  const rows = await User.query().orderBy('email');

  const table = new Table({
    head: ['id', 'Email', ' Name'],
  });

  table.push(...rows.map((i) => _.at(i, ['id', 'email', 'name'])));

  console.log(table.toString());
}
