import Currency from '@/src/models/Currency';
import _ from 'lodash';
import Table from 'cli-table';

export default {
  command: 'db:list-currencies',
  describe: 'List currencies',
  handler,
};

/**
 * handle command
 */
async function handler(): Promise<void> {
  // todo: eventually use CurrencyService here when figure out way to where/order/etc.
  const currencies = await Currency.query().orderBy('code');

  const table = new Table({
    head: ['ID', 'Code', ' Name', 'Symbol', 'Precision'],
  });

  table.push(...currencies.map((i) => _.at(i, ['code', 'name', 'symbol', 'precision'])));

  console.log(table.toString());
}
