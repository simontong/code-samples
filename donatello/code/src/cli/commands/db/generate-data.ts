import _ from 'lodash';
import * as faker from 'faker';
import container from '@/src/app/container';
import {
  factoryBankAccount,
  factoryBankCategory,
  factoryBankTag,
  factoryBankTransaction,
  factoryUser,
} from '@/database/factory';

type DbGenerateArgs = {
  userId?: string;
};

export default {
  command: 'db:generate-data [userId]',
  describe: 'Fill database with random data',
  handler,
};

/**
 * handle command
 */
async function handler({ userId }: DbGenerateArgs): Promise<void> {
  let count;

  const rand = (max = 50) => faker.datatype.number(max);
  const randArrEl = (els: Array<unknown>) => faker.random.arrayElement(els);
  const randArrEls = (els: Array<unknown>) => faker.random.arrayElements(els);

  const currencies = await container.cradle.currencyService.findCurrencies();

  count = rand();
  console.log('Generating %d users ...', count);
  const factoryUsers = await Promise.all(_.times(count, () => factoryUser.build()));

  let users;
  if (!userId) {
    users = await Promise.all(factoryUsers.map((user) => container.cradle.userService.createUser(user)));
  } else {
    const user = await container.cradle.userService.findUserById(userId);
    if (!user) {
      console.log('Could not find user by id %s', userId);
      return;
    }
    users = [user];
  }

  for (const user of users) {
    const currency = randArrEl(currencies);

    count = rand();
    console.log('Generating %d bank accounts ...', count);
    const bankAccounts = await Promise.all(_.times(count, () => factoryBankAccount.create({ user_id: user.id })));

    count = rand();
    console.log('Generating %d bank categories ...', count);
    const bankCategories = await Promise.all(_.times(count, () => factoryBankCategory.create({ user_id: user.id })));

    count = rand();
    console.log('Generating %d bank tags ...', count);
    const bankTags = await Promise.all(_.times(count, () => factoryBankTag.create({ user_id: user.id })));

    for (const bankAccount of bankAccounts) {
      count = rand(1000);
      console.log('Generating %d bank transactions for account %s ...', count, bankAccount.name);
      const bankTransactions = await Promise.all(
        _.times(count, () =>
          factoryBankTransaction.create({
            user_id: user.id,
            bank_account_id: randArrEl([null, bankAccount.id]) as string,
            bank_category_id: randArrEl([null, ..._.map(bankCategories, 'id')]) as string,
            // @ts-ignore
            currency_code: currency.code,
          }),
        ),
      );

      console.log('Attaching tags to bank transaction ...');
      for (const bankTransaction of bankTransactions) {
        const bankTagIds = _.map(randArrEls(bankTags), 'id');
        if (bankTagIds.length) {
          await container.cradle.bankTransactionService.attachBankTagIds(bankTransaction.id, bankTagIds);
        }
      }
    }
  }
}
