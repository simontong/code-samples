import commandHandler from '@/src/cli/commandHandler';
import { CommandModule } from 'yargs';
import apiRoutes from './api/routes';
import dbGenerateData from './db/generate-data';
import dbListCurrencies from './db/list-currencies';
import dbListUsers from './db/list-users';
import plaidGetTransactions from './plaid/get-transactions';
import queueProcess from './queue/process';
import userCreate from './user/create';

// prettier-ignore
const commands: CommandModule[] = [
  apiRoutes,
  // @ts-ignore - required args bug - https://github.com/yargs/yargs/issues/1649
  dbGenerateData,
  dbListCurrencies,
  dbListUsers,
  plaidGetTransactions,
  queueProcess,
  // @ts-ignore - required args bug - https://github.com/yargs/yargs/issues/1649
  userCreate,
];

// wrap command handlers
for (const command of commands) {
  command.handler = commandHandler(command.handler);
}

export default commands;
