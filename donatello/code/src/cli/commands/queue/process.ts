import container from '@/src/app/container';
import _ from 'lodash';
import { Arguments, Argv } from 'yargs';
import { processQueue, queueManagers } from '@/src/lib/queues';
import { Job } from 'bullmq/dist/classes/job';
import { captureError } from '@/src/lib/errors/errorHandling';
import { QueueError } from '@/src/lib/errors';
import { initDatabaseConnection } from '@/src/lib/database/utils';
import { initRedisConnection } from '@/src/lib/redis/utils';

type QueueProcessArgs = {
  name?: string;
};

export default {
  command: 'queues:process [name]',
  describe: 'Process a queue',
  handler,
  builder: (yargs: Argv): Argv => {
    return yargs.positional('name', { desc: 'Name' });
  },
};

async function handler({ name }: Arguments<QueueProcessArgs>): Promise<void> {
  const queueNames = name ? [name] : queueManagers.map((qm) => qm.name);

  // check db and redis connections working
  const cradle = container.cradle;
  await initDatabaseConnection(cradle.db, cradle.logger);
  await initRedisConnection(cradle.redis, cradle.logger);

  for (const name of queueNames) {
    const worker = await processQueue(name);
    worker
      .on('error', (err: Error) => {
        captureError(new QueueError(err, 'Error on queue %s', name));
      })
      .on('failed', (job: Job, err: Error) => {
        captureError(new QueueError(err, 'Failed job on queue %s', name).setInfo({ jobId: job.id, data: job.data }));
      })
      .on('active', async (job: Job, prev: string) => {
        cradle.logger.trace({ msg: `job ${job.id} active on queue '${job.queueName}'`, job });
      })
      .on('completed', async (job: Job) => {
        cradle.logger.trace({ msg: `job ${job.id} completed on queue '${job.queueName}'`, job });
      });
    // .on('drained', () => console.log('drained'))
    // .on('progress', (job: Job, progress: unknown) => console.log('progress %j %s', job, progress));

    cradle.logger.debug('* Waiting for incoming jobs: %s ...', name);
  }

  // hold command open
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  await new Promise(_.noop);
}
