import container from '@/src/app/container';
import { decrypt } from '@/src/lib/crypto';
import PlaidItem from '@/src/models/PlaidItem';
import { TransactionsGetRequest } from 'plaid';
import util from 'util';

export default {
  command: 'plaid:get-transactions',
  describe: 'Fetch transactions from Plaid',
  handler,
};

/**
 * handle command
 */
async function handler(): Promise<void> {
  const cradle = container.cradle;
  const [key, clientId, secret] = cradle.config.getManyOrThrow<string[]>(['app.encryptionKey', 'plaid.clientId', 'plaid.secret']);

  cradle.db; // init db
  const plaidItem = await PlaidItem.query().first();
  const accessToken = decrypt(plaidItem.access_token, key);

  const data: TransactionsGetRequest = {
    access_token: accessToken,
    client_id: clientId,
    secret,
    start_date: '2021-01-01',
    end_date: '2021-08-01',
  };
  const res = await cradle.plaidApiService.client.transactionsGet(data);

  console.log(util.inspect(res, { depth: null, colors: true, compact: true }));
}
