import { userUnique } from '@/src/lib/validators/rules/recordUnique';
import { Arguments, Argv } from 'yargs';
import { object, SchemaOf } from 'yup';
import validateOrThrow from '@/src/lib/validators/validateOrThrow';
import container from '@/src/app/container';
import { userRules } from '@/src/rules/userRules';

type UserCreateArgs = {
  name: string;
  email: string;
  password: string;
};

export default {
  command: 'user:create <email> <password> <name>',
  describe: 'Create a user',
  handler,
  builder: (yargs: Argv): Argv => {
    return yargs
      .positional('email', { desc: 'Email', default: '' })
      .positional('password', { desc: 'Password', default: '' })
      .positional('name', { desc: 'Name', default: '' });
  },
};

/**
 * validate
 */
const validate = (args: Arguments<UserCreateArgs>): Promise<UserCreateArgs> => {
  const schema: SchemaOf<UserCreateArgs> = object({
    name: userRules.name,
    email: userRules.email.test('userUnique', 'Email already taken', userUnique('email')),
    password: userRules.password,
  });

  return validateOrThrow(schema, args);
};

/**
 * handle command
 */
async function handler(args: Arguments<UserCreateArgs>): Promise<void> {
  const input = await validate(args);

  const newUser = await container.cradle.userService.createUser(input);
  console.log('New user created: [%s] %s', newUser.id, newUser.email);
}
