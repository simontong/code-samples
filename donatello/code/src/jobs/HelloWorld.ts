import { Job, JobsOptions } from 'bullmq';
import BaseJob from '@/src/lib/queues/BaseJob';

type HelloWorldData = {
  hello: string;
};

type HelloWorldJob = Job<HelloWorldData>;

export class HelloWorld extends BaseJob {
  static queueName = 'hello-world';

  constructor(data: HelloWorldData, opts?: JobsOptions) {
    super(data, opts);
  }

  static async handle({ data }: HelloWorldJob): Promise<void> {
    console.log('hello world: %o', { data });
  }
}
