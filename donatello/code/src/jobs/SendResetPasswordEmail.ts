import { webClientUrl } from '@/src/utils/webClientUrl';
import { Job, JobsOptions } from 'bullmq';
import { Options } from 'nodemailer/lib/mailer';
import UAParser from 'ua-parser-js';
import BaseJob from '@/src/lib/queues/BaseJob';
import mailer from '@/src/lib/mail';
import container from '@/src/app/container';
import { QueueError } from '@/src/lib/errors';
import { template } from '@/src/lib/mail/utils';

type SendResetPasswordEmailData = {
  userId: string;
  userAgent?: string;
};

export type SendResetPasswordEmailJob = Job<SendResetPasswordEmailData>;

export class SendResetPasswordEmail extends BaseJob {
  static queueName = 'emails';

  constructor(data: SendResetPasswordEmailData, opts?: JobsOptions) {
    super(data, opts);
  }

  static async handle({ data }: SendResetPasswordEmailJob): Promise<void> {
    const config = container.cradle.config;

    const user = await container.cradle.userService.findUserById(data.userId);
    if (!user) {
      throw new QueueError('Failed to find user with id: %s', data.userId);
    }

    // just in case somehow the confirm token gets nulled
    if (!user.password_reset_token) {
      throw new QueueError('Password reset token missing', data.userId);
    }

    const appName = config.getOrThrow('app.company.name');
    const from = config.getOrThrow<string>('app.emails.transactional');
    const companyAddress = config.getOrThrow<Array<string>>('app.company.address');

    const subject = `[${appName}]: Reset your password`;

    let operatingSystem = '';
    let browserName = '';
    if (data.userAgent) {
      const ua = new UAParser(data.userAgent);
      operatingSystem = ua?.getOS()?.name || '';
      browserName = ua?.getBrowser()?.name || '';
    }

    const textParams = {
      valid_for: container.cradle.userService.getPasswordResetTokenExpiry(user.password_reset_token).toRelative(),
      app_name: appName,
      app_url: webClientUrl(),
      name: user.name,
      action_url: webClientUrl(`/auth/lost-password-reset/${user.password_reset_token}`),
      operating_system: operatingSystem,
      browser_name: browserName,
      support_url: webClientUrl('/support'),
      year: new Date().getFullYear(),
      company_address: companyAddress.join('\n'),
    };

    const htmlParams = {
      ...textParams,
      company_address: companyAddress.join('<br>'),
    };

    const text = template('emails/lost-password/content.txt', textParams);
    const html = template('emails/lost-password/content.html', htmlParams);

    const opts: Options = {
      from,
      to: user.email,
      subject,
      text,
      html,
    };

    await mailer.sendMail(opts);
  }
}
