import { webClientUrl } from '@/src/utils/webClientUrl';
import { Job, JobsOptions } from 'bullmq';
import { Options } from 'nodemailer/lib/mailer';
import UAParser from 'ua-parser-js';
import BaseJob from '@/src/lib/queues/BaseJob';
import mailer from '@/src/lib/mail';
import container from '@/src/app/container';
import { QueueError } from '@/src/lib/errors';
import { template } from '@/src/lib/mail/utils';

type SendEmailConfirmEmailData = {
  userId: string;
  userAgent?: string;
};

export type SendEmailConfirmEmailJob = Job<SendEmailConfirmEmailData>;

export class SendEmailConfirmEmail extends BaseJob {
  static queueName = 'emails';

  constructor(data: SendEmailConfirmEmailData, opts?: JobsOptions) {
    super(data, opts);
  }

  static async handle({ data }: SendEmailConfirmEmailJob): Promise<void> {
    const config = container.cradle.config;

    const user = await container.cradle.userService.findUserById(data.userId);
    if (!user) {
      throw new QueueError('Failed to find user with id: %s', data.userId);
    }

    if (!user.new_email) {
      throw new QueueError('New email missing for %s', data.userId);
    }

    // just in case somehow the confirm token gets nulled
    if (!user.email_confirm_token) {
      throw new QueueError('Email confirm token missing for %s', data.userId);
    }

    const appName = config.getOrThrow('app.company.name');
    const from = config.getOrThrow<string>('app.emails.transactional');
    const companyAddress = config.getOrThrow<Array<string>>('app.company.address');

    const subject = `[${appName}]: Please confirm your email`;

    let operatingSystem = '';
    let browserName = '';
    if (data.userAgent) {
      const ua = new UAParser(data.userAgent);
      operatingSystem = ua?.getOS()?.name || '';
      browserName = ua?.getBrowser()?.name || '';
    }

    const textParams = {
      valid_for: container.cradle.userService.getEmailConfirmTokenExpiry(user.email_confirm_token).toRelative(),
      app_name: appName,
      app_url: webClientUrl(),
      name: user.name,
      action_url: webClientUrl(`/auth/confirm-email/${user.email_confirm_token}`),
      operating_system: operatingSystem,
      browser_name: browserName,
      support_url: webClientUrl('/support'),
      year: new Date().getFullYear(),
      company_address: companyAddress.join('\n'),
    };

    const htmlParams = {
      ...textParams,
      company_address: companyAddress.join('<br>'),
    };

    const text = template('emails/change-email/content.txt', textParams);
    const html = template('emails/change-email/content.html', htmlParams);

    const opts: Options = {
      from,
      to: user.new_email,
      subject,
      text,
      html,
    };

    await mailer.sendMail(opts);
  }
}
