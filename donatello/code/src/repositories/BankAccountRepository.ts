import { Cradle } from '@/src/app/container';
import IRepository from '@/src/types/IRepository';
import { DatabaseQueryError } from '@/src/lib/errors';
import BankAccount, { BankAccountShape } from '@/src/models/BankAccount';
import { ColumnRef, Expression, Operator, PrimitiveValue, Transaction } from 'objection';

export default class BankAccountRepository implements IRepository<BankAccount> {
  trx: Transaction;

  constructor({ trx }: Cradle) {
    this.trx = trx;
  }

  async find(obj: Partial<BankAccountShape>): Promise<BankAccount[]>;
  async find(col: keyof Partial<BankAccountShape>, expr: Expression<PrimitiveValue>): Promise<BankAccount[]>;
  async find(col: ColumnRef, op?: Operator, expr?: Expression<PrimitiveValue>): Promise<BankAccount[]> {
    try {
      if (op && expr) {
        return await BankAccount.query(this.trx).where(col, op, expr);
      }
      if (op) {
        return await BankAccount.query(this.trx).where(col, op);
      }
      return await BankAccount.query(this.trx).where(col);
    } catch (err) {
      const info = { col, op, expr };
      throw new DatabaseQueryError(err, 'Find failed').setInfo(info);
    }
  }

  async findOne(obj: Partial<BankAccountShape>): Promise<BankAccount>;
  async findOne(col: keyof Partial<BankAccountShape>, expr: Expression<PrimitiveValue>): Promise<BankAccount>;
  async findOne(col: ColumnRef, op?: Operator, expr?: Expression<PrimitiveValue>): Promise<BankAccount> {
    try {
      if (op && expr) {
        return await BankAccount.query(this.trx).findOne(col, op, expr);
      }
      if (op) {
        return await BankAccount.query(this.trx).findOne(col, op);
      }
      return await BankAccount.query(this.trx).findOne(col);
    } catch (err) {
      const info = { col, op, expr };
      throw new DatabaseQueryError(err, 'Find one failed').setInfo(info);
    }
  }

  async create(data: Partial<BankAccountShape>): Promise<BankAccount> {
    try {
      return await BankAccount.query(this.trx).insert(data).returning('*');
    } catch (err) {
      const info = { data };
      throw new DatabaseQueryError(err, 'Create failed').setInfo(info);
    }
  }

  async update(where: Partial<BankAccountShape>, data: Partial<BankAccountShape>): Promise<BankAccount> {
    try {
      return await BankAccount.query(this.trx)
        .throwIfNotFound()
        .where(where)
        .update(data)
        .returning('*')
        .first();
    } catch (err) {
      const info = { where, data };
      throw new DatabaseQueryError(err, 'Update failed').setInfo(info);
    }
  }

  async delete(where: Partial<BankAccountShape>): Promise<BankAccount> {
    try {
      return await BankAccount.query(this.trx).throwIfNotFound().where(where).delete().returning('*').first();
    } catch (err) {
      const info = { where };
      throw new DatabaseQueryError(err, 'Delete failed').setInfo(info);
    }
  }
}
