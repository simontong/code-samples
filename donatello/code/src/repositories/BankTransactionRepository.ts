import { Cradle } from '@/src/app/container';
import IRepository from '@/src/types/IRepository';
import { DatabaseQueryError } from '@/src/lib/errors';
import BankTransaction, { BankTransactionShape } from '@/src/models/BankTransaction';
import { ColumnRef, Expression, Operator, PrimitiveValue, Transaction } from 'objection';

export default class BankTransactionRepository implements IRepository<BankTransaction> {
  trx: Transaction;

  constructor({ trx }: Cradle) {
    this.trx = trx;
  }

  async find(obj: Partial<BankTransactionShape>): Promise<BankTransaction[]>;
  async find(col: keyof Partial<BankTransactionShape>, expr: Expression<PrimitiveValue>): Promise<BankTransaction[]>;
  async find(col: ColumnRef, op?: Operator, expr?: Expression<PrimitiveValue>): Promise<BankTransaction[]> {
    try {
      if (op && expr) {
        return await BankTransaction.query(this.trx).where(col, op, expr);
      }
      if (op) {
        return await BankTransaction.query(this.trx).where(col, op);
      }
      return await BankTransaction.query(this.trx).where(col);
    } catch (err) {
      const info = { col, op, expr };
      throw new DatabaseQueryError(err, 'Find failed').setInfo(info);
    }
  }

  async findOne(obj: Partial<BankTransactionShape>): Promise<BankTransaction>;
  async findOne(col: keyof Partial<BankTransactionShape>, expr: Expression<PrimitiveValue>): Promise<BankTransaction>;
  async findOne(col: ColumnRef, op?: Operator, expr?: Expression<PrimitiveValue>): Promise<BankTransaction> {
    try {
      if (op && expr) {
        return await BankTransaction.query(this.trx).findOne(col, op, expr);
      }
      if (op) {
        return await BankTransaction.query(this.trx).findOne(col, op);
      }
      return await BankTransaction.query(this.trx).findOne(col);
    } catch (err) {
      const info = { col, op, expr };
      throw new DatabaseQueryError(err, 'Find one failed').setInfo(info);
    }
  }

  async create(data: Partial<BankTransactionShape>): Promise<BankTransaction> {
    try {
      return await BankTransaction.query(this.trx).insert(data).returning('*');
    } catch (err) {
      const info = { data };
      throw new DatabaseQueryError(err, 'Create failed').setInfo(info);
    }
  }

  async bulkCreate(data: Partial<BankTransactionShape>[]): Promise<BankTransaction[]> {
    try {
      return await BankTransaction.query(this.trx).insert(data).returning('*');
    } catch (err) {
      const info = { data };
      throw new DatabaseQueryError(err, 'Bulk create failed').setInfo(info);
    }
  }

  async update(where: Partial<BankTransactionShape>, data: Partial<BankTransactionShape>): Promise<BankTransaction> {
    try {
      return await BankTransaction.query(this.trx)
        .throwIfNotFound()
        .where(where)
        .update(data)
        .returning('*')
        .first();
    } catch (err) {
      const info = { where, data };
      throw new DatabaseQueryError(err, 'Update failed').setInfo(info);
    }
  }

  async delete(where: Partial<BankTransactionShape>): Promise<BankTransaction> {
    try {
      return await BankTransaction.query(this.trx).throwIfNotFound().where(where).delete().returning('*').first();
    } catch (err) {
      const info = { where };
      throw new DatabaseQueryError(err, 'Delete failed').setInfo(info);
    }
  }
}
