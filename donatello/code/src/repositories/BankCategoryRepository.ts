import { Cradle } from '@/src/app/container';
import IRepository from '@/src/types/IRepository';
import { DatabaseQueryError } from '@/src/lib/errors';
import BankCategory, { BankCategoryShape } from '@/src/models/BankCategory';
import { ColumnRef, Expression, Operator, PrimitiveValue, Transaction } from 'objection';

export default class BankCategoryRepository implements IRepository<BankCategory> {
  trx: Transaction;

  constructor({ trx }: Cradle) {
    this.trx = trx;
  }

  async find(obj: Partial<BankCategoryShape>): Promise<BankCategory[]>;
  async find(col: keyof Partial<BankCategoryShape>, expr: Expression<PrimitiveValue>): Promise<BankCategory[]>;
  async find(col: ColumnRef, op?: Operator, expr?: Expression<PrimitiveValue>): Promise<BankCategory[]> {
    try {
      if (op && expr) {
        return await BankCategory.query(this.trx).where(col, op, expr);
      }
      if (op) {
        return await BankCategory.query(this.trx).where(col, op);
      }
      return await BankCategory.query(this.trx).where(col);
    } catch (err) {
      const info = { col, op, expr };
      throw new DatabaseQueryError(err, 'Find failed').setInfo(info);
    }
  }

  async findOne(obj: Partial<BankCategoryShape>): Promise<BankCategory>;
  async findOne(col: keyof Partial<BankCategoryShape>, expr: Expression<PrimitiveValue>): Promise<BankCategory>;
  async findOne(col: ColumnRef, op?: Operator, expr?: Expression<PrimitiveValue>): Promise<BankCategory> {
    try {
      if (op && expr) {
        return await BankCategory.query(this.trx).findOne(col, op, expr);
      }
      if (op) {
        return await BankCategory.query(this.trx).findOne(col, op);
      }
      return await BankCategory.query(this.trx).findOne(col);
    } catch (err) {
      const info = { col, op, expr };
      throw new DatabaseQueryError(err, 'Find one failed').setInfo(info);
    }
  }

  async create(data: Partial<BankCategoryShape>): Promise<BankCategory> {
    try {
      return await BankCategory.query(this.trx).insert(data).returning('*');
    } catch (err) {
      const info = { data };
      throw new DatabaseQueryError(err, 'Create failed').setInfo(info);
    }
  }

  async update(where: Partial<BankCategoryShape>, data: Partial<BankCategoryShape>): Promise<BankCategory> {
    try {
      return await BankCategory.query(this.trx)
        .throwIfNotFound()
        .where(where)
        .update(data)
        .returning('*')
        .first();
    } catch (err) {
      const info = { where, data };
      throw new DatabaseQueryError(err, 'Update failed').setInfo(info);
    }
  }

  async delete(where: Partial<BankCategoryShape>): Promise<BankCategory> {
    try {
      return await BankCategory.query(this.trx).throwIfNotFound().where(where).delete().returning('*').first();
    } catch (err) {
      const info = { where };
      throw new DatabaseQueryError(err, 'Delete failed').setInfo(info);
    }
  }
}
