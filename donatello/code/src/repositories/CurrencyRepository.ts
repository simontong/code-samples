import { Cradle } from '@/src/app/container';
import IRepository from '@/src/types/IRepository';
import { DatabaseQueryError } from '@/src/lib/errors';
import Currency, { CurrencyShape } from '@/src/models/Currency';
import { ColumnRef, Expression, Operator, PrimitiveValue, Transaction } from 'objection';

export default class CurrencyRepository implements IRepository<Currency> {
  trx: Transaction;

  constructor({ trx }: Cradle) {
    this.trx = trx;
  }

  async find(obj: Partial<CurrencyShape>): Promise<Currency[]>;
  async find(col: keyof Partial<CurrencyShape>, expr: Expression<PrimitiveValue>): Promise<Currency[]>;
  async find(col: ColumnRef, op?: Operator, expr?: Expression<PrimitiveValue>): Promise<Currency[]> {
    try {
      if (op && expr) {
        return await Currency.query(this.trx).where(col, op, expr);
      }
      if (op) {
        return await Currency.query(this.trx).where(col, op);
      }
      return await Currency.query(this.trx).where(col);
    } catch (err) {
      const info = { col, op, expr };
      throw new DatabaseQueryError(err, 'Find failed').setInfo(info);
    }
  }

  async findOne(obj: Partial<CurrencyShape>): Promise<Currency>;
  async findOne(col: keyof Partial<CurrencyShape>, expr: Expression<PrimitiveValue>): Promise<Currency>;
  async findOne(col: ColumnRef, op?: Operator, expr?: Expression<PrimitiveValue>): Promise<Currency> {
    try {
      if (op && expr) {
        return await Currency.query(this.trx).findOne(col, op, expr);
      }
      if (op) {
        return await Currency.query(this.trx).findOne(col, op);
      }
      return await Currency.query(this.trx).findOne(col);
    } catch (err) {
      const info = { col, op, expr };
      throw new DatabaseQueryError(err, 'Find one failed').setInfo(info);
    }
  }

  async create(data: Partial<CurrencyShape>): Promise<Currency> {
    try {
      return await Currency.query(this.trx).insert(data).returning('*');
    } catch (err) {
      const info = { data };
      throw new DatabaseQueryError(err, 'Create failed').setInfo(info);
    }
  }

  async update(where: Partial<CurrencyShape>, data: Partial<CurrencyShape>): Promise<Currency> {
    try {
      return await Currency.query(this.trx)
        .throwIfNotFound()
        .where(where)
        .update(data)
        .returning('*')
        .first();
    } catch (err) {
      const info = { where, data };
      throw new DatabaseQueryError(err, 'Update failed').setInfo(info);
    }
  }

  async delete(where: Partial<CurrencyShape>): Promise<Currency> {
    try {
      return await Currency.query(this.trx).throwIfNotFound().where(where).delete().returning('*').first();
    } catch (err) {
      const info = { where };
      throw new DatabaseQueryError(err, 'Delete failed').setInfo(info);
    }
  }
}
