import { Cradle } from '@/src/app/container';
import { DatabaseQueryError } from '@/src/lib/errors';
import PlaidAccount, { PlaidAccountShape } from '@/src/models/PlaidAccount';
import IRepository from '@/src/types/IRepository';
import { ColumnRef, Expression, Operator, PrimitiveValue, Transaction } from 'objection';

export default class PlaidAccountRepository implements IRepository<PlaidAccount> {
  trx: Transaction;

  constructor({ trx }: Cradle) {
    this.trx = trx;
  }

  async find(obj: Partial<PlaidAccountShape>): Promise<PlaidAccount[]>;
  async find(col: keyof Partial<PlaidAccountShape>, expr: Expression<PrimitiveValue>): Promise<PlaidAccount[]>;
  async find(col: ColumnRef, op?: Operator, expr?: Expression<PrimitiveValue>): Promise<PlaidAccount[]> {
    try {
      if (op && expr) {
        return await PlaidAccount.query(this.trx).where(col, op, expr);
      }
      if (op) {
        return await PlaidAccount.query(this.trx).where(col, op);
      }
      return await PlaidAccount.query(this.trx).where(col);
    } catch (err) {
      const info = { col, op, expr };
      throw new DatabaseQueryError(err, 'Find failed').setInfo(info);
    }
  }

  async findOne(obj: Partial<PlaidAccountShape>): Promise<PlaidAccount>;
  async findOne(col: keyof Partial<PlaidAccountShape>, expr: Expression<PrimitiveValue>): Promise<PlaidAccount>;
  async findOne(col: ColumnRef, op?: Operator, expr?: Expression<PrimitiveValue>): Promise<PlaidAccount> {
    try {
      if (op && expr) {
        return await PlaidAccount.query(this.trx).findOne(col, op, expr);
      }
      if (op) {
        return await PlaidAccount.query(this.trx).findOne(col, op);
      }
      return await PlaidAccount.query(this.trx).findOne(col);
    } catch (err) {
      const info = { col, op, expr };
      throw new DatabaseQueryError(err, 'Find one failed').setInfo(info);
    }
  }

  async create(data: Partial<PlaidAccountShape>): Promise<PlaidAccount> {
    try {
      return await PlaidAccount.query(this.trx).insert(data).returning('*');
    } catch (err) {
      const info = { data };
      throw new DatabaseQueryError(err, 'Create failed').setInfo(info);
    }
  }

  async update(where: Partial<PlaidAccountShape>, data: Partial<PlaidAccountShape>): Promise<PlaidAccount> {
    try {
      return await PlaidAccount.query(this.trx)
        .throwIfNotFound()
        .where(where)
        .update(data)
        .returning('*')
        .first();
    } catch (err) {
      const info = { where, data };
      throw new DatabaseQueryError(err, 'Update failed').setInfo(info);
    }
  }

  async delete(where: Partial<PlaidAccountShape>): Promise<PlaidAccount> {
    try {
      return await PlaidAccount.query(this.trx).throwIfNotFound().where(where).delete().returning('*').first();
    } catch (err) {
      const info = { where };
      throw new DatabaseQueryError(err, 'Delete failed').setInfo(info);
    }
  }
}
