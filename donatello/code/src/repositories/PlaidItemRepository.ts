import { Cradle } from '@/src/app/container';
import { DatabaseQueryError } from '@/src/lib/errors';
import PlaidItem, { PlaidItemShape } from '@/src/models/PlaidItem';
import IRepository from '@/src/types/IRepository';
import { ColumnRef, Expression, Operator, PrimitiveValue, Transaction } from 'objection';

export default class PlaidItemRepository implements IRepository<PlaidItem> {
  trx: Transaction;

  constructor({ trx }: Cradle) {
    this.trx = trx;
  }

  async find(obj: Partial<PlaidItemShape>): Promise<PlaidItem[]>;
  async find(col: keyof Partial<PlaidItemShape>, expr: Expression<PrimitiveValue>): Promise<PlaidItem[]>;
  async find(col: ColumnRef, op?: Operator, expr?: Expression<PrimitiveValue>): Promise<PlaidItem[]> {
    try {
      if (op && expr) {
        return await PlaidItem.query(this.trx).where(col, op, expr);
      }
      if (op) {
        return await PlaidItem.query(this.trx).where(col, op);
      }
      return await PlaidItem.query(this.trx).where(col);
    } catch (err) {
      const info = { col, op, expr };
      throw new DatabaseQueryError(err, 'Find failed').setInfo(info);
    }
  }

  async findOne(obj: Partial<PlaidItemShape>): Promise<PlaidItem>;
  async findOne(col: keyof Partial<PlaidItemShape>, expr: Expression<PrimitiveValue>): Promise<PlaidItem>;
  async findOne(col: ColumnRef, op?: Operator, expr?: Expression<PrimitiveValue>): Promise<PlaidItem> {
    try {
      if (op && expr) {
        return await PlaidItem.query(this.trx).findOne(col, op, expr);
      }
      if (op) {
        return await PlaidItem.query(this.trx).findOne(col, op);
      }
      return await PlaidItem.query(this.trx).findOne(col);
    } catch (err) {
      const info = { col, op, expr };
      throw new DatabaseQueryError(err, 'Find one failed').setInfo(info);
    }
  }

  async create(data: Partial<PlaidItemShape>): Promise<PlaidItem> {
    try {
      return await PlaidItem.query(this.trx).insert(data).returning('*');
    } catch (err) {
      const info = { data };
      throw new DatabaseQueryError(err, 'Create failed').setInfo(info);
    }
  }

  async update(where: Partial<PlaidItemShape>, data: Partial<PlaidItemShape>): Promise<PlaidItem> {
    try {
      return await PlaidItem.query(this.trx)
        .throwIfNotFound()
        .where(where)
        .update(data)
        .returning('*')
        .first();
    } catch (err) {
      const info = { where, data };
      throw new DatabaseQueryError(err, 'Update failed').setInfo(info);
    }
  }

  async delete(where: Partial<PlaidItemShape>): Promise<PlaidItem> {
    try {
      return await PlaidItem.query(this.trx).throwIfNotFound().where(where).delete().returning('*').first();
    } catch (err) {
      const info = { where };
      throw new DatabaseQueryError(err, 'Delete failed').setInfo(info);
    }
  }
}
