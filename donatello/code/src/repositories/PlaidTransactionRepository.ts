import { Cradle } from '@/src/app/container';
import { DatabaseQueryError } from '@/src/lib/errors';
import PlaidTransaction, { PlaidTransactionShape } from '@/src/models/PlaidTransaction';
import IRepository from '@/src/types/IRepository';
import { ColumnRef, Expression, Operator, PrimitiveValue, Transaction } from 'objection';

export default class PlaidTransactionRepository implements IRepository<PlaidTransaction> {
  trx: Transaction;

  constructor({ trx }: Cradle) {
    this.trx = trx;
  }

  async find(obj: Partial<PlaidTransactionShape>): Promise<PlaidTransaction[]>;
  async find(col: keyof Partial<PlaidTransactionShape>, expr: Expression<PrimitiveValue>): Promise<PlaidTransaction[]>;
  async find(col: ColumnRef, op?: Operator, expr?: Expression<PrimitiveValue>): Promise<PlaidTransaction[]> {
    try {
      if (op && expr) {
        return await PlaidTransaction.query(this.trx).where(col, op, expr);
      }
      if (op) {
        return await PlaidTransaction.query(this.trx).where(col, op);
      }
      return await PlaidTransaction.query(this.trx).where(col);
    } catch (err) {
      const info = { col, op, expr };
      throw new DatabaseQueryError(err, 'Find failed').setInfo(info);
    }
  }

  async findOne(obj: Partial<PlaidTransactionShape>): Promise<PlaidTransaction>;
  async findOne(col: keyof Partial<PlaidTransactionShape>, expr: Expression<PrimitiveValue>): Promise<PlaidTransaction>;
  async findOne(col: ColumnRef, op?: Operator, expr?: Expression<PrimitiveValue>): Promise<PlaidTransaction> {
    try {
      if (op && expr) {
        return await PlaidTransaction.query(this.trx).findOne(col, op, expr);
      }
      if (op) {
        return await PlaidTransaction.query(this.trx).findOne(col, op);
      }
      return await PlaidTransaction.query(this.trx).findOne(col);
    } catch (err) {
      const info = { col, op, expr };
      throw new DatabaseQueryError(err, 'Find one failed').setInfo(info);
    }
  }

  async create(data: Partial<PlaidTransactionShape>): Promise<PlaidTransaction> {
    try {
      return await PlaidTransaction.query(this.trx).insert(data).returning('*');
    } catch (err) {
      const info = { data };
      throw new DatabaseQueryError(err, 'Create failed').setInfo(info);
    }
  }

  async update(where: Partial<PlaidTransactionShape>, data: Partial<PlaidTransactionShape>): Promise<PlaidTransaction> {
    try {
      return await PlaidTransaction.query(this.trx)
        .throwIfNotFound()
        .where(where)
        .update(data)
        .returning('*')
        .first();
    } catch (err) {
      const info = { where, data };
      throw new DatabaseQueryError(err, 'Update failed').setInfo(info);
    }
  }

  async delete(where: Partial<PlaidTransactionShape>): Promise<PlaidTransaction> {
    try {
      return await PlaidTransaction.query(this.trx).throwIfNotFound().where(where).delete().returning('*').first();
    } catch (err) {
      const info = { where };
      throw new DatabaseQueryError(err, 'Delete failed').setInfo(info);
    }
  }
}
