import { Cradle } from '@/src/app/container';
import IRepository from '@/src/types/IRepository';
import { DatabaseQueryError } from '@/src/lib/errors';
import BankTag, { BankTagShape } from '@/src/models/BankTag';
import { ColumnRef, Expression, Operator, PrimitiveValue, Transaction } from 'objection';

export default class BankTagRepository implements IRepository<BankTag> {
  trx: Transaction;

  constructor({ trx }: Cradle) {
    this.trx = trx;
  }

  async find(obj: Partial<BankTagShape>): Promise<BankTag[]>;
  async find(col: keyof Partial<BankTagShape>, expr: Expression<PrimitiveValue>): Promise<BankTag[]>;
  async find(col: ColumnRef, op?: Operator, expr?: Expression<PrimitiveValue>): Promise<BankTag[]> {
    try {
      if (op && expr) {
        return await BankTag.query(this.trx).where(col, op, expr);
      }
      if (op) {
        return await BankTag.query(this.trx).where(col, op);
      }
      return await BankTag.query(this.trx).where(col);
    } catch (err) {
      const info = { col, op, expr };
      throw new DatabaseQueryError(err, 'Find failed').setInfo(info);
    }
  }

  async findOne(obj: Partial<BankTagShape>): Promise<BankTag>;
  async findOne(col: keyof Partial<BankTagShape>, expr: Expression<PrimitiveValue>): Promise<BankTag>;
  async findOne(col: ColumnRef, op?: Operator, expr?: Expression<PrimitiveValue>): Promise<BankTag> {
    try {
      if (op && expr) {
        return await BankTag.query(this.trx).findOne(col, op, expr);
      }
      if (op) {
        return await BankTag.query(this.trx).findOne(col, op);
      }
      return await BankTag.query(this.trx).findOne(col);
    } catch (err) {
      const info = { col, op, expr };
      throw new DatabaseQueryError(err, 'Find one failed').setInfo(info);
    }
  }

  async create(data: Partial<BankTagShape>): Promise<BankTag> {
    try {
      return await BankTag.query(this.trx).insert(data).returning('*');
    } catch (err) {
      const info = { data };
      throw new DatabaseQueryError(err, 'Create failed').setInfo(info);
    }
  }

  async update(where: Partial<BankTagShape>, data: Partial<BankTagShape>): Promise<BankTag> {
    try {
      return await BankTag.query(this.trx)
        .throwIfNotFound()
        .where(where)
        .update(data)
        .returning('*')
        .first();
    } catch (err) {
      const info = { where, data };
      throw new DatabaseQueryError(err, 'Update failed').setInfo(info);
    }
  }

  async delete(where: Partial<BankTagShape>): Promise<BankTag> {
    try {
      return await BankTag.query(this.trx).throwIfNotFound().where(where).delete().returning('*').first();
    } catch (err) {
      const info = { where };
      throw new DatabaseQueryError(err, 'Delete failed').setInfo(info);
    }
  }
}
