import { Cradle } from '@/src/app/container';
import IRepository from '@/src/types/IRepository';
import { DatabaseQueryError } from '@/src/lib/errors';
import BankInstitution, { BankInstitutionShape } from '@/src/models/BankInstitution';
import { ColumnRef, Expression, Operator, PrimitiveValue, Transaction } from 'objection';

export default class BankInstitutionRepository implements IRepository<BankInstitution> {
  trx: Transaction;

  constructor({ trx }: Cradle) {
    this.trx = trx;
  }

  async find(obj: Partial<BankInstitutionShape>): Promise<BankInstitution[]>;
  async find(col: keyof Partial<BankInstitutionShape>, expr: Expression<PrimitiveValue>): Promise<BankInstitution[]>;
  async find(col: ColumnRef, op?: Operator, expr?: Expression<PrimitiveValue>): Promise<BankInstitution[]> {
    try {
      if (op && expr) {
        return await BankInstitution.query(this.trx).where(col, op, expr);
      }
      if (op) {
        return await BankInstitution.query(this.trx).where(col, op);
      }
      return await BankInstitution.query(this.trx).where(col);
    } catch (err) {
      const info = { col, op, expr };
      throw new DatabaseQueryError(err, 'Find failed').setInfo(info);
    }
  }

  async findOne(obj: Partial<BankInstitutionShape>): Promise<BankInstitution>;
  async findOne(col: keyof Partial<BankInstitutionShape>, expr: Expression<PrimitiveValue>): Promise<BankInstitution>;
  async findOne(col: ColumnRef, op?: Operator, expr?: Expression<PrimitiveValue>): Promise<BankInstitution> {
    try {
      if (op && expr) {
        return await BankInstitution.query(this.trx).findOne(col, op, expr);
      }
      if (op) {
        return await BankInstitution.query(this.trx).findOne(col, op);
      }
      return await BankInstitution.query(this.trx).findOne(col);
    } catch (err) {
      const info = { col, op, expr };
      throw new DatabaseQueryError(err, 'Find one failed').setInfo(info);
    }
  }

  async create(data: Partial<BankInstitutionShape>): Promise<BankInstitution> {
    try {
      return await BankInstitution.query(this.trx).insert(data).returning('*');
    } catch (err) {
      const info = { data };
      throw new DatabaseQueryError(err, 'Create failed').setInfo(info);
    }
  }

  async update(where: Partial<BankInstitutionShape>, data: Partial<BankInstitutionShape>): Promise<BankInstitution> {
    try {
      return await BankInstitution.query(this.trx)
        .throwIfNotFound()
        .where(where)
        .update(data)
        .returning('*')
        .first();
    } catch (err) {
      const info = { where, data };
      throw new DatabaseQueryError(err, 'Update failed').setInfo(info);
    }
  }

  async delete(where: Partial<BankInstitutionShape>): Promise<BankInstitution> {
    try {
      return await BankInstitution.query(this.trx).throwIfNotFound().where(where).delete().returning('*').first();
    } catch (err) {
      const info = { where };
      throw new DatabaseQueryError(err, 'Delete failed').setInfo(info);
    }
  }
}
