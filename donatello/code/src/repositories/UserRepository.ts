import { Cradle } from '@/src/app/container';
import IRepository from '@/src/types/IRepository';
import { DatabaseQueryError } from '@/src/lib/errors';
import User, { UserShape } from '@/src/models/User';
import { ColumnRef, Expression, Operator, PrimitiveValue, Transaction } from 'objection';

export default class UserRepository implements IRepository<User> {
  trx: Transaction;

  constructor({ trx }: Cradle) {
    this.trx = trx;
  }

  async find(obj: Partial<UserShape>): Promise<User[]>;
  async find(col: keyof Partial<UserShape>, expr: Expression<PrimitiveValue>): Promise<User[]>;
  async find(col: ColumnRef, op?: Operator, expr?: Expression<PrimitiveValue>): Promise<User[]> {
    try {
      if (op && expr) {
        return await User.query(this.trx).where(col, op, expr);
      }
      if (op) {
        return await User.query(this.trx).where(col, op);
      }
      return await User.query(this.trx).where(col);
    } catch (err) {
      const info = { col, op, expr };
      throw new DatabaseQueryError(err, 'Find failed').setInfo(info);
    }
  }

  async findOne(obj: Partial<UserShape>): Promise<User>;
  async findOne(col: keyof Partial<UserShape>, expr: Expression<PrimitiveValue>): Promise<User>;
  async findOne(col: ColumnRef, op?: Operator, expr?: Expression<PrimitiveValue>): Promise<User> {
    try {
      if (op && expr) {
        return await User.query(this.trx).findOne(col, op, expr);
      }
      if (op) {
        return await User.query(this.trx).findOne(col, op);
      }
      return await User.query(this.trx).findOne(col);
    } catch (err) {
      const info = { col, op, expr };
      throw new DatabaseQueryError(err, 'Find one failed').setInfo(info);
    }
  }

  async create(data: Partial<UserShape>): Promise<User> {
    try {
      return await User.query(this.trx).insert(data).returning('*');
    } catch (err) {
      const info = { data };
      throw new DatabaseQueryError(err, 'Create failed').setInfo(info);
    }
  }

  async update(where: Partial<UserShape>, data: Partial<UserShape>): Promise<User> {
    try {
      return await User.query(this.trx)
        .throwIfNotFound()
        .where(where)
        .update(data)
        .returning('*')
        .first();
    } catch (err) {
      const info = { where, data };
      throw new DatabaseQueryError(err, 'Update failed').setInfo(info);
    }
  }

  async delete(where: Partial<UserShape>): Promise<User> {
    try {
      return await User.query(this.trx).throwIfNotFound().where(where).delete().returning('*').first();
    } catch (err) {
      const info = { where };
      throw new DatabaseQueryError(err, 'Delete failed').setInfo(info);
    }
  }
}
