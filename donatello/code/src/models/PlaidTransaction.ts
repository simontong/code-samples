import PlaidAccount from '@/src/models/PlaidAccount';
import User from '@/src/models/User';
import { ModelObject, RelationMappings } from 'objection';
import BaseModel from '@/src/lib/database/BaseModel';
import {
  Location,
  PaymentMeta,
  TransactionCode,
  TransactionPaymentChannelEnum,
  TransactionTransactionTypeEnum,
} from 'plaid';

export default class PlaidTransaction extends BaseModel {
  static tableName = 'plaid_transactions';

  // columns
  id!: string;
  user_id!: string;
  plaid_account_id!: string;
  transaction_eid!: string;
  category_eid!: string | null;
  category!: Array<string> | null;
  location!: Location | null;
  payment_meta!: PaymentMeta | null;
  account_owner!: string | null;
  name!: string;
  merchant_name!: string | null;
  original_description!: string | null;
  amount!: number;
  iso_currency_code!: string | null;
  unofficial_currency_code!: string | null;
  pending!: string;
  pending_channel!: TransactionPaymentChannelEnum;
  date!: string;
  datetime!: string | null;
  authorized_date!: string | null;
  authorized_datetime!: string | null;
  transaction_code!: TransactionCode | null;
  transaction_type!: TransactionTransactionTypeEnum | null;
  created_at!: string;
  updated_at!: string;

  // relations
  plaidAccount!: InstanceType<typeof PlaidAccount>;
  user!: InstanceType<typeof User>;

  // prettier-ignore
  static visible: Array<keyof PlaidTransactionShape> = [];

  static relationMappings: RelationMappings = {
    plaidAccount: {
      relation: BaseModel.BelongsToOneRelation,
      modelClass: 'PlaidAccount',
      join: {
        from: 'plaid_transactions.plaid_account_id',
        to: 'plaid_accounts.id',
      },
    },

    user: {
      relation: BaseModel.BelongsToOneRelation,
      modelClass: 'User',
      join: {
        from: 'plaid_transactions.user_id',
        to: 'users.id',
      },
    },
  };
}

export type PlaidTransactionShape = ModelObject<PlaidTransaction>;
