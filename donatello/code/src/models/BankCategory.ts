import { ModelObject, RelationMappings } from 'objection';
import BaseModel from '@/src/lib/database/BaseModel';
import User from '@/src/models/User';
import BankTransaction from '@/src/models/BankTransaction';

export default class BankCategory extends BaseModel {
  static tableName = 'bank_categories';

  // columns
  id!: string;
  user_id!: string;
  name!: string;
  created_at!: string;
  updated_at!: string;

  // relations
  bankTransactions!: InstanceType<typeof BankTransaction>;
  user!: InstanceType<typeof User>;

  // prettier-ignore
  static visible: Array<keyof BankCategoryShape> = [
    'id',
    'name',

    // relations
    'bankTransactions',
  ];

  static relationMappings: RelationMappings = {
    bankTransactions: {
      relation: BaseModel.HasManyRelation,
      modelClass: 'BankTransaction',
      join: {
        from: 'bank_categories.id',
        to: 'bank_transactions.bank_category_id',
      },
    },

    user: {
      relation: BaseModel.BelongsToOneRelation,
      modelClass: 'User',
      join: {
        from: 'bank_categories.user_id',
        to: 'users.id',
      },
    },
  };
}

export type BankCategoryShape = ModelObject<BankCategory>;
