import User from '@/src/models/User';
import { ModelObject, RelationMappings } from 'objection';
import BaseModel from '@/src/lib/database/BaseModel';

export default class PlaidItem extends BaseModel {
  static tableName = 'plaid_items';

  // columns
  id!: string;
  user_id!: string;
  item_eid!: string;
  institution_eid!: string | null;
  access_token!: string;
  created_at!: string;
  updated_at!: string;

  // relations
  user!: InstanceType<typeof User>;

  // prettier-ignore
  static visible: Array<keyof PlaidItemShape> = [];

  static relationMappings: RelationMappings = {
    user: {
      relation: BaseModel.BelongsToOneRelation,
      modelClass: 'User',
      join: {
        from: 'plaid_items.user_id',
        to: 'users.id',
      },
    },
  };
}

export type PlaidItemShape = ModelObject<PlaidItem>;
