import PlaidItem from '@/src/models/PlaidItem';
import User from '@/src/models/User';
import { ModelObject, RelationMappings } from 'objection';
import BaseModel from '@/src/lib/database/BaseModel';
import { AccountSubtype, AccountType } from 'plaid';

export default class PlaidAccount extends BaseModel {
  static tableName = 'plaid_accounts';

  // columns
  id!: string;
  user_id!: string;
  plaid_item_id!: string;
  account_eid!: string;
  mask!: string;
  name!: string;
  official_name!: string;
  type!: AccountType;
  subtype!: AccountSubtype;
  balance_available!: number | null;
  balance_current!: number | null;
  balance_limit!: number | null;
  iso_currency_code!: string | null;
  unofficial_currency_code!: string | null;
  created_at!: string;
  updated_at!: string;

  // relations
  plaidItem!: InstanceType<typeof PlaidItem>;
  user!: InstanceType<typeof User>;

  // prettier-ignore
  static visible: Array<keyof PlaidAccountShape> = [];

  static relationMappings: RelationMappings = {
    plaidItem: {
      relation: BaseModel.BelongsToOneRelation,
      modelClass: 'PlaidItem',
      join: {
        from: 'plaid_accounts.plaid_item_id',
        to: 'plaid_items.id',
      },
    },

    user: {
      relation: BaseModel.BelongsToOneRelation,
      modelClass: 'User',
      join: {
        from: 'plaid_accounts.user_id',
        to: 'users.id',
      },
    },
  };
}

export type PlaidAccountShape = ModelObject<PlaidAccount>;
