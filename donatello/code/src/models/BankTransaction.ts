import { ModelObject, RelationMappings } from 'objection';
import BankAccount from '@/src/models/BankAccount';
import BankCategory from '@/src/models/BankCategory';
import BankTag from '@/src/models/BankTag';
import BaseModel from '@/src/lib/database/BaseModel';
import Currency from '@/src/models/Currency';
import User from '@/src/models/User';

export default class BankTransaction extends BaseModel {
  static tableName = 'bank_transactions';

  // columns
  id!: string;
  user_id!: string;
  bank_account_id!: string | null;
  bank_category_id!: string | null;
  currency_code!: string;
  payee!: string;
  date!: string;
  amount!: number;
  note!: string | null;
  created_at!: string;
  updated_at!: string;

  // relations
  bankAccount!: InstanceType<typeof BankAccount>;
  bankCategory!: InstanceType<typeof BankCategory>;
  bankTags!: InstanceType<typeof BankTag>[];
  currency!: InstanceType<typeof Currency>;
  user!: InstanceType<typeof User>;

  // prettier-ignore
  static visible: Array<keyof BankTransactionShape> = [
    'id',
    'bank_account_id',
    'bank_category_id',
    'currency_code',
    'payee',
    'date',
    'amount',
    'note',
    'created_at',

    // relations
    'bankAccount',
    'bankCategory',
    'bankTags',
    'currency',
  ];

  static relationMappings: RelationMappings = {
    bankAccount: {
      relation: BaseModel.BelongsToOneRelation,
      modelClass: 'BankAccount',
      join: {
        from: 'bank_transactions.bank_account_id',
        to: 'bank_accounts.id',
      },
    },

    bankCategory: {
      relation: BaseModel.BelongsToOneRelation,
      modelClass: 'BankCategory',
      join: {
        from: 'bank_transactions.bank_category_id',
        to: 'bank_categories.id',
      },
    },

    bankTags: {
      relation: BaseModel.ManyToManyRelation,
      modelClass: 'BankTag',
      join: {
        from: 'bank_transactions.id',
        through: {
          modelClass: 'BankTagBankTransaction',
          from: 'bank_tag_bank_transaction.bank_transaction_id',
          to: 'bank_tag_bank_transaction.bank_tag_id',
        },
        to: 'bank_tags.id',
      },
    },

    currency: {
      relation: BaseModel.BelongsToOneRelation,
      modelClass: 'Currency',
      join: {
        from: 'bank_transactions.currency_code',
        to: 'currencies.code',
      },
    },

    user: {
      relation: BaseModel.BelongsToOneRelation,
      modelClass: User,
      join: {
        from: 'bank_transactions.user_id',
        to: 'users.id',
      },
    },
  };
}

export type BankTransactionShape = ModelObject<BankTransaction>;
