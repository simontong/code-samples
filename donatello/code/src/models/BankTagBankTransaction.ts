import { ModelObject, RelationMappings } from 'objection';
import BaseModel from '@/src/lib/database/BaseModel';

export default class BankTagBankTransaction extends BaseModel {
  static tableName = 'bank_tag_bank_transaction';

  // columns
  id!: string;
  bank_tag_id!: string;
  bank_transaction_id!: string;
  created_at!: string;
  updated_at!: string;

  // prettier-ignore
  static visible: Array<keyof BankTagBankTransactionShape> = [];

  static relationMappings: RelationMappings = {};
}

export type BankTagBankTransactionShape = ModelObject<BankTagBankTransaction>;
