import BankTransaction from '@/src/models/BankTransaction';
import User from '@/src/models/User';
import { ModelObject, RelationMappings } from 'objection';
import BaseModel from '@/src/lib/database/BaseModel';

export default class BankInstitution extends BaseModel {
  static tableName = 'bank_institutions';

  // columns
  id!: string;
  user_id!: string;
  name!: string;
  created_at!: string;
  updated_at!: string;

  // relations
  user!: InstanceType<typeof User>;

  // prettier-ignore
  static visible: Array<keyof BankInstitutionShape> = [
    'id',
    'name',
  ];

  static relationMappings: RelationMappings = {
    user: {
      relation: BaseModel.BelongsToOneRelation,
      modelClass: 'User',
      join: {
        from: 'bank_institutions.user_id',
        to: 'users.id',
      },
    },
  };
}

export type BankInstitutionShape = ModelObject<BankInstitution>;
