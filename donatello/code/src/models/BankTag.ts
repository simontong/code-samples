import { ModelObject, RelationMappings } from 'objection';
import BaseModel from '@/src/lib/database/BaseModel';
import User from '@/src/models/User';
import BankTransaction from '@/src/models/BankTransaction';

export default class BankTag extends BaseModel {
  static tableName = 'bank_tags';

  // columns
  id!: string;
  user_id!: string;
  name!: string;
  created_at!: string;
  updated_at!: string;

  // relations
  bankTransactions!: InstanceType<typeof BankTransaction>[];
  user!: InstanceType<typeof User>;

  // prettier-ignore
  static visible: Array<keyof BankTagShape> = [
    'id',
    'name',

    // relations
    'bankTransactions',
  ];

  static relationMappings: RelationMappings = {
    // bankTransactions: {
    //   relation: BaseModel.HasManyRelation,
    //   modelClass: 'BankTransaction',
    //   join: {
    //     from: 'bank_tags.id',
    //     to: 'bank_transactions.bank_tag_id',
    //   },
    // },

    user: {
      relation: BaseModel.BelongsToOneRelation,
      modelClass: 'User',
      join: {
        from: 'bank_tags.user_id',
        to: 'users.id',
      },
    },
  };
}

export type BankTagShape = ModelObject<BankTag>;
