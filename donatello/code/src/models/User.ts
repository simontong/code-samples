import { ModelObject, RelationMappings } from 'objection';
import BaseModel from '@/src/lib/database/BaseModel';
import BankAccount from '@/src/models/BankAccount';

export default class User extends BaseModel {
  static tableName = 'users';

  // columns
  id!: string;
  name!: string;
  email!: string;
  new_email!: string | null;
  email_confirm_token!: string | null;
  password!: string;
  password_reset_token!: string | null;
  created_at!: string;
  updated_at!: string;

  // relations
  bankAccounts!: InstanceType<typeof BankAccount>;

  // prettier-ignore
  static visible: Array<keyof UserShape> = [
    'id',
    'name',
    'email',
    'new_email',
    'email_confirm_token',

    // relations
    'bankAccounts',
  ];

  static relationMappings: RelationMappings = {
    bankAccounts: {
      relation: BaseModel.HasManyRelation,
      modelClass: 'BankAccount',
      join: {
        from: 'users.id',
        to: 'bank_accounts.user_id',
      },
    },
  };
}

export type UserShape = ModelObject<User>;
