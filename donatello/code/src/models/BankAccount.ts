import BankInstitution from '@/src/models/BankInstitution';
import { ModelObject, RelationMappings } from 'objection';
import BaseModel from '@/src/lib/database/BaseModel';
import User from '@/src/models/User';

export default class BankAccount extends BaseModel {
  static tableName = 'bank_accounts';

  id!: string;
  user_id!: string;
  bank_institution_id!: string;
  name!: string;
  created_at!: string;
  updated_at!: string;

  bankInstitution!: InstanceType<typeof BankInstitution>;
  user!: InstanceType<typeof User>;

  // prettier-ignore
  static visible: Array<keyof BankAccountShape> = [
    'id',
    'name',
    'bankInstitution',
  ];

  static relationMappings: RelationMappings = {
    bankInstitution: {
      relation: BaseModel.BelongsToOneRelation,
      modelClass: 'BankInstitution',
      join: {
        from: 'bank_accounts.bank_institution_id',
        to: 'bank_institutions.id',
      },
    },
    
    user: {
      relation: BaseModel.BelongsToOneRelation,
      modelClass: 'User',
      join: {
        from: 'bank_accounts.user_id',
        to: 'users.id',
      },
    },
  };
}

export type BankAccountShape = ModelObject<BankAccount>;
