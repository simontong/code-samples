import { ModelObject, RelationMappings } from 'objection';
import BaseModel from '@/src/lib/database/BaseModel';

export default class Currency extends BaseModel {
  static tableName = 'currencies';

  code!: string;
  name!: string;
  // name_plural!: string;
  symbol!: string;
  // symbol_native!: string;
  precision!: number;
  created_at!: string;
  updated_at!: string;

  // prettier-ignore
  static visible: Array<keyof CurrencyShape> = [
    'code',
    'name',
    'precision',
  ];

  static relationMappings: RelationMappings = {};
}

export type CurrencyShape = ModelObject<Currency>;
