export default {
  // uuid: '[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}',
  uuid: '[0-9a-f]{8}-[0-9a-f]{4}-[4][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}',
};

// const matchInternationalMoney = (thousand: string, decimal: string) => {
//   const re = '^(\\d+|(\\d{1,3})(\\' + thousand + '(?=\\d{3})\\d+)+)(\\' + decimal + '\\d+)?$';
//   return new RegExp(re);
// };
