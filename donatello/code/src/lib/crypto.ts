import crypto from 'crypto';
import argon2 from 'argon2';
import ow from 'ow';

/**
 * hash password for storage
 */
export async function hashPassword(password: string): Promise<string> {
  ow(password, 'password', ow.string.not.empty);

  return argon2.hash(password);
}

/**
 * compare hashed password with plain
 */
export async function verifyPassword(hash: string, password: string): Promise<boolean> {
  ow(hash, 'hash', ow.string.not.empty);
  ow(password, 'password', ow.string.not.empty);

  return argon2.verify(hash, password);
}

/**
 * encrypt data
 * from: https://gist.github.com/vlucas/2bd40f62d20c1d49237a109d491974eb#gistcomment-3771967
 */
export function encrypt(text: string, encryptionKey: string): string {
  ow(text, 'text', ow.string.not.empty);
  ow(encryptionKey, 'encryptionKey', ow.string.length(32));

  const ivLength = 16; // for AES, this is always 16
  const iv = Buffer.from(crypto.randomBytes(ivLength)).toString('hex').slice(0, ivLength);
  const cipher = crypto.createCipheriv('aes-256-cbc', encryptionKey, iv);

  let encrypted = cipher.update(text);
  encrypted = Buffer.concat([encrypted, cipher.final()]);

  return iv + ':' + encrypted.toString('hex');
}

/**
 * decrypt data
 * from: https://gist.github.com/vlucas/2bd40f62d20c1d49237a109d491974eb#gistcomment-3771967
 */
export function decrypt(text: string, encryptionKey: string): string {
  ow(text, 'text', ow.string.not.empty);
  ow(encryptionKey, 'encryptionKey', ow.string.length(32));

  const textParts = text.includes(':') ? text.split(':') : [];
  const iv = Buffer.from(textParts.shift() || '', 'binary');
  const encryptedText = Buffer.from(textParts.join(':'), 'hex');
  const decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(encryptionKey), iv);

  let decrypted = decipher.update(encryptedText);
  decrypted = Buffer.concat([decrypted, decipher.final()]);

  return decrypted.toString();
}
