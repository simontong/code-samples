import _ from 'lodash';
import { StatusCodes } from 'http-status-codes';
import { nativeErrorProps } from '@/src/lib/database/utils';
import ErrorExtended from './ExtendedError';

export class AppError extends ErrorExtended {}

export class ArgError extends ErrorExtended {}

export class CommandError extends ErrorExtended {}

export class ConfigError extends ErrorExtended {}

export class DatabaseError extends ErrorExtended {}

export class DatasourceError extends ErrorExtended {}

export class DatabaseQueryError extends ErrorExtended {
  setInfo(info: unknown): this {
    // @ts-ignore :: check that `info` is an object and that it doesn't already have
    // the property `nativeError` attached to it
    if (_.isPlainObject(info) && _.isUndefined(info.nativeError)) {
      // @ts-ignore :: patch
      info.nativeError = nativeErrorProps(this.cause as Error);
    }

    return super.setInfo(info);
  }
}

export class RedisError extends ErrorExtended {}

export class PlaidApiError extends ErrorExtended {}

export class PlaidWebhookError extends ErrorExtended {}

export class QueueError extends ErrorExtended {}

export class RuleError extends ErrorExtended {}

/**
 * http errors
 */
export class HttpErrorNotFound extends ErrorExtended {
  expose = true;
  statusCode = StatusCodes.NOT_FOUND;
}

export class HttpErrorUnauthorized extends ErrorExtended {
  expose = true;
  statusCode = StatusCodes.UNAUTHORIZED;
}

export class HttpErrorTooManyRequest extends ErrorExtended {
  expose = true;
  statusCode = StatusCodes.TOO_MANY_REQUESTS;
}
