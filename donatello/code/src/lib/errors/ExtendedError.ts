import util from 'util';
import { StatusCodes } from 'http-status-codes';

export default abstract class ExtendedError extends Error {
  cause?: Error;
  info?: unknown;
  code?: string;

  expose = false;
  statusCode?: number;

  // noinspection TypeScriptAbstractClassConstructorCanBeMadeProtected
  constructor(err?: unknown | Error, ...msgParams: unknown[]) {
    super();

    this.name = this.constructor.name;

    if (err instanceof Error) {
      this.cause = err;
      this.setMessageWithCause(...msgParams);
      return;
    }

    // assume first arg `err` is a message so set as error message along
    // with `msgParams` args
    const params = msgParams;
    if (typeof err !== 'undefined') {
      params.unshift(err);
    }
    this.setMessageWithCause(...params);
  }

  private setMessageWithCause(...msg: unknown[]) {
    this.setMessage(...msg);
    if (this.cause?.message) {
      this.message += `: ${this.cause.message}`;
    }
  }

  setExpose(state = true): this {
    this.expose = state;
    return this;
  }

  setStatusCode(statusCode: StatusCodes): this {
    this.statusCode = statusCode;
    return this;
  }

  setMessage(...msg: unknown[]): this {
    this.message = util.format(...msg);
    return this;
  }

  setInfo(info: unknown): this {
    this.info = info;
    return this;
  }

  setCode(errorCode: string): this {
    this.code = errorCode;
    return this;
  }
}
