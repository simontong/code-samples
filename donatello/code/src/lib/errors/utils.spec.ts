import _ from 'lodash';
import { unwindInfo, unwindStack } from './utils';
import ErrorExtended from './ExtendedError';

describe('lib: errors/utils', () => {
  class MyError extends ErrorExtended {}

  describe('unwinding stack', () => {
    /**
     * test
     */
    it('should cascade stack with bubbling errors', () => {
      const causes = [];
      causes.push(new MyError('1st error'));
      causes.push(new MyError(causes[0], '2nd error'));
      causes.push(new MyError(causes[1], '3rd error'));
      causes.push(new MyError(causes[2], '4th error'));
      const e = new MyError(causes[3], '%dth error', 5);

      expect(e.message).toEqual('5th error: 4th error: 3rd error: 2nd error: 1st error');

      const stack = unwindStack(e);
      expect(stack).toMatch(/MyError: 5th error: 4th error: 3rd error: 2nd error: 1st error/im);
      expect(stack).toMatch(/caused by MyError: 4th error: 3rd error: 2nd error: 1st error/im);
      expect(stack).toMatch(/caused by MyError: 3rd error: 2nd error: 1st error/im);
      expect(stack).toMatch(/caused by MyError: 2nd error: 1st error/im);
      expect(stack).toMatch(/caused by MyError: 1st error/im);
    });
  });

  describe('unwinding info', () => {
    /**
     * test
     */
    it('should keep info for bubbled errors', () => {
      const causes = [];
      causes.push(new MyError('1st error'));
      causes.push(new MyError(causes[0], '2nd error').setInfo({ hello: '2nd place' }));
      causes.push(new MyError(causes[1], '3rd error').setInfo({ hello: '3rd place' }));
      causes.push(new MyError(causes[2], '4th error').setInfo({ hello: '4th place' }));
      const e = new MyError(causes[3]);

      expect(_.get(e, 'cause.info')).toStrictEqual({ hello: '4th place' });
      expect(_.get(e, 'cause.cause.info')).toStrictEqual({ hello: '3rd place' });
      expect(_.get(e, 'cause.cause.cause.info')).toStrictEqual({ hello: '2nd place' });
    });

    /**
     * test
     */
    it('should return unwound info', () => {
      const causes = [];
      causes.push(new MyError('1st error'));
      causes.push(new MyError(causes[0], '2nd error').setInfo({ num: '2nd' }));
      causes.push(new MyError(causes[1], '3rd error').setInfo({ num: '3rd' }));
      causes.push(new MyError(causes[2], '4th error').setInfo('4th'));
      const e = new MyError(causes[3], '%dth error', 5).setInfo({ hello: '5th' });

      expect(unwindInfo(e)).toStrictEqual({
        '1:MyError': { hello: '5th' },
        '2:MyError': '4th',
        '3:MyError': { num: '3rd' },
        '4:MyError': { num: '2nd' },
      });
    });
  });
});
