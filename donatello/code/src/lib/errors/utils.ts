import _ from 'lodash';
import ExtendedError from './ExtendedError';

type UnwoundInfo = Record<string, unknown>;

/**
 * unwind nested info from error
 */
export const unwindInfo = (err: unknown | Error): UnwoundInfo | undefined => {
  let num = 0;
  const unwound: UnwoundInfo = {};

  if (!(err instanceof ExtendedError)) {
    return;
  }

  (function unwind(ee: ExtendedError) {
    num += 1;
    if (ee.info) {
      const key = `${num}:${ee.name}`;
      unwound[key] = ee.info;
    }

    if (ee.cause instanceof ExtendedError) {
      unwind(ee.cause);
    }
  })(err);

  if (_.isEmpty(unwound)) {
    return;
  }

  return unwound;
};

/**
 * unwind nested stack trace
 */
export const unwindStack = (err: unknown | Error): string | undefined => {
  const unwound: Array<string> = [];

  if (!(err instanceof Error)) {
    return;
  }

  (function unwind(e: Error) {
    if (e.stack) {
      const lines = [];
      let [msgLine, ...errLines] = e.stack.trim().split('\n');

      // add 'caused by' line if we are passed first stack
      if (unwound.length > 0) {
        msgLine = `caused by ${msgLine}`;
      }

      // add main message line
      lines.push(msgLine);

      // add stack trace lines
      lines.push(...errLines.map((line) => `    ${line.trim()}`));

      // add to all lines
      unwound.push(lines.join('\n'));
    }

    // if more nested then recurse
    if (e instanceof ExtendedError && e.cause) {
      unwind(e.cause as ExtendedError);
    }
  })(err);

  if (_.isEmpty(unwound)) {
    return;
  }

  return unwound.join('\n');
};
