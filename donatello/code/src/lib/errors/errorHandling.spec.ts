import container from '@/src/app/container';
import createMockContext from '@/test/mock/createMockContext';
import * as errorHandling from './errorHandling';

describe('lib: errors/errorHandling', () => {
  describe('captureError', () => {
    /**
     * test
     */
    it('logs message and reports', () => {
      const err = new Error('database failure');
      const ctx = createMockContext();
      const spies = {
        reportError: jest.spyOn(errorHandling, 'reportError').mockImplementation(),
        errorLogger: jest.spyOn(container.cradle.logger, 'error').mockImplementation(),
      };

      errorHandling.captureError(err, ctx);
      expect(spies.reportError).toBeCalledTimes(1);
      expect(spies.errorLogger).toBeCalledTimes(1);
    });
  });
});
