import container from '@/src/app/container';
import * as os from 'os';
import path from 'path';
import * as Sentry from '@sentry/node';
import { KoaContext } from '@/src/types/KoaContext';
import { unwindInfo } from './utils';

export const captureError = (err: Error | string, ctx?: KoaContext): void => {
  // pass as '{ err }' to allow for pino serializer to work
  const msg = err instanceof Error ? err.message : err;
  container.cradle.logger.error({ msg, err });

  // report to sentry
  reportError(err, ctx);
};

export const reportError = (err: Error | string, ctx?: KoaContext): void => {
  // don't report if sentry not enabled
  const client = Sentry.getCurrentHub().getClient();
  if (!client || !client.getOptions().enabled) {
    return;
  }

  const info = unwindInfo(err);

  // set sentry scope
  Sentry.configureScope((scope) => {
    // add extra info
    // @ts-ignore
    scope.setExtras(info);

    scope.setContext('os', {
      name: os.type(),
      version: os.release(),
      kernel_version: [os.type(), os.hostname(), os.version(), os.arch()].join(' '),
    });

    scope.setContext('runtime', {
      name: path.basename(process.title),
      version: process.version,
    });

    // scope.setContext('app', {});

    if (ctx) {
      scope.setContext('browser', {
        name: ctx.get('user-agent'),
      });

      scope.setTag('url', ctx.href);

      if (typeof ctx.state.user === 'object') {
        scope.setUser({ id: ctx.state.user.id });
      }
    }
  });

  // submit error
  container.cradle.logger.info('Reporting error to Sentry ...');

  // capture message (with stacktrace attached on init())
  if (typeof err === 'string') {
    Sentry.captureMessage(err);
    return;
  }

  // capture exception
  Sentry.captureException(err);
};

export const sentryInit = (): void => {
  const config = container.cradle.config;
  const environment = config.get<string>('app.env');
  const dsn = config.get<string>('sentry.dsn');
  const skipEnvironments = config.get<Array<string>>('sentry.skipEnvironments', []);
  const enabled = !!dsn && !skipEnvironments.includes(environment);

  Sentry.init({
    enabled,
    dsn,
    environment,
    attachStacktrace: true,
  });
};
