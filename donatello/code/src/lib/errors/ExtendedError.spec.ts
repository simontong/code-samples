import util from 'util';
import ErrorExtended from '@/src/lib/errors/ExtendedError';

class MyError extends ErrorExtended {}

describe('lib: errors/ExtendedError', () => {
  describe('throwing error', () => {
    /**
     * test
     */
    it('should contain name of error class', () => {
      const e = new MyError();

      expect(e.stack).toMatch(/^MyError:/);
    });
  });

  describe('passing first argument', () => {
    /**
     * test
     */
    it('should accept object as first argument', () => {
      const opts = { foo: 'bar', hello: 'world' };
      const e = new MyError(opts);
      const json = util.format(opts);

      expect(e.message).toEqual(json);
      expect(e.stack).toMatch(new RegExp(`^MyError: ${json}`));
    });

    it.each([1, 'hello world'])('should accept primitive as first argument', (value) => {
      const e = new MyError(value);

      expect(e.message).toEqual(String(value));
      expect(e.stack).toMatch(new RegExp(`^MyError: ${value}`));
    });

    /**
     * test
     */
    it('should work when only passing info', () => {
      const info = { foo: 'bar' };
      const e = new MyError().setInfo(info);

      expect(e.cause).toBeUndefined();
      expect(e.message).toBeEmpty();
      expect(e.info).toStrictEqual(info);
    });

    /**
     * test
     */
    it('should work when only passing cause', () => {
      const cause = new MyError('first error');
      const e = new MyError(cause, 'second error');

      expect(e.message).toEqual('second error: first error');
      expect(e.cause).toStrictEqual(cause);
      expect(e.cause?.message).toEqual('first error');
    });
  });

  describe('formatting messages', () => {
    /**
     * test
     */
    it('should format strings for message', () => {
      const message = 'foo: %s, hello: %s';
      const messageParams = ['bar', 'world'];
      const value = 'foo: bar, hello: world';
      const e = new MyError(message, ...messageParams);

      expect(e.message).toEqual(value);
      expect(e.stack).toMatch(new RegExp(`^MyError: ${value}`));
    });
  });
});
