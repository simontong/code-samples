import { IRateLimiterStoreOptions, RateLimiterRedis, RateLimiterRes } from 'rate-limiter-flexible';
import { AppError } from './errors';

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const makeRateLimiter = (opts: IRateLimiterStoreOptions) => {
  const rateLimiter = new RateLimiterRedis(opts);

  return async (key: string | number): Promise<boolean> => {
    let rateLimit: RateLimiterRes | null;

    try {
      rateLimit = await rateLimiter.get(key);
    } catch (err) {
      const info = { key, opts };
      throw new AppError(err, 'Rate limiter failed to get key').setInfo(info);
    }

    // check if rate limit reached
    if (rateLimit && rateLimit.consumedPoints > rateLimiter.points) {
      return true;
    }

    // consume points, if err through then we can assume this hit the limit
    try {
      await rateLimiter.consume(key);
    } catch (err) {
      if (err instanceof Error) {
        const info = { key, opts };
        throw new AppError(err, 'Rate limiter failed to consume key').setInfo(info);
      }

      return true;
    }
    return false;
  };
};
