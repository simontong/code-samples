import _ from 'lodash';
import { getManyProps, getManyPropsOrThrow, getProp, getPropOrThrow } from '@/src/utils/getProp';

type EnvKeyValues = Record<string, string>;

const all = (): EnvKeyValues => {
  return { ...(process.env as EnvKeyValues) };
};

const set = (key: string, value: string): void => {
  _.set(process.env, key, value);
};

const merge = (values: EnvKeyValues): void => {
  _.merge(process.env, values);
};

const unset = (key: string): void => {
  _.unset(process.env, key);
};

function get(keys: string, defaultValue?: unknown): string {
  return getProp<string>(all(), keys, defaultValue);
}

function getOrThrow(key: string, allowEmpty = false): string {
  return getPropOrThrow<string>(all(), key, allowEmpty);
}

function getMany(keys: string[]): string[] {
  return getManyProps<[string]>(all(), keys);
}

function getManyOrThrow(keys: string[], allowEmpty = false): string[] {
  return getManyPropsOrThrow<[string]>(all(), keys, allowEmpty);
}

export default {
  all,
  set,
  unset,
  merge,
  get,
  getMany,
  getOrThrow,
  getManyOrThrow,
};
