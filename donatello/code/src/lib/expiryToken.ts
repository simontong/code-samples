import { DateTime } from 'luxon';
import { v4 as uuidv4 } from 'uuid';
import ow from 'ow';
import regexes from '@/src/consts/regexes';

/**
 * regexp for token expiry
 */
export const EXPIRY_TOKEN_REGEXP = new RegExp('^(' + regexes.uuid + ')-([0-9]+)$', 'i');

/**
 * create expiry token (used for reset password / email confirm links)
 */
export const createExpiryToken = (): string => {
  const ms = DateTime.now().toMillis();
  return [uuidv4(), ms].join('-');
};

/**
 * extract token expiry date from token created with `createExpiryToken`
 */
export const getTokenExpiry = (token: string, expiresAfterMs: number): DateTime => {
  ow(token, 'token', ow.string.not.empty);
  ow(expiresAfterMs, 'expiresAfter', ow.number);

  const [, , created] = token.match(EXPIRY_TOKEN_REGEXP) || [];
  const expiry = DateTime.fromMillis(Number(created) + expiresAfterMs);

  return expiry;
};

/**
 * check if a token has expired
 */
export const hasTokenExpired = (token: string, expiresAfterMs: number): boolean => {
  const now = DateTime.now();
  const expiry = getTokenExpiry(token, expiresAfterMs);
  const diff = expiry.diff(now).toMillis();

  return diff <= 0;
};

/**
 * check if we have enough time left before expiry to be able to reuse token
 */
export const canReuseExpiryToken = (
  token: string | null,
  expiresAfterMs: number,
  thresholdMs = 1000 * 60 * 60 * 3,
): boolean => {
  if (!token) {
    return false;
  }

  // if this token still has 3 hours left then reuse it rather than creating a new one
  const now = DateTime.now();
  const expires = getTokenExpiry(token, expiresAfterMs);
  const diff = expires.diff(now).toMillis();

  return diff >= thresholdMs;
};
