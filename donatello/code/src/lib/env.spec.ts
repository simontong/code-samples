import env from './env';

describe('lib: env', () => {
  const defaultValues = process.env;
  beforeEach(() => {
    process.env = { ...defaultValues };
  });

  describe('getting env values', () => {
    /**
     * test
     */
    it('should get all env values from process.env', () => {
      expect(env.all()).toStrictEqual(process.env);
    });

    /**
     * test
     */
    it('should get a single env value', () => {
      const value = 'testValue';
      process.env.TEST_VALUE = value;

      expect(env.get('TEST_VALUE')).toEqual(value);
    });

    /**
     * test
     */
    it('should throw if value empty or missing', () => {
      const undefinedKey = 'AN_UNDEFINED_KEY';
      const emptyValueKey = 'AN_EMPTY_VALUE';
      process.env[emptyValueKey] = '';

      // key key is undefined
      expect(process.env[undefinedKey]).toBeUndefined();
      expect(() => env.getOrThrow(undefinedKey)).toThrow(/invalid prop keys: AN_UNDEFINED_KEY/i);

      // if key is defined but value is empty
      expect(process.env[emptyValueKey]).toEqual('');
      expect(() => env.getOrThrow(emptyValueKey)).toThrow(/invalid prop keys: AN_EMPTY_VALUE/i);
    });

    /**
     * test
     */
    it('should not throw if value empty but allowing empty values', () => {
      const undefinedKey = 'AN_UNDEFINED_KEY';
      const emptyValueKey = '';
      process.env[emptyValueKey] = '';

      // key key is undefined
      expect(process.env[undefinedKey]).toBeUndefined();
      expect(() => env.getOrThrow(undefinedKey, true)).toThrow(/invalid prop keys: AN_UNDEFINED_KEY/i);

      // if key is defined but value is empty
      expect(process.env[emptyValueKey]).toEqual('');
      expect(() => env.getOrThrow(emptyValueKey, true)).not.toThrow();
    });
  });

  describe('setting env values', () => {
    /**
     * test
     */
    it('should merge new values with existing process.env', () => {
      const newValues = {
        DB_NAME: 'test_db',
        DB_HOST: 'localhost',
        DB_USER: 'postgres',
      };
      const expectValues = {
        ...process.env,
        ...newValues,
      };

      env.merge(newValues);

      expect(env.all()).toStrictEqual(expectValues);
      expect(process.env).toStrictEqual(expectValues);
    });

    /**
     * test
     */
    it('should set env value', () => {
      env.set('DB_NAME', 'test_db');
      expect(process.env.DB_NAME).toEqual('test_db');
    });

    /**
     * test
     */
    it('should not be able to change values without using setter function', () => {
      let all;
      const key = 'RANDOM_KEY_VALUE';

      all = env.all();
      all[key] = 'test';

      expect(process.env[key]).toBeUndefined();
      expect(env.get(key)).toBeUndefined();

      all = env.all();
      expect(all).toStrictEqual(process.env);
    });

    /**
     * test
     */
    it('should unset env prop', () => {
      const key = 'NEW_VALUE';

      process.env[key] = 'new value';
      env.unset(key);

      expect(process.env[key]).toBeUndefined();
      expect(env.get(key)).toBeUndefined();
    });
  });
});
