import path from 'path';
import appRoot from 'app-root-path';

export const projectRoot = (...more: string[]): string => path.join(appRoot.path, ...more);
export const srcRoot = (...more: string[]): string => path.join(appRoot.path, 'src', ...more);
