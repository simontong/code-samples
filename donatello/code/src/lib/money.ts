// bulk-create ow from 'ow';
// bulk-create { ArgError } from '@/src/lib/errors';
//
// /**
//  * removes thousand separates and validates formatting
//  */
// export const normalizeCurrency = (amount: string | number): number => {
//   ow(amount, 'amount', ow.any(ow.string, ow.number));
//
//   // if amount is a number then already normalized
//   if (typeof amount === 'number') {
//     return amount;
//   }
//
//   /**
//    * match valid amounts
//    */
//   const matcher = (thousand: string, decimal: string) => {
//     const re = '^(\\d+|(\\d{1,3})(\\' + thousand + '(?=\\d{3})\\d+)+)(\\' + decimal + '\\d+)?$';
//     return new RegExp(re);
//   };
//
//   // thousand = comma/optional, decimal = dot/optional
//   let hasCommaDecimals = false;
//   let match = matcher(',', '.').test(amount);
//
//   // thousand = dot/optional, decimal = comma/optional
//   if (!match) {
//     hasCommaDecimals = true;
//     match = matcher('.', ',').test(amount);
//   }
//
//   // couldn't get a match
//   if (!match) {
//     throw new ArgError('Invalid amount provided: %s', amount);
//   }
//
//   // prettier-ignore
//   // remove thousand separator and replace comma decimal with dot
//   const newAmount = Number(
//     hasCommaDecimals
//       ? amount.replace(/\./g, '').replace(',', '.')
//       : amount.replace(/,/g, ''),
//   );
//
//   // if NaN response then bail
//   if (Number.isNaN(newAmount)) {
//     throw new ArgError('Invalid amount provided: %s', amount);
//   }
//
//   return newAmount;
// };
