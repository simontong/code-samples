import validator from 'validator';

/**
 * if value is empty then allow (default yup.uuid() does not allow this behavior)
 */
export default function uuidOrEmpty(uuid: unknown): boolean {
  // pass on empty
  if (!uuid) {
    return true;
  }

  // fails on not a string
  if (typeof uuid !== 'string') {
    return false;
  }

  return validator.isUUID(uuid);
}
