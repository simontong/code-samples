import { BankAccountShape } from '@/src/models/BankAccount';
import { BankCategoryShape } from '@/src/models/BankCategory';
import { CurrencyShape } from '@/src/models/Currency';
import { UserShape } from '@/src/models/User';
import * as faker from 'faker';
import {
  bankAccountExists,
  bankCategoryExists,
  bankTagExists,
  currencyExists,
  recordExists,
  userExists,
} from './recordExists';
import { createRecordPresenceSpies, RecordPresenceSpies } from '@/test/mock/lib/validation/rules/recordPresenseHelpers';

type AssertExistArgs = {
  bankAccount: {
    args: { userId: string; whereCol?: keyof BankAccountShape };
    value: unknown;
  };
  bankCategory: {
    args: { userId: string; whereCol?: keyof BankCategoryShape };
    value: unknown;
  };
  bankTag: {
    args: { userId: string; whereCol?: keyof BankCategoryShape };
    value: unknown;
  };
  currency: {
    args: { whereCol?: keyof CurrencyShape };
    value: unknown;
  };
  user: {
    args: { whereCol?: keyof UserShape };
    value: unknown;
  };
};

/**
 * test all record functions here
 */
const assertExistFunctions = async (spies: RecordPresenceSpies, args: AssertExistArgs, expected: boolean) => {
  // prettier-ignore
  // bank account
  {
    const { args: { userId, whereCol = 'id' }, value } = args.bankAccount;

    await expect(bankAccountExists(userId, whereCol)(value)).resolves.toEqual(expected);
    expect(spies.findBankAccount).toBeCalledTimes(1);
  }

  // prettier-ignore
  // bank category
  {
    const { args: { userId, whereCol = 'id' }, value } = args.bankCategory;

    await expect(bankCategoryExists(userId, whereCol)(value)).resolves.toEqual(expected);
    expect(spies.findBankCategory).toBeCalledTimes(1);
  }

  // prettier-ignore
  // bank tag
  {
    const { args: { userId, whereCol = 'id' }, value } = args.bankTag;

    await expect(bankTagExists(userId, whereCol)(value)).resolves.toEqual(expected);
    expect(spies.findBankTag).toBeCalledTimes(1);
  }

  // prettier-ignore
  // currency
  {
    // eslint-disable-next-line no-empty-pattern
    const { args: {}, value } = args.currency;

    await expect(currencyExists()(value)).resolves.toEqual(expected);
    expect(spies.findCurrency).toBeCalledTimes(1);
  }

  // prettier-ignore
  // user
  {
    const { args: { whereCol = 'id' }, value } = args.user;

    await expect(userExists(whereCol)(value)).resolves.toEqual(expected);
    expect(spies.findUser).toBeCalledTimes(1);
  }
};

describe('lib: validators/recordExists', () => {
  describe('errors', () => {
    /**
     * test
     */
    it('should throw when cannot match switch case for model', async () => {
      // @ts-ignore
      expect(() => recordExists('BogusName')).toThrow(/record finder function is invalid/i);
    });
  });

  describe('fails validation', () => {
    /**
     * test
     */
    // eslint-disable-next-line jest/expect-expect
    it('should fail when record does not exist', async () => {
      const spies = createRecordPresenceSpies(true);
      const userId = faker.datatype.uuid();
      const value = faker.datatype.uuid();
      const args: AssertExistArgs = {
        bankAccount: {
          args: { userId },
          value,
        },
        bankCategory: {
          args: { userId },
          value,
        },
        bankTag: {
          args: { userId },
          value,
        },
        currency: {
          args: {},
          value,
        },
        user: {
          args: {},
          value,
        },
      };

      await assertExistFunctions(spies, args, false);
    });
  });

  describe('passes validation', () => {
    /**
     * test
     */
    // eslint-disable-next-line jest/expect-expect
    it('should pass when record exists', async () => {
      const spies = createRecordPresenceSpies(false);
      const userId = faker.datatype.uuid();
      const value = faker.datatype.uuid();
      const args: AssertExistArgs = {
        bankAccount: {
          args: { userId },
          value,
        },
        bankCategory: {
          args: { userId },
          value,
        },
        bankTag: {
          args: { userId },
          value,
        },
        currency: {
          args: {},
          value,
        },
        user: {
          args: {},
          value,
        },
      };

      await assertExistFunctions(spies, args, true);
    });
  });
});
