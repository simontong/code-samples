import { isInvalidUuidError } from '@/src/lib/database/utils';
import { BankCategoryShape } from '@/src/models/BankCategory';
import { BankTagShape } from '@/src/models/BankTag';
import container from '@/src/app/container';
import _ from 'lodash';
import { UserShape } from '@/src/models/User';
import ValidationTestRule from '@/src/types/ValidationTestRule';
import { ArgError, RuleError } from '@/src/lib/errors';
import { captureError } from '@/src/lib/errors/errorHandling';

type FindRecordFn = (value: string) => Promise<unknown>;
export type ExcludeRecord = [string?, string?];

export const recordUnique = (
  findRecordFn: FindRecordFn,
  [excludeColumn, excludeValue]: ExcludeRecord = [],
): ValidationTestRule => {
  // if finder function isn't a function
  if (typeof findRecordFn !== 'function') {
    throw new ArgError('Record finder function is invalid');
  }

  return async (value) => {
    // ignore validation on empty value
    if (!value) {
      return true;
    }

    let record;
    try {
      record = await findRecordFn(value as string);
    } catch (err) {
      // if this is uuid error then don't report
      if (!isInvalidUuidError(err)) {
        captureError(new RuleError(err, 'Validation rule failed'));
      }
      return false;
    }

    // record does not exist, so unique, pass validation
    if (!record) {
      return true;
    }

    // if exclude column provided, check if it matches record, if it does then we are still unique
    if (excludeColumn && _.get(record, excludeColumn) === excludeValue) {
      return true;
    }

    // fail anything else
    return false;
  };
};

export const bankCategoryUnique = (
  userId: string,
  whereCol: keyof BankCategoryShape = 'id',
  exclude?: ExcludeRecord,
): ValidationTestRule => {
  // prettier-ignore
  return recordUnique(
    (value) => container.cradle.bankCategoryService.findBankCategory(userId, { [whereCol]: value }),
    exclude,
  );
};

export const bankTagUnique = (
  userId: string,
  whereCol: keyof BankTagShape = 'id',
  exclude?: ExcludeRecord,
): ValidationTestRule => {
  // prettier-ignore
  return recordUnique(
    (value) => container.cradle.bankTagService.findBankTag(userId, { [whereCol]: value }),
    exclude,
  );
};

export const userUnique = (whereCol: keyof UserShape = 'id', exclude?: ExcludeRecord): ValidationTestRule => {
  // prettier-ignore
  return recordUnique(
    (value) => container.cradle.userService.findUser({ [whereCol]: value }),
    exclude,
  );
};
