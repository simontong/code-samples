import { DateTime } from 'luxon';

export default function dateDatabaseValid(date: unknown): boolean {
  if (!date) {
    return true;
  }

  if (typeof date !== 'string') {
    return false;
  }

  // check date at least has the year-month-day part
  if (!/^\d{4}-\d{2}-\d{2}/.test(date)) {
    return false;
  }

  // finally check if this date iso
  let isValid = DateTime.fromISO(date).isValid;

  // '2020-01-05 01:01:01.123' is not a valid ISO date
  // so we check if its an SQL date and valid
  if (!isValid) {
    isValid = DateTime.fromSQL(date).isValid;
  }

  return isValid;
}
