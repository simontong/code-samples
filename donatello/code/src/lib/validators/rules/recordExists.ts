import _ from 'lodash';
import container from '@/src/app/container';
import { isInvalidUuidError } from '@/src/lib/database/utils';
import { BankAccountShape } from '@/src/models/BankAccount';
import { BankCategoryShape } from '@/src/models/BankCategory';
import { BankTagShape } from '@/src/models/BankTag';
import { CurrencyShape } from '@/src/models/Currency';
import { UserShape } from '@/src/models/User';
import ValidationTestRule from '@/src/types/ValidationTestRule';
import { ArgError, RuleError } from '@/src/lib/errors';
import { captureError } from '@/src/lib/errors/errorHandling';

type FindRecordFn = (value: string) => Promise<unknown>;

export const recordExists = (findRecordFn: FindRecordFn): ValidationTestRule => {
  // if finder function isn't a function
  if (typeof findRecordFn !== 'function') {
    throw new ArgError('Record finder function is invalid');
  }

  return async (value) => {
    // ignore validation on empty value
    if (!value) {
      return true;
    }

    let record;
    try {
      record = await findRecordFn(value as string);
    } catch (err) {
      // if this is uuid error then don't report
      if (!isInvalidUuidError(err)) {
        captureError(new RuleError(err, 'Validation rule failed'));
      }
      return false;
    }

    return !!record;
  };
};

export const bankAccountExists = (userId: string, whereCol: keyof BankAccountShape = 'id'): ValidationTestRule => {
  // prettier-ignore
  return recordExists(
    (value) => container.cradle.bankAccountService.findBankAccount(userId, { [whereCol]: value }),
  );
};

export const bankCategoryExists = (userId: string, whereCol: keyof BankCategoryShape = 'id'): ValidationTestRule => {
  // prettier-ignore
  return recordExists(
    (value) => container.cradle.bankCategoryService.findBankCategory(userId, { [whereCol]: value }),
  );
};

export const bankTagExists = (userId: string, whereCol: keyof BankTagShape = 'id'): ValidationTestRule => {
  // prettier-ignore
  return recordExists(
    (value) => container.cradle.bankTagService.findBankTag(userId, { [whereCol]: value }),
  );
};

export const currencyExists = (whereCol: keyof CurrencyShape = 'code'): ValidationTestRule => {
  // prettier-ignore
  // todo:  use a mix of _.wrap and _.memoize here to stop duplicate calls to db
  return recordExists(
    (value) => container.cradle.currencyService.findCurrency({ [whereCol]: value }),
  );
};

export const userExists = (whereCol: keyof UserShape = 'id'): ValidationTestRule => {
  // prettier-ignore
  return recordExists(
    (value) => container.cradle.userService.findUser({ [whereCol]: value }),
  );
};
