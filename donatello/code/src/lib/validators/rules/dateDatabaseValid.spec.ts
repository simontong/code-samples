import dateDatabaseValid from '@/src/lib/validators/rules/dateDatabaseValid';

// prettier-ignore
const validDates = [
  '2020-01-01',
  '2020-01-05T01:01:01.123Z',
  '2020-01-05 01:01:01.123',
];

// prettier-ignore
const invalidDates = [
  -1,
  'abc',
  {},
  [1, 2, 3],
  Date.now(),
  new Date(),
  '2020',
  '2020-01',
];

describe('lib: validators/dateIsValid', () => {
  describe('fails validation', () => {
    /**
     * test
     */
    it.each(invalidDates)('should fail on invalid date', (invalidDate) => {
      expect(dateDatabaseValid(invalidDate)).toBeFalse();
    });
  });

  describe('passes validation', () => {
    /**
     * test
     */
    it.each(validDates)('should pass on valid date', (date) => {
      expect(dateDatabaseValid(date)).toBeTrue();
    });
  });
});
