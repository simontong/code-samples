import uuidOrEmpty from '@/src/lib/validators/rules/uuidOrEmpty';
import * as faker from 'faker';

describe('lib: validators/uuidOrEmpty', () => {
  describe('fails validation', () => {
    /**
     * test
     */
    it('should fail on invalid uuid', () => {
      expect(uuidOrEmpty('my-fake-uuid')).toBeFalse();
    });

    /**
     * test
     */
    it.each([123, { foo: 'bar' }, [22, 33]])('should fail if not a string', (value) => {
      expect(uuidOrEmpty(value)).toBeFalse();
    });
  });

  describe('passes validation', () => {
    /**
     * test
     */
    it('should pass on valid uuid', () => {
      expect(uuidOrEmpty(faker.datatype.uuid())).toBeTrue();
    });

    /**
     * test
     */
    it.each([0, null, undefined, ''])('should pass on empty uuid', (value) => {
      expect(uuidOrEmpty(value)).toBeTrue();
    });
  });
});
