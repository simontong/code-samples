import { BankCategoryShape } from '@/src/models/BankCategory';
import { UserShape } from '@/src/models/User';
import * as faker from 'faker';
import { bankCategoryUnique, bankTagUnique, ExcludeRecord, recordUnique, userUnique } from './recordUnique';
import { createRecordPresenceSpies, RecordPresenceSpies } from '@/test/mock/lib/validation/rules/recordPresenseHelpers';

type AssertUniqueArgs = {
  bankCategory: {
    args: { userId: string; whereCol?: keyof BankCategoryShape; exclude?: ExcludeRecord };
    value: unknown;
  };
  bankTag: {
    args: { userId: string; whereCol?: keyof BankCategoryShape; exclude?: ExcludeRecord };
    value: unknown;
  };
  user: {
    args: { whereCol?: keyof UserShape; exclude?: ExcludeRecord };
    value: unknown;
  };
};

/**
 * test all record functions here
 */
const assertUniqueFunctions = async (spies: RecordPresenceSpies, args: AssertUniqueArgs, expected: boolean) => {
  // prettier-ignore
  // bank category
  {
    const { args: { userId, whereCol = 'id', exclude }, value } = args.bankCategory;

    await expect(bankCategoryUnique(userId, whereCol, exclude)(value)).resolves.toEqual(expected);
    expect(spies.findBankCategory).toBeCalledTimes(1);
  }

  // prettier-ignore
  // bank tag
  {
    const { args: { userId, whereCol = 'id', exclude }, value } = args.bankTag;

    await expect(bankTagUnique(userId, whereCol, exclude)(value)).resolves.toEqual(expected);
    expect(spies.findBankTag).toBeCalledTimes(1);
  }

  // prettier-ignore
  // user
  {
    const { args: { whereCol = 'id', exclude }, value } = args.user;

    await expect(userUnique(whereCol, exclude)(value)).resolves.toEqual(expected);
    expect(spies.findUser).toBeCalledTimes(1);
  }
};

describe('lib: validators/recordUnique', () => {
  describe('errors', () => {
    /**
     * test
     */
    it('should throw when cannot match switch case for model', async () => {
      // @ts-ignore
      expect(() => recordUnique('BogusName')).toThrow(/record finder function is invalid/i);
    });
  });

  describe('fails validation', () => {
    /**
     * test
     */
    // eslint-disable-next-line jest/expect-expect
    it('should fail when record already exists', async () => {
      const spies = createRecordPresenceSpies(false);
      const userId = faker.datatype.uuid();
      const args: AssertUniqueArgs = {
        bankCategory: {
          args: { userId, whereCol: 'name' },
          value: 'Business Expenses',
        },
        bankTag: {
          args: { userId, whereCol: 'name' },
          value: 'holiday',
        },
        user: {
          args: { whereCol: 'email' },
          value: 'john@smith.com',
        },
      };

      await assertUniqueFunctions(spies, args, false);
    });

    /**
     * test
     */
    // eslint-disable-next-line jest/expect-expect
    it('should fail when record is not unique and excluded column value not equal to value', async () => {
      const spies = createRecordPresenceSpies(false);
      const userId = faker.datatype.uuid();
      const args: AssertUniqueArgs = {
        bankCategory: {
          args: { userId, whereCol: 'name', exclude: ['name', 'Takeout'] },
          value: 'Transport',
        },
        bankTag: {
          args: { userId, whereCol: 'name', exclude: ['name', 'sport'] },
          value: 'reimbursements',
        },
        user: {
          args: { whereCol: 'email', exclude: ['email', 'jack@chain.com'] },
          value: 'john@example.com',
        },
      };

      await assertUniqueFunctions(spies, args, false);
    });
  });

  describe('passes validation', () => {
    /**
     * test
     */
    // eslint-disable-next-line jest/expect-expect
    it('should pass when record is unique', async () => {
      const spies = createRecordPresenceSpies(true);
      const userId = faker.datatype.uuid();
      const args: AssertUniqueArgs = {
        bankCategory: {
          args: { userId, whereCol: 'name' },
          value: 'Transport',
        },
        bankTag: {
          args: { userId, whereCol: 'name' },
          value: 'reimbursements',
        },
        user: {
          args: { whereCol: 'email' },
          value: 'john@example.com',
        },
      };

      await assertUniqueFunctions(spies, args, true);
    });

    /**
     * test
     */
    // eslint-disable-next-line jest/expect-expect
    it('should pass when record is not unique and excluded column value equals value', async () => {
      const spies = createRecordPresenceSpies(true);
      const userId = faker.datatype.uuid();
      const args: AssertUniqueArgs = {
        bankCategory: {
          args: { userId, whereCol: 'name', exclude: ['name', 'Eating Out'] },
          value: 'Eating Out',
        },
        bankTag: {
          args: { userId, whereCol: 'name', exclude: ['name', 'diet'] },
          value: 'diet',
        },
        user: {
          args: { whereCol: 'email', exclude: ['email', 'http@blah.com'] },
          value: 'http@blah.com',
        },
      };

      await assertUniqueFunctions(spies, args, true);
    });
  });
});
