import { ArgError } from '@/src/lib/errors';
import Validator, { ValidationSchema, ValidatorConstructorOptions } from 'fastest-validator';
import _ from 'lodash';
import { SchemaOf, ValidationError } from 'yup';
import { ValidateOptions } from 'yup/lib/types';

export type ErrorJson = {
  path?: string;
  error: string;
};

/**
 * Format validation errors
 */
export const formatErrors = (error: ValidationError): ErrorJson[] => {
  // format errors then order by name
  // const errors = err.inner.length ? err.inner.map(formatError) : [formatError(err)];
  const errorsFormatted = _.uniqBy(error.inner, 'path').map(formatError);

  return errorsFormatted;
};

/**
 * format validation error for request
 */
const formatError = (error: ValidationError): ErrorJson => ({
  path: error.path,
  error: error.errors[0],
});

/**
 * validate input used for CLI and web app
 * generic `U = T` is because IDE isn't assuming `data` on return but instead is assuming `schema`
 */
export default async function validateOrThrow<T, U = T>(
  schema: SchemaOf<U>,
  data: T,
  customOpts?: ValidateOptions,
): Promise<T> {
  const opts: ValidateOptions = {
    abortEarly: false,
    stripUnknown: true,
    strict: true, // bankTags in bank-transaction/create route doesn't validate properly unless this is strict
    ...customOpts,
  };

  // if invalid data passed (koa-better-body was passing a function for ctx.request.body and I didn't realise it)
  if (!['string', 'object', 'array'].includes(typeof data)) {
    throw new ArgError(`Invalid data type for body. Argument passed was of type '%s'`, typeof data);
  }

  // validate, will throw on failed
  await schema.validate(data || {}, opts);

  // cast data to validatable types
  const castData = schema.cast({ ...data }) as T;

  return castData;
}

// export default async function validateOrThrow<T, U = T>(
//   schema: ValidationSchema<U>,
//   data: T,
//   customOpts?: ValidatorConstructorOptions,
// ) {
//   const opts: ValidatorConstructorOptions = {
//     // todo
//     ...customOpts,
//   };
//
//   // if invalid data passed (koa-better-body was passing a function for ctx.request.body and I didn't realise it)
//   if (!['string', 'object', 'array'].includes(typeof data)) {
//     throw new ArgError(`Invalid data type for body. Argument passed was of type '%s'`, typeof data);
//   }
//
//   const check = new Validator(opts).compile(schema);
//
//   const dataClone = _.isObject(data) ? _.cloneDeep(data) : data;
//   const res = await check(dataClone);
//
//   if (res !== true) {
//   }
//
//   return res;
// }
