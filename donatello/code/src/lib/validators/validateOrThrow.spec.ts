import validateOrThrow from '@/src/lib/validators/validateOrThrow';
import { ValidationError } from 'yup';
import * as yup from 'yup';

describe('lib: validators/validOrThrow', () => {
  describe('errors', () => {
    /**
     * test
     */
    it('throws error on invalid data type (function for eg.)', async () => {
      const schema = yup.object({
        name: yup.string().required(),
        age: yup.number().required(),
      });

      // data is of type function this should not be able to be validate
      const data = jest.fn();

      await expect(validateOrThrow(schema, data)).rejects.toThrowError(/invalid data type for body/i);
    });
  });

  describe('validation fails', () => {
    /**
     * test
     */
    it('should throw when validation failed', async () => {
      const schema = yup.object({
        name: yup.string().required(),
        age: yup.number().required(),
      });
      const data = {
        name: '',
      };

      await expect(validateOrThrow(schema, data)).rejects.toThrow(ValidationError);
    });
  });

  describe('validation succeeds', () => {
    /**
     * test
     */
    it('should not throw', async () => {
      const schema = yup.object({
        name: yup.string().required(),
        age: yup.number().required(),
      });
      const data = {
        name: '2010',
        age: 12,
      };

      await expect(validateOrThrow(schema, data)).resolves.toBeObject();
    });
  });
});
