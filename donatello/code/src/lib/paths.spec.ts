import * as paths from './paths';
import path from 'path';

const rootPath = path.join(__dirname, '../..');
describe('lib: paths', () => {
  describe('project root path', () => {
    /**
     * test
     */
    it('should return project root path', () => {
      const projectRoot = paths.projectRoot();
      expect(projectRoot).toEqual(rootPath);
    });
  });

  describe('app root path', () => {
    /**
     * test
     */
    it('should return app root path', () => {
      const appRoot = paths.srcRoot();
      expect(appRoot).toEqual(path.join(rootPath, 'src'));
    });
  });
});
