import _ from 'lodash';
import { TestContext, ValidationError } from 'yup';
import BaseModel from '@/src/lib/database/BaseModel';
import BaseQueryBuilder from '@/src/lib/database/BaseQueryBuilder';
import { IDatasource, IParsedDatasourceInput } from './index';

/**
 * allowed sort directions
 */
type SortDirection = 'asc' | 'desc';

/**
 * options used in a concrete IDatasource class
 */
export type SortableOptions<T extends BaseModel> = {
  handler?(datasource: IDatasource<T>, query: BaseQueryBuilder<T>, input: IParsedDatasourceInput): void;
  columns?: {
    [key: string]: SortableColumn<T>;
  };
};

/**
 * sortable column
 */
type SortableColumn<T extends BaseModel> = {
  handler?(query: BaseQueryBuilder<T>, sortInput: ParsedSortInput): void;
  column?: string;
  directions?: SortDirection[];
};

/**
 * parsed input from the `parseSortInput()` func
 */
export type ParsedSortInput = {
  key: string;
  direction: SortDirection;
};

/**
 * transform filter input for validator
 */
export function transformSortInput(val: string, origVal: string): ParsedSortInput[] {
  const input = _.chain(_.isArray(origVal) ? origVal : origVal.split(','))
    .map((i) => parseSortInput(i.trim()))
    .filter()
    .uniqBy('key')
    .value();

  // @ts-ignore
  return input;
}

/**
 * parse sort input
 */
export function parseSortInput(sortInput: unknown): ParsedSortInput | undefined {
  if (!sortInput || typeof sortInput !== 'string') {
    return;
  }

  // parse filter
  const match = sortInput.match(/^(-)?(.+)$/);
  if (!match) {
    return;
  }

  // extract desc symbol ("-") and column key
  const [, descSymbol, key] = match;
  const direction: SortDirection = !descSymbol ? 'asc' : 'desc';

  return { key, direction };
}

/**
 * validate sort input
 */
export function validateSortInput<T extends BaseModel>(sortables?: SortableOptions<T>) {
  return (value: unknown, ctx: TestContext): boolean | ValidationError => {
    if (!value) return true;

    // expect sortInput to be array
    if (!Array.isArray(value)) {
      return ctx.createError({
        type: 'sortInput',
        message: 'Invalid sort',
      });
    }

    // assume `value` is ParsedSortInputs
    const sortInputs = value as ParsedSortInput[];

    // no inputs so ignore validation
    if (sortInputs.length === 0) {
      return true;
    }

    for (const sortInput of sortInputs) {
      const { key, direction } = sortInput;

      // key missing
      if (!key) {
        return ctx.createError({
          type: 'sortInput',
          message: `Empty sort key`,
        });
      }

      // check column allowed
      const sortableColumn: SortableColumn<T> | undefined = sortables?.columns?.[key];
      if (!sortableColumn) {
        return ctx.createError({
          type: 'sortInput',
          message: `Invalid sort key`,
        });
      }

      // check if asc/desc allowed
      if (Array.isArray(sortableColumn?.directions) && !sortableColumn.directions.includes(direction)) {
        return ctx.createError({
          type: 'sortInput',
          message: `Invalid sort direction for ${key}`,
        });
      }
    }

    return true;
  };
}

/**
 * apply sort to query
 */
export function applySorts<T extends BaseModel>(
  datasource: IDatasource<T>,
  query: BaseQueryBuilder<T>,
  parsedInput: IParsedDatasourceInput,
): void {
  if (!datasource.sortables) {
    return;
  }
  const sortables = datasource.sortables;

  // pass control of applySort down to `datasource` class
  if (typeof sortables.handler === 'function') {
    sortables.handler(datasource, query, parsedInput);
    return;
  }

  // no input so bail
  if (!parsedInput.sort || parsedInput.sort.length === 0) {
    return;
  }

  for (const sortInput of parsedInput.sort) {
    if (!sortInput) {
      continue;
    }

    const sortableColumn: SortableColumn<T> | undefined = sortables?.columns?.[sortInput.key];
    if (!sortableColumn) {
      continue;
    }

    // custom filter handler for column
    if (typeof sortableColumn.handler === 'function') {
      sortableColumn.handler(query, sortInput);
      continue;
    }

    if (!sortableColumn.column) {
      continue;
    }

    // default handler
    query.orderBy(sortableColumn.column, sortInput.direction);
  }
}
