import BaseModel from '@/src/lib/database/BaseModel';
import BaseQueryBuilder from '@/src/lib/database/BaseQueryBuilder';
import { IDatasource, IParsedDatasourceInput } from '@/src/lib/datasource/index';

export type SearchableOptions<T extends BaseModel> = {
  handler?(datasource: IDatasource<T>, query: BaseQueryBuilder<T>, input: IParsedDatasourceInput): void;
  columns: {
    [key: string]: SearchableColumn<T>;
  };
};

/**
 * search column
 */
type SearchableColumn<T extends BaseModel> = {
  handler?(query: BaseQueryBuilder<T>, searchableColumn: SearchableColumn<T>, searchInput: string): void;
  column?: string;
};

/**
 * validate search input
 */
export function validateSearchInput<T extends BaseModel>(searchable?: SearchableOptions<T>) {
  return (value: unknown): boolean => {
    if (!value) return true;

    // what do do here?
    return true;
  };
}

/**
 * apply global search to query
 */
export function applySearch<T extends BaseModel>(
  datasource: IDatasource<T>,
  query: BaseQueryBuilder<T>,
  parsedInput: IParsedDatasourceInput,
): void {
  if (!datasource.searchable) {
    return;
  }

  const searchable = datasource.searchable;

  // pass control of applySearch down to `datasource` class
  if (typeof searchable.handler === 'function') {
    searchable.handler(datasource, query, parsedInput);
    return;
  }

  const searchInput = parsedInput.search;

  // no input so bail
  if (!searchInput) {
    return;
  }

  query.where((qb: BaseQueryBuilder<T>) => {
    for (const searchableColumn of Object.values(searchable?.columns)) {
      // custom filter handler for column
      if (typeof searchableColumn.handler === 'function') {
        searchableColumn.handler(query, searchableColumn, searchInput);
        continue;
      }

      if (!searchableColumn.column) {
        continue;
      }

      // default handler -- todo: this should be improved
      qb.orWhereRaw('to_tsvector(cast(?? as text)) @@ plainto_tsquery(?)', [searchableColumn.column, searchInput]);
    }
  });

  // /**
  //  * default search handler
  //  */
  // const defaultHandler = (query: DatasourceQueryBuilder<TModel>, value: string) => {
  //   query.whereRaw(
  //     // todo: the to_tsvector() col here definitely needs to be indexed
  //     searchable?.columns.map(() => 'to_tsvector(cast(?? as text))').join(' || ') + ' @@ plainto_tsquery(?)',
  //     [...searchable?.columns, value],
  //   );
  // };
  //
  // // handle search
  // if (typeof searchable.handler !== 'function') {
  //   defaultHandler(query, input.search);
  // } else {
  //   searchable.handler(query, input.search);
  // }

  // todo
  // - search multiple fields with one keyword
  // - custom handler
  // validation?
}
