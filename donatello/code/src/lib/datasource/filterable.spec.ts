import BaseModel from '@/src/lib/database/BaseModel';
import * as yup from 'yup';
import {
  applyFilters,
  FilterableOperatorKey,
  FilterableOptions,
  ParsedFilterInput,
  parseFilterInput,
  transformFilterInput,
  validateFilterInput,
} from '@/src/lib/datasource/filterable';

class DummyProduct extends BaseModel {}

describe('lib: datasource/filterable', () => {
  describe('parseFilterable', () => {
    /**
     * test
     */
    it('should parse filter input', () => {
      const inputs: Array<[string, ParsedFilterInput]> = [
        [
          'date:2020-01-01',
          {
            key: 'date',
            operator: 'eq',
            value: '2020-01-01',
            operatorArgs: ['2020-01-01'],
          },
        ],
        [
          'amount:15500',
          {
            key: 'amount',
            operator: 'eq',
            value: '15500',
            operatorArgs: ['15500'],
          },
        ],
        [
          'note:heres a list:a,b,c,d',
          {
            key: 'note',
            operator: 'eq',
            value: 'heres a list:a,b,c,d',
            operatorArgs: ['heres a list:a,b,c,d'],
          },
        ],

        // with operators
        [
          'date@gt:2020-01-01',
          {
            key: 'date',
            operator: 'gt',
            value: '2020-01-01',
            operatorArgs: ['2020-01-01'],
          },
        ],
        [
          'amount@between:15500,50000',
          {
            key: 'amount',
            operator: 'between',
            value: '15500,50000',
            operatorArgs: ['15500', '50000'],
          },
        ],
        [
          'note@in:a,b,c,d',
          {
            key: 'note',
            operator: 'in',
            value: 'a,b,c,d',
            operatorArgs: ['a', 'b', 'c', 'd'],
          },
        ],
      ];

      for (const [input, output] of inputs) {
        expect(parseFilterInput(input)).toEqual(output);
      }
    });

    /**
     * test
     */
    it('should return undefined on bad input data', () => {
      expect(parseFilterInput({ filter: '' })).toBeUndefined();
      expect(parseFilterInput({ filter: 'payee' })).toBeUndefined();
      expect(parseFilterInput({ filter: 'date:' })).toBeUndefined();
      expect(parseFilterInput({ filter: 'category@' })).toBeUndefined();
      expect(parseFilterInput({ filter: 'tag@lt' })).toBeUndefined();
      expect(parseFilterInput({ filter: 'amount@gt:' })).toBeUndefined();
    });
  });

  describe('validateFilterInput', () => {
    /**
     * test
     */
    it('should fail on unspecified filterable columns', async () => {
      const filterables: FilterableOptions<DummyProduct> = {
        columns: {
          amount: {
            type: 'float',
            column: 'products.amount',
            operators: ['eq'],
          },
          age: {
            type: 'integer',
            column: 'products.age',
            operators: ['eq'],
          },
          created: {
            type: 'datetime',
            column: 'products.created_at',
            operators: ['gt'],
          },
        },
      };

      const schema = yup.object({
        filter: yup.array().transform(transformFilterInput).test(validateFilterInput(filterables)),
      });

      // list?filter=date@gt:2020-01-01 -- 'date' not specified in `filterables`
      await expect(schema.validate({ filter: 'date:2020-01-01' })).rejects.toMatchObject({
        errors: [expect.stringMatching(/invalid filter key/i)],
      });

      // list?filter=amount@gt:100 -- 'gt' not specified in `filterables` for `amount` column
      await expect(schema.validate({ filter: 'amount@gt:100' })).rejects.toMatchObject({
        errors: [expect.stringMatching(/invalid operator key for 'amount'/i)],
      });

      // list?filter=age@lt:18 -- 'lt' not specified in `filterables` for `age` column
      await expect(schema.validate({ filter: 'age@gt:18' })).rejects.toMatchObject({
        errors: [expect.stringMatching(/invalid operator key for 'age'/i)],
      });

      // list?filter=created@bogusOp:2020 -- 'bogusOp' not is not specified in operatormap
      await expect(schema.validate({ filter: 'created@bogusOp:2020' })).rejects.toMatchObject({
        errors: [expect.stringMatching(/invalid operator key for 'created'/i)],
      });
    });

    /**
     * test
     */
    it('should fail on value / column type mismatch', async () => {
      const filterables: FilterableOptions<DummyProduct> = {
        columns: {
          name: {
            column: 'products.name',
            operators: ['eq'],
          },
          price: {
            type: 'float',
            column: 'products.amount',
            operators: ['eq'],
          },
          created: {
            type: 'datetime',
            column: 'products.created_at',
            operators: ['eq'],
          },
          launchDate: {
            type: 'date',
            column: 'products.launch_date',
            operators: ['eq'],
          },
          count: {
            type: 'integer',
            column: 'products.count',
            operators: ['eq'],
          },
        },
      };

      const schema = yup.object({
        filter: yup.array().transform(transformFilterInput).test(validateFilterInput(filterables)),
      });

      // list?filter=price:abc -- fail because not a number
      await expect(schema.validate({ filter: 'price:abc' })).rejects.toMatchObject({
        errors: [expect.stringMatching(/invalid operator arg type for 'price'/i)],
      });

      // list?filter=created:foobar -- fail because not a datetime
      await expect(schema.validate({ filter: 'created:foobar' })).rejects.toMatchObject({
        errors: [expect.stringMatching(/invalid operator arg type for 'created'/i)],
      });

      // list?filter=launchDate:2020-01-01T01:01:01.001 -- fail because not a date
      await expect(schema.validate({ filter: 'launchDate:2020-01-01T01:01:01.001' })).rejects.toMatchObject({
        errors: [expect.stringMatching(/invalid operator arg type for 'launchDate'/i)],
      });

      // list?filter=count:123.56 -- fail because not an integer
      await expect(schema.validate({ filter: 'count:123.55' })).rejects.toMatchObject({
        errors: [expect.stringMatching(/invalid operator arg type for 'count'/i)],
      });
    });

    /**
     * test
     */
    it('should pass on valid value / column type', async () => {
      const filterables: FilterableOptions<DummyProduct> = {
        columns: {
          name: {
            type: 'text',
            column: 'products.name',
            operators: ['eq'],
          },
          price: {
            type: 'float',
            column: 'products.amount',
            operators: ['eq'],
          },
          created: {
            type: 'datetime',
            column: 'products.created_at',
            operators: ['eq'],
          },
          launchDate: {
            type: 'date',
            column: 'products.launch_date',
            operators: ['eq'],
          },
          count: {
            type: 'integer',
            column: 'products.count',
            operators: ['eq'],
          },
        },
      };

      const schema = yup.object({
        filter: yup.array().transform(transformFilterInput).test(validateFilterInput(filterables)),
      });

      // list?filter= -- empty
      await expect(schema.validate({ filter: '' })).resolves.toBeObject();

      // list?filter=foobar -- empty
      await expect(schema.validate({ filter: 'foobar' })).resolves.toBeObject();

      // list?filter=name: -- empty
      await expect(schema.validate({ filter: 'name:' })).resolves.toBeObject();

      // list?filter=name:toys
      await expect(schema.validate({ filter: 'name:toys' })).resolves.toBeObject();

      // list?filter=price:123.56
      await expect(schema.validate({ filter: 'price:123.56' })).resolves.toBeObject();

      // list?filter=price:150 -- number can pass float
      await expect(schema.validate({ filter: 'price:150' })).resolves.toBeObject();

      // list?filter=created:2020-01-01 -- pass datetime with just a date
      await expect(schema.validate({ filter: 'created:2020-01-01' })).resolves.toBeObject();

      // list?filter=launchDate:2020-01-01
      await expect(schema.validate({ filter: 'launchDate:2020-01-01' })).resolves.toBeObject();

      // list?filter=created:2020-01-01 -- pass datetime with just a date
      await expect(schema.validate({ filter: 'created:2020-01-01T11:11:11.111' })).resolves.toBeObject();

      // list?filter=count:124
      await expect(schema.validate({ filter: 'count:124' })).resolves.toBeObject();
    });

    /**
     * test
     */
    it('should pass on valid operators', async () => {
      const filterables: FilterableOptions<DummyProduct> = {
        columns: {
          name: {
            type: 'text',
            column: 'products.name',
            operators: ['eq'],
          },
          price: {
            type: 'float',
            column: 'products.amount',
            operators: ['gt', 'lt', 'gte', 'lte'],
          },
          created: {
            type: 'datetime',
            column: 'products.created_at',
            operators: ['between'],
          },
          tags: {
            type: 'text',
            column: 'products.tags',
            operators: ['in'],
          },
          colors: {
            type: 'text',
            column: 'products.colors',
            operators: ['notIn'],
          },
        },
      };

      const schema = yup.object({
        filter: yup.array().transform(transformFilterInput).test(validateFilterInput(filterables)),
      });

      // list?filter=name:cars
      await expect(schema.validate({ filter: 'name:cars' })).resolves.toBeObject();

      // list?filter=price@[gt|lt|gte|lte]:100
      const ops: FilterableOperatorKey[] = ['gt', 'lt', 'gte', 'lte'];
      for (const op of ops) {
        await expect(schema.validate({ filter: `price@${op}:100` })).resolves.toBeObject();
      }

      // list?filter=created@between:2020-01-01,2020-02-01
      await expect(schema.validate({ filter: 'created@between:2020-01-01,2020-02-01' })).resolves.toBeObject();

      // list?filter=tags@in:toys,cars,models
      await expect(schema.validate({ filter: 'tags@in:toys,cars,models' })).resolves.toBeObject();

      // list?filter=colors@notIn:red,blue,green,orange
      await expect(schema.validate({ filter: 'colors@notIn:red,blue,green,orange' })).resolves.toBeObject();
    });
  });

  describe('applyFilters', () => {
    /**
     * test
     */
    it.todo('should use custom handler if specified in datasource class');
    // it('should use custom handler if specified in datasource class', async () => {
    // test this without hitting database
    // class DummyDatasource implements IDatasource<DummyProduct> {
    //   filterables: FilterableOptions<DummyProduct> = {
    //     handler() {
    //       stub();
    //     },
    //   };
    //
    //   query() {
    //     return DummyProduct.query();
    //   }
    // }
    //
    // const stub = jest.fn();
    // const query = jest.fn();
    // const datasource = new DummyDatasource();
    // const parsedInput = {};
    //
    // applyFilters(datasource, query, parsedInput);
    // });
  });
});
