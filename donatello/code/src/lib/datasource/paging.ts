import BaseModel from '@/src/lib/database/BaseModel';
import { BasePage } from '@/src/lib/database/BasePage';
import BaseQueryBuilder from '@/src/lib/database/BaseQueryBuilder';
import { IDatasource, IParsedDatasourceInput } from '@/src/lib/datasource/index';
import { TestContext, ValidationError } from 'yup';

export type PagingOptions<T extends BaseModel> = {
  handler?(datasource: IDatasource<T>, query: BaseQueryBuilder<T>, input: IParsedDatasourceInput): Promise<BasePage<T>>;
  pageSize?: {
    steps?: number[];
    default?: number;
    min?: number;
    max?: number;
  };
};

/**
 * validate page number
 */
// eslint-disable-next-line @typescript-eslint/no-unused-vars
export function validatePageInput<T extends BaseModel>(paging?: PagingOptions<T>) {
  return (value: unknown, ctx: TestContext): boolean | ValidationError => {
    const pageInput = Number(value);

    if (Number.isNaN(pageInput)) {
      return ctx.createError({
        type: 'pageInput',
        message: 'Invalid page ',
      });
    }

    // check min page
    const minPage = 0;
    if (pageInput < minPage) {
      return ctx.createError({
        type: 'pageInput',
        message: `Page cannot be less than ${minPage}`,
      });
    }

    // check max page
    const maxPage = Number.MAX_SAFE_INTEGER;
    if (pageInput > maxPage) {
      return ctx.createError({
        type: 'pageInput',
        message: `Page cannot be more than ${maxPage}`,
      });
    }

    return true;
  };
}

/**
 * validate page size
 */
export function validatePageSizeInput<T extends BaseModel>(paging?: PagingOptions<T>) {
  return (value: unknown, ctx: TestContext): boolean | ValidationError => {
    if (typeof value !== 'number') return true;

    const pageSizeInput = Number(value);

    if (Number.isNaN(pageSizeInput)) {
      return ctx.createError({
        type: 'pageSizeInput',
        message: 'Invalid page size',
      });
    }

    // check min page size
    const minPageSize = paging?.pageSize?.min || 1;
    if (pageSizeInput < minPageSize) {
      return ctx.createError({
        type: 'pageSizeInput',
        message: `Page size cannot be less than ${minPageSize}`,
      });
    }

    // check max page size
    const maxPageSize = paging?.pageSize?.max || 1000;
    if (pageSizeInput > maxPageSize) {
      return ctx.createError({
        type: 'pageSizeInput',
        message: `Page size cannot be more than ${maxPageSize}`,
      });
    }

    // check steps valid
    const steps = paging?.pageSize?.steps || [];
    if (steps.length > 0 && !steps.includes(pageSizeInput)) {
      return ctx.createError({
        type: 'pageSizeInput',
        message: `Invalid page size. Available: ${steps.join(', ')}`,
      });
    }

    return true;
  };
}

/**
 * apply page and page sizing to query
 */
export function paginate<T extends BaseModel>(
  datasource: IDatasource<T>,
  query: BaseQueryBuilder<T>,
  parsedInput: IParsedDatasourceInput,
): Promise<BasePage<T>> {
  const paging = datasource.paging;

  // pass control of applyPaging down to `datasource` class
  if (typeof paging?.handler === 'function') {
    return paging.handler(datasource, query, parsedInput);
  }

  const { page, pageSize } = parsedInput;

  return query.page(page, pageSize);
}
