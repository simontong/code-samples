import BaseModel from '@/src/lib/database/BaseModel';
import * as yup from 'yup';
import {
  SortableOptions,
  parseSortInput,
  transformSortInput,
  validateSortInput,
  ParsedSortInput,
} from '@/src/lib/datasource/sortable';

describe('lib: datasource/sortable', () => {
  describe('parseSortable', () => {
    /**
     * test
     */
    it('should parse sort input', () => {
      const inputs: Array<[string, ParsedSortInput]> = [
        ['-date', { key: 'date', direction: 'desc' }],
        ['amount', { key: 'amount', direction: 'asc' }],
      ];

      for (const [input, output] of inputs) {
        expect(parseSortInput(input)).toEqual(output);
      }
    });

    /**
     * test
     */
    it('should return undefined on bad input data', () => {
      expect(parseSortInput({ sort: '' })).toBeUndefined();
      expect(parseSortInput({ sort: '-' })).toBeUndefined();
      expect(parseSortInput({ sort: ',' })).toBeUndefined();
    });
  });

  describe('validateSortInput', () => {
    /**
     * test
     */
    it('should fail on unspecified sortable columns', async () => {
      class DummyProduct extends BaseModel {}

      const sortables: SortableOptions<DummyProduct> = {
        columns: {
          amount: { column: 'products.amount' },
          created: {
            column: 'products.created_at',
            directions: ['desc'],
          },
        },
      };

      const schema = yup.object({
        sort: yup.array().transform(transformSortInput).test(validateSortInput(sortables)),
      });

      // list?sort=-date -- 'date' not specified in `sortables`
      await expect(schema.validate({ sort: '-date' })).rejects.toMatchObject({
        errors: [expect.stringMatching(/invalid sort key/i)],
      });

      // list?sort=amount,created -- even though '-created' is valid, 'absent' isn't present in sortables
      await expect(schema.validate({ sort: 'absent,-created' })).rejects.toMatchObject({
        errors: [expect.stringMatching(/invalid sort key/i)],
      });

      // list?sort=created -- 'created' isn't allowed 'asc', only 'desc' allowed in sortables
      await expect(schema.validate({ sort: 'created' })).rejects.toMatchObject({
        errors: [expect.stringMatching(/invalid sort direction for created/i)],
      });
    });

    /**
     * test
     */
    it('should pass on correct sortable columns', async () => {
      class DummyProduct extends BaseModel {}

      const sortables: SortableOptions<DummyProduct> = {
        columns: {
          amount: { column: 'products.amount' },
          price: {
            column: 'products.price',
            directions: ['desc'],
          },
          name: {
            column: 'products.name',
            directions: ['asc'],
          },
          count: {
            column: 'products.count',
            directions: ['asc', 'desc'],
          },
        },
      };

      const schema = yup.object({
        sort: yup.array().transform(transformSortInput).test(validateSortInput(sortables)),
      });

      // list?sort= -- empty sort
      await expect(schema.validate({ sort: '' })).resolves.toBeObject();

      // list?sort=[amount|-amount]
      await expect(schema.validate({ sort: 'amount' })).resolves.toBeObject();
      await expect(schema.validate({ sort: '-amount' })).resolves.toBeObject();

      // list?sort=-price
      await expect(schema.validate({ sort: '-price' })).resolves.toBeObject();

      // list?sort=name
      await expect(schema.validate({ sort: 'name' })).resolves.toBeObject();

      // list?sort=[count|-count]
      await expect(schema.validate({ sort: 'count' })).resolves.toBeObject();
      await expect(schema.validate({ sort: '-count' })).resolves.toBeObject();
    });
  });
});
