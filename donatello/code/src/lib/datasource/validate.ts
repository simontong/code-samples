import { validatePageInput, validatePageSizeInput } from '@/src/lib/datasource/paging';
import { validateSearchInput } from '@/src/lib/datasource/searchable';
import { array, number, object, SchemaOf, string } from 'yup';
import { ValidateOptions } from 'yup/lib/types';
import validateOrThrow from '@/src/lib/validators/validateOrThrow';
import BaseModel from '@/src/lib/database/BaseModel';
import { IDatasource, IDatasourceInput, IParsedDatasourceInput } from './index';
import { validateFilterInput, transformFilterInput } from './filterable';
import { transformSortInput, validateSortInput } from './sortable';

/**
 * validate user input from request
 */
export async function validate<T extends BaseModel>(
  datasource: IDatasource<T>,
  input: IDatasourceInput,
): Promise<IParsedDatasourceInput> {
  const { sortables, filterables, searchable, paging } = datasource;

  // set default page and page size
  const defaultPage = 0;
  const defaultPageSize = paging?.pageSize?.steps?.[0] || paging?.pageSize?.default || 25;

  const schema: SchemaOf<IDatasourceInput> = object({
    // prettier-ignore
    sort: array()
      .label('Sort')
      .transform(transformSortInput)
      .test(validateSortInput(sortables))
      .ensure()
      .compact(),

    // prettier-ignore
    filter: array()
      .label('Filter')
      .transform(transformFilterInput)
      .test(validateFilterInput(filterables))
      .ensure()
      .compact(),

    // prettier-ignore
    search: string()
      .label('Search')
      .trim()
      .test(validateSearchInput(searchable)),

    // prettier-ignore
    page: number()
      .default(defaultPage)
      .label('Page')
      .required()
      .integer()
      .test(validatePageInput(paging)),

    // prettier-ignore
    pageSize: number()
      .default(defaultPageSize)
      .label('Page')
      .required()
      .integer()
      .test(validatePageSizeInput(paging)),
  });

  const opts: ValidateOptions = {
    abortEarly: false,
    strict: false, // if true, only validate the input, and skip any coercion or transformation
  };

  return (await validateOrThrow(schema, input, opts)) as IParsedDatasourceInput;
}
