import ValuesOf from '@/src/types/ValuesOf';
import _ from 'lodash';
import BaseModel from '@/src/lib/database/BaseModel';
import BaseQueryBuilder from '@/src/lib/database/BaseQueryBuilder';
import { IDatasource, IParsedDatasourceInput } from '@/src/lib/datasource/index';
import validator from 'validator';
import { TestContext, ValidationError } from 'yup';

// [x] operators
// [x] allow only specific operators
// [x] custom handling for all cols
// [x] custom handling for single col
// [ ] handle AND / OR
// eg.
// /list?filter=date@range:2020-01-01,2020-01-30 (where date between 2020-01-01 and 2020-01-30)
// /list?filter=age@gte:18 (where age >= 18)
// /list?filter=date:2020-01-01 (where date = 2020-01-01)
// /list?filter=ids@in:1,2,3,4 (where ids in (1,2,3,4)
// /list?filter=date:2020-01-01|age@gte:18 (where (date = '2020-01-01' or age >= 18)

/**
 * Column types allowed for FilterableColumn.type
 */
const filterableColumnTypes = ['date', 'datetime', 'integer', 'float', 'text'] as const;
type FilterableColumnType = ValuesOf<typeof filterableColumnTypes>;

/**
 * key out the filter operations so args can use it as a type
 */
const filterableOperatorKeys = ['ts', 'eq', 'gt', 'gte', 'lt', 'lte', 'between', 'in', 'notIn'] as const;
export type FilterableOperatorKey = ValuesOf<typeof filterableOperatorKeys>;

/**
 * options used in a concrete IDatasource class
 */
export type FilterableOptions<T extends BaseModel> = {
  handler?(datasource: IDatasource<T>, query: BaseQueryBuilder<T>, input: IParsedDatasourceInput): void;
  columns?: {
    [key: string]: FilterableColumn<T>;
  };
};

/**
 * Filterable column
 */
type FilterableColumn<T extends BaseModel> = {
  handler?(query: BaseQueryBuilder<T>, filterInput: ParsedFilterInput): void;
  type?: FilterableColumnType;
  column?: string;
  operators?: FilterableOperatorKey[];
};

/**
 * parsed input from the `parseFilterInput()` func
 */
export type ParsedFilterInput = {
  key: string;
  operator?: FilterableOperatorKey;
  operatorArgs?: string[];
  value?: string;
};

/**
 * Parse FilterableColumn.column.operator
 */
const parseOperatorArgsMap = (value: string): Record<FilterableOperatorKey, () => string[]> => ({
  ts: () => [value.trim()],
  eq: () => [value.trim()],
  gt: () => [value.trim()],
  gte: () => [value.trim()],
  lt: () => [value.trim()],
  lte: () => [value.trim()],
  between: () => _.filter(value.split(',')),
  in: () => _.filter(value.split(',')),
  notIn: () => _.filter(value.split(',')),
});

/**
 * validate FilterableColumn.column.type
 */
const validateTypeMap = (value: string): Record<FilterableColumnType, () => boolean> => ({
  datetime: () => /^\d{4}-\d{2}-\d{2}(T\d{2}:\d{2}(:\d{2}(\.\d{3})?)?)?$/.test(value),
  date: () => /^\d{4}-\d{2}-\d{2}$/.test(value),
  integer: () => validator.isInt(value),
  float: () => validator.isFloat(value),
  text: () => /^.+$/.test(value),
});

/**
 * validate FilterableColumn.column.operator
 */
const validateOperatorArgCount = (argCount: number): Record<FilterableOperatorKey, () => boolean> => ({
  ts: () => argCount === 1,
  eq: () => argCount === 1,
  gt: () => argCount === 1,
  gte: () => argCount === 1,
  lt: () => argCount === 1,
  lte: () => argCount === 1,
  between: () => argCount === 2,
  in: () => argCount >= 1,
  notIn: () => argCount >= 1,
});

/**
 * apply filter operation
 */
export const applyOperatorMap = <T extends BaseModel>(
  query: BaseQueryBuilder<T>,
  column: string,
  value: string[],
): Record<FilterableOperatorKey, () => void> => ({
  ts: () => query.whereRaw('to_tsvector(??) @@ plainto_tsquery(?)', [column, value[0]]),
  eq: () => query.where(column, '=', value[0]),
  gt: () => query.where(column, '>', value[0]),
  gte: () => query.where(column, '>=', value[0]),
  lt: () => query.where(column, '<', value[0]),
  lte: () => query.where(column, '<=', value[0]),
  between: () => query.whereBetween(column, value as [string, string]),
  in: () => query.whereIn(column, value),
  notIn: () => query.whereNotIn(column, value),
});

/**
 * transform filter input for validator
 */
export function transformFilterInput(val: string, origVal: string): ParsedFilterInput[] {
  const input = _.chain(_.isArray(origVal) ? origVal : [origVal])
    .map((i) => parseFilterInput(i))
    .filter()
    .uniqBy('key')
    .value();

  // @ts-ignore
  return input;
}

/**
 * parse filter input
 */
export function parseFilterInput(filterInput: unknown): ParsedFilterInput | undefined {
  if (!filterInput || typeof filterInput !== 'string') {
    return;
  }

  // parse filter
  const match = filterInput.match(/^([^:@]+)(?:@([^:]+))?:(.*)$/);
  if (!match) {
    return;
  }

  // extract filterable column key and value
  const [, key, operator = 'eq', value] = match as [string, string, FilterableOperatorKey, string];

  // run parse operator map function to parse for the specific operator we use
  const parseOperator = parseOperatorArgsMap(value);
  const operatorArgs = _.filter(parseOperator[operator] ? parseOperator[operator]() : [value]);

  // empty operator args
  if (operatorArgs.length === 0) {
    return;
  }

  return { key, operator, operatorArgs, value };
}

/**
 * filter input validator
 */
export function validateFilterInput<T extends BaseModel>(filterables?: FilterableOptions<T>) {
  return (value: unknown, ctx: TestContext): boolean | ValidationError => {
    if (!value) return true;

    // expect filterInput to be array
    if (!Array.isArray(value)) {
      return ctx.createError({
        type: 'filterInput',
        message: 'Invalid filter',
      });
    }

    // assume `value` is ParsedFilterInputs
    const filterInputs = value as ParsedFilterInput[];

    // no inputs so ignore validation
    if (filterInputs.length === 0) {
      return true;
    }

    for (const filterInput of filterInputs) {
      const { key, operator, operatorArgs } = filterInput;

      // key missing
      if (!key) {
        return ctx.createError({
          type: 'filterInput',
          message: `Missing key`,
        });
      }

      if (!operator) {
        return ctx.createError({
          type: 'filterInput',
          message: `Missing operator`,
        });
      }

      if (!operatorArgs || operatorArgs.length === 0) {
        return ctx.createError({
          type: 'filterInput',
          message: `Empty operator args`,
        });
      }

      // check column allowed
      const filterableColumn: FilterableColumn<T> | undefined = filterables?.columns?.[key];
      if (!filterableColumn) {
        return ctx.createError({
          type: 'filterInput',
          message: `Invalid filter key`,
        });
      }

      // if no filterable operators available or operator was empty
      // noinspection SuspiciousTypeOfGuard
      if (!filterableColumn.operators || filterableColumn.operators.length === 0) {
        return ctx.createError({
          type: 'filterInput',
          message: `No operators for '${key}'`,
        });
      }

      // if filterable operators not allowed by this filterable OR if
      // `operatorMap` has disabled it then return false
      if (!filterableColumn.operators?.includes(operator) || !filterableOperatorKeys.includes(operator)) {
        return ctx.createError({
          type: 'filterInput',
          message: `Invalid operator key for '${key}'`,
        });
      }

      // make sure operator args are valid
      const operatorArgsIsValid = validateOperatorArgCount(operatorArgs.length)[operator]();
      if (!operatorArgsIsValid) {
        return ctx.createError({
          type: 'filterInput',
          message: `Invalid operator args for '${key}'`,
        });
      }

      // make sure column type is allowed
      const filterableColumnType = filterableColumn.type;
      if (!filterableColumnType || !filterableColumnTypes.includes(filterableColumnType)) {
        return ctx.createError({
          type: 'filterInput',
          message: `Invalid column type for '${key}'`,
        });
      }

      // make sure operator arg values match arg types
      for (const operatorArg of operatorArgs) {
        const validateType = validateTypeMap(operatorArg)[filterableColumnType];
        if (!validateType()) {
          return ctx.createError({
            type: 'filterInput',
            message: `Invalid operator arg type for '${key}'`,
          });
        }
      }
    }

    return true;
  };
}

/**
 * apply filters to query
 */
export function applyFilters<T extends BaseModel>(
  datasource: IDatasource<T>,
  query: BaseQueryBuilder<T>,
  parsedInput: IParsedDatasourceInput,
): void {
  if (!datasource.filterables) {
    return;
  }
  const filterables = datasource.filterables;

  // pass control of applyFilter down to concrete `IDatasource` class
  if (typeof filterables.handler === 'function') {
    filterables.handler(datasource, query, parsedInput);
    return;
  }

  // no input? bail
  if (!parsedInput.filter || parsedInput.filter.length === 0) {
    return;
  }

  for (const filterInput of parsedInput.filter) {
    if (!filterInput) {
      continue;
    }

    const filterableColumn: FilterableColumn<T> | undefined = filterables?.columns?.[filterInput.key];
    if (!filterableColumn) {
      continue;
    }

    // custom filter handler for column
    if (typeof filterableColumn.handler === 'function') {
      filterableColumn.handler(query, filterInput);
      continue;
    }

    if (!filterableColumn.column) {
      continue;
    }

    if (!filterInput.operator) {
      continue;
    }

    if (!filterInput.operatorArgs || filterInput.operatorArgs.length === 0) {
      continue;
    }

    // default handler
    applyOperatorMap(query, filterableColumn.column, filterInput.operatorArgs)[filterInput.operator]();
  }
}
