import BaseModel from '@/src/lib/database/BaseModel';
import BaseQueryBuilder from '@/src/lib/database/BaseQueryBuilder';
import { IDatasource, IParsedDatasourceInput } from '@/src/lib/datasource/index';
import { DatasourceError } from '@/src/lib/errors';
import { Knex } from 'knex';
import { PassThrough } from 'stream';
import Raw = Knex.Raw;

export type ExportableOptions<T extends BaseModel> = {
  handler?(datasource: IDatasource<T>, query: BaseQueryBuilder<T>, input: IParsedDatasourceInput): PassThrough;
  select: Array<string | Raw>;
};

/**
 * apply global export to query
 */
export function exporter<T extends BaseModel>(
  datasource: IDatasource<T>,
  query: BaseQueryBuilder<T>,
  parsedInput: IParsedDatasourceInput,
): PassThrough {
  const exportable = datasource.exportable;

  // must have exportable on datasource object to use export
  if (!exportable) {
    throw new DatasourceError('Missing exportable on datasource object');
  }

  // pass control of applyExport down to `datasource` class
  if (typeof exportable.handler === 'function') {
    return exportable.handler(datasource, query, parsedInput);
  }

  // make sure we have exportable.select on object if using default option
  if (!exportable.select) {
    throw new DatasourceError('Missing select on datasource.exportable object');
  }

  // prettier-ignore
  const q = query
    .toKnexQuery()
    .clearSelect()
    .select(exportable.select);

  // retain DISTINCT ON selection (after clean selects
  // @ts-ignore
  const distinctOn = query.findAllSelections('distinctOn').filter(i => i.column !== '*').map(i => `${i.table}.${i.column}`);
  if (distinctOn.length > 0) {
    q.distinctOn(distinctOn);
  }

  return q.stream();
}
