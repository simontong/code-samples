import * as faker from 'faker';
import _ from 'lodash';
import { factoryBankTransaction, factoryUser } from '@/database/factory';
import { FilterableOptions } from './filterable';
import { buildDatasource, IDatasource, IDatasourceInput } from './index';
import { SortableOptions } from './sortable';
import BankTransaction from '@/src/models/BankTransaction';
import { rollbackDatabaseTransaction, startDatabaseTransaction } from '@/test/utils/databaseTransaction';

beforeEach(async () => {
  await startDatabaseTransaction();
});

afterEach(async () => {
  await rollbackDatabaseTransaction();
});

class DummyBankTransactionDatasource implements IDatasource<BankTransaction> {
  filterables: FilterableOptions<BankTransaction> = {
    columns: {
      payee: {
        type: 'text',
        column: 'bank_transactions.payee',
        operators: ['ts', 'eq'],
      },
      amount: {
        type: 'integer',
        column: 'bank_transactions.amount',
        operators: ['lt', 'lte', 'between', 'in', 'notIn'],
      },
      date: {
        type: 'text',
        column: 'bank_transactions.date',
        operators: ['gt', 'gte'],
      },
    },
  };

  sortables: SortableOptions<BankTransaction> = {
    columns: {
      date: { column: 'bank_transactions.date' },
    },
  };

  constructor(readonly userId: string) {
  }

  query() {
    return BankTransaction.query().where('user_id', this.userId);
  }
}

describe('lib: datasource/index', () => {
  describe('sorting', () => {
    /**
     * test
     */
    it('should return sorted results', async () => {
      const count = 10;
      const user = await factoryUser.create();
      const rows = await Promise.all(_.times(count, () => factoryBankTransaction.create({ user_id: user.id })));
      const input: IDatasourceInput = {
        sort: 'date',
      };

      const res = await buildDatasource(new DummyBankTransactionDatasource(user.id)).paginate(input);

      const unsortedIds = _.map(rows, 'id');
      const sortedIds = _.chain(rows).sortBy('date').map('id').value();
      const resultIds = _.map(res.results, 'id');

      expect(unsortedIds).not.toEqual(sortedIds);
      expect(resultIds).toEqual(sortedIds);
    });
  });

  describe('filtering', () => {
    /**
     * test
     */
    it(`should filter results with 'eq' operator`, async () => {
      const user = await factoryUser.create();
      await Promise.all(_.times(5, () => factoryBankTransaction.create({ user_id: user.id })));
      await factoryBankTransaction.create({ user_id: user.id, payee: 'findme' });

      const input: IDatasourceInput = { filter: ['payee:findme'] };
      const res = await buildDatasource(new DummyBankTransactionDatasource(user.id)).paginate( input);

      expect(res.results).toHaveLength(1);
    });

    /**
     * test
     */
    it(`should filter results with operators: lt, lte, gt, gte, eq, between, in, notIn`, async () => {
      const user = await factoryUser.create();
      const count = 5;
      const dummyRows = await Promise.all(
        _.times(count, () =>
          factoryBankTransaction.create({
            user_id: user.id,
            amount: faker.datatype.number({ min: 1001, max: 50000 }),
            date: '2019-01-01',
          }),
        ),
      );
      const row = await factoryBankTransaction.create({
        user_id: user.id,
        payee: 'testxyz123 for data',
        amount: 1000,
        date: '2020-02-01',
      });

      const notInAmounts = _.map(dummyRows, 'amount');

      const filterInputs = [
        'payee@eq:testxyz123 for data',
        'payee@ts:testxyz123',
        'date@gt:2020-01-01',
        'date@gte:2020-02-01',
        'amount@lt:1001',
        'amount@lte:1000',
        'amount@between:999,1001',
        'amount@in:100,500,1000',
        `amount@notIn:${notInAmounts.join(',')}`,
      ];

      // check all results come back
      {
        const input: IDatasourceInput = {};
        const res = await buildDatasource(new DummyBankTransactionDatasource(user.id)).paginate(input);
        expect(res.results).toHaveLength(count + 1);
      }

      for (const filterInput of filterInputs) {
        const input: IDatasourceInput = { filter: [filterInput] };
        const res = await buildDatasource(new DummyBankTransactionDatasource(user.id)).paginate(input);

        expect(res.results).toHaveLength(1);
        expect(res.results[0].id).toEqual(row.id);
      }
    });
  });

  describe('searching', () => {
    /**
     * test
     */
    it.todo('write tests');
  });

  describe('paging', () => {
    /**
     * test
     */
    it.todo('write tests');
  });
});

// import { buildDatasource, IDatasource } from './index';
// import { SearchableOptions } from './/searchable';
// import { SortableOptions } from './/sortable';
// import { FilterableOptions } from './/filterable';
// import BankTransaction from '@/src/models/BankTransaction';
//
// class ProductDatasource implements IDatasource<BankTransaction> {
//   sortables: SortableOptions<BankTransaction> = {
//     columns: {
//       id: { column: 'bank_transactions.id' },
//       date: {
//         column: 'bank_transactions.date',
//         directions: ['desc'],
//       },
//       payee: { column: 'bank_transactions.date' },
//       amount: { column: 'bank_transactions.amount' },
//     },
//   };
//
//   filterables: FilterableOptions<BankTransaction> = {
//     columns: {},
//   };
//
//   searchable: SearchableOptions<BankTransaction> = {};
//
//   constructor(readonly userId: string) {}
//
//   query() {
//     return BankTransaction.query().where('user_id', this.userId);
//   }
// }
//
// describe('lib: datasource/index', () => {
//   describe('sorting', () => {
//     /**
//      * test
//      */
//     it('should fail validation on invalid column alias', async () => {
//       const userId = '8fa8f971-9792-41b8-8273-69426dac3942';
//       const input = {
//         page: 1,
//         pageSize: 25,
//         sort: 'id,date',
//         filter: ['date@gt:2020-01-01', 'id:123'],
//       };
//       const res = await buildDatasource(new ProductDatasource(userId), input);
//       console.log(res);
//     });
//
//     /**
//      * test
//      */
//     it('should fail validation with sort direction constrained', async () => {
//       // todo
//     });
//   });
//
//   describe('filtering', () => {
//     /**
//      * test
//      */
//     it('should fail validation on invalid column alias', async () => {
//       // todo
//     });
//
//     /**
//      * test
//      */
//     it('should fail validation on invalid column operator', async () => {
//       // todo
//     });
//
//     /**
//      * test
//      */
//     it('should fail validation on invalid column operator value', async () => {
//       // todo
//     });
//   });
//
//   describe('searching', () => {
//     // todo:
//   });
//
//   describe('paging', () => {
//     /**
//      * test
//      */
//     it('should fail validation on invalid page number', async () => {
//       // todo
//     });
//
//     /**
//      * test
//      */
//     it('should fail validation on exceeding max page number', async () => {
//       // todo
//     });
//
//     /**
//      * test
//      */
//     it('should fail validation on exceeding min page number', async () => {
//       // todo
//     });
//   });
//
//   describe('page size', () => {
//     /**
//      * test
//      */
//     it('should fail validation on invalid page size', async () => {
//       // todo
//     });
//
//     /**
//      * test
//      */
//     it('should fail validation on exceeding max page size', async () => {
//       // todo
//     });
//
//     /**
//      * test
//      */
//     it('should fail validation on exceeding min page size', async () => {
//       // todo
//     });
//
//     /**
//      * test
//      */
//     it('should fail validation on missing page size step', async () => {
//       // todo
//     });
//   });
// });
