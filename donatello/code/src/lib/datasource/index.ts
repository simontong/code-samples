import { ExportableOptions, exporter } from '@/src/lib/datasource/exportable';
import { PassThrough, Stream } from 'stream';
import BaseModel from '@/src/lib/database/BaseModel';
import { BasePage } from '@/src/lib/database/BasePage';
import BaseQueryBuilder from '@/src/lib/database/BaseQueryBuilder';
import { DatabaseQueryError } from '@/src/lib/errors';
import { applyFilters, FilterableOptions, ParsedFilterInput } from './filterable';
import { applySearch, SearchableOptions } from './searchable';
import { applySorts, ParsedSortInput, SortableOptions } from './sortable';
import { paginate, PagingOptions } from './paging';
import { validate } from './validate';

export interface IDatasource<T extends BaseModel> {
  sortables?: SortableOptions<T>;
  filterables?: FilterableOptions<T>;
  searchable?: SearchableOptions<T>;
  exportable?: ExportableOptions<T>;
  paging?: PagingOptions<T>;
  query(): BaseQueryBuilder<T>;
}

export type IDatasourceInput = {
  sort?: string | string[];
  filter?: string[];
  search?: string;
  page?: number;
  pageSize?: number;
};

export type IParsedDatasourceInput = {
  sort?: ParsedSortInput[];
  filter?: ParsedFilterInput[];
  search?: string;
  page: number;
  pageSize: number;
};

type BuildDatasource<T extends BaseModel> = {
  paginate(input: IDatasourceInput): Promise<BasePage<T>>;
  exportStream(input: IDatasourceInput): Promise<PassThrough>;
};

/**
 * build datasource
 */
export function buildDatasource<T extends BaseModel>(datasource: IDatasource<T>): BuildDatasource<T> {
  return {
    paginate: paginateDatasource(datasource),
    exportStream: exportDatasourceStream(datasource),
  };
}

/**
 * paginate result
 */
function paginateDatasource<T extends BaseModel>(datasource: IDatasource<T>) {
  return async (requestInput: IDatasourceInput) => {
    const parsedInput = await validate(datasource, requestInput);
    const query = datasource.query();

    applyFilters(datasource, query, parsedInput);
    applySearch(datasource, query, parsedInput);
    applySorts(datasource, query, parsedInput);

    try {
      return await paginate(datasource, query, parsedInput);
    } catch (err) {
      throw new DatabaseQueryError(err, 'Failed to paginate datasource');
    }
  };
}

/**
 * export result
 */
function exportDatasourceStream<T extends BaseModel>(datasource: IDatasource<T>) {
  return async (requestInput: IDatasourceInput) => {
    const parsedInput = await validate(datasource, requestInput);
    const query = datasource.query();

    applyFilters(datasource, query, parsedInput);
    applySearch(datasource, query, parsedInput);
    applySorts(datasource, query, parsedInput);

    return exporter(datasource, query, parsedInput);
  };
}
