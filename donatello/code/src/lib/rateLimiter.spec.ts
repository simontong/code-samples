import { RateLimiterRedis } from 'rate-limiter-flexible';
import { makeRateLimiter } from './rateLimiter';

const mockRateLimiterRedis = (points: number) => {
  let consumedPoints = 0;

  return {
    // @ts-ignore
    consume: jest.spyOn(RateLimiterRedis.prototype, 'consume').mockImplementation(async () => {
      consumedPoints += 1;

      if (consumedPoints > points) {
        throw {
          remainingPoints: 0,
          consumedPoints,
        };
      }
    }),

    // @ts-ignore
    get: jest.spyOn(RateLimiterRedis.prototype, 'get').mockImplementation(async () => {
      return {
        consumedPoints,
        points,
      };
    }),
  };
};

describe('middleware: rateLimiter', () => {
  describe('rate limit', () => {
    /**
     * test
     */
    it('should return true if rate limit hit and false if not', async () => {
      const points = 10;
      const spies = mockRateLimiterRedis(points);
      const mockRedis = {};

      const rateLimiterByHits = makeRateLimiter({
        storeClient: mockRedis,
        points,
        duration: 5000,
      });

      const getKey = () => `byHits`;

      // hit rate limit
      for (let i = 1; i <= points; i++) {
        await expect(rateLimiterByHits(getKey())).resolves.toBeFalse();
      }
      await expect(rateLimiterByHits(getKey())).resolves.toBeTrue();
      expect(spies.consume).toBeCalledTimes(points + 1);
      expect(spies.get).toBeCalledTimes(points + 1);
    });
  });
});
