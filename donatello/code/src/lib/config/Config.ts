import { getManyProps, getManyPropsOrThrow, getProp, getPropOrThrow } from '@/src/utils/getProp';
import _ from 'lodash';
import { ConfigError } from '@/src/lib/errors';

type ConfigProps = Record<string, unknown>;

export default class Config {
  private values: ConfigProps = {};

  constructor(values: ConfigProps) {
    this.load(values);
  }

  load(values: ConfigProps): void {
    if (!_.isPlainObject(values)) {
      throw new ConfigError('Config values empty');
    }
    this.values = values;
  }

  all(): ConfigProps {
    return { ...this.values };
  }

  set(key: string, value: unknown): void {
    _.set(this.values, key, value);
  }

  unset(key: string): void {
    this.getOrThrow(key);
    _.unset(this.values, key);
  }

  /**
   * get prop
   */
  get<T>(keys: string, defaultValue?: unknown): T {
    return getProp<T>(this.values, keys, defaultValue);
  }

  /**
   * get prop or throw
   */
  getOrThrow<T>(keys: string, allowEmpty = false): T {
    return getPropOrThrow<T>(this.values, keys, allowEmpty);
  }

  /**
   * get many props
   */
  getMany<T extends Array<unknown>>(keys: string[]): T {
    return getManyProps<T>(this.values, keys);
  }

  /**
   * get many props or throw
   */
  getManyOrThrow<T extends Array<unknown>>(keys: string[], allowEmpty = false): T {
    return getManyPropsOrThrow<T>(this.values, keys, allowEmpty);
  }
}
