import Config from './Config';
import * as fc from 'fast-check';

describe('lib: config', () => {
  describe('getting config values', () => {
    /**
     * test
     */
    it('should get all config values', () => {
      const values = {
        app: { title: 'Donatello', loggerName: 'donatello' },
        database: { name: 'test_db', user: 'postgres' },
      };
      const config = new Config(values);

      expect(config.all()).toStrictEqual(values);
    });

    /**
     * test
     */
    it('should get a single config value', () => {
      const values = {
        app: { title: 'Donatello' },
      };
      const config = new Config(values);

      expect(config.get('app.title')).toEqual(values.app.title);
      expect(config.get('app')).toStrictEqual(values.app);
    });

    /**
     * test
     */
    it('should throw if value empty or missing', () => {
      const config = new Config({
        emptyKey: '',
        anotherEmpty: '',
        another: null,
        oneMore: false,
        emptyArray: [],
        emptyObject: {},
        undefinedKey: undefined,
      });

      expect(() => config.getOrThrow('emptyKey')).toThrowError(/invalid prop keys: emptyKey/i);
      expect(() => config.getOrThrow('anotherEmpty')).toThrowError(/invalid prop keys: anotherEmpty/i);
      expect(() => config.getOrThrow('another')).toThrowError(/invalid prop keys: another/i);
      expect(() => config.getOrThrow('oneMore')).toThrowError(/invalid prop keys: oneMore/i);
      expect(() => config.getOrThrow('emptyArray')).toThrowError(/invalid prop keys: emptyArray/i);
      expect(() => config.getOrThrow('emptyObject')).toThrowError(/invalid prop keys: emptyObject/i);
      expect(() => config.getOrThrow('undefined')).toThrowError(/invalid prop keys: undefined/i);
      expect(() => config.getOrThrow('randomKeyMissing')).toThrowError(/invalid prop keys: randomKeyMissing/i);
    });

    /**
     * test
     */
    it('should not throw if value empty but allowing empty values', () => {
      const config = new Config({
        emptyKey: '',
        myEmpty: 0,
        anotherEmpty: '',
        another: null,
        oneMore: false,
        emptyArray: [],
        emptyObject: {},
        undefinedKey: undefined,
      });

      expect(() => config.getOrThrow('emptyKey', true)).not.toThrowError();
      expect(() => config.getOrThrow('myEmpty', true)).not.toThrowError();
      expect(() => config.getOrThrow('anotherEmpty', true)).not.toThrowError();
      expect(() => config.getOrThrow('another', true)).not.toThrowError();
      expect(() => config.getOrThrow('oneMore', true)).not.toThrowError();
      expect(() => config.getOrThrow('emptyArray', true)).not.toThrowError();
      expect(() => config.getOrThrow('emptyObject', true)).not.toThrowError();

      expect(() => config.getOrThrow('undefined', true)).toThrowError(/invalid prop keys: undefined/i);
    });
  });

  describe('setting config values', () => {
    /**
     * test
     */
    it('should load all config values passed in constructor', () => {
      const values = {
        app: { title: 'Donatello', loggerName: 'donatello' },
        database: { name: 'test_db', user: 'postgres' },
      };
      const config = new Config(values);

      expect(config.all()).toStrictEqual(values);
    });

    /**
     * test
     */
    it('should set config value', () => {
      let key;
      let value;
      const config = new Config({});

      // primative value test
      key = 'app';
      value = 'foobar';
      config.set(key, value);
      expect(config.get(key)).toEqual(value);

      // deep key testing
      key = 'app.logger.deepKey';
      value = 'foobar';
      config.set(key, value);
      expect(config.get(key)).toEqual(value);
      expect(config.get('app')).toStrictEqual({
        logger: { deepKey: value },
      });
    });

    /**
     * test
     */
    it('should not be able to change values without using setter function', () => {
      let all;
      const values = {
        app: { title: 'Donatello', loggerName: 'donatello' },
      };
      const config = new Config(values);
      const key = 'app.title.missingKey';

      all = config.all();
      all[key] = 'test';
      expect(config.get(key)).toBeUndefined();

      all = config.all();
      expect(all).toStrictEqual(values);
    });

    /**
     * test
     */
    it('should unset config prop', () => {
      const config = new Config({});
      fc.assert(
        fc.property(fc.string(), fc.anything(), (key, value) => {
          config.set(key, value);
          expect(config.get(key)).toStrictEqual(value);
        }),
      );
    });
  });
});
