import { Logger } from 'pino';
import { RedisError } from '@/src/lib/errors';
import RedisConnection from './RedisConnection';

export const initRedisConnection = async (redis: RedisConnection, logger: Logger): Promise<void> => {
  try {
    await redis.client.ping();
  } catch (err) {
    throw new RedisError(err, 'Ping failed');
  }

  const { host, port } = redis.client.options;
  logger.debug('* Redis client ready on redis://%s:%s', host, port);
};
