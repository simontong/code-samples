import { Cradle } from '@/src/app/container';
import Redis, { RedisOptions } from 'ioredis';
import { RedisError } from '@/src/lib/errors';

export default class RedisConnection {
  readonly client: Redis.Redis;
  isConnected = false;

  constructor({ config }: Cradle) {
    const opts: RedisOptions = {
      ...(config.getOrThrow<RedisOptions>('redis')),

      // always lazy connect so we don't start up a connection till we need it
      lazyConnect: true,
    };

    this.client = new Redis(opts);
    this.attachEvents();
  }

  private attachEvents() {
    const host = this.client.options.host;
    const port = this.client.options.port;
    this.client
      .on('connect', () => {
        this.isConnected = true;
      })
      .on('ready', () => {
        // logger.debug('* Redis client ready on redis://%s:%s', host, port);
        // console.log('* Redis client connected on redis://%s:%s', host, port);
      })
      .on('disconnect', () => {
        this.isConnected = false;
      })
      .on('end', () => {
        this.isConnected = false;
        // logger.info('* Redis client disconnected');
      })
      .on('reconnecting', () => {
        // captureError(new RedisError('Redis client lost connection. Attempting reconnect to redis://%s:%s ...', host, port));
        throw new RedisError('Redis client lost connection. Attempting reconnect to redis://%s:%s ...', host, port);
      })
      .on('warning', (msg) => {
        throw new RedisError('Redis client warning: %s', msg);
        // captureError(new RedisError('Redis client warning: %s', msg));
      })
      .on('error', (err) => {
        // if max clients reached on errors then disconnect redis
        if (err.message.indexOf('max number of clients reached') === -1) {
          throw new RedisError(err, 'Redis client error');
          // captureError(new RedisError(err, 'Redis client error'));
          // return;
        }

        this.client.disconnect();
        throw new RedisError(err, 'Max number of clients reached. Disconnecting Redis client');
        // captureError(new RedisError(err, 'Max number of clients reached. Disconnecting Redis client'));
      });
  }
}
