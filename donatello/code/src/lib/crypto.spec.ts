import * as faker from 'faker';
import { decrypt, encrypt, hashPassword, verifyPassword } from './crypto';

describe('lib: crypto', () => {
  describe('hashPassword', () => {
    /**
     * test
     */
    it('should give hashed password', async () => {
      const password = 'password123-abcd';
      const hash = await hashPassword(password);

      expect(hash).not.toBeFalsy();
      expect(hash).not.toEqual(password);
    });

    /**
     * test
     */
    it('should throw on empty password', async () => {
      await expect(hashPassword('')).rejects.toThrow(/expected string `password` to not be empty/i);
    });
  });

  describe('verifyPassword', () => {
    /**
     * test
     */
    it('should verify password', async () => {
      const password = 'passy-password-123';
      const hash = await hashPassword(password);
      const verifies = await verifyPassword(hash, password);

      expect(verifies).toEqual(true);
    });

    /**
     * test
     */
    it('should throw on empty password', async () => {
      const hash = await hashPassword('my-funky-password-ABD123');
      await expect(verifyPassword(hash, '')).rejects.toThrow(/expected string `password` to not be empty/i);
    });

    /**
     * test
     */
    it('should throw on empty hash', async () => {
      const password = 'passy-passwor';
      await expect(verifyPassword('', password)).rejects.toThrow(/expected string `hash` to not be empty/i);
    });
  });

  describe('encrypt', () => {
    /**
     * test
     */
    it('should throw when string empty', () => {
      const key = faker.random.alphaNumeric(32);
      expect(() => encrypt('', key)).toThrow(/expected string `text` to not be empty/i);
    });

    /**
     * test
     */
    it('should throw when encryption key is not exactly 32 chars', () => {
      const text = faker.lorem.words();
      const key = faker.random.alphaNumeric(30);

      expect(() => encrypt(text, key)).toThrow(/expected string `encryptionKey` to have length `32`/i);
    });

    /**
     * test
     */
    it('should encrypt a string', () => {
      const text = faker.lorem.words();
      const key = faker.random.alphaNumeric(32);
      const encryptText = encrypt(text, key);

      expect(encryptText).not.toEqual(text);
    });
  });

  describe('decrypt', () => {
    /**
     * test
     */
    it('should throw when string empty', () => {
      const key = faker.random.alphaNumeric(32);
      expect(() => decrypt('', key)).toThrow(/expected string `text` to not be empty/i);
    });

    /**
     * test
     */
    it('should decrypt a string', () => {
      const text = faker.lorem.words();
      const key = faker.random.alphaNumeric(32);
      const encryptText = encrypt(text, key);
      const decryptText = decrypt(encryptText, key);

      expect(encryptText).not.toEqual(text);
      expect(decryptText).toEqual(text);
    });
  });
});
