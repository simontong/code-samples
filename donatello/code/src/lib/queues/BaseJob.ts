import { Job, JobsOptions } from 'bullmq';
import { QueueError } from '@/src/lib/errors';

export default abstract class BaseJob {
  static queueName: string;
  readonly data: unknown;
  readonly opts?: JobsOptions;

  protected constructor(data: unknown, opts?: JobsOptions) {
    this.data = data;
    this.opts = opts;
  }

  static async handle(job: Job, token?: string): Promise<void> {
    throw new QueueError('Method BaseJob.handle must be overloaded. Failed job: %s (token: %s)', job.name, token);
  }
}
