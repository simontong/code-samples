import container from '@/src/app/container';
import _ from 'lodash';
import { Job, Worker } from 'bullmq';
import QueueManager from './QueueManager';
import { QueueError } from '@/src/lib/errors';
import BaseJob from './BaseJob';

const getQueueManagerOrThrow = (queueManagers: QueueManager[], name: string): QueueManager => {
  const qm: QueueManager | undefined = _.find(queueManagers, ['name', name]);
  if (!qm) {
    throw new QueueError('Queue missing %s', name);
  }
  return qm;
};

const getJobConstructorOrThrow = (jobs: typeof BaseJob[], name: string): typeof BaseJob => {
  const JC: typeof BaseJob | undefined = _.find(jobs, ['name', name]);
  if (!JC) {
    throw new QueueError('Job class missing %s', name);
  }
  return JC;
};

export const makeDispatcher = (queueManagers: QueueManager[]) => {
  return async (job: BaseJob): Promise<Job> => {
    const JC = job.constructor as typeof BaseJob;
    const queueName = JC.queueName;
    const jobName = job.constructor.name;

    // make sure job has handler
    if (typeof JC.handle !== 'function') {
      throw new QueueError("Job '%s' missing static handle() method", jobName);
    }

    const qm = getQueueManagerOrThrow(queueManagers, queueName);
    const queue = qm.queue();
    await queue.waitUntilReady();

    const newJob = await queue.add(jobName, job.data, job.opts);
    container.cradle.logger.info('Job dispatched: %s (id: %n)', newJob.name, newJob.id);

    return newJob;
  };
};

export const makeProcessor = (queueManagers: QueueManager[], jobs: typeof BaseJob[]) => {
  return async (queueName: string): Promise<Worker> => {
    const processor = async (job: Job, token?: string): Promise<void> => {
      const jobName = job.name;
      const JC = getJobConstructorOrThrow(jobs, jobName);
      await JC.handle(job, token);
    };

    const qm = getQueueManagerOrThrow(queueManagers, queueName);
    const worker = qm.createWorker(processor);
    await worker.waitUntilReady();

    return worker;
  };
};
