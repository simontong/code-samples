import container from '@/src/app/container';
import _ from 'lodash';
import exitHook from 'exit-hook';
import QueueManager, { QueueManagerOpts } from './QueueManager';
import * as jobs from '@/src/jobs';
import { makeDispatcher, makeProcessor } from './utils';

const opts = container.cradle.config.getOrThrow<QueueManagerOpts>('queues');

const jobsArr = _.values(jobs);

// create queue managers based on queues in Job.queueName
export const queueManagers = _.uniqBy(jobsArr, 'queueName').map((job) => {
  return new QueueManager(job.queueName, opts);
});

export const dispatchJob = makeDispatcher(queueManagers);
export const processQueue = makeProcessor(queueManagers, jobsArr);

// destroy connections on exit
exitHook(async () => {
  const openHandles = queueManagers.filter((qm) => qm.hasOpenHandles());
  if (!openHandles.length) {
    return;
  }

  // close all queue handles
  await Promise.all(
    openHandles.map((qm) => {
      container.cradle.logger.debug('* Closing open job queue: %s ...', qm.name);
      return qm.close();
    }),
  );
});
