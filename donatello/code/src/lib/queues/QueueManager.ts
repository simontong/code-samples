import {
  Processor,
  Queue,
  QueueEvents,
  QueueEventsOptions,
  QueueOptions,
  QueueScheduler,
  QueueSchedulerOptions,
  Worker,
  WorkerOptions,
} from 'bullmq';

export type QueueManagerOpts = {
  queue: QueueOptions;
  scheduler: QueueSchedulerOptions;
  events: QueueEventsOptions;
  worker: WorkerOptions;
};

export default class QueueManager {
  readonly name: string;
  readonly opts: QueueManagerOpts;

  private _queue?: Queue;
  private _scheduler?: QueueScheduler;
  private _events?: QueueEvents;
  private _workers: Worker[] = [];

  constructor(name: string, opts: QueueManagerOpts) {
    this.name = name;
    this.opts = opts;
  }

  queue(): Queue {
    if (!this._queue) this._queue = new Queue(this.name, this.opts?.queue);
    return this._queue;
  }

  scheduler(): QueueScheduler {
    if (!this._scheduler) this._scheduler = new QueueScheduler(this.name, this.opts?.scheduler);
    return this._scheduler;
  }

  events(): QueueEvents {
    if (!this._events) this._events = new QueueEvents(this.name, this.opts?.events);
    return this._events;
  }

  createWorker(processor: Processor): Worker {
    const worker = new Worker(this.name, processor, this.opts?.worker);
    this._workers.push(worker);

    return worker;
  }

  hasOpenHandles(): boolean {
    return !!this._queue || !!this._scheduler || !!this._events || !!this._workers.length;
  }

  async close(): Promise<Array<unknown>> {
    // prettier-ignore
    return Promise.all([
      this._queue?.close(),
      this._scheduler?.close(),
      this._events?.close(),
    ]);
  }
}
