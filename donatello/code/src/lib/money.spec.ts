// todo: write tests
describe('lib: money', () => {
  it.todo('write tests');
});

// // bulk-create { USD, BHD } from '@dinero.js/currencies';
// bulk-create { normalizeCurrency } from '@/src/lib/money';
// // bulk-create { Currency, dinero, toUnit } from 'dinero.js';
//
// const validAmounts: [string | number, number][] = [
//   // thousand = comma, decimal = dot
//   [USD, '252,240,035.92', 25224003592],
//   [BHD, '25,279,475.352', 25279475352],
//   [USD, '708,836.68', 70883668],
//   [BHD, '893,005.028', 893005028],
//
//   // thousand = dot, decimal = comma
//   [USD, '334.266.419,76', 33426641976],
//   [BHD, '17.418.625,718', 17418625718],
//   [USD, '284.251,16', 28425116],
//   [BHD, '62.043,034', 62043034],
//
//   // thousand = comma, decimal = none
//   [USD, '34,895,737,284', 3489573728400],
//   [USD, '20,234', 2023400],
//   [USD, '5,620', 562000],
//
//   // thousand = dot, decimal = none
//   [BHD, '85.178.257.246', 85178257246000],
//   [BHD, '296.248', 296248000], // todo: how should we know the dot is a thousand separator?
//   [BHD, '1.182', 1182000], // todo: how should we know the dot is a thousand separator?
//
//   // thousand = none, decimal = dot
//   [USD, 10568641.36, 1056864136],
//   [BHD, 98783139.792, 98783139792],
//   [USD, 0.15, 15],
//   [BHD, 0.15, 150],
//
//   // thousand = none, decimal = comma
//   [BHD, '572408030,98', 572408030980],
//   [BHD, '37753554,876', 37753554876],
//
//   // thousand = none, decimal = none
//   [USD, 48027363740, 4802736374000],
//   [BHD, 48027363740, 48027363740000],
//   [USD, 0, 0],
// ];
//
// const invalidAmounts: [Currency<number>, string][] = [
//   [USD, '1.1.1'],
//   [USD, '1,1,1'],
//   [USD, '1,1.1'],
//   [USD, '1.1,1'],
//
//   [USD, '1.11.1'],
//   [USD, '1,11,1'],
//   [USD, '1,11.1'],
//   [USD, '1.11,1'],
// ];
//
// describe('lib: money', () => {
//   describe('normalizeCurrency', () => {
//     it.each(validAmounts)('should normalize currency', (currency, amount, expected) => {
//       const newAmount = normalizeCurrency(amount);
//       // const amountDecimal = dinero({ amount: expected, currency });
//       // expect(newAmount).toEqual(toUnit(amountDecimal));
//     });
//
//     it.each(invalidAmounts)('should throw on invalid amount', (currency, amount) => {
//       expect(() => normalizeCurrency(amount)).toThrow(/invalid amount provided/i);
//     });
//   });
//
//   // describe('toMinorCurrencyUnit', () => {
//   //   it.each(validAmounts)('should convert valid amount', (currency, amount, expected) => {
//   //     const newAmount = toMinorCurrencyUnit(amount, currency);
//   //     expect(newAmount).toEqual(expected);
//   //   });
//   //
//   //   const roundAmounts: [Currency<number>, string, number][] = [
//   //     [USD, '123,123.123', 12312312],
//   //     [BHD, '123,123.1234', 123123123],
//   //   ];
//   //   it.each(roundAmounts)('should round decimal amounts', (currency, amount, expected) => {
//   //     const newAmount = toMinorCurrencyUnit(amount, currency);
//   //     expect(newAmount).toEqual(expected);
//   //   });
//   // });
//   //
//   // describe('toMajorCurrencyUnit', () => {
//   //   it.each(validAmounts)('should convert valid amount', (currency, expected, amount) => {
//   //     const decimalAmount = normalizeCurrency(expected);
//   //     const newAmount = toMajorCurrencyUnit(amount, currency);
//   //     expect(newAmount).toEqual(decimalAmount);
//   //   });
//   // });
// });
