// todo: write tests
describe('lib: cache/Cache', () => {
  describe('setting cache', () => {
    it.todo('should automatically stringify objects');
    it.todo('should set cache and expire');
    it.todo('should set cache indefinitely');
    it.todo('should remember result of a function and return cached');
  });

  describe('getting cache', () => {
    it.todo('should automatically parse stringified objects');
  });
});
