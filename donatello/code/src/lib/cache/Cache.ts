import { Cradle } from '@/src/app/container';
import RedisConnection from '@/src/lib/redis/RedisConnection';

type RememberCallback<T> = () => Promise<T>;

export default class Cache {
  private redis: RedisConnection;

  constructor({ redis }: Cradle) {
    this.redis = redis;
  }

  /**
   * remember cached item
   */
  async remember<T>(key: string, ttl: number, callback: RememberCallback<T>): Promise<T> {
    // return cached value
    const cachedValue = (await this.get(key)) as T;
    if (cachedValue !== null) {
      return cachedValue;
    }

    // get new value and store
    const value = await callback();
    await this.set(key, ttl, value);

    return value;
  }

  /**
   * set item
   */
  async set(key: string, ttl: number, value: unknown): Promise<void> {
    const json = JSON.stringify(value);
    await this.redis.client.set(key, json, 'ex', ttl);
  }

  /**
   * get cached item
   */
  async get(key: string): Promise<unknown> {
    let value = await this.redis.client.get(key);

    if (value !== null) {
      value = JSON.parse(value);
    }

    return value;
  }
}
