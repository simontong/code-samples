import {
  canReuseExpiryToken,
  createExpiryToken,
  EXPIRY_TOKEN_REGEXP,
  getTokenExpiry,
  hasTokenExpired,
} from './expiryToken';
import { DateTime } from 'luxon';
import validator from 'validator';

describe('lib: expiryToken', () => {
  describe('createExpiryToken', () => {
    /**
     * test
     */
    it('should create expiry token', () => {
      const token = createExpiryToken();
      expect(token).toMatch(EXPIRY_TOKEN_REGEXP);

      const [, uuid, created] = token.match(EXPIRY_TOKEN_REGEXP) || [];
      expect(validator.isUUID(uuid)).toBeTrue();
      expect(Number(created)).not.toBeNaN();
    });
  });

  describe('getTokenExpiry', () => {
    /**
     * test
     */
    it('should parse out token expiry from token', () => {
      const token = createExpiryToken();
      const expiresAfterMs = 1000 * 60 * 60; // 1 hour
      const [, , created] = token.match(EXPIRY_TOKEN_REGEXP) || [];
      const tokenExpires = getTokenExpiry(token, expiresAfterMs);

      expect(tokenExpires).toBeInstanceOf(DateTime);
      expect(tokenExpires.toMillis()).toEqual(Number(created) + expiresAfterMs);
    });
  });

  describe('hasTokenExpired', () => {
    /**
     * test
     */
    it('should return false on token that has not expired yet', () => {
      const token = createExpiryToken();
      const expiresAfterMs = 1000 * 60 * 60 * 24;

      expect(hasTokenExpired(token, expiresAfterMs)).toBeFalsy();
    });

    /**
     * test
     */
    it('should return true on expired token', () => {
      jest.useFakeTimers();
      jest.setSystemTime(100);
      const token = createExpiryToken();
      jest.useRealTimers();
      const expiresAfterMs = 1000 * 60 * 60 * 24;

      expect(hasTokenExpired(token, expiresAfterMs)).toBeTrue();
    });
  });

  describe('canReuseExpiryToken', () => {
    /**
     * test
     */
    it('should return true if token has enough time left before expiry', () => {
      const token = createExpiryToken();
      const expiresAfterMs = 1000 * 60 * 60 * 24;

      expect(canReuseExpiryToken(token, expiresAfterMs)).toBeTrue();
    });

    /**
     * test
     */
    it('should return false if token is going to expire soon', () => {
      const expiresAfterMs = 1000 * 60 * 60 * 24;
      const thresholdMs = 1000 * 60 * 60 * 2;

      // set time to 10ms over threshold, create token, then set time back
      const time = Date.now() - (expiresAfterMs - thresholdMs + 10);

      jest.useFakeTimers();
      jest.setSystemTime(time);
      const token = createExpiryToken();
      jest.useRealTimers();

      expect(canReuseExpiryToken(token, expiresAfterMs, thresholdMs)).toBeFalsy();
    });
  });
});
