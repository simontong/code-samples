import fs from 'fs';
import Mustache from 'mustache';
import { projectRoot } from '@/src/lib/paths';

export const template = (viewFile: string, params = {}): string => {
  const file = projectRoot('resources/views', viewFile);
  const output = fs.readFileSync(file, 'utf8');

  return Mustache.render(output, params);
};
