import container from '@/src/app/container';
import nodemailer from 'nodemailer';
import mg from 'nodemailer-mailgun-transport';

const mailerOpts = container.cradle.config.getOrThrow('mailer');

// @ts-ignore
let opts = mailerOpts.mailers[mailerOpts.mailer];

// @ts-ignore
if (mailerOpts.mailer === 'mailgun') {
  opts = mg(opts);
}

const transporter = nodemailer.createTransport(opts);

export default transporter;
