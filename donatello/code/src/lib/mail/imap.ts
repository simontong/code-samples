import Imap, { Box } from 'imap';
import EventEmitter from 'events';
import { ParsedMail, simpleParser } from 'mailparser';

const connect = async (config: Imap.Config): Promise<ImapExtended> => {
  return new Promise((resolve, reject) => {
    const imap = new Imap(config);

    const onReady = () => {
      const imape = new ImapExtended(imap);
      resolve(imape);
    };

    const onError = (err: Error) => {
      reject(err);
    };

    const onEnd = () => {};

    const onClose = () => {};

    imap.once('ready', onReady);
    imap.once('error', onError);
    imap.once('end', onEnd);
    imap.once('close', onClose);
    imap.connect();
  });
};

export class ImapExtended extends EventEmitter {
  private imap: Imap;

  constructor(imap: Imap) {
    super();
    this.imap = imap;

    // bind events emitted from Imap library to this class
    const events = ['ready', 'alert', 'mail', 'expunge', 'uidvalidity', 'update', 'error', 'close', 'end'];
    for (const event of events) {
      this.imap.on(event, this.emit.bind(this, event));
    }
  }

  async end(): Promise<void> {
    return new Promise((resolve) => {
      this.imap.once('end', () => resolve());
      this.imap.end();
    });
  }

  async openBox(mailboxName: string): Promise<Box> {
    return new Promise((resolve, reject) => {
      this.imap.openBox(mailboxName, true, (err, box) => {
        if (err) {
          reject(err);
          return;
        }
        resolve(box);
      });
    });
  }

  async closeBox(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.imap.closeBox((err) => {
        if (err) {
          reject(err);
          return;
        }
        resolve();
      });
    });
  }

  async fetchMessages(source: string): Promise<Array<ParsedMail>> {
    return new Promise((resolve, reject) => {
      const emails: Array<string> = [];

      const f = this.imap.seq.fetch(source, {
        bodies: '',
        struct: true,
      });

      f.once('error', (err) => {
        reject(err);
      });

      f.on('message', (msg) => {
        let buffer = '';

        msg.on('body', async (stream) => {
          stream.on('data', (chunk) => {
            buffer += chunk.toString();
          });
        });

        msg.once('end', function () {
          emails.push(buffer);
        });
      });

      f.once('end', async (err: Error) => {
        if (err) {
          reject(err);
          return;
        }

        // prettier-ignore
        const emailsParsed = await Promise.all(
          emails.map(input => simpleParser(input)),
        );

        resolve(emailsParsed);
      });
    });
  }
}

export default {
  connect,
};
