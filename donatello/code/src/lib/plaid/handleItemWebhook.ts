import container from '@/src/app/container';
import { PlaidWebhookItemRequestDto } from '@/src/dtos/PlaidWebhookItemRequestDto';
import { PlaidWebhookResponseDto } from '@/src/dtos/PlaidWebhookResponseDto';
import { PlaidWebhookError } from '@/src/lib/errors';
import { captureError } from '@/src/lib/errors/errorHandling';

export default async function handleItemWebhook(input: PlaidWebhookItemRequestDto): Promise<PlaidWebhookResponseDto> {
  const {
    webhook_code: webhookCode,
    item_id: itemId,
    error,
  } = input;

  switch (webhookCode) {
    // item has entered an error state
    case 'ERROR': {
      const info = { input };
      captureError(new PlaidWebhookError('Item webhook failed').setInfo(info));

      return {
        pushData: {
          eventName: 'plaidItemWebhook.error',
          data: {
            errorCode: error?.error_code,
          },
        },
      };
    }

    // item access is about to expire
    case 'PENDING_EXPIRATION': {
      // const { id: itemId } = await retrieveItemByPlaidItemId(plaidItemId);
      // await updateItemStatus(itemId, 'bad');
      // serverLogAndEmitSocket(
      //   `user needs to re-enter login credentials`,
      //   itemId,
      //   error
      // );

      return {
        pushData: {
          eventName: 'plaidItemWebhook.error',
          data: {
            errorCode: error?.error_code,
          },
        },
      };
    }

    // the user has revoked access to an item
    // case 'USER_PERMISSION_REVOKED': {
    //   return {};
    // }

    // item webhook url updated
    // case 'WEBHOOK_UPDATE_ACKNOWLEDGED': {
    //   return {};
    // }
  }

  const info = { input };
  throw new PlaidWebhookError('Unhandled webhook code: %s', input.webhook_code).setInfo(info);
}
