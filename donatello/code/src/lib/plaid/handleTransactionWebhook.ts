import { PlaidWebhookTransactionRequestDto } from '@/src/dtos/PlaidWebhookTransactionRequestDto';
import { PlaidWebhookError } from '@/src/lib/errors';
import { PlaidWebhookResponseDto } from '@/src/dtos/PlaidWebhookResponseDto';

export default async function handleTransactionsWebhook(input: PlaidWebhookTransactionRequestDto): Promise<PlaidWebhookResponseDto> {
  const {
    webhook_code: webhookCode,
    item_id: itemId,
    error,
    new_transactions: newTransactions,
    removed_transactions: removedTransactions,
  } = input;

  switch (input.webhook_code) {
    // initial transactions ready
    case 'INITIAL_UPDATE': {
      return {};
    }

    // historical transactions ready
    case 'HISTORICAL_UPDATE': {
      return {};
    }

    // new transactions available
    case 'DEFAULT_UPDATE': {
      return {};
    }

    // deleted transactions detected
    case 'TRANSACTIONS_REMOVED': {
      return {};
    }
  }

  const info = { input };
  throw new PlaidWebhookError('Unhandled webhook code: %s', input.webhook_code).setInfo(info);
}
