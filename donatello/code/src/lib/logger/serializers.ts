import { DatabaseQueryError } from '@/src/lib/errors';
import { Job, JobsOptions } from 'bullmq';
import { unwindStack, unwindInfo } from '@/src/lib/errors/utils';

export type ErrSerializer = {
  type: string;
  message: string;
  stack: string;
  info?: Record<string, unknown>;
};

export type QueueSerializer = {
  state?: string;
  id?: string;
  name: string;
  data: string;
  attempts: number;
  opts: JobsOptions;
  ts: number;
};

// we need to resolve the promise before passing in the job
// this type assumes that has happened
type JobWithStateResolved = Job & {
  state?: string;
};

// eslint-disable-next-line @typescript-eslint/ban-types
export const errSerializer = (input: unknown | Error): ErrSerializer | unknown => {
  let type = '';
  let message;
  let stack = '';
  let info;

  if (input instanceof Error) {
    type = input?.name;
    message = input?.message;
    stack = unwindStack(input) || '';
    info = unwindInfo(input);
  } else {
    message = JSON.stringify(input);
    stack = message;
  }

  return {
    type,
    message,
    stack,
    info,
    // raw: input,
  };
};

export const jobSerializer = (job: JobWithStateResolved): QueueSerializer => {
  return {
    id: job.id,
    name: job.name,
    data: job.data,
    attempts: job.attemptsMade,
    opts: job.opts,
    ts: job.timestamp,
  };
};
