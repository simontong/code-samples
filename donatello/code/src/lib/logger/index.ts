import { Cradle } from '@/src/app/container';
import pino, { Logger } from 'pino';
import pinoms, { Streams } from 'pino-multi-stream';
import fs from 'fs';
import path from 'path';
import { errSerializer, jobSerializer } from './serializers';
import { errPrettifier, jobPrettifier } from './prettifiers';

export default function logger({ config }: Cradle): Logger {
  const opts: pino.LoggerOptions = {
    name: config.get('app.id'),
    // this MUST be set at the lowest level of desired stream destinations
    level: 'trace',

    serializers: {
      ...pino.stdSerializers,
      error: errSerializer,
      err: errSerializer,
      job: jobSerializer,
    },
  };

  const streams: Streams = [];

  const logDir = config.getOrThrow<string>('logger.logDir');

  // create log dir if missing
  if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
  }

  // trace file
  streams.push({
    level: 'trace',
    stream: fs.createWriteStream(path.join(logDir, 'trace.log'), { flags: 'a' }),
  });

  // error file
  streams.push({
    level: 'error',
    stream: fs.createWriteStream(path.join(logDir, 'error.log'), { flags: 'a' }),
  });

  // pretty stdout
  streams.push({
    // @ts-ignore
    level: config.get<string>('logger.stdoutLevel', 'trace'),
    stream: pinoms.prettyStream({
      prettyPrint: {
        customPrettifiers: {
          // @ts-ignore
          error: errPrettifier,
          // @ts-ignore
          err: errPrettifier,
          // @ts-ignore
          job: jobPrettifier,
        },
        colorize: true,
        ignore: 'name,hostname,pid',
        translateTime: 'SYS:standard',
      },
    }),
  });

  const logger = pino(opts, pinoms.multistream(streams));
  return logger;
}
