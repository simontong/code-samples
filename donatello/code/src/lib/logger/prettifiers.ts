import chalk from 'chalk';
import util from 'util';
import { projectRoot } from '@/src/lib/paths';
import { ErrSerializer, QueueSerializer } from './serializers';

export const errPrettifier = (input: ErrSerializer): string => {
  const text = [];

  const lines = input.stack.split('\n').map((line, n) => {
    let txt = line;

    // if error message line in stack
    if (n === 0 || /^caused by/i.test(line)) {
      // txt = chalk.blue(txt);
    } else if (!line.includes(projectRoot()) || line.includes(projectRoot('node_modules'))) {
      txt = chalk.gray(txt);
    }

    // shorten paths
    txt = txt.replace(new RegExp(projectRoot(), 'g'), '.');

    return txt;
  });

  text.push(lines.join('\n'));

  if (input.info) {
    text.push('info: ' + util.inspect(input.info, { colors: true, compact: false, depth: null }));
  }

  return text.join('\n');
};

export const jobPrettifier = (input: QueueSerializer): string => {
  return util.inspect(input, { colors: true, compact: true, depth: null });
};
