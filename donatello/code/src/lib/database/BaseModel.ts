import { srcRoot } from '@/src/lib/paths';
import { Model, compose } from 'objection';
import visibilityPlugin from 'objection-visibility';
import BaseQueryBuilder from './BaseQueryBuilder';

// prettier-ignore
const mixins = compose(
  visibilityPlugin,
);

// @ts-ignore
export default class BaseModel extends mixins(Model) {
  // path to models so we can avoid cyclical dependency issues
  static modelPaths = [srcRoot('/models')];

  // enable auto update timestamps for all models
  static timestamp = true;

  // custom query builder
  static QueryBuilder = BaseQueryBuilder;
  QueryBuilderType!: BaseQueryBuilder<this>;
}