import BaseModel from '@/src/lib/database/BaseModel';

export interface BasePage<M extends BaseModel> {
  results: M[];
  total: number;
  currentPage: number;
  lastPage: number;
}
