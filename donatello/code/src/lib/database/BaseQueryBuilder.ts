import { PageQueryBuilder, QueryBuilder } from 'objection';
import BaseModel from '@/src/lib/database/BaseModel';
import { BasePage } from '@/src/lib/database/BasePage';

export default class BaseQueryBuilder<M extends BaseModel, R = M[]> extends QueryBuilder<M, R> {
  ArrayQueryBuilderType!: BaseQueryBuilder<M, M[]>;
  SingleQueryBuilderType!: BaseQueryBuilder<M, M>;
  NumberQueryBuilderType!: BaseQueryBuilder<M, number>;
  PageQueryBuilderType!: BaseQueryBuilder<M, BasePage<M>>;

  /**
   * add extra params to page results
   */
  page(page: number, pageSize: number): PageQueryBuilder<this> {
    return super.page(page, pageSize).runAfter(({ results, total }) => {
      return {
        results,
        currentPage: page + 1,
        lastPage: Math.ceil(total / pageSize),
        total,
      };
    });
  }
}
