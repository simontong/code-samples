import KnexClient, { Knex } from 'knex';
import exitHook from 'exit-hook';
import { Model } from 'objection';
import { isDatabaseConnectionLive } from '@/src/lib/database/utils';
import { types } from 'pg';
import { DateTime } from 'luxon';
import { Cradle } from '@/src/app/container';

export default function database({ config, logger }: Cradle): Knex {
  // init database client
  const opts = config.getOrThrow<Knex.Config>('database');
  const knex = KnexClient(opts);

  // use objection
  Model.knex(knex);

  // destroy connection on exit
  exitHook(async () => {
    if (!isDatabaseConnectionLive(knex)) {
      return;
    }

    const clientConfig = knex.client.config;
    const { host, port, database } = clientConfig.connection;
    logger.debug('* Disconnecting from database %s://%s:%s/%s ...', clientConfig.client, host, port, database);
    await knex.destroy();
  });

  return knex;
}

// set for dates:
//
// retain dates as strings rather than interpretting as Date. We also add padding on the end
// because it seems when postgres sends back the date it truncates zeros off the end of
// the milliseconds. eg. '2020-01-01 01:01:01.080' -> '2020-01-01 01:01:01.08'. the 23 number
// is the length of the entire timestamp.
// const parseDate = (val: string) => val.padEnd(23, '0');
const parseDate = (val: string) => (val === null ? null : DateTime.fromSQL(val, { zone: 'UTC' }).toISO());
types.setTypeParser(types.builtins.DATE, parseDate);
types.setTypeParser(types.builtins.TIMESTAMPTZ, parseDate);
types.setTypeParser(types.builtins.TIMESTAMP, parseDate);

// set for integers:
types.setTypeParser(types.builtins.INT8, 'text', parseInt);
