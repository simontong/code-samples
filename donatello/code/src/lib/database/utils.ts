import ExtendedError from '@/src/lib/errors/ExtendedError';
import _ from 'lodash';
import { Knex } from 'knex';
import { Logger } from 'pino';
import { DatabaseError } from '@/src/lib/errors';

export type NativeErrorProps = {
  length: number;
  name: string;
  severity: string;
  code: string;
  detail: string;
  hint: string;
  position: string;
  internalPosition: string;
  internalQuery: string;
  where: string;
  schema: string;
  table: string;
  column: string;
  dataType: string;
  constraint: string;
  file: string;
  line: number;
  routine: string;
  // stack: string;
  message: string;
};

export const initDatabaseConnection = async (knex: Knex, logger: Logger): Promise<void> => {
  try {
    await knex.raw('select 1 as testConnection');
  } catch (err) {
    throw new DatabaseError(err, 'Connection test failed');
  }

  const clientConfig = knex.client.config;
  const { host, port, database } = clientConfig.connection;
  logger.debug('* Database ready on %s://%s:%s/%s', clientConfig.client, host, port, database);
};

export const isDatabaseConnectionLive = (knex: Knex): boolean => {
  // check if any pools are awaiting connections
  const pool = _.at(knex.client.pool, [
    'used.length',
    'free.length',
    'pendingCreates.length',
    'pendingAcquires.length',
    'pendingDestroys.length',
    'pendingValidations.length',
  ]);

  const isConnected = !!_.sum(pool);
  return isConnected;
};

/**
 *  extract native error props for passing on errors
 */
export const nativeErrorProps = (err: Error): NativeErrorProps | undefined => {
  const nativeError = _.get(err, 'nativeError');
  if (!nativeError) {
    return;
  }

  // prettier-ignore
  const props = _.pick(nativeError, [
    'length',
    'name',
    'severity',
    'code',
    'detail',
    'hint',
    'position',
    'internalPosition',
    'internalQuery',
    'where',
    'schema',
    'table',
    'column',
    'dataType',
    'constraint',
    'file',
    'line',
    'routine',
    // 'stack',
    'message',
  ]);

  return props;
};

/**
 * check if this is an invalid uuid error
 * @param err
 */
export const isInvalidUuidError = (err: ExtendedError): boolean => {
  const nativeError: NativeErrorProps = _.get(err, 'info.nativeError');
  if (!_.isPlainObject(nativeError)) {
    return false;
  }

  return (
    nativeError.code === '22P02' &&
    nativeError.routine === 'string_to_uuid' &&
    /invalid input syntax for type uuid:/i.test(err.message)
  );
};
