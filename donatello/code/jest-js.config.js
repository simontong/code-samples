// eslint-disable-next-line no-undef
module.exports = {
  verbose: false,
  restoreMocks: true,
  rootDir: './dist',
  testEnvironment: 'node',
  setupFiles: ['<rootDir>/test/setupFile.js'],
  setupFilesAfterEnv: ['<rootDir>/test/setupFileAfterEnv.js'],
};
