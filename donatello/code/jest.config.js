// eslint-disable-next-line no-undef
module.exports = {
  verbose: false,
  restoreMocks: true,
  rootDir: './',
  testEnvironment: 'node',
  preset: 'ts-jest',
  setupFiles: ['<rootDir>/test/setupFile.ts'],
  setupFilesAfterEnv: ['<rootDir>/test/setupFileAfterEnv.ts'],
  modulePathIgnorePatterns: ['<rootDir>/dist'],
  globals: {
    'ts-jest': {
      isolatedModules: true,
    },
  },
  moduleNameMapper: {
    '^@/(.+)$': '<rootDir>/$1',
  },
};
