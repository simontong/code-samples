import { Knex } from 'knex';
import { applyTimestamps, applyUpdateTrigger } from '@/database/migrations/0_initial';

const tableName = 'bank_accounts';

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable(tableName, function (t) {
    t.uuid('id').primary().defaultTo(knex.raw('gen_random_uuid()'));
    t.uuid('user_id').unsigned().notNullable();
    t.uuid('bank_institution_id').unsigned();
    t.string('name').notNullable();
    // t.bigInteger('balance');
    applyTimestamps(knex, t);

    t.foreign('user_id').references('id').inTable('users');
    t.foreign('bank_institution_id').references('id').inTable('bank_institutions');
  });

  await applyUpdateTrigger(knex, tableName);
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable(tableName);
}
