import { applyTimestamps, applyUpdateTrigger } from '@/database/migrations/0_initial';
import { Knex } from 'knex';

const tableName = 'plaid_accounts';

// plaid_accounts
// id
// plaid_item_id                     - fk to plaid_items
// plaid_account_eid                 - plaid external id
// mask
// name
// official_name
// type
//   subtype
// balance_available
// balance_current
// balance_limit
// iso_currency_code
// unofficial_currency_code
// created_at
// updated_at

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable(tableName, function(t) {
    t.uuid('id').primary().defaultTo(knex.raw('gen_random_uuid()'));
    t.uuid('user_id').unsigned().notNullable();
    t.uuid('plaid_item_id').unsigned().notNullable();
    t.text('account_eid').notNullable();
    t.text('mask').notNullable();
    t.text('name').notNullable();
    t.text('official_name').notNullable();
    t.text('type').notNullable();
    t.text('subtype').notNullable();
    t.decimal('balance_available', 28, 10);
    t.decimal('balance_current', 28, 10);
    t.decimal('balance_limit', 28, 10);
    t.text('iso_currency_code');
    t.text('unofficial_currency_code');
    applyTimestamps(knex, t);

    t.foreign('user_id').references('id').inTable('users');
    t.foreign('plaid_item_id').references('id').inTable('plaid_items');
  });

  await applyUpdateTrigger(knex, tableName);
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable(tableName);
}
