import { Knex } from 'knex';
import { applyTimestamps, applyUpdateTrigger } from '@/database/migrations/0_initial';

const tableName = 'bank_transactions';

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable(tableName, function(t) {
    t.uuid('id').primary().defaultTo(knex.raw('gen_random_uuid()'));
    t.uuid('user_id').unsigned().notNullable();
    t.uuid('bank_account_id').unsigned();
    t.uuid('bank_category_id').unsigned();
    // t.uuid('currency_code').unsigned().notNullable();
    t.string('currency_code').notNullable();
    // t.specificType('currency_code', 'citext').notNullable();
    t.dateTime('date', { precision: 3 }).notNullable();
    t.string('payee').notNullable();
    // t.decimal('amount', 18, 0).notNullable();
    t.bigInteger('amount').notNullable();
    t.text('note');
    applyTimestamps(knex, t);

    t.foreign('user_id').references('id').inTable('users');
    t.foreign('bank_account_id').references('id').inTable('bank_accounts');
    t.foreign('bank_category_id').references('id').inTable('bank_categories');
    t.foreign('currency_code').references('code').inTable('currencies');
  });

  await applyUpdateTrigger(knex, tableName);
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable(tableName);
}
