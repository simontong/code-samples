import { Knex } from 'knex';
import { applyTimestamps, applyUpdateTrigger } from '@/database/migrations/0_initial';

const tableName = 'users';

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable(tableName, function(t) {
    t.uuid('id').primary().defaultTo(knex.raw('gen_random_uuid()'));
    t.string('name').notNullable();
    t.string('email').notNullable().unique('email');
    t.string('new_email');
    t.specificType('email_confirm_token', 'char(50)');
    t.text('password').notNullable();
    t.specificType('password_reset_token', 'char(50)');
    applyTimestamps(knex, t);
  });

  await applyUpdateTrigger(knex, tableName);
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable(tableName);
}
