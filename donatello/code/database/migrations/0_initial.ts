import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  // await knex.raw('create extension if not exists "uuid-ossp"');
  await knex.raw('create extension if not exists "citext"');

  await knex.raw(`
    create or replace function trigger_set_timestamp()
    returns trigger as $$
    begin
      new.updated_at = now();
      return new;
    end;
    $$ language plpgsql;
  `);
}

export async function down(knex: Knex): Promise<void> {
  // await knex.raw('drop extension if not exists "uuid-ossp"');
  await knex.raw('drop extension if exists "citext"');

  await knex.raw('drop function if exists trigger_set_timestamp()');
}

/**
 * apply update trigger to tables
 */
export async function applyUpdateTrigger(knex: Knex, tableName: string): Promise<void> {
  await knex.raw(`
    create trigger ${tableName}_updated_at_timestamp
    before update on ??
    for each row
    execute procedure trigger_set_timestamp();
  `, tableName);
}

/**
 * apply timestamps
 * @param knex
 * @param t
 */
export function applyTimestamps(knex: Knex, t: Knex.CreateTableBuilder): void {
  t.dateTime('created_at', { precision: 3 }).defaultTo(knex.fn.now());
  t.dateTime('updated_at', { precision: 3 }).defaultTo(knex.fn.now());
}
