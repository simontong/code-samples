import { applyTimestamps, applyUpdateTrigger } from '@/database/migrations/0_initial';
import { Knex } from 'knex';

const tableName = 'plaid_transactions';

// plaid_transactions
// id
// plaid_account_id                  - fk to plaid_bank_accounts
// plaid_transaction_eid                  - plaid external id
// plaid_pending_transaction_eid          - plaid external id
// plaid_category_eid                     - plaid external id
// category                         - json
// location                         - json
// payment_meta                     - json
// account_owner
// name
// merchant_name
// original_description
// amount
// iso_currency_code
// unofficial_currency_code
// pending
// payment_channel
// date
// datetime
// authorized_date
// authorized_datetime
// transaction_code
// transaction_type
// created_at
// updated_at

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable(tableName, function(t) {
    t.uuid('id').primary().defaultTo(knex.raw('gen_random_uuid()'));
    t.uuid('user_id').unsigned().notNullable();
    t.uuid('plaid_account_id').unsigned().notNullable();
    t.text('transaction_eid').notNullable().unique('transaction_eid');
    t.text('category_eid').unique('category_eid');
    t.json('category');
    t.json('location');
    t.json('payment_meta');
    t.text('account_owner');
    t.text('name').notNullable();
    t.text('merchant_name');
    t.text('original_description');
    t.decimal('amount', 28, 10).notNullable();
    t.text('iso_currency_code');
    t.text('unofficial_currency_code');
    t.boolean('pending').notNullable();
    t.text('pending_channel').notNullable();
    t.date('date').notNullable();
    t.dateTime('datetime', { precision: 3 });
    t.date('authorized_date');
    t.dateTime('authorized_datetime', { precision: 3 });
    t.text('transaction_code');
    t.text('transaction_type');
    applyTimestamps(knex, t);

    t.foreign('user_id').references('id').inTable('users');
    t.foreign('plaid_account_id').references('id').inTable('plaid_accounts');
  });

  await applyUpdateTrigger(knex, tableName);
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable(tableName);
}
