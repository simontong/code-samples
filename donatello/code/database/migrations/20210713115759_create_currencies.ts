import { Knex } from 'knex';
import { applyTimestamps, applyUpdateTrigger } from '@/database/migrations/0_initial';

const tableName = 'currencies';

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable(tableName, function(t) {
    // t.uuid('id').primary().defaultTo(knex.raw('gen_random_uuid()'));
    // t.string('code').notNullable().unique('code');
    t.string('code').notNullable().primary();
    // t.specificType('code', 'citext').notNullable().primary();
    t.string('name').notNullable();
    // t.string('name_plural').notNullable();
    t.string('symbol').notNullable();
    // t.string('symbol_native').notNullable();
    t.integer('precision').notNullable();
    // t.string('rounding').notNullable();
    applyTimestamps(knex, t);
  });

  await applyUpdateTrigger(knex, tableName);
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable(tableName);
}
