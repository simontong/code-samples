import { applyTimestamps, applyUpdateTrigger } from '@/database/migrations/0_initial';
import { Knex } from 'knex';

const tableName = 'plaid_items';

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable(tableName, function(t) {
    t.uuid('id').primary().defaultTo(knex.raw('gen_random_uuid()'));
    t.uuid('user_id').unsigned().notNullable();
    t.text('item_eid').notNullable().unique('item_eid');
    t.text('institution_eid');
    t.text('access_token').notNullable().unique('access_token');
    applyTimestamps(knex, t);

    t.foreign('user_id').references('id').inTable('users');
  });

  await applyUpdateTrigger(knex, tableName);
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable(tableName);
}
