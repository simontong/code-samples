import { Knex } from 'knex';
import currencies from '@/resources/data/currencies.json';

const tableName = 'currencies';

export async function up(knex: Knex): Promise<void> {
  await knex.table(tableName).insert(
    currencies.map((currency) => ({
      code: currency.code,
      name: currency.name,
      // name_plural: currency.namePlural,
      symbol: currency.symbol,
      // symbol_native: currency.symbolNative,
      precision: currency.decimalDigits,
      // rounding: currency.rounding,
    })),
  );
}

export async function down(knex: Knex): Promise<void> {
  await knex
    .table(tableName)
    .whereIn(
      'code',
      currencies.map((currency) => currency.code),
    )
    .delete();
}
