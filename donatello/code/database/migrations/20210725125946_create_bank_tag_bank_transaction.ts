import { Knex } from 'knex';
import { applyTimestamps, applyUpdateTrigger } from '@/database/migrations/0_initial';

const tableName = 'bank_tag_bank_transaction';

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable(tableName, function(t) {
    t.uuid('id').primary().defaultTo(knex.raw('gen_random_uuid()'));
    t.uuid('bank_tag_id').unsigned().notNullable();
    t.uuid('bank_transaction_id').unsigned().notNullable();
    applyTimestamps(knex, t);

    t.unique(['bank_tag_id', 'bank_transaction_id']);
    t.foreign('bank_transaction_id').references('id').inTable('bank_transactions');
    t.foreign('bank_tag_id').references('id').inTable('bank_tags');
  });

  await applyUpdateTrigger(knex, tableName);
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable(tableName);
}
