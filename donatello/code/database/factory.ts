import { DateTime } from 'luxon';
import Currency from '@/src/models/Currency';
import container from '@/src/app/container';
import faker from 'faker';
import ObjectionFactoryAdapter from '@/database/ObjectionFactoryAdapter';
import factory, { Attributes, BuildOptions } from 'factory-girl';
import BankAccount from '@/src/models/BankAccount';
import BankCategory from '@/src/models/BankCategory';
import BankInstitution from '@/src/models/BankInstitution';
import BankTag from '@/src/models/BankTag';
import BankTransaction from '@/src/models/BankTransaction';
import User from '@/src/models/User';

/**
 * create build or create function
 */
const factoryBuildOrCreate = <T>(model: string, create = false) => {
  return async (attrs?: Attributes<Partial<T>>, buildOptions?: BuildOptions): Promise<T> => {
    return !create
      ? await factory.build(model, attrs, buildOptions?.buildOptions)
      : await factory.create(model, attrs, buildOptions?.buildOptions);
  };
};

// sometimes we don't want to return the model instance because
// it removes certain attributes when .toJSON() is run on it. we
// can pass the option `asPlainObject` to the `buildOptions` object
// to convert it to a plain object
const buildPojo = <T>(model: string) => {
  return async (attrs?: Attributes<Partial<T>>, buildOptions?: BuildOptions): Promise<T> => {
    const res = await factoryBuildOrCreate<T>(model)(attrs, buildOptions);
    return { ...res };
  };
};

factory.setAdapter(new ObjectionFactoryAdapter());

// seed value so always get the same data
faker.seed(12345);

/**
 * BankAccount
 */
factory.define('BankAccount', BankAccount, {
  name: () => faker.unique(() => faker.finance.accountName() + '-' + faker.random.alphaNumeric(10)),
});

/**
 * BankCategory
 */
factory.define('BankCategory', BankCategory, {
  // give uniqueness to `name` col which is unique
  name: () => faker.unique(() => faker.commerce.productAdjective() + '-' + faker.random.alphaNumeric(10)),
});

/**
 * BankInstitution
 */
factory.define('BankInstitution', BankInstitution, {
  // give uniqueness to `name` col which is unique
  name: () => faker.unique(() => faker.finance.accountName() + '-' + faker.random.alphaNumeric(10)),
});

/**
 * BankTag
 */
factory.define('BankTag', BankTag, {
  // give uniqueness to `name` col which is unique
  name: () => faker.unique(() => faker.commerce.color() + '-' + faker.random.alphaNumeric(10)),
});

/**
 * BankTransaction
 */
factory.define('BankTransaction', BankTransaction, {
  bank_account_id: null,
  bank_category_id: null,
  // @ts-ignore
  currency_code: async () => faker.random.arrayElement(await container.cradle.currencyService.findCurrencies()).code,
  date: () => DateTime.fromJSDate(faker.datatype.datetime()).toUTC().toISO(),
  // date: () => faker.datatype.datetime().toISOString(),
  payee: () => faker.company.companyName() + '-' + faker.random.alphaNumeric(10),
  amount: () => faker.datatype.number(),
  note: () => faker.random.arrayElement([null, faker.lorem.words()]),
});

/**
 * Currency
 */
factory.define('Currency', Currency, {
  code: () => faker.unique(() => faker.finance.currencyCode()),
  name: () => faker.finance.currencyName(),
  symbol: () => faker.finance.currencySymbol(),
  precision: () => 2,
});

/**
 * User
 */
factory.define('User', User, {
  name: () => faker.name.findName(),
  email: () => faker.internet.email(),
  password: () => faker.internet.password(14, false, /^.*$/, 'aBcD!12-'),
  new_email: null,
  email_confirm_token: null,
});

export const factoryBankAccount = {
  build: buildPojo<BankAccount>('BankAccount'),
  buildModel: factoryBuildOrCreate<BankAccount>('BankAccount'),
  create: factoryBuildOrCreate<BankAccount>('BankAccount', true),
};

export const factoryBankCategory = {
  build: buildPojo<BankCategory>('BankCategory'),
  buildModel: factoryBuildOrCreate<BankCategory>('BankCategory'),
  create: factoryBuildOrCreate<BankCategory>('BankCategory', true),
};

export const factoryBankInstitution = {
  build: buildPojo<BankInstitution>('BankInstitution'),
  buildModel: factoryBuildOrCreate<BankInstitution>('BankInstitution'),
  create: factoryBuildOrCreate<BankInstitution>('BankInstitution', true),
};

export const factoryBankTag = {
  build: buildPojo<BankTag>('BankTag'),
  buildModel: factoryBuildOrCreate<BankTag>('BankTag'),
  create: factoryBuildOrCreate<BankTag>('BankTag', true),
};

export const factoryBankTransaction = {
  build: buildPojo<BankTransaction>('BankTransaction'),
  buildModel: factoryBuildOrCreate<BankTransaction>('BankTransaction'),
  create: factoryBuildOrCreate<BankTransaction>('BankTransaction', true),
};

export const factoryCurrency = {
  build: buildPojo<Currency>('Currency'),
  buildModel: factoryBuildOrCreate<Currency>('Currency'),
  create: factoryBuildOrCreate<Currency>('Currency', true),
};

export const factoryUser = {
  build: buildPojo<User>('User'),
  buildModel: factoryBuildOrCreate<User>('User'),
  create: factoryBuildOrCreate<User>('User', true),
};

export default factory;
