import BaseModel from '@/src/lib/database/BaseModel';

type ModelProps = Record<keyof BaseModel, unknown>;

export default class ObjectionFactoryAdapter {
  build(Model: typeof BaseModel, props: ModelProps): BaseModel {
    return Object.assign(new Model(), props);
  }

  async save(model: BaseModel, Model: typeof BaseModel): Promise<BaseModel> {
    return Model.query().insert(model);
  }

  async destroy(model: BaseModel, Model: typeof BaseModel): Promise<BaseModel> {
    // @ts-ignore
    return Model.query().deleteById(model.id);
  }

  get(model: BaseModel, attr: keyof BaseModel): unknown {
    return model[attr];
  }

  set(props: ModelProps, model: BaseModel): BaseModel {
    return Object.assign(model, props);
  }
}
