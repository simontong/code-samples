To complete your account set-up, please verify your email address by clicking on the confirmation link below. This link is only valid for the next {{valid_for}}.

{{app_name}} ( {{{app_url}}} )

************
Hi {{name}},
************

Hi, Thank you for signing up with {{app_name}}.

To complete your account set-up, please verify your email address by clicking on the confirmation link below.

Confirm your email ( {{{action_url}}} )

For security, this request was received from a {{operating_system}} device using {{browser_name}}. If you did not register an account with us, please ignore this email or contact support ( {{{support_url}}} ) if you have questions.

Thanks,
The {{app_name}} Team

If you’re having trouble with the button above, copy and paste the URL below into your web browser.

{{{action_url}}}

© {{year}} {{app_name}}. All rights reserved.

{{{company_address}}}
